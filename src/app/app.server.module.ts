import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';

import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { GeneralModule } from '@app/general.module';

@NgModule({
  imports: [GeneralModule, ServerModule],
  bootstrap: [AppComponent],
})
export class AppServerModule {}
