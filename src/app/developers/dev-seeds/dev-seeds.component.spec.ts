import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevSeedsComponent } from './dev-seeds.component';

describe('DevSeedsComponent', () => {
  let component: DevSeedsComponent;
  let fixture: ComponentFixture<DevSeedsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DevSeedsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DevSeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
