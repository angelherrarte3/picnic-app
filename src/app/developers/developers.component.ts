import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { ThemeService } from '@app/services/theme.service';

@Component({
  selector: 'app-developers',
  templateUrl: './developers.component.html',
  styleUrls: ['./developers.component.scss'],
  animations: [
    trigger('slideInOut', [
      state(
        'in',
        style({
          transform: 'translate3d(0, 0, 0)',
        })
      ),
      state(
        'out',
        style({
          transform: 'translate3d(-100%, 0, 0)',
        })
      ),
      transition('in => out', animate('300ms ease-in-out')),
      transition('out => in', animate('300ms ease-in-out')),
    ]),
  ],
})
export class DevelopersComponent implements OnInit {
  items = [
    {
      label: 'Introduction',
      route: 'introduction',
    },
    {
      label: 'Getting Started',
      route: 'getting-started',
    },
    {
      label: 'Create',
      route: 'create',
    },
    {
      label: 'Apps',
      route: 'apps',
    },
    {
      label: 'My Apps',
      route: 'my-apps',
    },
    // {
    //   label: 'Run',
    //   route: 'run',
    // },
    // {
    //   label: 'Handle',
    //   route: 'handle',
    // },
  ];
  graphqlItems = [
    {
      route: 'auth',
      label: 'Authentication',
    },
    {
      route: 'circle',
      label: 'Circle',
    },
    {
      route: 'comments',
      label: 'Comments',
    },
    {
      route: 'feed',
      label: 'Feed',
    },
    {
      route: 'profile',
      label: 'Profile',
    },
    {
      route: 'seeds',
      label: 'Seeds',
    },
  ];
  sidebarMobileVisible: boolean = false;

  constructor(private themeService: ThemeService) {}

  ngOnInit(): void {}

  toggleTheme() {
    this.themeService.toggleTheme();
  }

  toggleSidebarMobile() {
    this.sidebarMobileVisible = !this.sidebarMobileVisible;
  }
}
