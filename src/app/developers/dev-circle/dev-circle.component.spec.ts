import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevCircleComponent } from './dev-circle.component';

describe('DevCircleComponent', () => {
  let component: DevCircleComponent;
  let fixture: ComponentFixture<DevCircleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DevCircleComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DevCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
