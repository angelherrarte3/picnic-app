import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { DevelopersComponent } from './developers.component';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { IntroductionComponent } from './introduction/introduction.component';
import { GettingStartedComponent } from './getting-started/getting-started.component';
import { CreateComponent } from './create/create.component';
import { RunComponent } from './run/run.component';
import { HandleComponent } from './handle/handle.component';
import { DevAuthComponent } from './dev-auth/dev-auth.component';
import { DevCircleComponent } from './dev-circle/dev-circle.component';
import { DevCommentsComponent } from './dev-comments/dev-comments.component';
import { DevFeedComponent } from './dev-feed/dev-feed.component';
import { DevSeedsComponent } from './dev-seeds/dev-seeds.component';
import { DevProfileComponent } from './dev-profile/dev-profile.component';
import { AppsComponent } from './apps/apps.component';
import { MyAppsComponent } from '@app/developers/my-apps/my-apps.component';

const routes: Routes = [
  {
    path: 'api',
    component: DevelopersComponent,
    data: { title: marker('Picnic Developer') },
    children: [
      {
        path: '',
        redirectTo: 'introduction',
        pathMatch: 'full',
      },
      {
        path: 'introduction',
        component: IntroductionComponent,
      },
      {
        path: 'getting-started',
        component: GettingStartedComponent,
      },
      {
        path: 'create',
        component: CreateComponent,
      },
      {
        path: 'apps',
        component: AppsComponent,
      },
      {
        path: 'my-apps',
        component: MyAppsComponent,
      },
      {
        path: 'run',
        component: RunComponent,
      },
      {
        path: 'handle',
        component: HandleComponent,
      },
      {
        path: 'auth',
        component: DevAuthComponent,
      },
      {
        path: 'circle',
        component: DevCircleComponent,
      },
      {
        path: 'comments',
        component: DevCommentsComponent,
      },
      {
        path: 'feed',
        component: DevFeedComponent,
      },
      {
        path: 'profile',
        component: DevProfileComponent,
      },
      {
        path: 'seeds',
        component: DevSeedsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DevelopersRoutingModule {}
