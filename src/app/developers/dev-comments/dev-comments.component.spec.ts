import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevCommentsComponent } from './dev-comments.component';

describe('DevCommentsComponent', () => {
  let component: DevCommentsComponent;
  let fixture: ComponentFixture<DevCommentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DevCommentsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DevCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
