import { Component, OnInit } from '@angular/core';
import { App, SearchAppsGQL } from '@app/graphql/app';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';

@Component({
  selector: 'app-apps',
  templateUrl: './apps.component.html',
  styleUrls: ['./apps.component.scss'],
})
export class AppsComponent extends GQLPaginationComponent implements OnInit {
  apps: App[] = [];
  constructor(private searchAppsGQL: SearchAppsGQL) {
    super();
  }

  ngOnInit() {
    this.loadMoreItems();
  }

  override loadMoreItems() {
    this.searchAppsGQL
      .fetch({
        data: {
          cursor: {
            id: this.cursorId,
            limit: 10,
          },
          nameStartsWith: '',
        },
      })
      .subscribe(({ data }) => {
        this.apps.push(...data.searchApps.edges.map((v) => v.node));
        this.hasNextPage = data.searchApps.pageInfo.hasNextPage;
        this.cursorId = data.searchApps.pageInfo.lastId;
      });
  }
}
