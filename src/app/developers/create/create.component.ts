import { Component, OnInit } from '@angular/core';
import {
  AppPermission,
  AppSubscription,
  AppTag,
  CreateAppGQL,
  GetAppPermissionsGQL,
  GetAppSubscriptionsGQL,
  GetAppTagsGQL,
  MyAppsGQL,
} from '@app/graphql/app';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  tags: AppTag[] = [];
  permissions: AppPermission[] = [];
  subscriptions: AppSubscription[] = [];
  form!: FormGroup;

  constructor(
    private getAppTags: GetAppTagsGQL,
    private getAppPermissions: GetAppPermissionsGQL,
    private getAppSubscriptions: GetAppSubscriptionsGQL,
    private formBuilder: FormBuilder,
    private createAppGQL: CreateAppGQL,
    private myApps: MyAppsGQL
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.getAppTags.fetch({}).subscribe(({ data }) => {
      this.tags = data.getAppTags;
    });
    this.getAppPermissions.fetch({}).subscribe(({ data }) => {
      this.permissions = data.getAppPermissions;
    });
    this.getAppSubscriptions.fetch({}).subscribe(({ data }) => {
      this.subscriptions = data.getAppSubscriptions;
    });
  }

  private createForm() {
    this.form = this.formBuilder.group({
      url: ['', Validators.required],
      name: ['', Validators.required],
      tagIds: [[]],
      permissionIds: [[]],
      subscriptionIds: [[]],
      imageUrl: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  create() {
    this.createAppGQL
      .mutate(
        {
          data: {
            description: this.form.value.description,
            name: this.form.value.name,
            url: this.form.value.url,
            imageUrl: this.form.value.imageUrl,
            permissionIds: this.form.value.permissionIds,
            subscriptionIds: this.form.value.subscriptionIds,
            tagIds: this.form.value.tagIds,
          },
        },
        { refetchQueries: [this.myApps.document] }
      )
      .subscribe(({ data }) => {
        this.form.reset();
        alert('saved');
      });
  }
}
