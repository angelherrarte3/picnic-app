import { Component, OnInit } from '@angular/core';
import { App, MyAppsGQL } from '@app/graphql/app';

@Component({
  selector: 'app-my-apps',
  templateUrl: './my-apps.component.html',
  styleUrls: ['./my-apps.component.scss'],
})
export class MyAppsComponent implements OnInit {
  apps: App[] = [];

  constructor(private myAppsGQL: MyAppsGQL) {}

  ngOnInit() {
    this.myAppsGQL.watch().valueChanges.subscribe(({ data }) => {
      this.apps = data.myApps;
    });
  }
}
