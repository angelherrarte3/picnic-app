import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DevelopersRoutingModule } from './developers-routing.module';
import { DevelopersComponent } from './developers.component';
import { IntroductionComponent } from './introduction/introduction.component';
import { ComponentsModule } from '@app/components/components.module';
import { GettingStartedComponent } from './getting-started/getting-started.component';
import { CreateComponent } from './create/create.component';
import { RunComponent } from './run/run.component';
import { HandleComponent } from './handle/handle.component';
import { DevAuthComponent } from './dev-auth/dev-auth.component';
import { DevCircleComponent } from './dev-circle/dev-circle.component';
import { DevCommentsComponent } from './dev-comments/dev-comments.component';
import { DevFeedComponent } from './dev-feed/dev-feed.component';
import { DevProfileComponent } from './dev-profile/dev-profile.component';
import { DevSeedsComponent } from './dev-seeds/dev-seeds.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppsComponent } from './apps/apps.component';
import { MyAppsComponent } from './my-apps/my-apps.component';

@NgModule({
  declarations: [
    DevelopersComponent,
    IntroductionComponent,
    GettingStartedComponent,
    CreateComponent,
    RunComponent,
    HandleComponent,
    DevAuthComponent,
    DevCircleComponent,
    DevCommentsComponent,
    DevFeedComponent,
    DevProfileComponent,
    DevSeedsComponent,
    AppsComponent,
    MyAppsComponent,
  ],
  imports: [CommonModule, DevelopersRoutingModule, ComponentsModule, FormsModule, ReactiveFormsModule],
})
export class DevelopersModule {}
