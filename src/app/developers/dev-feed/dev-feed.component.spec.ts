import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevFeedComponent } from './dev-feed.component';

describe('DevFeedComponent', () => {
  let component: DevFeedComponent;
  let fixture: ComponentFixture<DevFeedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DevFeedComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DevFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
