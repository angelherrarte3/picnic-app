import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DebugRoutingModule } from './debug-routing.module';
import { DebugComponent } from './debug.component';
import { I18nModule } from '@app/i18n';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [DebugComponent],
  imports: [NgbTooltipModule, CommonModule, DebugRoutingModule, I18nModule],
})
export class DebugModule {}
