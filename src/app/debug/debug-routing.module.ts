import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { DebugComponent } from '@app/debug/debug.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'about',
      component: DebugComponent,
      data: { title: marker('About') },
    },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DebugRoutingModule {}
