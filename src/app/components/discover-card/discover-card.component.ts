import { Component, Input, OnInit } from '@angular/core';
import { Circle } from '@app/graphql/circle';

@Component({
  selector: 'discover-card[circle]',
  templateUrl: './discover-card.component.html',
  styleUrls: ['./discover-card.component.scss'],
})
export class DiscoverCardComponent implements OnInit {
  @Input() circle?: Circle;

  constructor() {}

  ngOnInit(): void {}

  get coverImage() {
    if (!this.circle?.coverImageFile?.trim()) return undefined;
    return `url(${this.circle.coverImageFile})`;
  }
}
