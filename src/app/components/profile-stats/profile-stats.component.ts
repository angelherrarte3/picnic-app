import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileStats } from '@app/graphql/profile';

@Component({
  selector: 'profile-stats[profileStats]',
  templateUrl: './profile-stats.component.html',
  styleUrls: ['./profile-stats.component.scss'],
})
export class ProfileStatsComponent implements OnInit {
  @Input() profileStats?: ProfileStats;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {}

  get likes(): number {
    return this.profileStats?.contentStatsForProfile?.likes ?? 0;
  }

  get followers(): number {
    return this.profileStats?.contentStatsForProfile?.followers ?? 0;
  }

  get views(): number {
    return this.profileStats?.contentStatsForProfile?.views ?? 0;
  }

  navigateToFollowers() {
    this.router.navigate(['./followers'], { relativeTo: this.route });
  }
}
