import { Component, OnInit } from '@angular/core';
import { Router, Event as NavigationEvent, NavigationStart, NavigationEnd } from '@angular/router';
import { ChatSidebarService } from '@app/services/chat-sidebar.service';
import { TmpStateService } from '@app/services/tmp-state.service';
import { CredentialsService } from '@app/auth';

import { UnreadChatsGQL } from '@app/graphql/chat';
import { GlobalModalService } from '@app/services/global-modal.service';

export enum BottomBarTabs {
  Feed = 'feed',
  Discover = 'discover',
  Post = 'post',
  Messages = 'messages',
  Profile = 'profile',
}

@Component({
  selector: 'bottom-bar',
  templateUrl: './bottom-bar.component.html',
  styleUrls: ['./bottom-bar.component.scss'],
})
export class BottomBarComponent implements OnInit {
  activeTab?: BottomBarTabs;
  guest: boolean;
  unreadChats = 0;

  constructor(
    public state: TmpStateService,
    private chatSidebar: ChatSidebarService,
    private router: Router,
    private credentialsService: CredentialsService,
    private modalService: GlobalModalService,
    private unreadChatsGQL: UnreadChatsGQL
  ) {
    this.router.events.subscribe((event: NavigationEvent) => {
      if (event instanceof NavigationEnd) {
        if (event.url === '/') {
          this.activeTab = BottomBarTabs.Feed;
        } else if (event.url.includes('/chats')) {
          this.activeTab = BottomBarTabs.Messages;
        } else if (event.url.includes('create-post')) {
          this.activeTab = BottomBarTabs.Post;
        } else if (event.url === '/u') {
          this.activeTab = BottomBarTabs.Profile;
        } else if (event.url.includes('/d')) {
          this.activeTab = BottomBarTabs.Discover;
        } else {
          this.activeTab = undefined;
        }
      }
    });
  }

  ngOnInit() {
    this.guest = this.isGuestUser;

    if (this.isGuestUser) {
      return;
    }

    this.unreadChatsGQL.fetch().subscribe(({ data }) => {
      this.unreadChats = data.unreadChats.length;
    });
  }

  get isGuestUser() {
    return !this.credentialsService.isAuthenticated();
  }

  openSignUpModal() {
    this.modalService.open('sign-up');
  }

  onTapMessages() {
    this.chatSidebar.toggle();
  }

  get activeMessagesTab() {
    return this.activeTab === BottomBarTabs.Messages;
  }

  get activeFeedTab() {
    return this.activeTab === BottomBarTabs.Feed;
  }

  get activeDiscoverTab() {
    return this.activeTab === BottomBarTabs.Discover;
  }
}
