import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicnicTextButtonComponent } from './picnic-text-button.component';

describe('PicnicTextButtonComponent', () => {
  let component: PicnicTextButtonComponent;
  let fixture: ComponentFixture<PicnicTextButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PicnicTextButtonComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PicnicTextButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
