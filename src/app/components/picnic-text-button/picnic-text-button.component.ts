import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export type PicnicTextAlign = 'left' | 'center' | 'right';

@Component({
  selector: 'picnic-text-button[label]',
  templateUrl: './picnic-text-button.component.html',
  styleUrls: ['./picnic-text-button.component.scss'],
})
export class PicnicTextButtonComponent implements OnInit {
  @Input() label: string = '';
  @Input() labelSize?: number;
  @Input() labelWeight?: number;
  @Input() color: string = '';
  @Input() radius: string = '';
  @Input() padding: string = '';
  @Input() height?: string = '';
  @Input() width?: string = '';
  @Input() disabled: boolean = false;
  @Input() hover: boolean = true;
  @Input() textAlign: PicnicTextAlign = 'center';
  @Input() btnId = '';
  @Output() byClick = new EventEmitter<void>();

  style = {};

  ngOnInit(): void {
    this.style = this.getStyle;
  }

  get getStyle() {
    return {
      color: this.color,
      'border-radius': this.radius,
      padding: this.padding,
      width: this.width,
      height: this.height,
      'justify-content': this.textAlignment,
      background: !this.hover ? 'none' : '',
    };
  }

  private get textAlignment() {
    if (this.textAlign === 'left') return 'flex-start';
    if (this.textAlign === 'right') return 'flex-end';
    return 'center';
  }

  handleClick = () => this.byClick.emit();
}
