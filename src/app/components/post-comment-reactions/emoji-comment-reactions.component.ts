import { Component, Input, OnInit } from '@angular/core';
import { CredentialsService } from '@app/auth';
import { CommentEdge, SetReactCommentGQL } from '@app/graphql/comment';
import { comment } from 'postcss';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';

@Component({
  selector: 'emoji-comment-reactions[comment]',
  templateUrl: './emoji-comment-reactions.component.html',
  styleUrls: ['./emoji-comment-reactions.component.scss'],
})
export class EmojiCommentReactionsComponent implements OnInit {
  @Input() comment?: CommentEdge;

  constructor(
    private credentialsService: CredentialsService,
    private setReactGQL: SetReactCommentGQL,
    private analytics: AnalyticsService
  ) {}

  ngOnInit(): void {}

  like() {
    if (!this.credentialsService.isAuthenticated()) {
      return;
    }

    this.analytics.logEvent(
      new AnalyticsEventTap(AnalyticsTapTarget.postCommentsLikeButton, !this.comment!.node.iReacted)
    );

    this.setReactGQL
      .mutate({
        react: !this.comment!.node.iReacted,
        id: this.comment!.node.id,
      })
      .subscribe(({ data }) => {
        if (!data!.setReactComment.success) {
          return;
        }

        this.comment!.node.iReacted = !this.comment!.node.iReacted;
        if (this.comment!.node.iReacted) {
          this.comment!.node.likesCount++;
        } else {
          this.comment!.node.likesCount--;
        }
      });
  }

  get isLiked() {
    return this.comment!.node.iReacted;
  }
}
