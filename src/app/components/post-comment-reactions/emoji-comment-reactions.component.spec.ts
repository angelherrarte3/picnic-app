import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmojiCommentReactionsComponent } from './emoji-comment-reactions.component';

describe('EmojiCommentReactionsComponent', () => {
  let component: EmojiCommentReactionsComponent;
  let fixture: ComponentFixture<EmojiCommentReactionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmojiCommentReactionsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(EmojiCommentReactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
