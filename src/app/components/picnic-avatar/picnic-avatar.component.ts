import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'picnic-avatar[src]',
  templateUrl: './picnic-avatar.component.html',
  styleUrls: ['./picnic-avatar.component.scss'],
})
export class PicnicAvatarComponent implements OnInit {
  @Input() src: string = '';
  @Input() size: number = 48;
  @Input() follows: boolean = false;
  @Input() showFollows: boolean = true;
  @Input() emojiMode: boolean = false;
  @Input() emojiSize?: string;
  @Input() bgColor?: string;

  @Output() private byFollowClick = new EventEmitter<void>();
  @Output() private byAvatarClick = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  handleFollowClick = () => this.byFollowClick.emit();

  handleAvatarClick = () => this.byAvatarClick.emit();
}
