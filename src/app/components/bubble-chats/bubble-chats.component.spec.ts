import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BubbleChatsComponent } from './bubble-chats.component';

describe('BubbleChatsComponent', () => {
  let component: BubbleChatsComponent;
  let fixture: ComponentFixture<BubbleChatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BubbleChatsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(BubbleChatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
