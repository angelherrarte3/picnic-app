import { trigger, transition, style, animate, state } from '@angular/animations';
import { Component, isDevMode, OnInit } from '@angular/core';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';
import { Chat } from '@app/graphql/chat';
import { ChatSidebarService } from '@app/services/chat-sidebar.service';
import { CredentialsService } from '@app/auth';
import { GlobalModalService } from '@app/services/global-modal.service';

@Component({
  selector: 'bubble-chats',
  templateUrl: './bubble-chats.component.html',
  styleUrls: ['./bubble-chats.component.scss'],
  animations: [
    trigger('bubbleChatAnimation', [
      transition(':enter', [
        style({ transform: 'scale(0.2)' }),
        animate('150ms ease-out', style({ transform: 'scale(1)' })),
      ]),
      transition(':leave', [style({ height: '*' }), animate('200ms ease-in', style({ height: 0 }))]),
    ]),
  ],
})
export class BubbleChatsComponent implements OnInit {
  activeChats$ = this.bubbleChatState.activeChats;
  devMode: boolean | undefined;

  constructor(
    private bubbleChatState: BubbleChatStateService,
    public credentialsService: CredentialsService,
    private modalService: GlobalModalService,
    private chatSidebarService: ChatSidebarService
  ) {}

  ngOnInit(): void {
    this.devMode = isDevMode();
  }

  onTapCloseChat(chatId: string) {
    this.bubbleChatState.removeChat(chatId);
  }

  onTapOpenChat(chat: Chat) {
    this.bubbleChatState.addModalChat(chat);
  }

  get isGuestUser() {
    return !this.credentialsService.isAuthenticated();
  }

  showSignUpModal = () => this.modalService.open('sign-up');

  showChatSidebar() {
    if (this.isGuestUser) {
      this.showSignUpModal();
      return;
    }
    this.chatSidebarService.show();
  }

  avatar(chat: Chat) {
    if (chat.chatType === 'CIRCLE') {
      return chat.circle?.imageFile ? chat.circle.imageFile : chat.circle?.image;
    }

    return chat.chatImage;
  }

  emojiMode(chat: Chat) {
    if (chat.chatType === 'CIRCLE') {
      return !chat.circle?.imageFile;
    }

    return false;
  }
}
