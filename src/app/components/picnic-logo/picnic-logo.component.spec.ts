import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicnicLogoComponent } from './picnic-logo.component';

describe('PicnicLogoComponent', () => {
  let component: PicnicLogoComponent;
  let fixture: ComponentFixture<PicnicLogoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PicnicLogoComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PicnicLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
