import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'picnic-logo',
  templateUrl: './picnic-logo.component.html',
  styleUrls: ['./picnic-logo.component.scss'],
})
export class PicnicLogoComponent implements OnInit {
  @Input() size?: string;
  @Input() openPicnicPage: boolean = false;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  handleOpenPicnicPage = () => (this.openPicnicPage ? this.router.navigateByUrl('/') : null);
}
