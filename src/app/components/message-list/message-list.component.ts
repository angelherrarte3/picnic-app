import { Component, HostListener, Input, OnInit } from '@angular/core';
import { CommentEdge, CreateCommentGQL, GetTreeCommentsGQL } from '@app/graphql/comment';
import { PostCommentMode } from '../post-comment/post-comment.component';
import { SimpleDropdownItem } from '../simple-dropdown/simple-dropdown.component';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';

export enum MessageListMode {
  Vertical,
  Horizontal,
}

export enum CommentsSorting {
  POPULAR_ALL_TIME = 'POPULARITY_ALL_TIME',
  RECENT = 'RECENT',
  TRENDING_THIS_WEEK = 'TRENDING_THIS_WEEK',
  TRENDING_THIS_MONTH = 'TRENDING_THIS_MONTH',
}

@Component({
  selector: 'message-list[postId]',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss'],
})
export class MessageListComponent extends GQLPaginationComponent implements OnInit {
  @Input() mode: MessageListMode = MessageListMode.Vertical;
  readonly messageListMode = MessageListMode;
  override hasNextPage = true;

  _postId!: string;
  loading = false;
  comments: CommentEdge[] = [];
  postCommentMode: PostCommentMode = PostCommentMode.Simplified;
  replyToComment?: CommentEdge;
  loadingCommentCreation = false;

  //TODO(GS-8532) - enable when backend ready
  showCommentSorting = false;
  sortItems: SimpleDropdownItem<CommentsSorting>[] = [
    {
      label: '🔥 popular all time',
      value: CommentsSorting.POPULAR_ALL_TIME,
    },
    {
      label: '🆕  recent',
      value: CommentsSorting.RECENT,
    },
    {
      label: '⏰ trending this week',
      value: CommentsSorting.TRENDING_THIS_WEEK,
    },
    {
      label: '📅 trending this month',
      value: CommentsSorting.TRENDING_THIS_MONTH,
    },
  ];
  sortSelected: CommentsSorting = CommentsSorting.RECENT;

  constructor(private getComments: GetTreeCommentsGQL, private createCommentGQL: CreateCommentGQL) {
    super();
  }

  ngOnInit(): void {
    this.postCommentMode =
      this.mode === MessageListMode.Vertical ? PostCommentMode.Simplified : PostCommentMode.Detailed;
  }

  //this is the function that is called when the user scrolls to the bottom of the page
  @HostListener('document:wheel', ['$event'])
  onScroll(event: any) {
    const windowHeight = 'innerHeight' in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight
    );
    console.log('scrolling');
    const windowBottom = windowHeight + window.pageYOffset;
    if (windowBottom >= docHeight && !this.loading) {
      this.loadMoreItems();
    }
  }

  @Input() set postId(postId: string) {
    this._postId = postId;
    this.cursorId = '';
    this.comments = [];
    this.loadMoreItems(true);
  }

  loadMoreItems(initialLoad: boolean = false): void {
    console.log('loading more items');
    if (!this.hasNextPage) return;
    this.loading = true;
    this.getComments
      .watch({
        postId: this._postId,
        cursor: {
          id: this.cursorId,
          limit: 5,
          dir: 'forward',
        },
      })
      .valueChanges.subscribe(({ data }) => {
        this.comments = initialLoad ? data.getComments.edges : [...this.comments, ...data.getComments.edges];
        this.hasNextPage = data.getComments.pageInfo.hasNextPage;
        this.cursorId = this.hasNextPage ? data.getComments.pageInfo.lastId : this.cursorId;

        console.log(this.comments.length);
        this.loading = false;
      });
  }

  createComment(replyText: string) {
    if (replyText.length <= 0) return;

    this.loadingCommentCreation = true;

    this.createCommentGQL
      .mutate({
        postId: this._postId,
        parentId: this.replyToComment?.node.id,
        text: replyText,
      })
      .subscribe(({ data }) => {
        if (this.replyToComment) {
          if (!this.replyToComment.node.repliesConnection) {
            this.replyToComment.node.repliesConnection = {
              pageInfo: {
                lastId: '',
                firstId: '',
                hasNextPage: false,
              },
              edges: [],
            };
          }
          this.replyToComment.node.repliesConnection.edges.push({
            node: data!.createComment,
            cursorId: '',
          });
        } else {
          this.comments.push({ node: data!.createComment, cursorId: '' });
        }
        this.replyToComment = undefined;
        this.loadingCommentCreation = false;
      });
  }

  handleSortChange(sortItem: SimpleDropdownItem<CommentsSorting>): void {
    this.sortSelected = sortItem.value;
  }

  get emptyComments() {
    return this.comments.length === 0;
  }
}
