import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Reason, ReportType } from '@app/graphql/circle';
import { PicnicModalComponent, PicnicModalOptions, PicnicModalType } from '../picnic-modal/picnic-modal.component';
import { SimpleDropdownItem } from '../simple-dropdown/simple-dropdown.component';
import { ReportGQL } from '@app/graphql/content';
import { ModalComponent } from '@app/types/modal-component';
import { ModalName } from '@app/services/global-modal.service';

export interface ReportModalParams {
  reportType: ReportType;
  circleId: string;
  anyId: string;
  contentAuthorId: string;
}

@Component({
  selector: 'report-modal',
  templateUrl: './report-modal.component.html',
  styleUrls: ['./report-modal.component.scss'],
})
export class ReportModalComponent extends ModalComponent implements OnInit {
  override name: ModalName = 'report-content';

  reasonOptions: SimpleDropdownItem<Reason>[] = [
    {
      label: 'other',
      value: 'OTHER',
    },
    {
      label: 'spam',
      value: 'SPAM',
    },
    {
      label: 'sexual content',
      value: 'SEXUAL_CONTENT',
    },
    {
      label: 'graphic content',
      value: 'GRAPHIC_CONTENT',
    },
  ];
  reasonSelected: Reason = 'OTHER';
  loading: boolean = false;
  reportSent: boolean = false;
  contentAuthorId?: string;
  form: FormGroup;

  constructor(element: ElementRef, private reportGQL: ReportGQL) {
    super(element);
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = new FormGroup({
      comment: new FormControl('', {
        nonNullable: true,
      }),
      reason: new FormControl(this.reasonSelected, {
        nonNullable: true,
        validators: [Validators.required],
      }),
      reportType: new FormControl(null, {
        nonNullable: false,
        validators: [Validators.required],
      }),
      circleId: new FormControl(null, {
        nonNullable: false,
        validators: [Validators.required],
      }),
      anyId: new FormControl(null, {
        nonNullable: false,
        validators: [Validators.required],
      }),
      contentAuthorId: new FormControl(this.contentAuthorId, {
        nonNullable: false,
        validators: [Validators.required],
      }),
    });
  }

  override resetState() {
    this.reasonSelected = 'OTHER';
    this.form.reset();
    this.reportSent = false;
  }

  override open(options?: PicnicModalOptions) {
    if (options) {
      this.options = options;
      const reportParams = options.reportParams;

      if (reportParams) {
        this.form.patchValue({
          reportType: reportParams.reportType,
          circleId: reportParams.circleId,
          anyId: reportParams.anyId,
          contentAuthorId: reportParams.contentAuthorId,
        });
      }
    }
    this.isOpen = true;
    document.body.appendChild(this.element.nativeElement);
  }

  onTapReport() {
    this.form.markAllAsTouched();
    if (this.form.invalid) return;

    this.loading = true;
    const reportInput = this.form.value;

    this.reportGQL.mutate({ reportInput }).subscribe({
      next: (value) => {
        this.reportSent = true;
        this.reasonSelected = 'OTHER';
        this.form.reset();
      },
      error: (err) => {
        this.reportSent = false;
        this.loading = false;
      },
      complete: () => {
        this.loading = false;
      },
    });
  }

  onTapSelectReason(item: SimpleDropdownItem<Reason>) {
    this.reasonSelected = item.value;
    this.form.patchValue({
      reason: this.reasonSelected,
    });
  }
}
