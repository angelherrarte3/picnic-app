import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CirclesGridComponent } from './circles-grid.component';

describe('CirclesGridComponent', () => {
  let component: CirclesGridComponent;
  let fixture: ComponentFixture<CirclesGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CirclesGridComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CirclesGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
