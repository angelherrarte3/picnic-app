import { Component, Input, OnInit } from '@angular/core';
import { CircleEdge } from '@app/graphql/circle';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileStateService } from '@app/profile/profile-state.service';
import { GlobalModalService } from '@app/services/global-modal.service';

@Component({
  selector: 'circles-grid',
  templateUrl: './circles-grid.component.html',
  styleUrls: ['./circles-grid.component.scss'],
})
export class CirclesGridComponent implements OnInit {
  @Input() modCircles: CircleEdge[] = [];
  @Input() circles: CircleEdge[] = [];
  @Input() ownProfile: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private profileState: ProfileStateService,
    private modalService: GlobalModalService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((_) => {
      this.ownProfile = this.profileState.ownProfile;
    });
  }

  navigateToCircle(circle: CircleEdge) {
    this.router.navigate([`/c/${circle.node.urlName ? circle.node.urlName : circle.node.name}`]);
  }

  openCreateCircleModal() {
    this.modalService.open('create-circle', { dismissClickOutside: false });
  }
}
