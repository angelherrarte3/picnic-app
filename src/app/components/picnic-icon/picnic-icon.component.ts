import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'picnic-icon[name]',
  templateUrl: './picnic-icon.component.html',
  styleUrls: ['./picnic-icon.component.scss'],
})
export class PicnicIconComponent implements OnInit {
  @Input() name: string = '';
  @Input() color: string = '';
  @Input() bgColor: string = '';
  @Input() fill: string = '';
  @Input() size: number = 24;
  @Input() padding: number = 0;
  @Input() margin: number = 0;
  @Input() borderRadius: string = '';

  style = {};

  constructor() {}

  ngOnInit(): void {
    this.style = this.getStyle;
  }

  get getStyle() {
    return {
      color: this.color,
      'border-radius': this.borderRadius,
      background: this.bgColor,
      'width.px': this.size,
      'height.px': this.size,
      'padding.px': this.padding,
      'margin.px': this.margin,
      fill: this.fill,
    };
  }
}
