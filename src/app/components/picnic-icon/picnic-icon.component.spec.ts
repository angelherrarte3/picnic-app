import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicnicIconComponent } from './picnic-icon.component';

describe('PicnicIconComponent', () => {
  let component: PicnicIconComponent;
  let fixture: ComponentFixture<PicnicIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PicnicIconComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PicnicIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
