import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'badge-text[text]',
  templateUrl: './badge-text.component.html',
  styleUrls: ['./badge-text.component.scss'],
})
export class BadgeTextComponent implements OnInit {
  @Input() text: string = '';
  @Input() color?: string;
  @Input() textSize?: number;
  @Input() textWeight?: number;
  @Input() badgeCount?: number;
  @Input() badgeColor?: string;

  constructor() {}

  ngOnInit(): void {}

  get getStyle() {
    return {
      color: this.color,
      'font-size.px': this.textSize,
      'font-weight': this.textWeight,
    };
  }
}
