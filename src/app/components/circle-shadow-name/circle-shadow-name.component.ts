import { Component, Input, OnInit } from '@angular/core';
import { Circle } from '@app/graphql/circle';

@Component({
  selector: 'circle-shadow-name[circle]',
  templateUrl: './circle-shadow-name.component.html',
  styleUrls: ['./circle-shadow-name.component.scss'],
})
export class CircleShadowNameComponent implements OnInit {
  @Input() circle?: Circle;

  constructor() {}

  ngOnInit(): void {}
}
