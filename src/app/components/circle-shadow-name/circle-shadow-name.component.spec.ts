import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CircleShadowNameComponent } from './circle-shadow-name.component';

describe('CircleShadowNameComponent', () => {
  let component: CircleShadowNameComponent;
  let fixture: ComponentFixture<CircleShadowNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CircleShadowNameComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CircleShadowNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
