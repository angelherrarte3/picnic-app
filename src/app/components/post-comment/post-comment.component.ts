import { animate, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommentEdge, GetTreeCommentsGQL } from '@app/graphql/comment';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';
import { AnalyticsService } from '@app/services/analytics/analytics.service';

export enum PostCommentMode {
  Simplified,
  Detailed,
  Report,
}

export interface ReplyingData {
  replying: boolean;
  comment?: CommentEdge;
}

@Component({
  selector: 'post-comment[comment]',
  templateUrl: './post-comment.component.html',
  styleUrls: ['./post-comment.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ height: 0, opacity: 0 }),
        animate('150ms ease-out', style({ height: '*', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ height: '*', opacity: 1 }),
        animate('150ms ease-in', style({ height: 0, opacity: 0 })),
      ]),
    ]),
  ],
})
export class PostCommentComponent implements OnInit {
  @Input() comment?: CommentEdge;
  @Input() commentMode: PostCommentMode = PostCommentMode.Simplified;
  @Output() byReply = new EventEmitter<CommentEdge>();
  @Output() byCommentCreated = new EventEmitter<string>();
  replyingData: ReplyingData = {
    replying: false,
  };

  readonly commentModeEnum = PostCommentMode;

  constructor(private getCommentsGQL: GetTreeCommentsGQL, private analytics: AnalyticsService) {}

  ngOnInit(): void {}

  get isMoreChildren() {
    return this.comment?.node.repliesConnection?.pageInfo.hasNextPage ?? false;
  }

  get isReplying() {
    return this.replyingData.replying && this.replyingData.comment?.node?.id === this.comment?.node?.id;
  }

  getNextChildren() {
    this.getCommentsGQL
      .fetch({
        postId: this.comment!.node.postId!,
        parentId: this.comment!.node.id,
        cursor: {
          id: this.comment!.node.repliesConnection!.pageInfo.lastId,
          limit: 10,
          dir: 'forward',
        },
      })
      .subscribe(({ data }) => {
        if (!this.comment!.node.repliesConnection!.edges) {
          this.comment!.node.repliesConnection!.edges = [];
        }
        this.comment!.node.repliesConnection!.edges.push(...data.getComments.edges);
        this.comment!.node.repliesConnection!.pageInfo = data.getComments.pageInfo;
      });
  }

  handleReply(reply: CommentEdge) {
    if (this.replyingData.replying && this.replyingData.comment?.node?.id === reply.node.id) {
      this.handleCancelReply();
      return;
    }

    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.postCommentsReplyButton));

    this.replyingData = {
      replying: true,
      comment: reply,
    };
    this.byReply.emit(reply);
  }

  handleCancelReply() {
    this.replyingData = {
      replying: false,
      comment: undefined,
    };
  }

  handleCommentCreated(comment: string) {
    this.byCommentCreated.emit(comment);
    this.handleCancelReply();
  }
}
