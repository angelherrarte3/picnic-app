import { Component, OnInit, ViewChild } from '@angular/core';
import { CredentialsService } from '@app/auth';
import { Collection, CollectionsConnectionGQL } from '@app/graphql/collections';
import { Circle, GetLastViewedCirclesGQL } from '@app/graphql/circle';
import { TmpStateService } from '@app/services/tmp-state.service';
import { Router } from '@angular/router';
import { FeedSidebarService } from '@app/services/feed-sidebar.service';
import { CreateCircleModalComponent } from '../create-circle-modal/create-circle-modal.component';
import { GlobalModalService } from '@app/services/global-modal.service';

@Component({
  selector: 'feed-sidebar',
  templateUrl: './feed-sidebar.component.html',
  styleUrls: ['./feed-sidebar.component.scss'],
})
export class FeedSidebarComponent implements OnInit {
  collections: Collection[] = [];
  recentCircles: Circle[] = [];

  constructor(
    private creds: CredentialsService,
    private collectionsConnection: CollectionsConnectionGQL,
    private getLastViewedCircles: GetLastViewedCirclesGQL,
    public state: TmpStateService,
    private router: Router,
    private feedSidebarService: FeedSidebarService,
    private modalService: GlobalModalService
  ) {}

  ngOnInit() {
    if (!this.creds.isAuthenticated()) {
      return;
    }

    this.collectionsConnection
      .fetch({
        cursor: {
          id: '',
          limit: 5,
        },
        withPreviewPosts: true,
        userId: this.creds.credentials!.userid,
        returnSavedPostsCollection: true,
      })
      .subscribe(({ data }) => {
        this.collections = data.collectionsConnection.edges.map((v) => v.node);
      });

    this.getLastViewedCircles
      .fetch({
        cursor: {
          id: '',
          limit: 10,
        },
      })
      .subscribe(({ data }) => {
        this.recentCircles = data.getLastViewedCircles.edges.map((v) => v.node);
      });
  }

  previewImgCollection(c: Collection) {
    if ((c.previewPosts?.length ?? 0) == 0) {
      return null;
    }

    const p = c.previewPosts![0];
    switch (p.type) {
      case 'IMAGE':
        return p.imageContent!.url;
      case 'VIDEO':
        return p.videoContent!.thumbnailUrl ?? null;
      default:
        return null;
    }
  }

  onTapCreateCircle() {
    this.feedSidebarService.hide();
    this.modalService.open('create-circle', { dismissClickOutside: false });
  }

  onTapDiscover() {
    this.feedSidebarService.hide();
    this.router.navigate(['/d']);
  }

  onTapViewAllCollections() {
    this.feedSidebarService.hide();
    this.router.navigate(['/u/collections']);
  }

  onTapViewAllCircles() {
    this.feedSidebarService.hide();
    this.router.navigate(['/u/circles']);
  }

  onSwipeLeftSidebar() {
    if (this.feedSidebarService.isVisible) {
      this.feedSidebarService.hide();
    }
  }

  get emptyCollections() {
    return this.collections.length == 0;
  }

  get emptyRecentCircles() {
    return this.recentCircles.length == 0;
  }
}
