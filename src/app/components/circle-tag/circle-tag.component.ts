import { Component, Input, OnInit } from '@angular/core';
import { Circle, JoinCirclesGQL } from '@app/graphql/circle';
import { Router } from '@angular/router';
import { CredentialsService } from '@app/auth';

import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';
import { GlobalModalService } from '@app/services/global-modal.service';

@Component({
  selector: 'circle-tag[circle]',
  templateUrl: './circle-tag.component.html',
  styleUrls: ['./circle-tag.component.scss'],
})
export class CircleTagComponent implements OnInit {
  @Input() circle?: Circle;
  @Input() follows: boolean = false;

  constructor(
    private router: Router,
    private followCircle: JoinCirclesGQL,
    private creds: CredentialsService,
    private modalService: GlobalModalService,
    private analytics: AnalyticsService
  ) {}

  ngOnInit(): void {}

  handleClick() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.postCircleTap));
    if (this.circle?.id) {
      this.router.navigate([`/c/${this.circle.urlName ? this.circle.urlName : this.circle.name}`]);
    }
  }

  circleFollow() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.postJoinCircleButton));
    if (this.isGuestUser) {
      this.modalService.open('sign-up');
      return;
    }

    this.followCircle
      .mutate({
        circlesIds: [this.circle?.id],
      })
      .subscribe(({ data }) => {
        if (data?.joinCircles.success) this.follows = true;
      });
  }

  get isGuestUser() {
    return !this.creds.isAuthenticated();
  }
}
