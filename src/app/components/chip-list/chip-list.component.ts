import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

export interface Chip<Type> {
  label: string;
  selected: boolean;
  value: Type;
}

@Component({
  selector: 'chip-list[items]',
  templateUrl: './chip-list.component.html',
  styleUrls: ['./chip-list.component.scss'],
})
export class ChipListComponent<Type> implements OnInit {
  @Input() items: Chip<Type>[] = [];
  @Input() primaryColor?: string;
  @Output() byItemSelected = new EventEmitter<Chip<Type>>();

  constructor() {}

  ngOnInit(): void {}

  toggleItemSelection(chip: Chip<Type>): void {
    chip.selected = !chip.selected;
    this.byItemSelected.emit(chip);
  }
}
