import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { PostEdge } from '@app/graphql/content';

@Component({
  selector: 'post-grid',
  templateUrl: './post-grid.component.html',
  styleUrls: ['./post-grid.component.scss'],
})
export class PostGridComponent {
  @Input() posts: PostEdge[] = [];

  constructor(private router: Router) {}

  getImage(post: PostEdge): string | undefined {
    if (post.node.imageContent) return post.node.imageContent.url;
    if (post.node.videoContent) return post.node.videoContent.thumbnailUrl;

    return undefined;
  }

  goToPreview = (post: PostEdge) => this.router.navigateByUrl(`p/${post.node.shortId}`);

  getLeftPollImage(post: PostEdge): string | undefined {
    return post.node.pollContent?.answers[0].imageUrl;
  }

  getRightPollImage(post: PostEdge): string | undefined {
    return post.node.pollContent?.answers[1].imageUrl;
  }
}
