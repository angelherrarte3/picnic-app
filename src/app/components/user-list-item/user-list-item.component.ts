import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'user-list-item',
  templateUrl: './user-list-item.component.html',
  styleUrls: ['./user-list-item.component.scss'],
})
export class UserListItemComponent<Type1> {
  @Input() avatarImage?: string;
  @Input() username?: string;
  @Input() data?: Type1;

  @Output() byClickAvatar = new EventEmitter<Type1 | undefined>();

  @Input() profileData?: Type1;

  handleClickAvatar = () => this.byClickAvatar.emit(this.profileData);

  constructor() {}
}
