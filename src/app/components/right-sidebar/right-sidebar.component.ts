import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Circle, ElectionParticipant, GetMembersGQL } from '@app/graphql/circle';
import { TmpStateService } from '@app/services/tmp-state.service';

@Component({
  selector: 'right-sidebar',
  templateUrl: './right-sidebar.component.html',
  styleUrls: ['./right-sidebar.component.scss'],
})
export class RightSidebarComponent implements OnInit {
  @Input() circle?: Circle;
  @Input() showRules: boolean = false;
  @Input() showElections: boolean = false;
  @Input() showCircleSummary: boolean = false;

  members: ElectionParticipant[] = [];
  currentPage: string;

  constructor(
    private getMembers: GetMembersGQL,
    private state: TmpStateService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.url.subscribe((url) => {
      this.currentPage = url[0].path;
    });
  }

  ngOnInit(): void {
    this.initMembers();
  }
  get coverImage() {
    if (!this.circle?.coverImageFile?.trim()) return undefined;
    return `url(${this.circle.coverImageFile})`;
  }

  initMembers(): void {
    this.getMembers
      .watch({
        circleId: this.circle?.id ?? '',
        cursor: {
          id: '',
          limit: 10,
          dir: 'forward',
        },
        isBanned: false,
        roles: ['DIRECTOR', 'MODERATOR'],
        searchQuery: '',
      })
      .valueChanges.subscribe(({ data }) => {
        this.members = data.getMembers.edges.map((edge) => edge.node);
      });
  }

  get url() {
    return `c/${this.circle?.urlName ? this.circle.urlName : this.circle?.name}`;
  }

  goToMembers() {
    this.router.navigateByUrl(`${this.url}/members`).then((r) => console.log(r));
  }

  goToRules() {
    this.router.navigateByUrl(`${this.url}/rules`).then((r) => console.log(r));
  }

  goToProfile(username?: string) {
    this.router.navigateByUrl(`u/${username}`).then((r) => console.log(r));
  }

  get rulesText(): string {
    return this.circle?.rulesText?.trim() ?? '';
  }

  get moderators() {
    return this.members.filter((m) => m.role == 'MODERATOR');
  }

  get director() {
    return this.members.find((member) => member.role === 'DIRECTOR');
  }

  get seedsCount(): number {
    return (
      this.circle?.seedsConnection?.edges?.find((v) => v.node.owner!.id === this.state.profile?.id)?.node.amountTotal ??
      0
    );
  }
}
