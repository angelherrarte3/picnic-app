import { Location } from '@angular/common';
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationService } from '@app/services/navigation.service';
import { GoBackNavigation } from '@app/types/go-back-navigation';

@Component({
  selector: 'page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
})
export class PageHeaderComponent extends GoBackNavigation {
  @Input() title?: string;
  @Input() backRoute?: string;
  @Input() showBackButton: boolean = true;
  @Input() showBackButtonBackground: boolean = true;

  constructor(protected override navigationService: NavigationService) {
    super(navigationService);
  }
}
