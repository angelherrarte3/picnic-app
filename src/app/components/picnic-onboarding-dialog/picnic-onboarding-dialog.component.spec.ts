import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicnicOnboardingDialogComponent } from './picnic-onboarding-dialog.component';

describe('PicnicOnboardingDialogComponent', () => {
  let component: PicnicOnboardingDialogComponent;
  let fixture: ComponentFixture<PicnicOnboardingDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PicnicOnboardingDialogComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PicnicOnboardingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
