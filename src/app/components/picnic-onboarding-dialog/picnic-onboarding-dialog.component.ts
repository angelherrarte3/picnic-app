import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'picnic-onboarding-dialog',
  templateUrl: './picnic-onboarding-dialog.component.html',
  styleUrls: ['./picnic-onboarding-dialog.component.scss'],
})
export class PicnicOnboardingDialogComponent implements OnInit {
  @Input() title?: string;
  @Input() subtitle?: string;
  @Input() emoji?: string;
  @Input() emojiSize?: number;
  @Input() emojiBg?: string;
  @Input() emojiPadding?: string;
  @Input() progress?: string;
  @Input() loading?: boolean;

  constructor() {}

  ngOnInit(): void {}
}
