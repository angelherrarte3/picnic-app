import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss'],
})
export class SwitchComponent implements OnInit {
  @Output() byToggle = new EventEmitter<boolean>();
  @Input() checked: boolean = false;

  constructor() {}

  ngOnInit(): void {}

  handleToggle(event: any) {
    this.checked = event.target.checked;
    this.byToggle.emit(this.checked);
  }
}
