import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

export interface BreadcrumbItem {
  label: string;
  path: string;
  selectable: boolean;
}

@Component({
  selector: 'breadcrumb-navigation[items]',
  templateUrl: './breadcrumb-navigation.component.html',
  styleUrls: ['./breadcrumb-navigation.component.scss'],
})
export class BreadcrumbNavigationComponent implements OnInit {
  @Input() items: BreadcrumbItem[] = [];

  constructor() {}

  ngOnInit(): void {}
}
