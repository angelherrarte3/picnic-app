import { Component, OnInit } from '@angular/core';
import { FeedConnectionGQL, FeedEdge } from '@app/graphql/feed';
import { Router } from '@angular/router';
import { CredentialsService } from '@app/auth';
import { FeedSidebarService } from '@app/services/feed-sidebar.service';
import { ChatSidebarService } from '@app/services/chat-sidebar.service';
import { GlobalModalService } from '@app/services/global-modal.service';

enum FeedTabs {
  ForYou = 'for you',
  Circles = 'circles',
}

@Component({
  selector: 'feed-navbar',
  templateUrl: './feed-navbar.component.html',
  styleUrls: ['./feed-navbar.component.scss'],
})
export class FeedNavbarComponent implements OnInit {
  feeds: FeedEdge[] = [];
  guest: boolean;

  constructor(
    private router: Router,
    private feedConnection: FeedConnectionGQL,
    private feedSidebarService: FeedSidebarService,
    private chatSidebarService: ChatSidebarService,
    private credentialsService: CredentialsService,
    private modalService: GlobalModalService
  ) {}

  ngOnInit(): void {
    this.guest = this.isGuestUser;
    this.feedConnection
      .watch({
        cursor: {
          id: '',
          limit: 7,
        },
      })
      .valueChanges.subscribe(({ data }) => {
        this.feeds = data.feedsConnection.edges;
      });
  }

  get isGuestUser() {
    return !this.credentialsService.isAuthenticated();
  }

  get forYouFeed() {
    return this.feeds.find((feed) => feed.node.name === 'For you');
  }

  get activeFeedTab(): FeedTabs {
    return this.router.url === '/my-circles' ? FeedTabs.Circles : FeedTabs.ForYou;
  }

  openSignUpModal() {
    this.modalService.open('sign-up');
  }

  toggleFeedSidebar() {
    if (this.chatSidebarService.isVisible) {
      this.chatSidebarService.hide();
    }
    this.feedSidebarService.toggle();
  }

  onTapForYouFeed() {
    this.router.navigateByUrl('');
  }

  onTapCirclesFeed() {
    if (!this.credentialsService.isAuthenticated()) {
      this.openSignUpModal();
      return;
    }
    this.router.navigateByUrl('my-circles');
  }
}
