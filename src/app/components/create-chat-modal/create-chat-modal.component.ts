import { trigger, transition, style, animate } from '@angular/animations';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Component, ElementRef, Inject, OnDestroy, OnInit, PLATFORM_ID } from '@angular/core';
import { PublicProfile, UserConnectionGQL } from '@app/graphql/profile';
import { ModalName } from '@app/services/global-modal.service';
import { ModalComponent } from '@app/types/modal-component';
import { Subject, debounceTime, map, switchMap } from 'rxjs';
import { CreateGroupChatGQL, CreateSingleChatGQL } from '@app/graphql/chat';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';
import { Router } from '@angular/router';
import { ChatSidebarService } from '@app/services/chat-sidebar.service';

enum CreateChatMode {
  CreateChat = 0,
  EditChat = 1,
}

@Component({
  selector: 'create-chat-modal',
  templateUrl: './create-chat-modal.component.html',
  styleUrls: ['./create-chat-modal.component.scss'],
  animations: [
    trigger('fadeAnimation', [
      transition(':enter', [
        style({ height: 0, opacity: 0 }),
        animate('100ms ease-out', style({ height: '*', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ height: '*', opacity: 1 }),
        animate('100ms ease-in', style({ height: 0, opacity: 0 })),
      ]),
    ]),
  ],
})
export class CreateChatModalComponent extends ModalComponent implements OnInit {
  override name: ModalName = 'create-chat';

  private readonly searchUsers$ = new Subject<string>();
  userResults: PublicProfile[] = [];

  loading: boolean = false;
  mode: CreateChatMode = CreateChatMode.CreateChat;
  usersSelected: PublicProfile[] = [];

  groupName = '';
  windowWidth = 0;

  constructor(
    element: ElementRef,
    private userConnection: UserConnectionGQL,
    @Inject(PLATFORM_ID) private platformId: string,
    private createSingleChatGQL: CreateSingleChatGQL,
    private createGroupChatGQL: CreateGroupChatGQL,
    private bubbleChatState: BubbleChatStateService,
    private router: Router,
    private chatSidebarService: ChatSidebarService
  ) {
    super(element);

    if (isPlatformBrowser(this.platformId)) {
      this.windowWidth = window.innerWidth;
    }
  }

  ngOnInit(): void {
    if (isPlatformServer(this.platformId)) return;

    this.initSearchUsers();
    this.onSearchUsers('');
  }

  get isMobileLayout() {
    return this.windowWidth <= 768;
  }

  get creatingChat(): boolean {
    return this.mode === CreateChatMode.CreateChat;
  }

  get usersSelectedCount(): number {
    return this.usersSelected.length;
  }

  override resetState() {
    this.loading = false;
    this.mode = CreateChatMode.CreateChat;
  }

  private initSearchUsers() {
    this.searchUsers$
      .pipe(
        debounceTime(300),
        switchMap((searchQuery) => {
          return this.userConnection
            .fetch({
              searchQuery: searchQuery,
              cursor: {
                id: '',
                limit: 20,
                dir: 'forward',
              },
            })
            .pipe(map(({ data }) => data.usersConnection.edges));
        })
      )
      .subscribe((users) => {
        this.userResults = users.map((user) => user.node);
      });
  }

  onTapNext() {
    this.mode = CreateChatMode.EditChat;
  }

  onTapBack() {
    this.mode = CreateChatMode.CreateChat;
    this.loading = false;
  }

  onTapCreateChat() {
    this.loading = true;

    if (this.usersSelected.length == 1) {
      this.createSingleChatGQL
        .mutate({
          userIds: [this.usersSelected[0].id],
        })
        .subscribe(({ data }) => {
          this.resetState();
          this.close();
          if (this.isMobileLayout) {
            this.router.navigate(['chat', data?.createSingleChat.id]);
            return;
          }

          this.bubbleChatState.addModalChat(data!.createSingleChat);
          this.bubbleChatState.rebuildActiveChats(data!.createSingleChat);
          this.chatSidebarService.addChat(data!.createSingleChat);
          this.loading = false;
        });
    } else {
      this.createGroupChatGQL
        .mutate({
          data: {
            name: this.groupName,
            userIds: this.usersSelected.map((v) => v.id),
          },
        })
        .subscribe(({ data }) => {
          this.resetState();
          this.close();
          if (this.isMobileLayout) {
            this.router.navigate(['chat', data?.createGroupChat.id]);
            return;
          }

          this.bubbleChatState.addModalChat(data!.createGroupChat);
          this.bubbleChatState.rebuildActiveChats(data!.createGroupChat);
          this.chatSidebarService.addChat(data!.createGroupChat);
          this.loading = false;
        });
    }
  }

  onTapUserItem(user: PublicProfile) {
    const userIndex = this.usersSelected.findIndex((selectedUser) => selectedUser.id === user.id);

    if (userIndex === -1) {
      this.usersSelected.push(user);
    } else {
      this.usersSelected.splice(userIndex, 1);
    }
  }

  onTapRemoveUser(user: PublicProfile) {
    const userIndex = this.usersSelected.findIndex((selectedUser) => selectedUser.id === user.id);

    if (userIndex === -1) return;

    this.usersSelected.splice(userIndex, 1);
  }

  userIsSelected(user: PublicProfile): boolean {
    return this.usersSelected.some((selectedUser) => selectedUser.id === user.id);
  }

  onSearchUsers(search: Event | string) {
    if (typeof search === 'string') {
      this.searchUsers$.next(search);
      return;
    }

    const searchQuery = (search.target as HTMLInputElement).value;
    this.searchUsers$.next(searchQuery);
  }
}
