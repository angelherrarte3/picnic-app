import { Component, HostListener, Inject, Input, OnInit, PLATFORM_ID } from '@angular/core';
import { TextContent } from '@app/graphql/content';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'text-content',
  templateUrl: './text-content.component.html',
  styleUrls: ['./text-content.component.scss'],
})
export class TextContentComponent implements OnInit {
  @Input() content?: TextContent;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.windowWidth = event.target.innerWidth;
  }

  windowWidth = 1920;

  get isSmallMobile() {
    return this.windowWidth <= 450;
  }

  constructor(@Inject(PLATFORM_ID) private platformId: string) {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    this.windowWidth = window.innerWidth;
  }

  ngOnInit(): void {}
}
