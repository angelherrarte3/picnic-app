import { Component, Input, OnInit, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { LinkPostContent } from '@app/graphql/content';

@Component({
  selector: 'link-content',
  templateUrl: './link-content.component.html',
  styleUrls: ['./link-content.component.scss'],
})
export class LinkContentComponent implements OnInit {
  @Input() content?: LinkPostContent;
  linkImage?: string;
  linkAddress?: string;

  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit(): void {
    this.linkImage =
      this.sanitizer.sanitize(SecurityContext.STYLE, `url(${this.content?.linkMetaData?.imageUrl})`) ?? '';
    this.linkAddress = this.sanitizer.sanitize(SecurityContext.URL, `${this.content?.url}`) ?? '';
  }

  openLink = () => window.open(this.linkAddress);
}
