import { Component, Input, OnInit } from '@angular/core';
import { ImagePostContent } from '@app/graphql/content';

@Component({
  selector: 'image-content[content]',
  templateUrl: './image-content.component.html',
  styleUrls: ['./image-content.component.scss'],
})
export class ImageContentComponent implements OnInit {
  @Input() content?: ImagePostContent;

  constructor() {}

  ngOnInit(): void {}
}
