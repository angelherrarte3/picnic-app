import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PollContentComponent } from './poll-content.component';

describe('PollContentComponent', () => {
  let component: PollContentComponent;
  let fixture: ComponentFixture<PollContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PollContentComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PollContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
