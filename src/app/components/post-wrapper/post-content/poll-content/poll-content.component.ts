import { Component, EventEmitter, Input, OnInit, Output, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { PollAnswer, PollPostContent, Post } from '@app/graphql/content';
import { MyProfileStateService } from '@app/profile/my-profile-state.service';
import { PrivateProfile, PublicProfile } from '@app/graphql/profile';
import { CredentialsService } from '@app/auth';
import { PostReactService } from '@app/services/post-react.service';
import { GlobalModalService } from '@app/services/global-modal.service';

@Component({
  selector: 'poll-content',
  templateUrl: './poll-content.component.html',
  styleUrls: ['./poll-content.component.scss'],
})
export class PollContentComponent implements OnInit {
  @Input() content?: PollPostContent;
  @Input() post?: Post;

  @Output() private byVote = new EventEmitter<PollAnswer>();

  leftImage?: string;
  rightImage?: string;
  leftBarPercent = 0;
  rightBarPercent = 0;
  profile?: PublicProfile | PrivateProfile;

  constructor(
    private sanitizer: DomSanitizer,
    private profileState: MyProfileStateService,
    private creds: CredentialsService,
    private modalService: GlobalModalService,
    private postReactService: PostReactService
  ) {}

  ngOnInit(): void {
    this.leftImage = this.sanitizer.sanitize(SecurityContext.STYLE, `url(${this.content?.answers[0].imageUrl})`) ?? '';
    this.rightImage = this.sanitizer.sanitize(SecurityContext.STYLE, `url(${this.content?.answers[1].imageUrl})`) ?? '';
    this.profileState.profile.subscribe((p) => {
      this.profile = p;
    });
    this.calcPercents();
  }

  get isGuestUser() {
    return !this.creds.isAuthenticated();
  }

  handleLeftVote = () => (this.isVoted() ? null : this.handleVote(0));

  handleRightVote = () => (this.isVoted() ? null : this.handleVote(1));

  private handleVote(answerIndex: number) {
    if (this.content) {
      if (this.isGuestUser) {
        this.modalService.open('sign-up');
        return;
      }
      const answersCopy = [...this.content.answers];
      answersCopy[answerIndex] = {
        ...answersCopy[answerIndex],
        votesCount: answersCopy[answerIndex].votesCount + 1,
      };
      this.content = {
        ...this.content,
        answers: answersCopy,
        votesTotal: this.content.votesTotal + 1,
        votedAnswer: answersCopy[answerIndex].id,
      };
      this.calcPercents();
      this.byVote.emit(this.content.answers[answerIndex]);
    }
  }

  private calcPercents() {
    if (this.content) {
      this.leftBarPercent = Math.round((this.content.answers[0].votesCount / this.content.votesTotal) * 100);
      this.rightBarPercent = 100 - this.leftBarPercent;
    }
  }

  isVoted() {
    return this.content?.votedAnswer;
  }

  isLeftPositive() {
    return this.leftBarPercent >= this.rightBarPercent;
  }

  isRightPositive() {
    return this.rightBarPercent >= this.leftBarPercent;
  }

  isLeftVoted() {
    if (!this.content?.votedAnswer) {
      return false;
    }
    return this.content?.answers[0].id === this.content?.votedAnswer;
  }

  isRightVoted() {
    if (!this.content?.votedAnswer) {
      return false;
    }
    return this.content?.answers[1].id === this.content?.votedAnswer;
  }

  like = () => this.postReactService.likePost(this.post?.id ?? '');
}
