import { Component, Input, OnInit } from '@angular/core';
import { MyProfileStateService } from '@app/profile/my-profile-state.service';
import { PrivateProfile, PublicProfile } from '@app/graphql/profile';
import { trigger, style, state, animate, transition } from '@angular/animations';

@Component({
  selector: 'poll-vote-bar',
  templateUrl: './poll-vote-bar.component.html',
  styleUrls: ['./poll-vote-bar.component.scss'],
  animations: [
    trigger('position', [
      state(
        'start',
        style({
          height: '{{barHeight}}%',
        }),
        { params: { barHeight: '0' } }
      ),
      transition('void => start', animate('1s ease-in-out')),
    ]),
  ],
})
export class PollVoteBarComponent implements OnInit {
  @Input() isMyVote: boolean = false;
  @Input() isPositive: boolean = false;
  @Input() barPercent: number = 0;

  readonly maxBarLineHeightMultiplier = 0.5;

  position = 'start';
  profile?: PublicProfile | PrivateProfile;

  constructor(private profileState: MyProfileStateService) {}

  ngOnInit(): void {
    this.profileState.profile.subscribe((p) => {
      this.profile = p;
    });
  }

  barHeight() {
    return this.barPercent * this.maxBarLineHeightMultiplier;
  }
}
