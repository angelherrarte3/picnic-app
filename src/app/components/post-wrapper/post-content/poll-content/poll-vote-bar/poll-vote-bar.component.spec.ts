import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PollVoteBarComponent } from './poll-vote-bar.component';

describe('PollVoteBarComponent', () => {
  let component: PollVoteBarComponent;
  let fixture: ComponentFixture<PollVoteBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PollVoteBarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PollVoteBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
