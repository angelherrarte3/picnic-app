import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CredentialsService } from '@app/auth';
import { PollAnswer, Post, VoteInPollGQL } from '@app/graphql/content';
import { GlobalModalService } from '@app/services/global-modal.service';
import { PostReactService } from '@app/services/post-react.service';

@Component({
  selector: 'post-content[post]',
  templateUrl: './post-content.component.html',
  styleUrls: ['./post-content.component.scss'],
})
export class PostContentComponent implements OnInit {
  @Input() post?: Post;

  showMetaWords = false;

  constructor(
    private voteInPollGQL: VoteInPollGQL,
    private credentialsService: CredentialsService,
    private router: Router,
    private modalService: GlobalModalService,
    private postReactService: PostReactService,
    route: ActivatedRoute
  ) {
    this.showMetaWords = route.snapshot.url.some((segment) => segment.path === 'p');
  }

  ngOnInit(): void {}

  get isGuestUser() {
    return !this.credentialsService.isAuthenticated();
  }

  get metaWords() {
    return this.post?.circle?.metaWords ?? [];
  }

  onPollVote(answer: PollAnswer) {
    if (this.post?.pollContent?.votedAnswer) {
      return;
    }
    this.voteInPollGQL
      .mutate({
        postId: this.post!.id,
        variantId: answer.id,
      })
      .subscribe(({ data }) => {
        this.post!.pollContent!.votedAnswer = answer.id;
        this.post!.pollContent!.votesTotal++;
        answer.votesCount++;
      });
  }

  goToPreview = () => this.router.navigateByUrl(`p/${this.post?.shortId}`);

  like = () => this.postReactService.likePost(this.post?.id ?? '');
}
