import { AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { VideoPostContent } from '@app/graphql/content';
import { PreferencesService } from '@app/services/preferences.service';
import videojs from 'video.js';
import '@app/videojs/components/nuevo.js';
import '@app/videojs/components/videojs.hotkeys.js';
import '@app/videojs/components/videojs.events.js';
import { Subscription } from 'rxjs';
import { VideoTypeService } from '@app/services/video-type.service';
import { PlatformService } from '@app/services/platform.service';

@Component({
  selector: 'video-player[content]',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss'],
})
export class VideoPlayerComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() content?: VideoPostContent;
  @Input() initialShouldPlay?: boolean;
  @ViewChild('videoPlayer') video!: ElementRef;

  volume: number = 0;
  isMuted: boolean = false;
  isSystemMuted: boolean = false;
  shouldPlay: boolean = false;

  player?: videojs.Player;
  volumeSubscription?: Subscription;
  isMutedSubscription?: Subscription;
  isSystemMutedSubscription?: Subscription;
  clickSubscription?: Subscription;

  constructor(
    private preferences: PreferencesService,
    private platformService: PlatformService,
    private videoTypeService: VideoTypeService
  ) {
    window.VIDEOJS_NO_DYNAMIC_STYLE = true;
  }

  ngOnInit() {
    this.volumeSubscription = this.preferences.volume.subscribe((volume) => {
      this.volume = volume;
      this.updateNaviteVolume();
    });

    this.isMutedSubscription = this.preferences.isMuted.subscribe((isMuted) => {
      this.isMuted = isMuted;
      this.updateNativeMuted();
    });

    this.isSystemMutedSubscription = this.preferences.isSystemMuted.subscribe((isSystemMuted) => {
      this.isSystemMuted = isSystemMuted;
      this.updateNativeMuted();
    });
  }

  ngAfterViewInit() {
    this.createPlayer();
    this.updateNaviteVolume();
    if (this.initialShouldPlay) {
      this.updateShouldPlay(true);
    }
  }

  private createPlayer() {
    const videoType = this.videoTypeService.getVideoTypeByUrl(this.content?.url ?? '');

    var source = {
      src: this.content?.url ?? '',
    };

    if (videoType) {
      source['type'] = videoType;
    }

    const player_options = {
      poster: this.content?.thumbnailUrl ?? '',
      sources: [source],
    };

    const nuevo_options = {
      singlePlay: true,
      buttonForward: true,
      rewindforward: 5,
    };

    this.player = videojs(this.video.nativeElement, player_options);

    if (!this.platformService.isIOS()) {
      this.player.nuevo(nuevo_options);
    }

    this.player.hotkeys({
      volumeStep: 0.1,
      seekStep: 5,
      enableVolumeScroll: false,
      enableInactiveFocus: false,
      alwaysCaptureHotkeys: false,
    });

    this.player.events({ analytics: false });

    this.player.on('track', (e, data) => {
      if (data.event === 'mute') {
        this.preferences.setMuted(true);
      }
      if (data.event === 'unmute') {
        this.preferences.setMuted(false);
      }
    });
  }

  updateShouldPlay(shouldPlay: boolean): void {
    try {
      if (this.shouldPlay === shouldPlay) return;
      this.shouldPlay = shouldPlay;
      if (this.shouldPlay && this.player!.paused()) {
        this.startPlayer();
        return;
      }

      if (!this.shouldPlay && !this.player!.paused()) {
        this.stopPlayer();
        return;
      }
      this.shouldPlay = !this.player!.paused();
    } catch (e) {
      this.shouldPlay = false;
    }
  }

  private startPlayer(): void {
    try {
      this.updateNativeMuted();
      var promise = this.player!.play();

      if (promise !== undefined) {
        promise
          .then((_) => {
            // Autoplay started!
            if (this.isSystemMuted) {
              this.isSystemMuted = false;
              this.preferences.setSystemMuted(false);
            }
          })
          .catch((error) => {
            // Autoplay not allowed!
            // Mute video and try to play again
            this.preferences.setSystemMuted(true);
            this.isSystemMuted = true;
            this.updateNativeMuted();
            this.player?.play()?.catch((error) => {});
            if (!this.shouldPlay) {
              this.stopPlayer();
            }
          });
      }
      if (!this.shouldPlay) {
        this.stopPlayer();
      }
    } catch (e) {}
  }

  private stopPlayer(): void {
    try {
      this.player?.pause();
    } catch (e) {}
  }

  ngOnDestroy() {
    if (this.player) {
      this.player.dispose();
    }
    this.volumeSubscription?.unsubscribe();
    this.isMutedSubscription?.unsubscribe();
    this.isSystemMutedSubscription?.unsubscribe();
    this.clickSubscription?.unsubscribe();
  }

  onVolumeChange(event: any): void {
    const volume = event.target.volume;
    const muted = event.target.muted;
    if (muted || volume <= 0) return;
    this.preferences.setVolume(event.target.volume);
  }

  private updateNaviteVolume(): void {
    try {
      this.video.nativeElement.volume = this.volume;
    } catch (e) {}
  }

  private updateNativeMuted(): void {
    try {
      this.player?.muted(this.isSystemMuted || this.isMuted);
    } catch (e) {}
  }
}
