import {
  AfterViewInit,
  Component,
  ComponentRef,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { VideoPostContent } from '@app/graphql/content';
import { VideoItemPlayState, VideoItemVisibilityEvent, VideoQueueService } from '@app/services/video-queue.service';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'video-content[content]',
  templateUrl: './video-content.component.html',
  styleUrls: ['./video-content.component.scss'],
})
export class VideoContentComponent implements AfterViewInit, OnDestroy {
  @Input() content?: VideoPostContent;
  @ViewChild('videoContainer') videoContainer!: ElementRef;
  @ViewChild('viewContainerRef', { read: ViewContainerRef }) vcr!: ViewContainerRef;
  ref!: ComponentRef<VideoPlayerComponent>;
  private subscription?: Subscription;

  constructor(private videoQueueService: VideoQueueService) {}

  ngAfterViewInit() {
    this.subscription = this.videoQueueService
      .observeVideoItemState(this.videoContainer.nativeElement)
      .subscribe((event: VideoItemVisibilityEvent) => {
        this.updatePlayerVisibility(event.visible);
        if (this.ref) {
          if (event.playState == VideoItemPlayState.Playing) {
            this.ref.instance.initialShouldPlay = true;
            this.ref.instance.updateShouldPlay(true);
          }
          if (event.playState == VideoItemPlayState.Paused) {
            this.ref.instance.updateShouldPlay(false);
          }
        }
      });
  }

  private updatePlayerVisibility(visible: boolean): void {
    var index = -1;
    var exists = false;
    if (this.ref) {
      index = this.vcr.indexOf(this.ref.hostView);
      exists = index != -1;
    }
    if (visible && !exists) {
      this.ref = this.vcr.createComponent(VideoPlayerComponent);
      this.ref.instance.content = this.content;
    } else if (!visible && exists) {
      this.vcr.remove(index);
    }
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
    this.subscription = undefined;
  }
}
