import { Component, Input, OnInit } from '@angular/core';
import { Post } from '@app/graphql/content';
import { PicnicBannerMode } from '@app/components/picnic-banner/picnic-banner.component';
import { MessageListMode } from '@app/components/message-list/message-list.component';

@Component({
  selector: 'post-vertical-layout[post]',
  templateUrl: './post-vertical-layout.component.html',
  styleUrls: ['./post-vertical-layout.component.scss'],
})
export class PostVerticalLayoutComponent implements OnInit {
  @Input() post?: Post;
  bannerMode = PicnicBannerMode.Chat;
  messageListMode = MessageListMode.Vertical;

  constructor() {}

  ngOnInit(): void {}
}
