import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostVerticalLayoutComponent } from './post-vertical-layout.component';

describe('PostVerticalLayoutComponent', () => {
  let component: PostVerticalLayoutComponent;
  let fixture: ComponentFixture<PostVerticalLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostVerticalLayoutComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PostVerticalLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
