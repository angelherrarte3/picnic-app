import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostHorizontalLayoutComponent } from './post-horizontal-layout.component';

describe('PostHorizontalLayoutComponent', () => {
  let component: PostHorizontalLayoutComponent;
  let fixture: ComponentFixture<PostHorizontalLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostHorizontalLayoutComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PostHorizontalLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
