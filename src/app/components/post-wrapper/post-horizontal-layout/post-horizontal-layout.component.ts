import { Component, ContentChild, HostListener, Inject, Input, OnInit, PLATFORM_ID, TemplateRef } from '@angular/core';
import { PicnicBannerMode } from '@app/components/picnic-banner/picnic-banner.component';
import { Post } from '@app/graphql/content';
import { MessageListMode } from '@app/components/message-list/message-list.component';
import { Router } from '@angular/router';
import { CredentialsService } from '@app/auth';
import { PostReactService } from '@app/services/post-react.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { isPlatformBrowser } from '@angular/common';
import { EventManager } from '@angular/platform-browser';

@Component({
  selector: 'post-horizontal-layout[post][showComments]',
  templateUrl: './post-horizontal-layout.component.html',
  styleUrls: ['./post-horizontal-layout.component.scss'],
})
export class PostHorizontalLayoutComponent implements OnInit {
  @Input() post?: Post;
  @ContentChild(TemplateRef) contentRef?: TemplateRef<any>;
  @Input() showComments: boolean = true;
  bannerMode = PicnicBannerMode.Chat;
  messageListMode = MessageListMode.Horizontal;
  isMobileLayout = false;

  constructor(
    @Inject(PLATFORM_ID) private platformId: string,
    private router: Router,
    private creds: CredentialsService,
    private postReactService: PostReactService,
    private analytics: AnalyticsService
  ) {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    this.isMobileLayout = window.innerWidth < 600;
  }

  ngOnInit(): void {}

  @HostListener('window:resize', ['$event'])
  onWindowResize() {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    this.isMobileLayout = window.innerWidth < 600;
  }

  get postCaption(): string {
    const captionByType = {
      IMAGE: this.post?.imageContent?.text,
      VIDEO: this.post?.videoContent?.text,
      TEXT: this.post?.textContent?.text,
      LINK: this.post?.linkContent?.linkMetaData?.description,
      POLL: this.post?.pollContent?.question,
      default: undefined,
    };
    return captionByType[this.post?.type ?? 'default'] ?? this.post?.title ?? '';
  }

  get postCaptionExtra(): string {
    return this.post?.textContent?.more?.trim() ?? '';
  }

  get isGuestUser() {
    return !this.creds.isAuthenticated();
  }

  goToPreview = () => this.router.navigateByUrl(`p/${this.post?.shortId}`);

  like() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.postLikeDoubleTap));
    this.postReactService.likePost(this.post?.id ?? '');
  }
}
