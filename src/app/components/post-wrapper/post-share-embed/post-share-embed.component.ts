import { Component, Input, OnInit } from '@angular/core';
import { Post } from '@app/graphql/content';
import { environment } from '@env/environment';

@Component({
  selector: 'post-share-embed',
  templateUrl: './post-share-embed.component.html',
  styleUrls: ['./post-share-embed.component.scss'],
})
export class PostShareEmbedComponent implements OnInit {
  @Input() post?: Post;
  open = false;
  iframeText = '';
  constructor() {}

  ngOnInit(): void {}

  onShareClick() {
    this.open = true;

    this.iframeText = `<iframe width="560" height="315" src="${environment.host}/embed/${this.post?.id}" title="picnic video" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>`;
  }
}
