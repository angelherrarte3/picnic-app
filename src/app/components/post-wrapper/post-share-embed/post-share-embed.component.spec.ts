import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostShareEmbedComponent } from './post-share-embed.component';

describe('PostShareEmbedComponent', () => {
  let component: PostShareEmbedComponent;
  let fixture: ComponentFixture<PostShareEmbedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostShareEmbedComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PostShareEmbedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
