import { AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Post, SavePostScreenTimeGQL } from '@app/graphql/content';
import { PostItemVisibilityEvent, PostScreenTimeService } from '@app/services/post-screen-time.service';
import { Subscription } from 'rxjs';

export enum PostLayout {
  Vertical,
  Horizontal,
}

@Component({
  selector: 'post-wrapper[post][showComments]',
  templateUrl: './post-wrapper.component.html',
  styleUrls: ['./post-wrapper.component.scss'],
})
export class PostWrapperComponent implements OnInit, AfterViewInit, OnDestroy {
  _post?: Post;
  postLayout = PostLayout.Vertical;
  @Input() showComments: boolean = true;

  readonly layoutEnum = PostLayout;

  @Input() set post(post: Post | undefined) {
    this._post = post;
    if (this.post?.type === 'TEXT' || this.post?.type === 'POLL') {
      this.postLayout = PostLayout.Horizontal;
    } else {
      // MVP v1.0
      // this.postLayout = PostLayout.Vertical;
      this.postLayout = PostLayout.Horizontal;
    }
  }

  get post(): Post | undefined {
    return this._post;
  }

  @ViewChild('postContainer') postContainer!: ElementRef;
  private subscription?: Subscription;
  private isPostVisible: boolean = false;
  private postVisibleStartDate?: Date;

  constructor(
    private postScreenTimeService: PostScreenTimeService,
    private savePostScreenTimeGQL: SavePostScreenTimeGQL
  ) {}

  ngOnInit(): void {
    console.log('PostWrapperComponent ngOnInit');
  }

  ngAfterViewInit() {
    this.subscription = this.postScreenTimeService
      .observePostItem(this.postContainer.nativeElement)
      .subscribe((event: PostItemVisibilityEvent) => {
        this.postVisibilityUpdated(event.visible);
      });
  }

  private postVisibilityUpdated(visible: boolean): void {
    if (visible && !this.postVisibleStartDate) {
      this.postVisibleStartDate = new Date();
      return;
    }
    if (!visible && this.postVisibleStartDate) {
      const msBetweenDates = new Date().getTime() - this.postVisibleStartDate.getTime();
      const secondsBetweenDates = Math.round(msBetweenDates / 1000);
      console.log(`Post visible for ${secondsBetweenDates} seconds`);
      this.postVisibleStartDate = undefined;
      if (this.post) {
        this.savePostScreenTimeGQL
          .mutate({
            postID: this.post.id,
            duration: secondsBetweenDates,
          })
          .subscribe();
      }
    }
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
    this.subscription = undefined;
  }
}
