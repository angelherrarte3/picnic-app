import { Component, Input } from '@angular/core';

@Component({
  selector: 'modal-wrapper[isOpen]',
  templateUrl: './modal-wrapper.component.html',
  styleUrls: ['./modal-wrapper.component.scss'],
})
export class ModalWrapperComponent {
  @Input() isOpen: boolean;
}
