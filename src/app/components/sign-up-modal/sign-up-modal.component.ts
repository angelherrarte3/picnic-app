import { Component, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ModalName } from '@app/services/global-modal.service';
import { ModalComponent } from '@app/types/modal-component';

@Component({
  selector: 'sign-up-modal',
  templateUrl: './sign-up-modal.component.html',
  styleUrls: ['./sign-up-modal.component.scss'],
})
export class SignUpModalComponent extends ModalComponent {
  override name: ModalName = 'sign-up';

  constructor(element: ElementRef, private router: Router) {
    super(element);
  }

  handleDownloadPicnic() {
    this.close();
    this.router.navigateByUrl('/download');
  }

  goToLogin() {
    this.close();
    this.router.navigate(['/login']);
  }

  override resetState() {}
}
