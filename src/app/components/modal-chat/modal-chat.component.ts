import { Component } from '@angular/core';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';
import { SendChatMessageGQL } from '@app/graphql/chat';
import { UploadAttachmentGQL } from '@app/graphql/attachment';
import { Router } from '@angular/router';
import { ChatSidebarService } from '@app/services/chat-sidebar.service';
import { ChatComponent } from '@app/types/chat-component';

@Component({
  selector: 'modal-chat[chat]',
  templateUrl: './modal-chat.component.html',
  styleUrls: ['./modal-chat.component.scss'],
})
export class ModalChatComponent extends ChatComponent {
  constructor(
    bubbleChatState: BubbleChatStateService,
    chatSidebarService: ChatSidebarService,
    sendMessageGQL: SendChatMessageGQL,
    uploadAttachmentGQL: UploadAttachmentGQL,
    router: Router
  ) {
    super(bubbleChatState, chatSidebarService, router, sendMessageGQL, uploadAttachmentGQL);
  }

  onTapCloseModal(chatId: string) {
    this.bubbleChatState.removeModalChat(chatId);
  }
}
