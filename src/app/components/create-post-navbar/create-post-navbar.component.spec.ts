import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePostNavbarComponent } from './create-post-navbar.component';

describe('CreatePostNavbarComponent', () => {
  let component: CreatePostNavbarComponent;
  let fixture: ComponentFixture<CreatePostNavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreatePostNavbarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CreatePostNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
