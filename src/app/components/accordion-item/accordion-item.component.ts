import { Component, Input, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

export const expandAnimation = trigger('expandAnimation', [
  state(
    'collapsed',
    style({
      maxHeight: '0',
      overflow: 'hidden',
    })
  ),
  state(
    'expanded',
    style({
      maxHeight: '500px',
      overflow: 'hidden',
    })
  ),
  transition('collapsed <=> expanded', animate('250ms ease-in-out')),
]);

@Component({
  selector: 'accordion-item',
  templateUrl: './accordion-item.component.html',
  styleUrls: ['./accordion-item.component.scss'],
  animations: [expandAnimation],
})
export class AccordionItemComponent implements OnInit {
  @Input() title?: string;
  isExpanded: boolean = true;

  constructor() {}

  ngOnInit(): void {}

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
