import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { cloneDeep } from '@apollo/client/utilities';
import { Chat, ChatMessage, ChatMessagesConnectionGQL } from '@app/graphql/chat';
import { ChatSidebarService } from '@app/services/chat-sidebar.service';
import { ChatService } from '@app/services/chat.service';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';

@Component({
  selector: 'chat-message-list',
  templateUrl: './chat-message-list.component.html',
  styleUrls: ['./chat-message-list.component.scss'],
})
export class ChatMessageListComponent extends GQLPaginationComponent implements OnInit, AfterViewInit {
  @Input() chat: Chat;
  @Output() byReplyTo = new EventEmitter<ChatMessage>();
  @ViewChild('anchor') anchor: ElementRef;

  override hasNextPage = true;
  messages: ChatMessage[] = [];
  first: boolean = true;

  constructor(
    public chatSidebarService: ChatSidebarService,
    private messagesGQL: ChatMessagesConnectionGQL,
    private chatSvc: ChatService
  ) {
    super();
  }

  ngOnInit() {
    this.loadMoreItems();
    this.chatSvc.socket.subscribeToChannel(this.chat.id, (data) => {
      if (data.event === 'message.new') {
        this.messages.unshift(data.payload);
      }
    });
  }

  ngAfterViewInit() {
    new IntersectionObserver(
      ([entry]) => {
        if (this.first) {
          this.first = false;
          return;
        }
        if (entry.isIntersecting) {
          this.loadMoreItems();
        }
      },
      { threshold: 0.1 }
    ).observe(this.anchor.nativeElement);
  }

  override loadMoreItems() {
    if (!this.hasNextPage) {
      return;
    }

    this.loadingNext = true;
    this.messagesGQL
      .fetch({
        chatId: this.chat.id,
        cursor: {
          id: this.cursorId,
          limit: 10,
        },
      })
      .subscribe(({ data }) => {
        this.messages.push(...cloneDeep(data.chatMessagesConnection.edges.map((v) => v.node)));
        this.cursorId = data.chatMessagesConnection.pageInfo.lastId;
        this.hasNextPage = data.chatMessagesConnection.pageInfo.hasNextPage;
        this.loadingNext = false;
      });
  }

  onTapReply(message: ChatMessage) {
    this.byReplyTo.emit(message);
  }
}
