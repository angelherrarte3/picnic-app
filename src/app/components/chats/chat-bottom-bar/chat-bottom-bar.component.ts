import { animate, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChatMessage } from '@app/graphql/chat';

@Component({
  selector: 'chat-bottom-bar',
  templateUrl: './chat-bottom-bar.component.html',
  styleUrls: ['./chat-bottom-bar.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ height: 0, opacity: 0 }),
        animate('150ms ease-out', style({ height: '*', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ height: '*', opacity: 1 }),
        animate('150ms ease-in', style({ height: 0, opacity: 0 })),
      ]),
    ]),
  ],
})
export class ChatBottomBarComponent implements OnInit {
  @Input() replyTo?: ChatMessage;
  @Input() files: File[] = [];

  @Output() byCancelReply = new EventEmitter<void>();
  @Output() byDeleteAttachment = new EventEmitter<number>();
  @Output() bySendMessage = new EventEmitter<string>();
  @Output() byUploadAttachment = new EventEmitter<void>();

  chatMessage: string = '';

  constructor() {}

  ngOnInit(): void {}

  onTapCancelReply = () => this.byCancelReply.emit();

  onTapDeleteAttachment(index: number) {
    this.byDeleteAttachment.emit(index);
  }

  onTapSendMessage() {
    this.bySendMessage.emit(this.chatMessage);
    this.chatMessage = '';
  }

  onTapUploadAttachment = () => this.byUploadAttachment.emit();
}
