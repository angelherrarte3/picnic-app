import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'picnic-button',
  templateUrl: './picnic-button.component.html',
  styleUrls: ['./picnic-button.component.scss'],
})
export class PicnicButtonComponent implements OnInit {
  @Input() label: string = '';
  @Input() color: string = '#007BFF';
  @Input() borderColor: string = '';
  @Input() textColor: string = '';
  @Input() textSize: string = '';
  @Input() textWeight?: number;
  @Input() radius: string = '';
  @Input() padding: string = '';
  @Input() height?: string = '';
  @Input() width?: string = '';
  @Input() border?: string = '';
  @Input() disabled: boolean = false;
  @Input() backgroundTransition: boolean = true;
  @Input() btnId = '';
  @Output() byClick = new EventEmitter<void>();

  style = {};

  ngOnInit(): void {
    this.style = this.getStyle;
  }

  get getStyle() {
    return (this.style = {
      'background-color': this.color,
      color: this.textColor,
      'border-radius': this.radius,
      'border-color': this.borderColor,
      padding: this.padding,
      width: this.width,
      height: this.height,
      border: this.border,
      'pointer-events': this.disabled ? 'none' : 'auto',
      transition: this.backgroundTransition ? 'background-color 0.2s' : 'none',
    });
  }

  handleClick = () => this.byClick.emit();
}
