import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicnicButtonComponent } from './picnic-button.component';

describe('PicnicButtonComponent', () => {
  let component: PicnicButtonComponent;
  let fixture: ComponentFixture<PicnicButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PicnicButtonComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PicnicButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
