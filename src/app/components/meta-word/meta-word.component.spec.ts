import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetaWordComponent } from './meta-word.component';

describe('MetaWordComponent', () => {
  let component: MetaWordComponent;
  let fixture: ComponentFixture<MetaWordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaWordComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaWordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
