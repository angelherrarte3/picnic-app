import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'meta-word[value]',
  templateUrl: './meta-word.component.html',
  styleUrls: ['./meta-word.component.scss'],
})
export class MetaWordComponent {
  @Input() value: string;
  @Output() byClick = new EventEmitter<void>();

  constructor() {}

  onTap() {
    this.byClick.emit();
  }
}
