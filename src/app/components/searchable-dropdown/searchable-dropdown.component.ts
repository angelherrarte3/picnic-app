import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CircleEdge, JoinCirclesGQL, LeaveCirclesGQL } from '@app/graphql/circle';
import { PublicProfileEdge } from '@app/graphql/profile';
import { CredentialsService } from '@app/auth';
import { Router } from '@angular/router';
import { PostEdge } from '@app/graphql/content';
import { animate, style, transition, trigger } from '@angular/animations';
import { App, AppEdge } from '@app/graphql/app';
import { environment } from '@env/environment';

import { TmpStateService } from '@app/services/tmp-state.service';
import { GlobalModalService } from '@app/services/global-modal.service';

enum SearchTab {
  ALL = 'ALL',
  APPS = 'APPS',
  CIRCLES = 'CIRCLES',
  USERS = 'USERS',
  POSTS = 'POSTS',
}

@Component({
  selector: 'searchable-dropdown',
  templateUrl: './searchable-dropdown.component.html',
  styleUrls: ['./searchable-dropdown.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ height: 0, opacity: 0 }),
        animate('170ms ease-out', style({ height: '*', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ height: '*', opacity: 1 }),
        animate('150ms ease-in', style({ height: 0, opacity: 0 })),
      ]),
    ]),
  ],
})
export class SearchableDropdownComponent implements OnInit {
  @Output() bySearch = new EventEmitter<string>();
  @Output() byClickCircleAction = new EventEmitter<CircleEdge>();
  @Input() inputHeight?: string;
  @Input() iconMode: boolean = false;
  @Input() dropdownHeight?: string;
  @Input() dropdownWidth?: string;
  @Input() loading: boolean = false;
  @Input() searchResults: {
    apps: AppEdge[];
    circles: CircleEdge[];
    users: PublicProfileEdge[];
    posts: PostEdge[];
  } = { apps: [], circles: [], users: [], posts: [] };
  @Input() initialApps: AppEdge[] = [];

  showDropdown = false;
  searchText = '';
  selectedTab = SearchTab.ALL;
  version = environment.version;

  constructor(
    public credentialsService: CredentialsService,
    private router: Router,
    private joinCircles: JoinCirclesGQL,
    private leaveCircles: LeaveCirclesGQL,
    private modalService: GlobalModalService,
    private tmpState: TmpStateService
  ) {}

  ngOnInit(): void {}

  onSearch(): void {
    if (this.searchText.trim() === '') {
      this.searchResults = { apps: [], circles: [], users: [], posts: [] };
      return;
    }

    if (this.searchText.replace(/\s/g, '') === 'cmd:pc--version') {
      this.searchText = 'V :: ' + this.version;
      return;
    }

    // test fyp feature
    if (
      this.searchText.replace(/\s/g, '') === 'cmd:pctest--feature=fypon' ||
      this.searchText.replace(/\s/g, '') === 'cmd:pctestf=fypon'
    ) {
      this.tmpState.newFyp = true;
      this.searchText = 'DONE';
      this.router.navigate(['/d']);
      setTimeout(() => {
        this.router.navigate(['/']);
      }, 1000);
      return;
    }

    // test fyp feature
    if (
      this.searchText.replace(/\s/g, '') === 'cmd:pctest--feature=fypoff' ||
      this.searchText.replace(/\s/g, '') === 'cmd:pctestf=fypoff'
    ) {
      this.tmpState.newFyp = false;
      this.searchText = 'DONE';
      this.router.navigate(['/d']);
      setTimeout(() => {
        this.router.navigate(['/']);
      }, 1000);
      return;
    }

    this.showDropdown = true;
    this.bySearch.emit(this.searchText);
  }

  onClear(): void {
    this.searchResults = { apps: [], circles: [], users: [], posts: [] };
    this.searchText = '';
  }

  hideDropdown = () => (this.showDropdown = false);

  handleCircleAction(circle: CircleEdge) {
    if (circle.node.iJoined) {
      this.leaveCircles
        .mutate({
          circlesIds: circle.node.id,
        })
        .subscribe(({ data }) => {
          if (data?.leaveCircles.success) {
            circle.node.iJoined = false;
          }
        });
    } else {
      this.joinCircles
        .mutate({
          circlesIds: circle.node.id,
        })
        .subscribe(({ data }) => {
          if (data?.joinCircles.success) {
            circle.node.iJoined = true;
          }
        });
    }
    this.byClickCircleAction.emit(circle);
  }

  handleTabChange(tab: string) {
    this.selectedTab = SearchTab[tab];
  }

  onTapPod(pod: App) {
    if (this.isGuestUser) {
      this.modalService.open('sign-up');
      return;
    }
    this.router.navigateByUrl(`pods/${pod.id}`);
  }

  get circleSearchResults(): CircleEdge[] {
    return this.searchResults.circles;
  }

  get userSearchResults(): PublicProfileEdge[] {
    return this.searchResults.users;
  }

  get postsSearchResults(): PostEdge[] {
    return this.searchResults.posts;
  }

  get appSearchResults(): AppEdge[] {
    return this.searchResults.apps;
  }

  get emptyResults(): boolean {
    return this.searchResults.circles.length + this.searchResults.users.length + this.searchResults.posts.length === 0;
  }

  get emptyCircleResults(): boolean {
    return this.circleSearchResults.length === 0;
  }

  get emptyUserResults(): boolean {
    return this.userSearchResults.length === 0;
  }

  get emptyPostResults(): boolean {
    return this.postsSearchResults.length === 0;
  }

  get emptyAppsResults(): boolean {
    return this.appSearchResults.length === 0;
  }

  get selectedTabValue(): string {
    return this.selectedTab.toString();
  }

  get emptySearchText(): boolean {
    return this.searchText.trim().length === 0;
  }

  get isGuestUser(): boolean {
    return !this.credentialsService.isAuthenticated();
  }

  get currentEmptyLabel(): string {
    switch (this.selectedTab) {
      case SearchTab.ALL:
        return 'results';
      case SearchTab.APPS:
        return 'apps';
      case SearchTab.CIRCLES:
        return 'circles';
      case SearchTab.USERS:
        return 'users';
      case SearchTab.POSTS:
        return 'posts';
    }
  }

  postMedia(post: PostEdge): string | undefined {
    switch (post.node.type) {
      case 'IMAGE':
        return post.node.imageContent?.url;
      case 'VIDEO':
        return post.node.videoContent?.thumbnailUrl;
      case 'LINK':
        return post.node.linkContent?.linkMetaData?.imageUrl;
      default:
        return '';
    }
  }

  leftPollImage(post: PostEdge): string | undefined {
    return post.node.pollContent?.answers[0]?.imageUrl;
  }

  rightPollImage(post: PostEdge): string | undefined {
    return post.node.pollContent?.answers[1]?.imageUrl;
  }

  navigateToCircle(circle: CircleEdge) {
    this.showDropdown = false;
    this.onClear();
    this.router.navigate([`/c/${circle.node.urlName ? circle.node.urlName : circle.node.name}`]);
  }

  navigateToProfile(profile: PublicProfileEdge) {
    this.showDropdown = false;
    this.onClear();
    this.router.navigate([`/u/${profile.node.username}`]);
  }

  navigateToPost(post: PostEdge) {
    this.showDropdown = false;
    this.onClear();
    this.router.navigateByUrl(`p/${post.node.shortId}`);
  }
}
