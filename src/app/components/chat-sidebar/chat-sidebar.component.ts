import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { ChatSidebarService } from '@app/services/chat-sidebar.service';
import {
  Chat,
  ChatExcerpt,
  ChatExcerptEdge,
  ChatFeedConnectionGQL,
  ChatsConnectionGQL,
  CreateSingleChatGQL,
  GetChatByIdGQL,
} from '@app/graphql/chat';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';
import { debounceTime, map, Subject, switchMap } from 'rxjs';
import { PublicProfile, PublicProfileEdge, UserConnectionGQL } from '@app/graphql/profile';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { isPlatformServer } from '@angular/common';
import { CreateChatModalComponent } from '../create-chat-modal/create-chat-modal.component';
import { GlobalModalService } from '@app/services/global-modal.service';

enum ChatSidebarTab {
  Dms = 'DMS',
  Circles = 'CIRCLES',
  ChatFeed = 'CHAT_FEED',
}

@Component({
  selector: 'chat-sidebar',
  templateUrl: './chat-sidebar.component.html',
  styleUrls: ['./chat-sidebar.component.scss'],
  animations: [
    trigger('slideInOut', [
      state(
        'in',
        style({
          transform: 'translate3d(0, 0, 0)',
        })
      ),
      state(
        'out',
        style({
          transform: 'translate3d(100%, 0, 0)',
        })
      ),
      transition('in => out', animate('300ms ease-in-out')),
      transition('out => in', animate('300ms ease-in-out')),
    ]),
  ],
})
export class ChatSidebarComponent extends GQLPaginationComponent implements OnInit {
  chatFeed: ChatExcerptEdge[] = [];
  selectedTab: ChatSidebarTab = ChatSidebarTab.ChatFeed;

  searchingDmsDropdown: boolean = false;
  searchDmsUsers: PublicProfileEdge[] = [];
  private readonly searchDmsSubject = new Subject<string>();

  constructor(
    public chatSidebarService: ChatSidebarService,
    private chatsConnectionGQL: ChatsConnectionGQL,
    private bubbleChatState: BubbleChatStateService,
    private userConnection: UserConnectionGQL,
    private createSingleChatGQL: CreateSingleChatGQL,
    private chatFeedGQL: ChatFeedConnectionGQL,
    private getChatByIdGQL: GetChatByIdGQL,
    private modalService: GlobalModalService,
    @Inject(PLATFORM_ID) private platformId: string
  ) {
    super();
  }

  ngOnInit() {
    if (isPlatformServer(this.platformId)) {
      return;
    }

    this.searchDmsSubject
      .pipe(
        debounceTime(300),
        switchMap((searchQuery) => {
          return this.userConnection
            .fetch({
              searchQuery: searchQuery,
              cursor: {
                id: '',
                limit: 20,
                dir: 'forward',
              },
            })
            .pipe(map(({ data }) => data.usersConnection.edges));
        })
      )
      .subscribe((users) => {
        this.searchDmsUsers = users;
      });

    this.chatSidebarService.loadChats();
    this.loadMoreItems();
  }

  circleImage(c: Chat) {
    return c.circle?.imageFile ? c.circle?.imageFile : c.circle?.image;
  }
  circleEmoji(c: Chat) {
    return !c.circle?.imageFile;
  }

  onOpen(c: Chat) {
    this.chatSidebarService.hide();
    this.bubbleChatState.addModalChat(c);
  }

  onDmsSearch(v: string) {
    if (!v) {
      this.searchingDmsDropdown = false;
      return;
    }
    this.searchingDmsDropdown = true;
    this.searchDmsSubject.next(v);
  }

  onCreateDm(u: PublicProfile) {
    this.createSingleChatGQL
      .mutate({
        userIds: [u.id],
      })
      .subscribe(({ data }) => {
        this.bubbleChatState.addModalChat(data!.createSingleChat);
      });
    this.searchingDmsDropdown = false;
    this.chatSidebarService.hide();
  }

  onTapStartChat() {
    this.modalService.open('create-chat');
  }

  override loadMoreItems() {
    if (this.selectedTab !== ChatSidebarTab.ChatFeed) return;

    this.chatFeedGQL
      .fetch({
        cursor: {
          id: this.cursorId,
          limit: 10,
          dir: 'forward',
        },
      })
      .subscribe(({ data }) => {
        this.chatFeed.push(...data.chatFeedConnection.edges);
        this.cursorId = data.chatFeedConnection.pageInfo.lastId;
        this.hasNextPage = data.chatFeedConnection.pageInfo.hasNextPage;
      });
  }

  onOpenChatFeed(f: ChatExcerpt) {
    this.getChatByIdGQL
      .fetch({
        id: f.id,
      })
      .subscribe(({ data }) => {
        this.bubbleChatState.addModalChat(data.chat);
        this.searchingDmsDropdown = false;
        this.chatSidebarService.hide();
      });
  }

  onSelectTab(tab: string) {
    const tabEnum = Object.values(ChatSidebarTab).find((v) => v === tab);
    this.selectedTab = tabEnum!;
  }
}
