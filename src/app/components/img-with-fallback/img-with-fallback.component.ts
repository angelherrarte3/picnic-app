import { AfterContentInit, Component, ContentChild, Directive, ElementRef } from '@angular/core';
import { first, fromEvent } from 'rxjs';

@Directive({ selector: '[imgWithFallback]' })
export class ImageWithFallbackDirective {
  constructor(public el: ElementRef) {}
}

@Component({
  selector: 'img-with-fallback',
  templateUrl: './img-with-fallback.component.html',
  styleUrls: ['./img-with-fallback.component.scss'],
})
export class ImgWithFallbackComponent implements AfterContentInit {
  @ContentChild(ImageWithFallbackDirective) image!: ImageWithFallbackDirective;

  error = false;

  ngAfterContentInit() {
    fromEvent(this.image.el.nativeElement, 'error')
      .pipe(first())
      .subscribe(() => (this.error = true));
    fromEvent(this.image.el.nativeElement, 'load')
      .pipe(first())
      .subscribe(() => (this.error = false));
  }
}
