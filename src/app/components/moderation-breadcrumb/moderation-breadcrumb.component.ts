import { Component, Input, OnInit } from '@angular/core';
import { Circle } from '@app/graphql/circle';
import { SettingItem } from '../settings-list/settings-list.component';
import { Router } from '@angular/router';

@Component({
  selector: 'moderation-breadcrumb[circle][activePath]',
  templateUrl: './moderation-breadcrumb.component.html',
  styleUrls: ['./moderation-breadcrumb.component.scss'],
})
export class ModerationBreadcrumbComponent implements OnInit {
  @Input() circle?: Circle;
  @Input() activePath?: SettingItem;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  goToCircle() {
    this.router.navigateByUrl(`/circle/${this.circle?.id}`);
  }
}
