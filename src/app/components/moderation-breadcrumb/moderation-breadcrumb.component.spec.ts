import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModerationBreadcrumbComponent } from './moderation-breadcrumb.component';

describe('ModerationBreadcrumbComponent', () => {
  let component: ModerationBreadcrumbComponent;
  let fixture: ComponentFixture<ModerationBreadcrumbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModerationBreadcrumbComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ModerationBreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
