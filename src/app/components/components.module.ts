import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostGridComponent } from './post-grid/post-grid.component';
import { CirclesGridComponent } from './circles-grid/circles-grid.component';
import { CollectionsGridComponent } from './collections-grid/collections-grid.component';
import { PicnicBannerComponent } from './picnic-banner/picnic-banner.component';
import { ProfileNavbarComponent } from './profile-navbar/profile-navbar.component';
import { SeedsCounterComponent } from './seeds-counter/seeds-counter.component';
import { LoaderComponent } from './loader/loader.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { TabNavbarComponent } from './tab-navbar/tab-navbar.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { PicnicButtonComponent } from './picnic-button/picnic-button.component';
import { CirclesNavbarComponent } from './circles-navbar/circles-navbar.component';
import { FeedNavbarComponent } from './feed-navbar/feed-navbar.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { PicnicIconButtonComponent } from './picnic-icon-button/picnic-icon-button.component';
import { RouterModule } from '@angular/router';
import { CircleTagComponent } from './circle-tag/circle-tag.component';
import { PicnicIconComponent } from './picnic-icon/picnic-icon.component';
import { PicnicAvatarComponent } from './picnic-avatar/picnic-avatar.component';
import { PostWrapperComponent } from './post-wrapper/post-wrapper.component';
import { PipesModule } from '@app/pipes/pipes.module';
import { ViewsCountComponent } from './views-count/views-count.component';
import { MessageListComponent } from './message-list/message-list.component';
import { PostCreatorInfoComponent } from './post-creator-info/post-creator-info.component';
import { PostActionsComponent } from './post-actions/post-actions.component';
import { PostHorizontalLayoutComponent } from './post-wrapper/post-horizontal-layout/post-horizontal-layout.component';
import { PostVerticalLayoutComponent } from './post-wrapper/post-vertical-layout/post-vertical-layout.component';
import { PostContentComponent } from './post-wrapper/post-content/post-content.component';
import { ImageContentComponent } from './post-wrapper/post-content/image-content/image-content.component';
import { VideoContentComponent } from './post-wrapper/post-content/video-content/video-content.component';
import { TextContentComponent } from './post-wrapper/post-content/text-content/text-content.component';
import { LinkContentComponent } from './post-wrapper/post-content/link-content/link-content.component';
import { PollContentComponent } from './post-wrapper/post-content/poll-content/poll-content.component';
import { PollVoteBarComponent } from './post-wrapper/post-content/poll-content/poll-vote-bar/poll-vote-bar.component';
import { PicnicLogoComponent } from './picnic-logo/picnic-logo.component';
import { CircleShadowNameComponent } from './circle-shadow-name/circle-shadow-name.component';
import { CircleStatsComponent } from './circle-stats/circle-stats.component';
import { PageNavbarItemComponent } from './page-navbar-item/page-navbar-item.component';
import { PicnicTextButtonComponent } from './picnic-text-button/picnic-text-button.component';
import { EmojiCommentReactionsComponent } from './post-comment-reactions/emoji-comment-reactions.component';
import { PostCommentComponent } from './post-comment/post-comment.component';
import { NgbDropdownModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModeratorItemComponent } from './moderator-item/moderator-item.component';
import { TextInputComponent } from './text-input/text-input.component';
import { SimpleDropdownComponent } from './simple-dropdown/simple-dropdown.component';
import { DirectivesModule } from '@app/directives/directives.module';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { TextInputDropdownComponent } from './text-input-dropdown/text-input-dropdown.component';
import { ChipListComponent } from './chip-list/chip-list.component';
import { PicnicOnboardingDialogComponent } from './picnic-onboarding-dialog/picnic-onboarding-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbNavigationComponent } from './breadcrumb-navigation/breadcrumb-navigation.component';
import { GradientTextComponent } from './gradient-text/gradient-text.component';
import { ProfileStatsComponent } from './profile-stats/profile-stats.component';
import { UserListItemComponent } from './user-list-item/user-list-item.component';
import { NotificationListItemComponent } from './notification-list-item/notification-list-item.component';
import { SearchableDropdownComponent } from './searchable-dropdown/searchable-dropdown.component';
import { CollectionCardComponent } from './collection-card/collection-card.component';
import { CreatePostNavbarComponent } from './create-post-navbar/create-post-navbar.component';
import { SignUpModalComponent } from './sign-up-modal/sign-up-modal.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CommentInputComponent } from './comment-input/comment-input.component';
import { PostShareEmbedComponent } from './post-wrapper/post-share-embed/post-share-embed.component';
import { SettingsListComponent } from './settings-list/settings-list.component';
import { ListItemComponent } from './list-item/list-item.component';
import { SwitchComponent } from './switch/switch.component';
import { BadgeTextComponent } from './badge-text/badge-text.component';
import { LeftSidebarComponent } from './left-sidebar/left-sidebar.component';
import { RightSidebarComponent } from './right-sidebar/right-sidebar.component';
import { ExpandableSidebarItemsComponent } from './expandable-sidebar-items/expandable-sidebar-items.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ShorthandPipeModule } from '@app/pipes/shorthand.pipe';
import { ModerationBreadcrumbComponent } from './moderation-breadcrumb/moderation-breadcrumb.component';
import { AccordionItemComponent } from './accordion-item/accordion-item.component';
import { PicnicModalComponent } from './picnic-modal/picnic-modal.component';
import { VideoPlayerComponent } from './post-wrapper/post-content/video-content/video-player/video-player.component';
import { CreateCircleModalComponent } from './create-circle-modal/create-circle-modal.component';
import { DiscoverCardComponent } from './discover-card/discover-card.component';
import { BubbleChatsComponent } from './bubble-chats/bubble-chats.component';
import { ModalChatsComponent } from './modal-chats/modal-chats.component';
import { ModalChatComponent } from './modal-chat/modal-chat.component';
import { ChatMessageComponent } from './chat-message/chat-message.component';
import { ChatSidebarComponent } from './chat-sidebar/chat-sidebar.component';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { LanguageItemSelectorComponent } from '@app/onboarding/components/language-item-selector/language-item-selector.component';
import { ImageWithFallbackDirective, ImgWithFallbackComponent } from './img-with-fallback/img-with-fallback.component';
import { MarkdownModule } from 'ngx-markdown';
import { ReportModalComponent } from './report-modal/report-modal.component';
import { MetaWordComponent } from './meta-word/meta-word.component';
import { InAppNotificationComponent } from './in-app-notification/in-app-notification.component';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';
import { FeedSidebarComponent } from './feed-sidebar/feed-sidebar.component';
import { MomentModule } from 'ngx-moment';
import { ChatBottomBarComponent } from './chats/chat-bottom-bar/chat-bottom-bar.component';
import { ChatMessageListComponent } from './chats/chat-message-list/chat-message-list.component';
import { CreateChatModalComponent } from './create-chat-modal/create-chat-modal.component';
import { BuildingFeatureModalComponent } from './building-feature-modal/building-feature-modal.component';
import { ModalWrapperComponent } from './modal-wrapper/modal-wrapper.component';

@NgModule({
  declarations: [
    PostGridComponent,
    CirclesGridComponent,
    CollectionsGridComponent,
    PicnicBannerComponent,
    ProfileNavbarComponent,
    SeedsCounterComponent,
    LoaderComponent,
    PageHeaderComponent,
    PageHeaderComponent,
    TabNavbarComponent,
    SearchInputComponent,
    PicnicButtonComponent,
    CirclesNavbarComponent,
    FeedNavbarComponent,
    PicnicIconButtonComponent,
    CircleTagComponent,
    PicnicIconComponent,
    PicnicAvatarComponent,
    PostWrapperComponent,
    ViewsCountComponent,
    MessageListComponent,
    PostCreatorInfoComponent,
    PostActionsComponent,
    PostHorizontalLayoutComponent,
    PostVerticalLayoutComponent,
    PostContentComponent,
    ImageContentComponent,
    VideoContentComponent,
    VideoPlayerComponent,
    TextContentComponent,
    LinkContentComponent,
    PollContentComponent,
    PollVoteBarComponent,
    PicnicLogoComponent,
    CircleShadowNameComponent,
    CircleStatsComponent,
    PageNavbarItemComponent,
    PostCommentComponent,
    PicnicTextButtonComponent,
    EmojiCommentReactionsComponent,
    ModeratorItemComponent,
    TextInputComponent,
    SimpleDropdownComponent,
    TextInputDropdownComponent,
    ChipListComponent,
    PicnicOnboardingDialogComponent,
    BreadcrumbNavigationComponent,
    GradientTextComponent,
    ProfileStatsComponent,
    UserListItemComponent,
    NotificationListItemComponent,
    SearchableDropdownComponent,
    CollectionCardComponent,
    CreatePostNavbarComponent,
    SignUpModalComponent,
    CommentInputComponent,
    PostShareEmbedComponent,
    SettingsListComponent,
    ListItemComponent,
    SwitchComponent,
    BadgeTextComponent,
    LeftSidebarComponent,
    RightSidebarComponent,
    ExpandableSidebarItemsComponent,
    ModerationBreadcrumbComponent,
    AccordionItemComponent,
    PicnicModalComponent,
    CreateCircleModalComponent,
    DiscoverCardComponent,
    BubbleChatsComponent,
    ModalChatsComponent,
    ModalChatComponent,
    ChatMessageComponent,
    ChatSidebarComponent,
    LanguageItemSelectorComponent,
    ImageWithFallbackDirective,
    ImgWithFallbackComponent,
    ReportModalComponent,
    MetaWordComponent,
    InAppNotificationComponent,
    LoadingSpinnerComponent,
    BottomBarComponent,
    FeedSidebarComponent,
    ChatBottomBarComponent,
    ChatMessageListComponent,
    CreateChatModalComponent,
    BuildingFeatureModalComponent,
    ModalWrapperComponent,
  ],
  imports: [
    CommonModule,
    AngularSvgIconModule,
    RouterModule,
    PipesModule,
    NgbDropdownModule,
    NgbModule,
    DirectivesModule,
    NgScrollbarModule,
    ReactiveFormsModule,
    FormsModule,
    InfiniteScrollModule,
    BrowserAnimationsModule,
    ShorthandPipeModule,
    MomentModule,
    PickerModule,
    MarkdownModule.forChild(),
  ],
  exports: [
    PostGridComponent,
    CirclesGridComponent,
    CollectionsGridComponent,
    PicnicBannerComponent,
    ProfileNavbarComponent,
    SeedsCounterComponent,
    LoaderComponent,
    PageHeaderComponent,
    TabNavbarComponent,
    SearchInputComponent,
    PicnicButtonComponent,
    CirclesNavbarComponent,
    FeedNavbarComponent,
    PicnicIconButtonComponent,
    CircleTagComponent,
    PicnicIconComponent,
    PicnicAvatarComponent,
    PostWrapperComponent,
    ViewsCountComponent,
    MessageListComponent,
    PostCreatorInfoComponent,
    PostActionsComponent,
    PostVerticalLayoutComponent,
    PostHorizontalLayoutComponent,
    PicnicLogoComponent,
    CircleShadowNameComponent,
    CircleStatsComponent,
    PageNavbarItemComponent,
    PostCommentComponent,
    PicnicTextButtonComponent,
    EmojiCommentReactionsComponent,
    ModeratorItemComponent,
    TextInputComponent,
    SimpleDropdownComponent,
    TextInputDropdownComponent,
    ChipListComponent,
    PicnicOnboardingDialogComponent,
    BreadcrumbNavigationComponent,
    GradientTextComponent,
    ProfileStatsComponent,
    UserListItemComponent,
    NotificationListItemComponent,
    SearchableDropdownComponent,
    CollectionCardComponent,
    CreatePostNavbarComponent,
    SignUpModalComponent,
    CommentInputComponent,
    SettingsListComponent,
    ListItemComponent,
    SwitchComponent,
    BadgeTextComponent,
    LeftSidebarComponent,
    RightSidebarComponent,
    ModerationBreadcrumbComponent,
    AccordionItemComponent,
    PicnicModalComponent,
    ExpandableSidebarItemsComponent,
    CreateCircleModalComponent,
    DiscoverCardComponent,
    BubbleChatsComponent,
    ModalChatsComponent,
    ModalChatComponent,
    ChatMessageComponent,
    ChatSidebarComponent,
    LanguageItemSelectorComponent,
    ImgWithFallbackComponent,
    ImageWithFallbackDirective,
    ReportModalComponent,
    MetaWordComponent,
    InAppNotificationComponent,
    LoadingSpinnerComponent,
    BottomBarComponent,
    FeedSidebarComponent,
    ChatBottomBarComponent,
    ChatMessageListComponent,
    CreateChatModalComponent,
    BuildingFeatureModalComponent,
    ModalWrapperComponent,
  ],
})
export class ComponentsModule {}
