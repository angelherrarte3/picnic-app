import { Component, Input, isDevMode, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService, CredentialsService } from '@app/auth';
import { CircleEdge, CirclesConnectionGQL } from '@app/graphql/circle';
import { PublicProfileEdge, UserConnectionGQL } from '@app/graphql/profile';
import { Subject, Subscription, debounceTime, distinctUntilChanged, switchMap, forkJoin, map, startWith } from 'rxjs';
import { cloneDeep } from '@apollo/client/utilities';
import { TmpStateService } from '@app/services/tmp-state.service';
import { SimpleDropdownItem } from '../simple-dropdown/simple-dropdown.component';
import { PostEdge } from '@app/graphql/content';
import { ThemeService } from '../../services/theme.service';
import { AppEdge, SearchAppsGQL } from '@app/graphql/app';
enum AvatarDropdownActions {
  OPEN_PROFILE = 'OPEN_PROFILE',
  SIGN_OUT = 'SIGN_OUT',
}

@Component({
  selector: 'tab-navbar',
  templateUrl: './tab-navbar.component.html',
  styleUrls: ['./tab-navbar.component.scss'],
})
export class TabNavbarComponent implements OnInit, OnDestroy {
  @Input() title: string = '';
  isAuthenticate: boolean = false;
  searchResults: {
    apps: AppEdge[];
    circles: CircleEdge[];
    users: PublicProfileEdge[];
    posts: PostEdge[];
  } = { apps: [], circles: [], users: [], posts: [] };
  searchInitialApps: AppEdge[] = [];
  avatarDropdownItems: SimpleDropdownItem<AvatarDropdownActions>[] = [
    {
      label: 'my profile',
      value: AvatarDropdownActions.OPEN_PROFILE,
    },
    {
      label: 'sign out',
      value: AvatarDropdownActions.SIGN_OUT,
    },
  ];
  loadingSearchResults: boolean = false;

  private readonly searchSubject = new Subject<string>();
  private searchSubscription?: Subscription;
  private searchLoadingSubscription?: Subscription;

  // tmp
  devMode: boolean | undefined;

  constructor(
    public credentialsService: CredentialsService,
    private router: Router,
    private authService: AuthenticationService,
    private circlesConnection: CirclesConnectionGQL,
    private userConnection: UserConnectionGQL,
    private searchAppsGQL: SearchAppsGQL,
    public state: TmpStateService,
    private themeService: ThemeService
  ) {}

  toggleTheme() {
    this.themeService.toggleTheme();
  }

  ngOnInit(): void {
    this.isAuthenticate = this.credentialsService.isAuthenticated();
    this.searchAppsGQL
      .fetch({
        data: {
          nameStartsWith: '',
          cursor: {
            id: '',
            limit: 10,
          },
        },
      })
      .subscribe(({ data }) => {
        this.searchInitialApps = data.searchApps.edges;
      });

    this.devMode = isDevMode();

    this.searchLoadingSubscription = this.searchSubject.pipe(distinctUntilChanged()).subscribe(() => {
      this.loadingSearchResults = true;
    });
    this.searchSubscription = this.searchSubject
      .pipe(
        debounceTime(300),
        switchMap((searchQuery) => {
          return forkJoin([
            this.circlesConnection
              .fetch({
                searchQuery: searchQuery,
                cursor: {
                  id: '',
                  limit: 5,
                  dir: 'forward',
                },
              })
              .pipe(map(({ data }) => cloneDeep(data.circlesConnection.edges))),
            this.userConnection
              .fetch({
                searchQuery: searchQuery,
                cursor: {
                  id: '',
                  limit: 20,
                  dir: 'forward',
                },
              })
              .pipe(map(({ data }) => data.usersConnection.edges)),
            this.searchAppsGQL
              .fetch({
                data: {
                  nameStartsWith: searchQuery,
                  cursor: {
                    id: '',
                    limit: 20,
                  },
                },
              })
              .pipe(map(({ data }) => data.searchApps.edges)),
            // TODO: Fetch posts from the server
          ]);
        })
      )
      .subscribe(([circles, users, apps]) => {
        this.searchResults = {
          apps: apps,
          circles: circles,
          users: users,
          posts: [],
          // TODO: Fetch posts from the server
        };
        this.loadingSearchResults = false;
      });
  }

  handleOnSearch = (searchQuery: string) => this.searchSubject.next(searchQuery.trim());

  navigateToLogin(): void {
    this.router.navigate(['/onboarding']);
  }

  signOut() {
    this.authService.logout().subscribe();
    window.location.href = '/';
  }

  handleOnAvatarDropdownSelect(item: SimpleDropdownItem<AvatarDropdownActions>) {
    switch (item.value) {
      case AvatarDropdownActions.OPEN_PROFILE:
        this.router.navigateByUrl('/u');
        break;

      case AvatarDropdownActions.SIGN_OUT:
        this.signOut();
        break;
    }
  }

  ngOnDestroy(): void {
    this.searchSubscription?.unsubscribe();
    this.searchLoadingSubscription?.unsubscribe();
  }
}
