import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicnicModalComponent } from './picnic-modal.component';

describe('PicnicModalComponent', () => {
  let component: PicnicModalComponent;
  let fixture: ComponentFixture<PicnicModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PicnicModalComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PicnicModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
