import { Component, ContentChild, ElementRef, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { PicnicModalService } from '@app/services/picnic-modal.service';
import { PlatformService } from '@app/services/platform.service';
import { ReportModalParams } from '../report-modal/report-modal.component';

export enum PicnicModalType {
  BuildingFeature,
  Custom,
}

export interface PicnicModalOptions {
  dismissClickOutside?: boolean;
  type?: PicnicModalType;
  reportParams?: ReportModalParams;
}

@Component({
  selector: 'picnic-modal',
  templateUrl: './picnic-modal.component.html',
  styleUrls: ['./picnic-modal.component.scss'],
})
export class PicnicModalComponent implements OnInit, OnDestroy {
  @ContentChild(TemplateRef) contentRef?: TemplateRef<any>;
  options: PicnicModalOptions = {
    dismissClickOutside: true,
    type: PicnicModalType.BuildingFeature,
  };
  isOpen = false;
  modalTypeEnum = PicnicModalType;

  constructor(
    private element: ElementRef,
    private picnicModal: PicnicModalService,
    private platformService: PlatformService
  ) {}

  ngOnInit() {
    this.picnicModal.registerModal(this);
  }

  onCloseOutside(event: MouseEvent) {
    if (this.options.dismissClickOutside) {
      if (event.target === this.element.nativeElement.children[0]) this.close();
    }
  }

  open(options?: PicnicModalOptions) {
    if (options) this.options = options;
    this.isOpen = true;
    document.body.appendChild(this.element.nativeElement);
  }

  close() {
    this.isOpen = false;
    this.element.nativeElement.remove();
  }

  openMobileApp() {
    const isAndroid = this.platformService.isAndroid();
    const isIOS = this.platformService.isIOS();

    if (isIOS) {
      const url = 'picnic://';
      window.open(url, '_blank');
      return;
    }

    if (isAndroid) {
      const url = 'intent://getpicnic.app/#Intent;scheme=https;package=com.ambertech.amber;end';
      window.open(url, '_blank');
      return;
    }

    const url = 'https://picnic.zone/download';
    window.open(url, '_blank');

    setTimeout(() => {
      this.close();
    }, 50);
  }

  ngOnDestroy() {
    this.picnicModal.unregisterModal();
  }
}
