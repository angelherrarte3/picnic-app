import { Component, Input, OnInit } from '@angular/core';
import { Circle } from '@app/graphql/circle';

@Component({
  selector: 'circle-stats[circle]',
  templateUrl: './circle-stats.component.html',
  styleUrls: ['./circle-stats.component.scss'],
})
export class CircleStatsComponent implements OnInit {
  @Input() circle?: Circle;

  constructor() {}

  ngOnInit(): void {}
}
