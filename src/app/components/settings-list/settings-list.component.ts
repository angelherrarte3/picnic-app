import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalModalService } from '@app/services/global-modal.service';

export interface SettingItem {
  label: string;
  iconName?: string;
  path: string;
  notificationCount?: number;
  buildingFeature?: boolean;
}

@Component({
  selector: 'settings-list',
  templateUrl: './settings-list.component.html',
  styleUrls: ['./settings-list.component.scss'],
})
export class SettingsListComponent implements OnInit {
  @Input() settings: SettingItem[] = [];
  @Input() otherItems: SettingItem[] = [];
  @Input() initialSelection?: SettingItem;
  @Input() showArrow: boolean = false;
  @Input() firstTitle?: string;
  @Input() secondTitle?: string;
  @Input() showActiveItem: boolean = true;
  @Output() byItemSelected = new EventEmitter<SettingItem>();
  selectedItem?: SettingItem;

  constructor(private modalService: GlobalModalService, private router: Router) {}

  ngOnInit(): void {
    if (this.initialSelection) this.selectedItem = this.initialSelection;
    else this.selectedItem = this.settings[0];
  }

  handleItemSelected(item: SettingItem) {
    if (item.buildingFeature) {
      this.modalService.open('building-feature');
      return;
    }

    this.selectedItem = item;
    this.byItemSelected.emit(item);
  }
}
