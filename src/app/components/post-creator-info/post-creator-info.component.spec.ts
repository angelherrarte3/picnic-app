import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCreatorInfoComponent } from './post-creator-info.component';

describe('PostCreatorInfoComponent', () => {
  let component: PostCreatorInfoComponent;
  let fixture: ComponentFixture<PostCreatorInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostCreatorInfoComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PostCreatorInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
