import { Component, Input, OnInit } from '@angular/core';
import { Post } from '@app/graphql/content';
import { Router } from '@angular/router';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';

@Component({
  selector: 'post-creator-info[post]',
  templateUrl: './post-creator-info.component.html',
  styleUrls: ['./post-creator-info.component.scss'],
})
export class PostCreatorInfoComponent implements OnInit {
  @Input() post?: Post;

  constructor(private router: Router, private analytics: AnalyticsService) {}

  ngOnInit(): void {}

  navigateToUser() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.postUserTap));
    this.router.navigate(['/u/' + this.post?.author?.username, { redirectForYou: 'false' }]);
  }
}
