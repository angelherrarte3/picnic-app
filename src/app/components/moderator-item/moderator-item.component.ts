import { Component, Input, OnInit } from '@angular/core';
import { ElectionParticipantEdge } from '@app/graphql/circle';

@Component({
  selector: 'moderator-item[moderator]',
  templateUrl: './moderator-item.component.html',
  styleUrls: ['./moderator-item.component.scss'],
})
export class ModeratorItemComponent implements OnInit {
  @Input() moderator?: ElectionParticipantEdge;

  constructor() {}

  ngOnInit(): void {}
}
