import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import {
  Post,
  PostInfoWrapper,
  PostReaction,
  ReactToPostGQL,
  SavePostStatusGQL,
  SharePostGQL,
  UnreactToPostGQL,
} from '@app/graphql/content';
import { CredentialsService } from '@app/auth';

import { ClipboardService } from 'ngx-clipboard';
import { TooltipService } from '@app/services/tooltip.service';
import { SimpleDropdownItem } from '../simple-dropdown/simple-dropdown.component';
import { TooltipConfig } from '@app/directives/tooltip/tooltip.directive';
import { environment } from '@env/environment';
import { Router } from '@angular/router';
import { PostReactService } from '@app/services/post-react.service';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';
import { ReportModalComponent } from '../report-modal/report-modal.component';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { GlobalModalService } from '@app/services/global-modal.service';

enum ShareDropdownActions {
  SHARE_LINK = 'SHARE_LINK',
  EMBED_CODE = 'EMBED_CODE',
}

@UntilDestroy()
@Component({
  selector: 'post-actions[post]',
  templateUrl: './post-actions.component.html',
  styleUrls: ['./post-actions.component.scss'],
})
export class PostActionsComponent implements OnInit {
  @Input() mobileLayout: boolean = false;
  @ViewChild('shareButtonRef') shareButtonRef!: ElementRef;

  shareDropdownItems: SimpleDropdownItem<ShareDropdownActions>[] = [
    {
      label: 'copy share link',
      value: ShareDropdownActions.SHARE_LINK,
    },
    {
      label: 'copy embed code',
      value: ShareDropdownActions.EMBED_CODE,
    },
  ];

  showExpand: boolean = true;
  saved: boolean = false;
  tooltipConfig: TooltipConfig = {
    text: '',
    customPosition: { top: 4, left: -92 },
    autoHideTimeout: 1700,
    textColor: '#fff',
  };

  postInfoWrapper?: PostInfoWrapper;

  private get _post(): Post | undefined {
    return this.postInfoWrapper?.post;
  }

  liked: boolean = false;
  disliked: boolean = false;
  likesCount: number = 0;
  savesCount: number = 0;
  sharesCount: number = 0;

  constructor(
    private creds: CredentialsService,
    private setReactPost: ReactToPostGQL,
    private unReactPost: UnreactToPostGQL,
    private modalService: GlobalModalService,
    private clipboard: ClipboardService,
    private tooltip: TooltipService,
    private router: Router,
    private postReactService: PostReactService,
    private analytics: AnalyticsService,
    private savePostStatusGQL: SavePostStatusGQL,
    private sharePostGQL: SharePostGQL
  ) {}

  ngOnInit(): void {
    if (this.router.url.includes('preview')) {
      this.showExpand = false;
    }
    this.postReactService.likesEmmiter.pipe(untilDestroyed(this)).subscribe((postId) => {
      if (this._post?.id === postId) this.parentLike();
    });
  }

  @Input() set post(v: Post | undefined) {
    if (v) {
      this.postInfoWrapper = new PostInfoWrapper(v);
      this.updateValues();
    } else {
      this.postInfoWrapper = undefined;
    }
  }

  get isGuestUser() {
    return !this.creds.isAuthenticated();
  }

  parentLike() {
    if (this.isGuestUser || this.liked) return;

    if (!this.postInfoWrapper) {
      return;
    }

    this.setReaction(PostReaction.Like);

    if (!this.postInfoWrapper!.iLiked) {
      this.setReaction(PostReaction.Like);
    }
  }

  like() {
    if (this.isGuestUser) {
      this.modalService.open('sign-up');
      return;
    }

    if (!this.postInfoWrapper) {
      return;
    }

    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.postLikeButton, !this.postInfoWrapper!.iLiked));

    if (!this.postInfoWrapper!.iLiked) {
      this.setReaction(PostReaction.Like);
    } else {
      this.unreact();
    }
  }

  dislike() {
    if (this.isGuestUser) {
      this.modalService.open('sign-up');
      return;
    }

    if (!this.postInfoWrapper) {
      return;
    }

    this.analytics.logEvent(
      new AnalyticsEventTap(AnalyticsTapTarget.postDislikeButton, !this.postInfoWrapper!.iDisliked)
    );

    if (!this.postInfoWrapper!.iDisliked) {
      this.setReaction(PostReaction.Dislike);
    } else {
      this.unreact();
    }
  }

  private setReaction(reaction: PostReaction) {
    if (!this.postInfoWrapper) {
      return;
    }

    this.setReactPost
      .mutate({
        postId: this._post!.id,
        reaction: reaction,
      })
      .subscribe(({ data }) => {
        if (data?.reactToPost.success) {
          this.postInfoWrapper!.reaction = reaction;
          this.updateValues();
        }
      });
  }

  private unreact() {
    if (!this.postInfoWrapper) {
      return;
    }
    this.unReactPost
      .mutate({
        postId: this._post!.id,
      })
      .subscribe(({ data }) => {
        if (data?.unreactToPost.success) {
          this.postInfoWrapper!.reaction = PostReaction.None;
          this.updateValues();
        }
      });
  }

  private updateValues() {
    if (!this.postInfoWrapper) {
      return;
    }
    this.liked = this.postInfoWrapper.iLiked;
    this.disliked = this.postInfoWrapper.iDisliked;
    this.likesCount = this.postInfoWrapper.likesCount;
    this.saved = this.postInfoWrapper.iSaved;
    this.savesCount = this.postInfoWrapper.savesCount;
    this.sharesCount = this.postInfoWrapper.sharesCount;
  }

  share() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.postShareButton));
    const previewUrl = `${window.location.host}/p/${this._post?.shortId}`;
    this.clipboard.copy(previewUrl);
    if (this.shareButtonRef) {
      this.tooltipConfig.text = 'link copied';
      this.tooltip.showTooltip(this.shareButtonRef, this.tooltipConfig);
      this.sharePostGQL.mutate({ postId: this._post!.id }).subscribe((result) => {
        if (result.data?.sharePost.success) {
          this.postInfoWrapper!.sharesCount = this.sharesCount + 1;
          this.updateValues;
        }
      });
    }
  }

  embedCode() {
    const embedFrameCode = `<iframe width="560" height="315" src="${environment.host}/embed/${this._post?.id}" title="picnic video" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>`;
    this.clipboard.copy(embedFrameCode);

    if (this.shareButtonRef) {
      this.tooltipConfig.text = 'code copied';
      this.tooltipConfig.customPosition = { top: 4, left: -100 };
      this.tooltip.showTooltip(this.shareButtonRef, this.tooltipConfig);
      this.sharePostGQL.mutate({ postId: this._post!.id }).subscribe((result) => {
        if (result.data?.sharePost.success) {
          this.postInfoWrapper!.sharesCount = this.sharesCount + 1;
          this.updateValues;
        }
      });
    }
  }

  save() {
    if (this.isGuestUser) {
      this.modalService.open('sign-up');
      return;
    }
  }

  onTapReport() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.postReportLongTap));
    if (this.isGuestUser) {
      this.modalService.open('sign-up');
      return;
    }
    if (!this.postInfoWrapper) return;

    this.modalService?.open('report-content', {
      dismissClickOutside: true,
      reportParams: {
        reportType: 'POST',
        circleId: this._post!.circle!.id,
        anyId: this._post!.id,
        contentAuthorId: this._post!.author!.id,
      },
    });
  }

  bookmark() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.postBookmarkButton));

    if (this.isGuestUser) {
      this.modalService.open('sign-up');
      return;
    }
    if (!this.postInfoWrapper) return;

    this.savePostStatusGQL.mutate({ postId: this._post!.id, saveStatus: !this.saved }).subscribe((result) => {
      if (result.data?.savePostStatus.success) {
        this.postInfoWrapper!.savesCount = this.saved ? this.savesCount - 1 : this.savesCount + 1;
        this.postInfoWrapper!.iSaved = !this.postInfoWrapper!.iSaved;
        this.updateValues();
      }
    });
  }

  handleOnAvatarDropdownSelect(item: SimpleDropdownItem<ShareDropdownActions>) {
    switch (item.value) {
      case ShareDropdownActions.SHARE_LINK:
        this.share();
        break;

      case ShareDropdownActions.EMBED_CODE:
        this.embedCode();
        break;
    }
  }

  goToPreview = () => this.router.navigateByUrl(`/p/${this._post?.shortId}`);

  onClickComments() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.postOpenChatButton));
    this.goToPreview();
  }
}
