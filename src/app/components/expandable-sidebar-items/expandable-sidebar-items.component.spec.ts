import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandableSidebarItemsComponent } from './expandable-sidebar-items.component';

describe('ExpandableSidebarItemsComponent', () => {
  let component: ExpandableSidebarItemsComponent;
  let fixture: ComponentFixture<ExpandableSidebarItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExpandableSidebarItemsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ExpandableSidebarItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
