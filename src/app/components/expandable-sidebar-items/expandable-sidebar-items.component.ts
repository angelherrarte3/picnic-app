import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Circle } from '@app/graphql/circle';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';

export interface ExpandableSidebarItem {
  label: string;
  route: string;
  value: string;
  data?: Circle;
}

@Component({
  selector: 'expandable-sidebar-items[label][items]',
  templateUrl: './expandable-sidebar-items.component.html',
  styleUrls: ['./expandable-sidebar-items.component.scss'],
  animations: [
    trigger('dropdownAnimation', [
      state(
        'collapsed',
        style({
          height: '55px',
        })
      ),
      state(
        'expanded',
        style({
          height: '*',
        })
      ),
      transition('collapsed => expanded', animate('200ms ease-in')),
      transition('expanded => collapsed', animate('200ms ease-out')),
    ]),
  ],
})
export class ExpandableSidebarItemsComponent implements OnInit {
  @Input() label?: string;
  @Input() items: ExpandableSidebarItem[] = [];
  @Input() initialSelectedItem?: ExpandableSidebarItem;
  @Output() byLoadMoreItems = new EventEmitter();
  selectedItem?: ExpandableSidebarItem;
  expanded: boolean = true;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.selectedItem = this.initialSelectedItem;
  }

  expand = () => (this.expanded = true);

  minimize = () => (this.expanded = false);

  toggle = () => (this.expanded = !this.expanded);

  loadMoreItems = () => this.byLoadMoreItems.emit();
}
