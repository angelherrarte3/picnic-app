import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'views-count[count]',
  templateUrl: './views-count.component.html',
  styleUrls: ['./views-count.component.scss'],
})
export class ViewsCountComponent implements OnInit {
  @Input() count: number = 0;
  @Input() verticalLayout: boolean = false;
  @Input() color?: string;
  @Input() iconSize?: number;
  @Input() textSize?: number;
  @Input() opacity: number = 1;

  constructor() {}

  ngOnInit(): void {}
}
