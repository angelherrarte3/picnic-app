import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNavbarItemComponent } from './page-navbar-item.component';

describe('PageNavbarItemComponent', () => {
  let component: PageNavbarItemComponent;
  let fixture: ComponentFixture<PageNavbarItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PageNavbarItemComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PageNavbarItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
