import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'page-navbar-item[path]',
  templateUrl: './page-navbar-item.component.html',
  styleUrls: ['./page-navbar-item.component.scss'],
})
export class PageNavbarItemComponent implements OnInit {
  @Input() label: string = '';
  @Input() labelWeight?: number;
  @Input() labelSize: string = '';
  @Input() padding?: string;
  @Input() radius?: string;
  @Input() iconName: string = '';
  @Input() path: string = '';

  constructor() {}

  ngOnInit(): void {}
}
