import { Component, isDevMode, OnInit, ViewChild } from '@angular/core';
import { ExpandableSidebarItem } from '../expandable-sidebar-items/expandable-sidebar-items.component';
import { FeedConnectionGQL, Feed } from '@app/graphql/feed';
import { ProfileStateService } from '@app/profile/profile-state.service';
import { CredentialsService } from '@app/auth';
import { CreateCircleModalComponent } from '../create-circle-modal/create-circle-modal.component';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import trendingCircles from '../../../cache/trending-circles.json';
import { GlobalModalService } from '@app/services/global-modal.service';

@UntilDestroy()
@Component({
  selector: 'left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.scss'],
})
export class LeftSidebarComponent extends GQLPaginationComponent implements OnInit {
  trendingCircleItems: ExpandableSidebarItem[] = [];
  forYouFeed?: Feed;

  devMode: boolean | undefined;

  constructor(
    private feedConnection: FeedConnectionGQL,
    private profileState: ProfileStateService,
    private creds: CredentialsService,
    private modalService: GlobalModalService
  ) {
    super();
  }

  ngOnInit(): void {
    this.initTrendingCircleItems();

    this.devMode = isDevMode();
  }

  initTrendingCircleItems() {
    if (!this.creds.isAuthenticated()) {
      this.trendingCircleItems = trendingCircles.map((e: any) => {
        return {
          label: e?.node?.name,
          route: `c/${e?.node?.circle?.urlName ? e.node.circle.urlName : e.node.name}`,
          value: e?.node?.id,
          data: e?.node.circle!,
        };
      });
      this.trendingCircleItems.sort((a, b) => {
        const aMembersCount = a?.data?.membersCount ?? 0;
        const bMembersCount = b?.data?.membersCount ?? 0;

        if (aMembersCount < bMembersCount) {
          return 1;
        } else if (aMembersCount > bMembersCount) {
          return -1;
        } else {
          return 0;
        }
      });
    } else {
      this.feedConnection
        .watch({
          cursor: {
            id: this.cursorId,
            limit: 11,
            dir: 'forward',
          },
        })
        .valueChanges.pipe(untilDestroyed(this))
        .subscribe(({ data }) => {
          this.forYouFeed = data.feedsConnection.edges[0]?.node;
          this.profileState.forYouFeedId.next(this.forYouFeed?.id);

          const trendingFeeds = [...data.feedsConnection.edges].slice(1);
          this.trendingCircleItems = trendingFeeds.map((e) => {
            return {
              label: e?.node?.name,
              route: `c/${e?.node?.circle?.urlName ? e.node.circle.urlName : e.node.name}`,
              value: e?.node?.id,
              data: e?.node.circle!,
            };
          });
          this.trendingCircleItems.sort((a, b) => {
            const aMembersCount = a?.data?.membersCount ?? 0;
            const bMembersCount = b?.data?.membersCount ?? 0;

            if (aMembersCount < bMembersCount) {
              return 1;
            } else if (aMembersCount > bMembersCount) {
              return -1;
            } else {
              return 0;
            }
          });

          this.cursorId = data.feedsConnection.pageInfo!.lastId;
          this.hasNextPage = data.feedsConnection.pageInfo!.hasNextPage;
        });
    }
  }

  get isGuestUser() {
    return !this.creds.isAuthenticated();
  }

  openCreateCircleModal() {
    this.modalService.open('create-circle', { dismissClickOutside: false });
  }

  override loadMoreItems(): void {
    this.feedConnection
      .watch({
        cursor: {
          id: this.cursorId,
          limit: 11,
          dir: 'forward',
        },
      })
      .valueChanges.subscribe(({ data }) => {
        const trendingFeeds = [...data.feedsConnection.edges];
        const mappedTrendingCircles = trendingFeeds.map((e) => {
          return {
            label: e?.node?.name,
            route: `c/${e?.node?.name}`,
            value: e?.node?.id,
            data: e?.node.circle,
          };
        });
        mappedTrendingCircles.sort((a, b) => {
          const aMembersCount = a?.data?.membersCount ?? 0;
          const bMembersCount = b?.data?.membersCount ?? 0;

          if (aMembersCount < bMembersCount) {
            return 1;
          } else if (aMembersCount > bMembersCount) {
            return -1;
          } else {
            return 0;
          }
        });

        this.trendingCircleItems.push(...mappedTrendingCircles);
        this.cursorId = data.feedsConnection.pageInfo!.lastId;
        this.hasNextPage = data.feedsConnection.pageInfo!.hasNextPage;
      });
  }
}
