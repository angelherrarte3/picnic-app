import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CredentialsService } from '@app/auth';

import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';
import { GlobalModalService } from '@app/services/global-modal.service';

@Component({
  selector: 'comment-input',
  templateUrl: './comment-input.component.html',
  styleUrls: ['./comment-input.component.scss'],
})
export class CommentInputComponent implements OnInit {
  @Output() byCommentCreated = new EventEmitter<string>();
  @Input() loadingComment: boolean = false;
  @Input() replyMode: boolean = false;
  replyText: string = '';

  constructor(
    private modalService: GlobalModalService,
    private creds: CredentialsService,
    private analytics: AnalyticsService
  ) {}

  ngOnInit(): void {}

  handleCommentCreated() {
    if (!this.replyText || this.isGuestUser) return;
    this.byCommentCreated.emit(this.replyText);
    this.replyText = '';
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.postCommentsSendCommentButton));
  }

  showModalForGuest() {
    if (!this.isGuestUser) return;
    this.modalService.open('sign-up');
  }

  get isGuestUser() {
    return !this.creds.isAuthenticated();
  }

  get disableCommentCreation(): boolean {
    return !this.replyText || this.isGuestUser;
  }
}
