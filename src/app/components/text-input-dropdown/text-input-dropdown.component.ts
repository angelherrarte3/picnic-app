import { Component, EventEmitter, Input, OnInit, Optional, Output, Self, ViewChild } from '@angular/core';
import { ControlContainer, ControlValueAccessor, FormGroupDirective, NgControl } from '@angular/forms';
import { NgScrollbar } from 'ngx-scrollbar';

export interface TextInputDropdownItem<Type> {
  label: string;
  value: Type;
  preffix: string;
}

export const NOOP_VALUE_ACCESSOR: ControlValueAccessor = {
  writeValue(): void {},
  registerOnChange(): void {},
  registerOnTouched(): void {},
};

export interface TextInputDropdownEmptyItem {
  emoji: string;
  message: string;
}

@Component({
  selector: 'text-input-dropdown[items]',
  templateUrl: './text-input-dropdown.component.html',
  styleUrls: ['./text-input-dropdown.component.scss'],
  viewProviders: [
    {
      provide: ControlContainer,
      useExisting: FormGroupDirective,
    },
  ],
})
export class TextInputDropdownComponent<Type> implements OnInit {
  @Input() items: TextInputDropdownItem<Type>[] = [];
  @Input() initialValue?: Type;

  @Input() inputHeight?: string;
  @Input() inputBgColor?: string;
  @Input() type?: string;
  @Input() color?: string;
  @Input() maxHeight?: number;
  @Input() customPreffix: boolean = false;

  @Input() hint: string = '';
  @Input() errorText?: string;

  @Input() formControlName?: string;

  @Input() searchHint?: string;
  @Input() searchable: boolean = false;
  @Input() emptyItemData: TextInputDropdownEmptyItem = { emoji: '🔍', message: 'no results found' };

  @Output() private bySelect = new EventEmitter<TextInputDropdownItem<Type>>();
  @Output() private byInput = new EventEmitter<string>();
  @Output() private bySearch = new EventEmitter<string>();
  @ViewChild(NgScrollbar, { static: true }) scrollbarRef?: NgScrollbar;

  selectedItem?: TextInputDropdownItem<Type>;
  isOpen: boolean = false;
  style = {};

  constructor(@Self() @Optional() public ngControl: NgControl) {
    if (this.ngControl) this.ngControl.valueAccessor = NOOP_VALUE_ACCESSOR;
  }

  ngOnInit(): void {
    if (this.initialValue) this.selectedItem = this.items.find((item) => item.value === this.initialValue);

    this.style = this.getStyle;
  }

  get getStyle() {
    return {
      height: this.inputHeight,
      background: this.inputBgColor,
      color: this.color,
    };
  }

  resetScroll = () => this.scrollbarRef?.scrollTo({ top: 0, duration: 0 }).then();

  hideDropdown() {
    this.isOpen = false;
    this.resetScroll();
  }

  toggleDropdown() {
    this.isOpen = !this.isOpen;
    this.resetScroll();
  }

  handleSelectItem(item: TextInputDropdownItem<Type>) {
    this.toggleDropdown();
    this.isOpen = false;

    if (this.selectedItem?.value === item.value) return;

    this.selectedItem = item;
    this.bySelect.emit(this.selectedItem);
  }

  handleInput = (event: any) => this.byInput.emit(event.target.value);

  handleSearch = (value: string) => this.bySearch.emit(value);
}
