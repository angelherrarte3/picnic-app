import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextInputDropdownComponent } from './text-input-dropdown.component';

describe('TextInputDropdownComponent', () => {
  let component: TextInputDropdownComponent;
  let fixture: ComponentFixture<TextInputDropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TextInputDropdownComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TextInputDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
