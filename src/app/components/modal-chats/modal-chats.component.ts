import { Component, OnInit } from '@angular/core';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';
import { ChatService } from '@app/services/chat.service';
import { CredentialsService } from '@app/auth';

@Component({
  selector: 'modal-chats',
  templateUrl: './modal-chats.component.html',
  styleUrls: ['./modal-chats.component.scss'],
})
export class ModalChatsComponent implements OnInit {
  activeModalChats$ = this.bubbleChatState.activeModalChats;

  constructor(
    private bubbleChatState: BubbleChatStateService,
    private chatSvc: ChatService,
    private credentials: CredentialsService
  ) {}

  ngOnInit(): void {
    if (!this.credentials.isAuthenticated()) {
      return;
    }
    this.chatSvc.open();
  }
}
