import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalChatsComponent } from './modal-chats.component';

describe('ModalChatsComponent', () => {
  let component: ModalChatsComponent;
  let fixture: ComponentFixture<ModalChatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModalChatsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ModalChatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
