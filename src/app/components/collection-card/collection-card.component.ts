import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Collection } from '@app/graphql/collections';
import { Post } from '@app/graphql/content';

@Component({
  selector: 'collection-card[collection]',
  templateUrl: './collection-card.component.html',
  styleUrls: ['./collection-card.component.scss'],
})
export class CollectionCardComponent implements OnInit {
  @Input() collection: Collection;
  @Output() byClickCollection = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  itemExist = (index: number) => index >= 0 && index < (this.collection.previewPosts?.length ?? 0);

  handleClick = () => this.byClickCollection.emit();

  itemValue(p: Post) {
    switch (p.type) {
      case 'IMAGE':
        return p.imageContent!.url;
      case 'VIDEO':
        return p.videoContent!.thumbnailUrl ?? null;
      default:
        return null;
    }
  }
}
