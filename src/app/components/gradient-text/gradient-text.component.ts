import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'gradient-text[text]',
  templateUrl: './gradient-text.component.html',
  styleUrls: ['./gradient-text.component.scss'],
})
export class GradientTextComponent implements OnInit {
  @Input() text: string = '';
  @Input() fontSize: string = '';
  @Input() formatUsername: boolean = false;

  constructor() {}

  ngOnInit(): void {}
}
