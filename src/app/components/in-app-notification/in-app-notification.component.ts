import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CredentialsService } from '@app/auth';
import { Centrifuge } from 'centrifuge';
import { environment } from '@env/environment';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';

export interface ToastInfo {
  message: string;
  header: string;
  action: string;
  subSourceId: string;
}

@Component({
  selector: 'in-app-notification',
  templateUrl: './in-app-notification.component.html',
  styleUrls: ['./in-app-notification.component.scss'],
})
export class InAppNotificationComponent implements OnInit {
  toasts: ToastInfo[] = [];

  constructor(private creds: CredentialsService, private bubbleChats: BubbleChatStateService) {}

  ngOnInit() {
    if (!this.creds.isAuthenticated()) return;

    const cli = new Centrifuge(environment.inAppUrl, {
      token: this.creds.credentials?.token,
    });
    cli.connect();
    cli.on('publication', (ctx: any) => {
      this.toasts.push({
        message: ctx.data.message,
        header: ctx.data.title,
        action: ctx.data.action,
        subSourceId: ctx.data.subSourceId,
      });
    });
  }

  remove(toast: ToastInfo) {
    this.toasts = this.toasts.filter((t) => t != toast);
  }

  onClick(toast: ToastInfo) {
    if (toast.action === 'message') {
      this.bubbleChats.createAndOpen(toast.subSourceId);
    }
  }
}
