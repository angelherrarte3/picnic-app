import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Notification } from '@app/graphql/notifications';
import { Router } from '@angular/router';
import { GetPostByIdGQL } from '@app/graphql/content';

@Component({
  selector: 'notification-list-item',
  templateUrl: './notification-list-item.component.html',
  styleUrls: ['./notification-list-item.component.scss'],
})
export class NotificationListItemComponent<Type1> {
  @Input() notification?: Notification;
  @Output() byClickFollow = new EventEmitter<Type1 | undefined>();
  @Output() byClickAvatar = new EventEmitter<Type1 | undefined>();

  @Input() followData?: Type1;
  @Input() profileData?: Type1;

  handleClickFollow = () => this.byClickFollow.emit(this.followData);

  handleClickAvatar = () => this.byClickAvatar.emit(this.profileData);

  get viewed() {
    return !!this.notification?.readAt;
  }

  constructor(private router: Router, private getPost: GetPostByIdGQL) {}

  get image() {
    switch (this.notification?.action) {
      case NotificationType.glitterbomb:
      case NotificationType.follow:
      case NotificationType.message:
      case NotificationType.commentReaction:
      case NotificationType.postComment:
      case NotificationType.userJoinedACircle:
      case NotificationType.seedsReceived:
      case NotificationType.postReaction:
        return this.notification?.fromInfo.avatar ?? '';
      case NotificationType.bannedFromCircle:
      case NotificationType.bannedFromApp:
      case NotificationType.postRemovedModerator:
      case NotificationType.postRemovedPicnic:
      case NotificationType.postReportedModerator:
      case NotificationType.postShared:
      case NotificationType.postSaved:
        return this.notification?.sourceInfo.avatar ?? '';
    }
    return null;
  }

  showPostThumbnail() {
    switch (this.notification?.action) {
      case NotificationType.commentReaction:
      case NotificationType.postReaction:
      case NotificationType.postComment:
        return true;
      default:
        return false;
    }
  }

  navigateToPost(id: string | undefined) {
    this.getPost.fetch({ postId: id }).subscribe(({ data }) => {
      this.router.navigateByUrl(`p/${data.getPost.shortId}`);
    });
  }

  showImage() {
    switch (this.notification?.action) {
      case NotificationType.glitterbomb:
      case NotificationType.follow:
      case NotificationType.message:
      case NotificationType.commentReaction:
      case NotificationType.userJoinedACircle:
      case NotificationType.postReaction:
      case NotificationType.postComment:
      case NotificationType.seedsReceived:
      case NotificationType.bannedFromApp:
      case NotificationType.bannedFromCircle:
        return true;
      default:
        return false;
    }
  }

  get title() {
    switch (this.notification?.action) {
      case NotificationType.glitterbomb:
      case NotificationType.follow:
      case NotificationType.message:
      case NotificationType.commentReaction:
      case NotificationType.postComment:
        return this.notification?.fromInfo.name;
      case NotificationType.seedsReceived:
        return `${this.notification?.sourceInfo.name} seeds received`;
      case NotificationType.postShared:
        return 'someone shared your post';
      case NotificationType.postSaved:
        return 'someone saved your post';
      case NotificationType.postReportedModerator:
        return `${this.notification?.sourceInfo.name} - report received`;
      case NotificationType.bannedFromCircle:
        return `you have been banned from ${this.notification?.sourceInfo.name}`;
      case NotificationType.unbannedFromCircle:
        return `you have been unbanned from ${this.notification?.sourceInfo.name}`;
      case NotificationType.bannedFromApp:
        return `your account has been banned`;
      case NotificationType.postReaction:
        return `${this.notification?.fromInfo.name}`;
      case NotificationType.unbannedFromApp:
        return `your account has been unbanned`;
      case NotificationType.postRemovedModerator:
        return `your post got removed from ${this.notification?.sourceInfo.name}`;
      case NotificationType.postRemovedPicnic:
        return 'your post got removed';
      case NotificationType.userJoinedACircle:
        return `circle joined`;
    }
    return this.notification?.fromInfo.name;
  }

  get description() {
    switch (this.notification?.action) {
      case NotificationType.glitterbomb:
        return 'glitterbombed you';
      case NotificationType.follow:
        return 'started following you';
      case NotificationType.commentReaction:
        return 'reacted to your post';
      case NotificationType.postComment:
        return 'left a comment on your post';
      case NotificationType.postReaction:
        return 'reacted to your post';
      case NotificationType.seedsReceived:
        return `${this.notification?.fromInfo.name} sent you seeds`;
      case NotificationType.postReportedModerator:
        return `${this.notification?.fromInfo.name} reported a post`;
      case NotificationType.postRemovedModerator:
      case NotificationType.postRemovedPicnic:
      case NotificationType.message:
      case NotificationType.postShared:
      case NotificationType.postSaved:
      case NotificationType.bannedFromCircle:
      case NotificationType.unbannedFromCircle:
      case NotificationType.bannedFromApp:
      case NotificationType.unbannedFromApp:
        return null;
      case NotificationType.userJoinedACircle:
        return `${this.notification?.fromInfo.name} joined ${this.notification?.sourceInfo.name} circle`;
    }
    return null;
  }
}

enum NotificationType {
  glitterbomb = 'glitterbomb',
  follow = 'follow',
  postComment = 'post_comment',
  commentReaction = 'comment_reaction',
  postReaction = 'post_reaction',
  messageReply = 'message_reply',
  message = 'message',
  seedsReceived = 'seeds_received',
  postShared = 'post_shared',
  postSaved = 'post_saved',
  postReportedModerator = 'post_reported_to_circle_moderators',
  bannedFromCircle = 'user_banned_from_circle',
  unbannedFromCircle = 'user_unbanned_from_circle',
  bannedFromApp = 'user_banned_from_app',
  unbannedFromApp = 'user_unbanned_from_app',
  postRemovedModerator = 'post_removed_by_moderator',
  postRemovedPicnic = 'post_removed_by_picnic',
  electionAboutToLose = 'election_about_to_lose',
  electionSomeonePassed = 'election_someone_passed',
  circle = 'circle',
  post = 'post',
  profile = 'profile',
  user = 'user',
  userJoinedACircle = 'user_joined_a_circle',
  userInvitedToACircle = 'user_invited_to_a_circle',
  unknown = '',
}
