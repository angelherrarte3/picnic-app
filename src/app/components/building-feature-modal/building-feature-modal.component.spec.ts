import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildingFeatureModalComponent } from './building-feature-modal.component';

describe('BuildingFeatureModalComponent', () => {
  let component: BuildingFeatureModalComponent;
  let fixture: ComponentFixture<BuildingFeatureModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuildingFeatureModalComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(BuildingFeatureModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
