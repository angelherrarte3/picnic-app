import { Component, ElementRef } from '@angular/core';
import { ModalName } from '@app/services/global-modal.service';
import { PlatformService } from '@app/services/platform.service';
import { ModalComponent } from '@app/types/modal-component';

@Component({
  selector: 'building-feature-modal',
  templateUrl: './building-feature-modal.component.html',
  styleUrls: ['./building-feature-modal.component.scss'],
})
export class BuildingFeatureModalComponent extends ModalComponent {
  override name: ModalName = 'building-feature';

  constructor(element: ElementRef, private platformService: PlatformService) {
    super(element);
  }

  openMobileApp() {
    const isAndroid = this.platformService.isAndroid();
    const isIOS = this.platformService.isIOS();

    if (isIOS) {
      const url = 'picnic://';
      window.open(url, '_blank');
      return;
    }

    if (isAndroid) {
      const url = 'intent://getpicnic.app/#Intent;scheme=https;package=com.ambertech.amber;end';
      window.open(url, '_blank');
      return;
    }

    const url = 'https://picnic.zone/download';
    window.open(url, '_blank');

    setTimeout(() => {
      this.close();
    }, 50);
  }

  override resetState(): void {}
}
