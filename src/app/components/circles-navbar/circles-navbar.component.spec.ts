import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CirclesNavbarComponent } from './circles-navbar.component';

describe('CirclesNavbarComponent', () => {
  let component: CirclesNavbarComponent;
  let fixture: ComponentFixture<CirclesNavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CirclesNavbarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CirclesNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
