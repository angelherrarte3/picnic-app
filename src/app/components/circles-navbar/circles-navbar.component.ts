import { Component } from '@angular/core';

@Component({
  selector: 'circles-navbar',
  templateUrl: './circles-navbar.component.html',
  styleUrls: ['./circles-navbar.component.scss'],
})
export class CirclesNavbarComponent {}
