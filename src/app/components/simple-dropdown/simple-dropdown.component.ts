import {
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';

export interface SimpleDropdownItem<Type1, Type2 = void> {
  label: string;
  value: Type1;
  data?: Type2;
}

export interface DropdownPosition {
  top?: number;
  bottom?: number;
  left?: number;
  right?: number;
}

@Component({
  selector: 'simple-dropdown[items]',
  templateUrl: './simple-dropdown.component.html',
  styleUrls: ['./simple-dropdown.component.scss'],
})
export class SimpleDropdownComponent<Type1, Type2> implements OnInit {
  @Input() items: SimpleDropdownItem<Type1, Type2>[] = [];
  @Input() initialValue?: Type1;
  @Input() textSize?: string;
  @Input() allowInput = false;
  @Input() inputHeight?: string;
  @Input() maxHeight?: number;
  @Input() maxWidth?: number;
  @Input() inputBgColor?: string;
  @Input() color?: string;
  @Input() hint: string = '';
  @Input() lightHintColor: boolean = false;
  @Input() errorText?: string;
  @Input() inputBorder?: string;
  @Input() loadingNext: boolean = false;
  @Input() showOpen: boolean = true;
  @Input() customPosition?: DropdownPosition;
  @Input() emitRepeatedValues: boolean = false;
  @Input() inputPadding?: string;

  @Output() private bySelect = new EventEmitter<SimpleDropdownItem<Type1, Type2>>();
  @Output() private byInput = new EventEmitter<string>();
  @Output() private byScrolled = new EventEmitter<void>();

  @ViewChild('scrollable') scrollable?: ElementRef;
  @ContentChild('customItem', { static: false })
  itemTemplateRef?: TemplateRef<any>;

  @ContentChild('customButton', { static: false })
  buttonTemplateRef?: TemplateRef<any>;
  selectedItem?: SimpleDropdownItem<Type1, Type2>;
  isOpen: boolean = false;
  style = {};

  constructor() {}

  get emptyItems() {
    return this.items.length === 0;
  }

  get getStyle() {
    return {
      height: this.inputHeight,
      background: this.inputBgColor,
      color: this.color,
      cursor: !this.allowInput ? 'pointer' : 'auto',
      fontSize: this.textSize,
      padding: this.inputPadding,
      border: this.inputBorder,
    };
  }

  ngOnInit(): void {
    if (this.initialValue) this.selectedItem = this.items.find((item) => item.value === this.initialValue);

    this.style = this.getStyle;
  }

  resetScroll() {
    if (this.scrollable) {
      this.scrollable.nativeElement.scrollTop = 0;
    }
  }

  onScroll = () => this.byScrolled.emit();

  hideDropdown() {
    this.isOpen = false;
    this.resetScroll();
  }

  toggleDropdown() {
    this.isOpen = !this.isOpen;
    this.resetScroll();
  }

  handleSelectItem(item: SimpleDropdownItem<Type1, Type2>) {
    this.toggleDropdown();
    this.isOpen = false;

    if (this.selectedItem?.value === item.value && !this.emitRepeatedValues) return;

    this.selectedItem = item;
    this.bySelect.emit(this.selectedItem);
  }

  handleInput = (event: any) => this.byInput.emit(event.target.value);

  reset(value?: Type1) {
    if (value) {
      this.selectedItem = this.items.find((item) => item.value === value);
      this.bySelect.emit(this.selectedItem);
    } else {
      this.selectedItem = undefined;
    }
    this.hideDropdown();
    this.resetScroll();
  }
}
