import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicnicBannerComponent } from './picnic-banner.component';

describe('PicnicBannerComponent', () => {
  let component: PicnicBannerComponent;
  let fixture: ComponentFixture<PicnicBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PicnicBannerComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PicnicBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
