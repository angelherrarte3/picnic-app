import { Component, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';

export enum PicnicBannerMode {
  Default,
  Chat,
}

@Component({
  selector: 'picnic-banner',
  templateUrl: './picnic-banner.component.html',
  styleUrls: ['./picnic-banner.component.scss'],
})
export class PicnicBannerComponent {
  @Input() mode: PicnicBannerMode = PicnicBannerMode.Default;
  @Output() byClose = new EventEmitter<void>();
  readonly bannerMode = PicnicBannerMode;

  constructor(private router: Router) {}

  //Event that is executed in the parent component when the banner is closed
  //The intention is to remove the banner with Renderer2 in the parent component
  handleClose() {
    this.byClose.emit();
  }

  handleDownloadPicnic = () => this.router.navigateByUrl('/download');
}
