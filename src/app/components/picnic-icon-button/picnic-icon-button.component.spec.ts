import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicnicIconButtonComponent } from './picnic-icon-button.component';

describe('PicnicIconButtonComponent', () => {
  let component: PicnicIconButtonComponent;
  let fixture: ComponentFixture<PicnicIconButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PicnicIconButtonComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PicnicIconButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
