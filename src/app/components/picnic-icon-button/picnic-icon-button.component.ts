import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

/** Note: requires the [name] attribute */
@Component({
  selector: 'picnic-icon-button[name]',
  templateUrl: './picnic-icon-button.component.html',
  styleUrls: ['./picnic-icon-button.component.scss'],
})
export class PicnicIconButtonComponent implements OnInit {
  @Input() name: string = '';
  @Input() iconColor: string = '';
  @Input() iconSize: number = 24;
  @Input() label: string = '';
  @Input() verticalLayout: boolean = false;
  @Input() selected: boolean = false;
  @Input() labelSize: string = '';
  @Input() disabled: boolean = false;
  @Input() borderRadius: number = 12;
  @Input() bgColor: string = '';
  @Input() size?: number;
  @Input() padding?: string;
  @Input() rounded: boolean = false;
  @Input() loading: boolean = false;
  @Input() loadingPadding?: string;
  @Input() badgeCounter?: number;

  @Output() byClick = new EventEmitter<any>();

  iconStyle = {};
  style = {};

  constructor() {}

  ngOnInit(): void {
    this.style = this.getStyle;
    this.iconStyle = this.getIconStyle;
  }

  get getStyle() {
    return {
      padding: this.padding,
      width: `${this.size}px`,
      height: `${this.size}px`,
      'border-radius': this.rounded ? '1000px' : `${this.borderRadius}px`,
      'background-color': this.bgColor,
    };
  }

  get getIconStyle() {
    return {
      color: this.iconColor,
      'width.px': this.iconSize,
      'height.px': this.iconSize,
      padding: 0,
      'margin.px': 0,
    };
  }

  get getLoadingStyle() {
    return {
      padding: this.loadingPadding,
    };
  }

  handleClick = (event: any) => this.byClick.emit(event);
}
