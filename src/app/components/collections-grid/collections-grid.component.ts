import { Component, OnInit } from '@angular/core';
import { cloneDeep } from '@apollo/client/utilities';
import { CredentialsService } from '@app/auth';
import { Collection, CollectionsConnectionGQL } from '@app/graphql/collections';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';

@Component({
  selector: 'collections-grid',
  templateUrl: './collections-grid.component.html',
  styleUrls: ['./collections-grid.component.scss'],
})
export class CollectionsGridComponent extends GQLPaginationComponent implements OnInit {
  collections: Collection[] = [];

  constructor(private collectionsConnection: CollectionsConnectionGQL, private creds: CredentialsService) {
    super();
  }

  ngOnInit(): void {
    this.collectionsConnection
      .fetch({
        cursor: {
          id: '',
          limit: 8,
          dir: 'forward',
        },
        withPreviewPosts: true,
        userId: this.creds.credentials!.userid,
        returnSavedPostsCollection: true,
      })
      .subscribe(({ data }) => {
        this.collections = data.collectionsConnection.edges.map((v) => v.node);
      });
  }

  override loadMoreItems(): void {
    if (!this.hasNextPage) return;

    this.loadingNext = true;
    this.collectionsConnection
      .fetch({
        cursor: {
          id: this.cursorId,
          limit: 8,
          dir: 'forward',
        },
        withPreviewPosts: true,
        userId: this.creds.credentials!.userid,
        returnSavedPostsCollection: true,
      })
      .subscribe(({ data }) => {
        this.collections = this.collections.concat(
          ...data.collectionsConnection.edges.map((e) => {
            return cloneDeep(e.node);
          })
        );
        this.loadingNext = false;
        this.cursorId = data.collectionsConnection.pageInfo.lastId;
        this.hasNextPage = data.collectionsConnection.pageInfo.hasNextPage;
      });
  }
}
