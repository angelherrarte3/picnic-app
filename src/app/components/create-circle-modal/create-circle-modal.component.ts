import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PicnicModalService } from '@app/services/picnic-modal.service';
import { SimpleDropdownComponent, SimpleDropdownItem } from '../simple-dropdown/simple-dropdown.component';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ListLanguagesGQL, PLanguage } from '@app/graphql/languages';
import { CreateCircleGQL, CirclesInput, Group, ListGroupsGQL } from '@app/graphql/circle';
import { ReactiveFormValidation } from '@app/types/reactive-form-component.ts';
import { combineLatest, map } from 'rxjs';
import { trigger, transition, style, animate } from '@angular/animations';
import { ClassUtils } from '@app/utils/class.utils';
import { ModalComponent } from '@app/types/modal-component';
import { ModalName } from '@app/services/global-modal.service';

export interface PrivacyOption {
  title: string;
  description: string;
  value: string;
}

export enum AvatarImageType {
  Emoji = 'emoji',
  File = 'file',
}

@Component({
  selector: 'create-circle-modal',
  templateUrl: './create-circle-modal.component.html',
  styleUrls: ['./create-circle-modal.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate('0.2s ease-out', style({ opacity: 1 }))]),
      transition(':leave', [style({ opacity: 1 }), animate('0.2s ease-in', style({ opacity: 0 }))]),
    ]),
    trigger('inOutSizeAnimation', [
      transition(':enter', [style({ height: 0 }), animate('0.2s ease-out', style({ height: 40 }))]),
      transition(':leave', [
        style({ height: 40, opacity: 1 }),
        animate('0.2s ease-in', style({ height: 0, opacity: 0 })),
      ]),
    ]),
  ],
})
export class CreateCircleModalComponent extends ModalComponent implements OnInit {
  override name: ModalName = 'create-circle';

  @ViewChild('scrollContainer') scrollContainer: ElementRef;
  @ViewChild('avatarFileInput', { static: false }) avatarFileInput: ElementRef;
  @ViewChild('coverFileInput', { static: false }) coverFileInput: ElementRef;
  @ViewChild('groupDropdownRef') groupDropdownRef?: SimpleDropdownComponent<Group, void>;
  @ViewChild('languageDropdownRef')
  languageDropdownRef?: SimpleDropdownComponent<PLanguage, void>;
  circleGroups: SimpleDropdownItem<Group>[] = [];
  languages: SimpleDropdownItem<PLanguage>[] = [];
  privacyOptions: PrivacyOption[] = [
    {
      title: 'open 👀',
      description: 'open circles are discoverable and anyone can join the community freely without permissions',
      value: 'OPENED',
    },
    {
      title: 'closed 🔓',
      description:
        'closed circles are discoverable, but users need to request to join in order to be a member of the community',
      value: 'CLOSED',
    },
    {
      title: 'private 🔒',
      description: 'private circles are NOT discoverable and members are joined via invitation or links',
      value: 'PRIVATE',
    },
  ];
  avatarImageOptions: SimpleDropdownItem<AvatarImageType>[] = [
    {
      label: 'emoji',
      value: AvatarImageType.Emoji,
    },
    {
      label: 'file',
      value: AvatarImageType.File,
    },
  ];
  groupSelected?: Group;
  languageSelected?: PLanguage;
  privacySelected: PrivacyOption = this.privacyOptions[0];
  circleCreatedSucceded = false;
  loading: boolean = false;
  emojiPickerVisible = false;
  avatarFileUrl?: string;
  coverFileUrl?: string;

  form?: FormGroup;
  formValidation: ReactiveFormValidation;

  constructor(
    element: ElementRef,
    private picnicModal: PicnicModalService,
    private router: Router,
    private languagesGQL: ListLanguagesGQL,
    private listGroupGQL: ListGroupsGQL,
    private createCircleGQL: CreateCircleGQL
  ) {
    super(element);
  }

  ngOnInit() {
    combineLatest([this.initLanguages(), this.initGroups()]).subscribe(() => {
      this.initForm();
    });
  }

  initForm(): void {
    this.form = new FormGroup({
      coverImage: new FormControl(null),
      image: new FormControl('😀', {
        nonNullable: true,
        validators: [Validators.required],
      }),
      name: new FormControl('', {
        nonNullable: true,
        validators: [Validators.required, Validators.maxLength(20)],
      }),
      description: new FormControl('', {
        nonNullable: true,
        validators: [Validators.required],
      }),
      group: new FormControl(this.groupSelected?.groupId, {
        nonNullable: true,
        validators: [Validators.required],
      }),
      language: new FormControl(this.languageSelected?.tag, {
        nonNullable: true,
        validators: [Validators.required],
      }),
      privacy: new FormControl(this.privacySelected.value, {
        nonNullable: true,
        validators: [Validators.required],
      }),
    });
    this.formValidation = new ReactiveFormValidation(this.form);
  }

  private initLanguages() {
    return this.languagesGQL
      .watch({
        filter: 'ENABLED',
      })
      .valueChanges.pipe(
        map(({ data }) => {
          this.languages = data.listLanguages.map((language) => ({
            label: language.name.toLocaleLowerCase(),
            value: language,
          }));
          this.languages = this.languages.filter((language) => language.value.enabled === true && language.value.iso3);
          if (this.languages.length === 0) return data;

          this.languageSelected = this.languages[0].value;
          return data;
        })
      );
  }

  private initGroups() {
    return this.listGroupGQL.fetch({}).pipe(
      map(({ data }) => {
        this.circleGroups = data.listGroups.edges.map((group) => ({
          label: group.node.name,
          value: group.node,
        }));
        this.groupSelected = this.circleGroups.find((group) => group.value.name.toLocaleLowerCase() === 'other')?.value;

        return data;
      })
    );
  }

  override resetState() {
    this.groupSelected = this.circleGroups.find((group) => group.value.name.toLocaleLowerCase() === 'other')?.value;
    this.languageSelected = this.languages[0].value;
    this.privacySelected = this.privacyOptions[0];
    this.groupDropdownRef?.reset(this.groupSelected);
    this.languageDropdownRef?.reset(this.languageSelected);
    this.avatarFileUrl = undefined;
    this.coverFileUrl = undefined;
    this.emojiPickerVisible = false;
    this.form?.reset();
    this.circleCreatedSucceded = false;
  }

  get avatarExists() {
    return this.form?.value.image;
  }

  get coverExists() {
    return this.form?.value.coverImage;
  }

  get isEmojiAvatar(): boolean {
    if (!this.avatarExists) return false;

    return typeof this.form?.value.image === 'string';
  }

  get isFileAvatar(): boolean {
    if (!this.avatarExists) return false;
    return this.form?.value.image instanceof File;
  }

  onTapCreateCircle() {
    this.form?.markAllAsTouched();
    if (!this.form || this.form.invalid) return;

    this.loading = true;
    let circleInput: CirclesInput = {
      name: this.form.value.name,
      description: this.form.value.description,
      groupId: this.form.value.group,
      languageCode: this.form.value.language,
      visibility: this.form.value.privacy,
      image: this.isEmojiAvatar ? this.form.value.image : undefined,
      imageFile: this.isFileAvatar ? this.form.value.image : undefined,
      coverImage: this.form.value.coverImage,
    };
    circleInput = ClassUtils.removeNullEmptyKeys(circleInput);

    this.createCircleGQL.mutate({ data: circleInput }).subscribe({
      next: (value) => {
        this.circleCreatedSucceded = true;
        this.groupSelected = this.circleGroups.find((group) => group.value.name.toLocaleLowerCase() === 'other')?.value;
        this.languageSelected = this.languages[0].value;
        this.privacySelected = this.privacyOptions[0];
        this.groupDropdownRef?.reset(this.groupSelected);
        this.languageDropdownRef?.reset(this.languageSelected);
        this.avatarFileUrl = undefined;
        this.coverFileUrl = undefined;
        this.emojiPickerVisible = false;
        this.form?.reset();
      },
      error: (err) => {
        this.circleCreatedSucceded = false;
        this.loading = false;
      },
      complete: () => {
        this.loading = false;
      },
    });
  }

  onTapCreatePost() {
    this.close();
    this.router.navigateByUrl('/create-post');
  }

  onTapSelectGroup(item: SimpleDropdownItem<Group>) {
    this.groupSelected = item.value;
    this.form?.patchValue({
      group: item.value.groupId,
    });
  }

  onTapSelectLanguage(item: SimpleDropdownItem<PLanguage>) {
    this.languageSelected = item.value;
    this.form?.patchValue({
      language: item.value.tag,
    });
  }

  onTapSelectPrivacy(item: PrivacyOption) {
    this.privacySelected = item;
    this.form?.patchValue({
      privacy: item.value,
    });
  }

  onTapSelectAvatar(item: SimpleDropdownItem<AvatarImageType>) {
    if (item.value === AvatarImageType.Emoji) {
      this.emojiPickerVisible = true;
    }

    if (item.value === AvatarImageType.File) {
      this.openAvatarFileSelector();
    }
  }

  onEmojiSelect(emojiData: any) {
    this.form?.patchValue({
      image: emojiData.emoji.native,
    });
    this.emojiPickerVisible = false;
  }

  openAvatarFileSelector() {
    this.avatarFileInput.nativeElement.value = null;
    this.avatarFileInput.nativeElement.click();
  }

  openCoverFileSelector() {
    this.coverFileInput.nativeElement.value = null;
    this.coverFileInput.nativeElement.click();
  }

  onAvatarFileSelected(event: any) {
    const selectedFile = event.target.files[0];
    if (!selectedFile) return;

    this.avatarFileUrl = URL.createObjectURL(selectedFile);
    this.form?.patchValue({
      image: selectedFile,
    });
  }

  onCoverFileSelected(event: any) {
    const selectedFile = event.target.files[0];
    if (!selectedFile) return;

    this.coverFileUrl = URL.createObjectURL(selectedFile);
    this.form?.patchValue({
      coverImage: selectedFile,
    });
  }

  onTapDiscardCover() {
    this.coverFileUrl = undefined;
    this.form?.patchValue({
      coverImage: null,
    });
  }
}
