import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss'],
})
export class ListItemComponent implements OnInit {
  @Input() image?: string;
  @Input() imageSize?: number;
  @Input() emojiSize?: number;
  @Input() imageBg?: string;
  @Input() emojiMode: boolean = false;
  @Input() title?: string;
  @Input() subtitle?: string;
  @Input() forceImage?: boolean;
  @ContentChild(TemplateRef) contentRef?: TemplateRef<any>;

  constructor() {}

  ngOnInit(): void {}
}
