import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Attachment } from '@app/graphql/attachment';
import { ChatCircleInvite, ChatMessage } from '@app/graphql/chat';
import { PrivateProfile, PublicProfile } from '@app/graphql/profile';
import { ProfileStateService } from '@app/profile/profile-state.service';
import { TmpStateService } from '@app/services/tmp-state.service';
import { JoinCirclesGQL } from '@app/graphql/circle';

@Component({
  selector: 'chat-message[message]',
  templateUrl: './chat-message.component.html',
  styleUrls: ['./chat-message.component.scss'],
})
export class ChatMessageComponent implements OnInit {
  @Input() message: ChatMessage;

  constructor(private state: TmpStateService, private router: Router, private joinCircles: JoinCirclesGQL) {}

  ngOnInit(): void {}

  get isMe() {
    return this.state.profile?.id === this.message.authorId;
  }

  get componentPayloadInvite() {
    return this.message.component!.payload as ChatCircleInvite;
  }

  onTapProfile(profile: PublicProfile) {
    this.router.navigateByUrl(`/u/${profile.username}`);
  }

  get media(): Attachment[] {
    return (
      this.message.attachments?.filter((v) => v.fileType.startsWith('image') || v.fileType.startsWith('video')) ?? []
    );
  }

  onJoinClick() {
    if (this.componentPayloadInvite.circle.iJoined) {
      this.router.navigate(['/c', this.componentPayloadInvite.circleId]);
      return;
    }
    this.joinCircles
      .mutate({
        circlesIds: [this.componentPayloadInvite.circleId],
      })
      .subscribe(({ data }) => {
        this.componentPayloadInvite.circle.iJoined = true;
      });
  }
}
