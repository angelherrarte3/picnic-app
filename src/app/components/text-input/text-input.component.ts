import { trigger, transition, style, animate } from '@angular/animations';
import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from 'rxjs';

@Component({
  selector: 'text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ height: 0, opacity: 0 }),
        animate('150ms ease-out', style({ height: '*', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ height: '*', opacity: 1 }),
        animate('150ms ease-in', style({ height: 0, opacity: 0 })),
      ]),
    ]),
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextInputComponent),
      multi: true,
    },
  ],
})
export class TextInputComponent implements OnInit, ControlValueAccessor {
  @Input() width?: string;
  @Input() height?: string;
  @Input() bgColor?: string;
  @Input() color?: string;
  @Input() hint?: string;
  @Input() type?: string;
  @Input() fontSize?: string;
  @Input() errorText?: string;
  @Input() maxLength?: number;
  @Input() minLength?: number;
  @Input() disabled: boolean = false;
  @Input() showClear: boolean = false;
  @Input() textArea: boolean = false;
  @Input() textAreaRows?: number;
  @Input() border?: string;
  @Input() borderRadius?: string;
  @Input() padding?: string;
  @Output() private byInput = new EventEmitter<string | null>();
  @Output() private byClear = new EventEmitter<void>();

  private innerValue: any = '';

  style = {};

  constructor() {}

  ngOnInit(): void {
    this.style = this.getStyle;
  }

  get getStyle() {
    return {
      width: this.width,
      height: this.height,
      background: this.errorText ? 'white' : this.bgColor,
      outline: this.errorText ? '1px solid #CC4A62' : 'none',
      color: this.color,
      border: this.border,
      'border-radius': this.borderRadius,
      padding: this.padding,
    };
  }

  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

  //get accessor
  get value(): any {
    return this.innerValue;
  }

  //set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }

  handleInput = (event: any) => this.byInput.emit(event.target.value);

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  writeValue(value: any): void {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }

  handleClear() {
    this.innerValue = '';
    this.onChangeCallback('');
    this.byClear.emit();
  }
}
