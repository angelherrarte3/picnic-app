import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'seeds-counter',
  templateUrl: './seeds-counter.component.html',
  styleUrls: ['./seeds-counter.component.scss'],
})
export class SeedsCounterComponent implements OnInit {
  @Input() seedsCount: number | undefined | null = 0;
  @Input() preffix?: string;
  @Input() suffix?: string;
  @Input() suffixColor?: string;
  @Input() preffixColor?: string;
  @Output() bySeedsClick = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onSeedsClick = () => this.bySeedsClick.emit();
}
