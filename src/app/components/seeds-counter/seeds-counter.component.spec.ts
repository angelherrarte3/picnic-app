import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeedsCounterComponent } from './seeds-counter.component';

describe('SeedsCounterComponent', () => {
  let component: SeedsCounterComponent;
  let fixture: ComponentFixture<SeedsCounterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SeedsCounterComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SeedsCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
