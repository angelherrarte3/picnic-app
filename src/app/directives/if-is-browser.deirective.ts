import { Directive, Inject, PLATFORM_ID, TemplateRef, ViewContainerRef } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Directive({
  selector: '[ifIsBrowser]',
})
export class IfIsBrowserDirective {
  constructor(
    @Inject(PLATFORM_ID) private platformId: string,
    templateRef: TemplateRef<any>,
    viewContainer: ViewContainerRef
  ) {
    if (isPlatformBrowser(platformId)) {
      viewContainer.createEmbeddedView(templateRef);
    }
  }
}
