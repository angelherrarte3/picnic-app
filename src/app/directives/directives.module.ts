import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClickOutsideDirective } from './click-outside/click-outisde.directive';
import { TooltipDirective } from './tooltip/tooltip.directive';
import { TooltipService } from '@app/services/tooltip.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClickDoubleDirective } from './click-double/click-double.directive';
import { IfIsBrowserDirective } from '@app/directives/if-is-browser.deirective';
import { ClickStopDirective } from './click-stop.directive';

@NgModule({
  imports: [CommonModule, BrowserAnimationsModule],
  declarations: [
    ClickOutsideDirective,
    TooltipDirective,
    ClickDoubleDirective,
    IfIsBrowserDirective,
    ClickStopDirective,
  ],
  exports: [ClickOutsideDirective, TooltipDirective, ClickDoubleDirective, IfIsBrowserDirective, ClickStopDirective],
  providers: [TooltipService],
})
export class DirectivesModule {}
