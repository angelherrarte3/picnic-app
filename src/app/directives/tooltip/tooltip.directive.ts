import { Directive, ElementRef, HostListener, Input, Optional, Renderer2 } from '@angular/core';
import { TooltipService } from '@app/services/tooltip.service';

export enum TooltipPosition {
  Top = 'top',
  Bottom = 'bottom',
  Left = 'left',
  Right = 'right',
}

export interface TooltipConfig {
  text: string;
  position?: TooltipPosition;
  customPosition?: { top?: number; left?: number };
  backgroundColor?: string;
  textColor?: string;
  textSize?: string;
  autoHideTimeout?: number;
}

@Directive({
  selector: '[appTooltip]',
})
export class TooltipDirective {
  @Input('appTooltip') config: TooltipConfig = {
    text: '',
    position: TooltipPosition.Top,
  };
  private showTimeout: any;
  private hideTimeout: any;
  private tooltipElement?: HTMLElement;
  private autoHideTimeout: any;

  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    @Optional() private tooltipService?: TooltipService
  ) {
    if (!this.tooltipService) {
      this.createTooltipElement();
    }
  }

  @HostListener('mouseenter') onMouseEnter() {
    clearTimeout(this.hideTimeout);
    this.showTimeout = setTimeout(() => {
      if (this.tooltipService) {
        this.tooltipService.showTooltip(this.el, this.config);
      } else {
        this.showTooltip();
      }
    }, 300);
  }

  @HostListener('mouseleave') onMouseLeave() {
    clearTimeout(this.showTimeout);
    this.hideTimeout = setTimeout(() => {
      if (this.tooltipService) {
        this.tooltipService.hideTooltip();
      } else {
        this.hideTooltip();
      }
    }, 300);
  }

  private createTooltipElement() {
    this.tooltipElement = this.renderer.createElement('div');
    this.renderer.addClass(this.tooltipElement, 'tooltip-container');
    this.renderer.addClass(this.tooltipElement, 'tooltip-animate');
    this.renderer.appendChild(document.body, this.tooltipElement);
  }

  private showTooltip() {
    if (!this.tooltipElement) return;

    this.renderer.setProperty(this.tooltipElement, 'textContent', this.config.text);

    if (this.config.backgroundColor)
      this.renderer.setStyle(this.tooltipElement, 'backgroundColor', this.config.backgroundColor);

    if (this.config.textColor) this.renderer.setStyle(this.tooltipElement, 'color', this.config.textColor);

    const hostPosition = this.el.nativeElement.getBoundingClientRect();
    const tooltipPosition = this.tooltipElement.getBoundingClientRect();

    const style = window.getComputedStyle(this.el.nativeElement);
    const paddingTop = parseFloat(style.paddingTop) || 0;
    const paddingBottom = parseFloat(style.paddingBottom) || 0;
    const paddingLeft = parseFloat(style.paddingLeft) || 0;
    const paddingRight = parseFloat(style.paddingRight) || 0;

    let top: number;
    let left: number;
    const position = this.config.position || TooltipPosition.Top;

    switch (position) {
      case TooltipPosition.Top:
        top = hostPosition.top - tooltipPosition.height - 5;
        left = hostPosition.left + (hostPosition.width - tooltipPosition.width) / 2;
        break;
      case TooltipPosition.Bottom:
        top = hostPosition.bottom - paddingBottom + 5;
        left = hostPosition.left + (hostPosition.width - tooltipPosition.width) / 2;
        break;
      case TooltipPosition.Left:
        top =
          hostPosition.top +
          (hostPosition.height - paddingTop - paddingBottom - tooltipPosition.height) / 2 +
          paddingTop;
        left = hostPosition.left - paddingLeft - tooltipPosition.width - 5;
        break;
      case TooltipPosition.Right:
        top =
          hostPosition.top +
          (hostPosition.height - paddingTop - paddingBottom - tooltipPosition.height) / 2 +
          paddingTop;
        left = hostPosition.right - paddingRight + 5;
        break;
    }

    if (this.config.customPosition) {
      if (this.config.customPosition.top !== undefined) {
        top = hostPosition.top + this.config.customPosition.top;
      }
      if (this.config.customPosition.left !== undefined) {
        left = hostPosition.left + this.config.customPosition.left;
      }
    }

    this.renderer.setStyle(this.tooltipElement, 'top', `${top}px`);
    this.renderer.setStyle(this.tooltipElement, 'left', `${left}px`);
    this.renderer.addClass(this.tooltipElement, 'tooltip-show');

    if (this.config.autoHideTimeout) {
      clearTimeout(this.autoHideTimeout);
      this.autoHideTimeout = setTimeout(() => {
        this.hideTooltip();
      }, this.config.autoHideTimeout);
    }
  }

  private hideTooltip() {
    this.renderer.removeClass(this.tooltipElement, 'tooltip-show');
  }
}
