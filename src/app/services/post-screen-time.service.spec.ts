import { TestBed } from '@angular/core/testing';

import { PostScreenTimeService } from './post-screen-time.service';

describe('PostScreenTimeService', () => {
  let service: PostScreenTimeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PostScreenTimeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
