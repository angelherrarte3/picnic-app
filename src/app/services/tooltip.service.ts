import { Injectable, ElementRef, Renderer2, RendererFactory2, Inject, PLATFORM_ID } from '@angular/core';
import { TooltipConfig, TooltipPosition } from '@app/directives/tooltip/tooltip.directive';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class TooltipService {
  private tooltipElement?: HTMLElement;
  private renderer: Renderer2;
  private autoHideTimeout: any;
  private isTooltipShown: boolean = false;
  private hostElement?: ElementRef;
  private customPosition?: { top?: number; left?: number };
  private config?: TooltipConfig;

  constructor(@Inject(PLATFORM_ID) private platformId: string, rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
    this.tooltipElement = this.renderer.createElement('div');
    this.renderer.addClass(this.tooltipElement, 'tooltip-container');
    this.renderer.addClass(this.tooltipElement, 'tooltip-animate');
    if (isPlatformBrowser(this.platformId)) {
      this.renderer.appendChild(document.body, this.tooltipElement);
      this.renderer.listen(window, 'resize', () => {
        if (this.hostElement) {
          this.updateTooltipPosition();
        }
      });
    }
  }

  showTooltip(hostElement: ElementRef, config: TooltipConfig) {
    if (!this.tooltipElement || this.isTooltipShown) return;

    this.hostElement = hostElement;
    this.config = config;
    this.customPosition = config.customPosition;

    this.isTooltipShown = true;
    this.renderer.setProperty(this.tooltipElement, 'textContent', config.text);

    if (config.backgroundColor) {
      this.renderer.setStyle(this.tooltipElement, 'backgroundColor', config.backgroundColor);
    }
    if (config.textColor) {
      this.renderer.setStyle(this.tooltipElement, 'color', config.textColor);
    }

    if (config.textSize) {
      this.renderer.setStyle(this.tooltipElement, 'fontSize', config.textSize);
    }

    this.updateTooltipPosition();

    this.renderer.addClass(this.tooltipElement, 'tooltip-show');

    if (config.autoHideTimeout) {
      clearTimeout(this.autoHideTimeout);
      this.autoHideTimeout = setTimeout(() => {
        this.hideTooltip();
      }, config.autoHideTimeout);
    }

    this.updateTooltipPosition();
  }

  hideTooltip() {
    this.isTooltipShown = false;
    clearTimeout(this.autoHideTimeout);
    this.renderer.removeClass(this.tooltipElement, 'tooltip-show');
  }

  private calculateTopPosition(hostRect: DOMRect, position: TooltipPosition, tooltipHeight: number): number {
    switch (position) {
      case TooltipPosition.Top:
        return hostRect.top - tooltipHeight - 10;
      case TooltipPosition.Bottom:
        return hostRect.bottom + 10;
      case TooltipPosition.Left:
      case TooltipPosition.Right:
      default:
        return hostRect.top + (hostRect.height - tooltipHeight) / 2;
    }
  }

  private calculateLeftPosition(hostRect: DOMRect, position: TooltipPosition, tooltipWidth: number): number {
    switch (position) {
      case TooltipPosition.Top:
      case TooltipPosition.Bottom:
      default:
        return hostRect.left + (hostRect.width - tooltipWidth) / 2;
      case TooltipPosition.Left:
        return hostRect.left - tooltipWidth - 10;
      case TooltipPosition.Right:
        return hostRect.right + 10;
    }
  }

  private updateTooltipPosition() {
    if (!this.hostElement) return;
    if (!this.tooltipElement) return;

    const hostRect = this.hostElement.nativeElement.getBoundingClientRect();

    let top: number;
    let left: number;

    if (this.customPosition) {
      top = hostRect.top + this.customPosition.top;
      left = hostRect.left + this.customPosition.left;
    } else {
      const tooltipRect = this.tooltipElement.getBoundingClientRect();
      top = this.calculateTopPosition(hostRect, this.config?.position ?? TooltipPosition.Top, tooltipRect.height);
      left = this.calculateLeftPosition(hostRect, this.config?.position ?? TooltipPosition.Left, tooltipRect.width);
    }

    this.renderer.setStyle(this.tooltipElement, 'top', `${top}px`);
    this.renderer.setStyle(this.tooltipElement, 'left', `${left}px`);
  }
}
