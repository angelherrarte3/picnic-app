import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PostReactService {
  likesEmmiter = new EventEmitter<string>();

  constructor() {}

  likePost = (postId: string) => this.likesEmmiter.emit(postId);
}
