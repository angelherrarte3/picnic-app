import { Injectable } from '@angular/core';
import { PicnicModalComponent, PicnicModalOptions } from '@app/components/picnic-modal/picnic-modal.component';

@Injectable({
  providedIn: 'root',
})
export class PicnicModalService {
  private modal?: PicnicModalComponent;

  registerModal = (modal: PicnicModalComponent) => (this.modal = modal);

  unregisterModal = () => (this.modal = undefined);

  open = (options?: PicnicModalOptions) => this.modal?.open(options);

  close = () => this.modal?.close();
}
