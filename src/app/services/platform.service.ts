import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class PlatformService {
  private userAgent: string;

  constructor(@Inject(PLATFORM_ID) private platformId: string) {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    this.userAgent = window.navigator.userAgent;
  }

  isAndroid(): boolean {
    return /Android/i.test(this.userAgent);
  }

  isIOS(): boolean {
    return /iPhone|iPad|iPod/i.test(this.userAgent);
  }

  isWindows(): boolean {
    return /Windows NT/i.test(this.userAgent);
  }
}
