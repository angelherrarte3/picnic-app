import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class VideoTypeService {
  constructor() {}

  getVideoTypeByUrl(url: string): string | null {
    if (url.includes('.3gp')) {
      return 'video/3gpp';
    }
    if (url.includes('.mp4')) {
      return 'video/mp4';
    }
    if (url.includes('.mov')) {
      return 'video/mp4';
    }
    if (url.includes('.webm')) {
      return 'video/webm';
    }
    if (url.includes('.ogg')) {
      return 'video/ogg';
    }
    return null;
  }
}
