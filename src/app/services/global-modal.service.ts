import { Injectable } from '@angular/core';
import { PicnicModalOptions } from '@app/components/picnic-modal/picnic-modal.component';
import { ModalComponent } from '@app/types/modal-component';

export type ModalName = 'create-chat' | 'sign-up' | 'building-feature' | 'create-circle' | 'report-content';

@Injectable({
  providedIn: 'root',
})
export class GlobalModalService {
  private modals: ModalComponent[] = [];

  registerModals(modals: ModalComponent[]) {
    this.modals.push(...modals);
  }

  open(modalName: ModalName, options?: PicnicModalOptions) {
    const modal = this.modals.find((modal) => modal.name === modalName);
    modal?.open(options);
  }

  close(modalName: ModalName) {
    const modal = this.modals.find((modal) => modal.name === modalName);
    modal?.close();
  }
}
