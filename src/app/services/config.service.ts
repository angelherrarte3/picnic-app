import { Injectable } from '@angular/core';
import { GetSignInCaptchaParamsGQL } from '@app/graphql/auth';
import { forkJoin, map, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  private _recaptchaSiteKey = '';
  get recaptchaSiteKey() {
    return this._recaptchaSiteKey;
  }

  constructor(private getSignInCaptchaParamsGQL: GetSignInCaptchaParamsGQL) {}

  loadConfig() {
    return forkJoin([this.getRecaptchaSiteKey()]);
  }

  private getRecaptchaSiteKey() {
    return this.getSignInCaptchaParamsGQL
      .fetch()
      .pipe(map(({ data }) => data.getSignInCaptchaParamsV3.recaptchaSiteKey))
      .pipe(
        tap((key) => {
          this._recaptchaSiteKey = key;
        })
      );
  }
}
