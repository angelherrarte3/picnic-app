import { TestBed } from '@angular/core/testing';

import { VideoQueueService } from './video-queue.service';

describe('VideoQueueService', () => {
  let service: VideoQueueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VideoQueueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
