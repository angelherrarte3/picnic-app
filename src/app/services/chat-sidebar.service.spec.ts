import { TestBed } from '@angular/core/testing';

import { ChatSidebarService } from './chat-sidebar.service';

describe('ChatSidebarService', () => {
  let service: ChatSidebarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChatSidebarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
