import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import { LocalStorageService } from './local-storage.service';
import { SessionStorageService } from './session-storage.service';

@Injectable({ providedIn: 'root' })
export class NavigationService {
  private history: string[] = [];

  constructor(private router: Router, private location: Location, private sessionStorage: SessionStorageService) {
    this.history = this.sessionStorage.get('history') ?? [];
  }

  public startSaveHistory(): void {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.equalPreviousUrl(event.urlAfterRedirects)) return;
        if (this.history.length > 15) this.history.splice(0, 1);

        this.history.push(event.urlAfterRedirects);
        this.sessionStorage.set('history', this.history);
      }
    });
  }

  public getHistory(): string[] {
    return this.history;
  }

  public goBack(): void {
    this.history.pop();

    if (this.history.length > 0) {
      this.location.back();
    } else {
      this.router.navigateByUrl('/');
    }
  }

  public getPreviousUrl(): string {
    if (this.history.length > 0) {
      return this.history[this.history.length - 2];
    }

    return '';
  }

  private equalPreviousUrl(currentUrl: string): boolean {
    return this.history.length > 0 && this.history[this.history.length - 1] === currentUrl;
  }
}
