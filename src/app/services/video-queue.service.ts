import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { Subscription, fromEvent, Observable, BehaviorSubject, of } from 'rxjs';
import { auditTime } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class VideoQueueService {
  private scrollSubscription?: Subscription;
  private resizeSubscription?: Subscription;
  private elements: Element[] = [];
  private scrollSubject = new BehaviorSubject<number>(0);
  private windowHeight: number = 0;

  constructor(@Inject(PLATFORM_ID) private platformId: string) {}

  observeVideoItemState(element: Element): Observable<VideoItemVisibilityEvent> {
    if (!isPlatformBrowser(this.platformId)) {
      return of();
    }
    this.elements.push(element);
    if (this.scrollSubscription === undefined) {
      this.startScrollObserver();
    }

    return new Observable<VideoItemVisibilityEvent>((observer) => {
      const subscription = this.scrollSubject.subscribe((x) => {
        const containerRect = element.getBoundingClientRect();
        const containerX = containerRect.top;
        const containerHeight = containerRect.height;

        const diff = Math.abs(this.windowHeight / 2 - containerHeight / 2 - containerX);

        var state: VideoItemPlayState;

        if (diff < containerHeight * 0.4) {
          state = VideoItemPlayState.Playing;
        } else if (diff > containerHeight * 0.7) {
          state = VideoItemPlayState.Paused;
        } else {
          state = VideoItemPlayState.PlayingWithoutConcurrent;
        }

        observer.next({
          playState: state,
          visible: diff < containerHeight * 3,
        });
      });

      return () => {
        subscription.unsubscribe();
        this.elements = this.elements.filter((e) => e !== element);
        this.stopScrollObserver();
      };
    });
  }

  private startScrollObserver() {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    if (this.scrollSubscription === undefined) {
      this.windowHeight = window.innerHeight;
      this.scrollSubscription = fromEvent(window, 'scroll', { capture: true })
        .pipe(auditTime(300))
        .subscribe(() => {
          this.scrollSubject.next(0);
        });

      this.resizeSubscription = fromEvent(window, 'resize', { capture: true }).subscribe(() => {
        this.scrollSubject.next(0);
      });
    }
  }

  private stopScrollObserver() {
    if (this.elements.length === 0) {
      this.scrollSubscription?.unsubscribe();
      this.scrollSubscription = undefined;
      this.resizeSubscription?.unsubscribe();
      this.resizeSubscription = undefined;
    }
  }
}

export enum VideoItemPlayState {
  Playing,
  PlayingWithoutConcurrent,
  Paused,
}

export interface VideoItemVisibilityEvent {
  playState: VideoItemPlayState;
  visible: boolean;
}
