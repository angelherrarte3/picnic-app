import { AnalyticsEvent } from './analytics-event';
import { AnalyticsTapTarget } from './analytics-tap-target';

export class AnalyticsEventTap implements AnalyticsEvent {
  target: AnalyticsTapTarget;
  targetValue?: any;

  constructor(target: AnalyticsTapTarget, targetValue?: any) {
    this.target = target;
    this.targetValue = targetValue;
  }

  get name(): string {
    return 'tap';
  }

  get params(): { [key: string]: any } {
    return {
      target: this.target,
      target_value: this.targetValue,
    };
  }
}
