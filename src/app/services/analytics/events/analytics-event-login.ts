import { AnalyticsEvent } from './analytics-event';

export class AnalyticsEventLogin implements AnalyticsEvent {
  loginType: string;
  result: string;

  get name(): string {
    return 'login';
  }

  get params(): { [key: string]: any } {
    return {
      method: this.loginType,
      result: this.result,
    };
  }
}
