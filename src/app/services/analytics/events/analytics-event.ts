export interface AnalyticsEvent {
  name: string;
  params: { [key: string]: any };
}
