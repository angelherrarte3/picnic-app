import { Injectable } from '@angular/core';
import { AngularFireAnalytics } from '@angular/fire/compat/analytics';
import { AnalyticsEvent } from './events/analytics-event';
import { CredentialsService } from '@app/auth';
import { distinctUntilChanged } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AnalyticsService {
  constructor(private analytics: AngularFireAnalytics, private credentialsService: CredentialsService) {
    this.credentialsService.credentialsObservable.pipe(distinctUntilChanged()).subscribe((credentials) => {
      this.analytics.setUserId(credentials?.userid ?? 'anonymous');
      this.analytics.setUserProperties({
        username: credentials?.username ?? 'anonymous',
      });
    });
  }

  logEvent(event: AnalyticsEvent) {
    this.analytics.logEvent(event.name, event.params);
  }
}
