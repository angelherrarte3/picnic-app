import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SessionStorageService {
  constructor() {}

  get<T>(key: string): T | undefined {
    try {
      const item = sessionStorage.getItem(key);
      return item ? JSON.parse(item) : null;
    } catch (error) {
      return undefined;
    }
  }

  set(key: string, value: any): void {
    try {
      sessionStorage.setItem(key, JSON.stringify(value));
    } catch (error) {}
  }

  remove(key: string): void {
    try {
      sessionStorage.removeItem(key);
    } catch (error) {}
  }
}
