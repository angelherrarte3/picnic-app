import { TestBed } from '@angular/core/testing';

import { PostReactService } from './post-react.service';

describe('PostReactService', () => {
  let service: PostReactService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PostReactService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
