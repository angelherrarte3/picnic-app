import { TestBed } from '@angular/core/testing';

import { PicnicModalService } from './picnic-modal.service';

describe('PicnicModalService', () => {
  let service: PicnicModalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PicnicModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
