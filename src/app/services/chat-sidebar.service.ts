import { Injectable } from '@angular/core';
import { Chat, ChatExcerptEdge, ChatsConnectionGQL } from '@app/graphql/chat';
import { cloneDeep } from '@apollo/client/utilities';

@Injectable({
  providedIn: 'root',
})
export class ChatSidebarService {
  dms: Chat[] = [];
  circles: Chat[] = [];

  constructor(private chatsConnectionGQL: ChatsConnectionGQL) {}

  isVisible = false;

  toggle = () => (this.isVisible = !this.isVisible);

  show = () => (this.isVisible = true);

  hide = () => (this.isVisible = false);

  loadChats() {
    this.chatsConnectionGQL
      .fetch({
        chatTypes: ['SINGLE', 'GROUP'],
      })
      .subscribe(({ data }) => {
        this.dms = data.chatsConnection.edges.map((v) => v.node);
      });

    this.chatsConnectionGQL
      .fetch({
        chatTypes: ['CIRCLE'],
      })
      .subscribe(({ data }) => {
        this.circles = data.chatsConnection.edges.map((v) => v.node);
      });
  }

  addChat(chat: Chat) {
    if (chat.chatType === 'CIRCLE') {
      const idx = this.circles.findIndex((v) => v.id === chat.id);
      if (idx >= 0) {
        this.circles.splice(idx, 1);
      }

      this.circles.unshift(chat);
    } else {
      const idx = this.dms.findIndex((v) => v.id === chat.id);
      if (idx >= 0) {
        this.dms.splice(idx, 1);
      }

      this.dms.unshift(chat);
    }
  }
}
