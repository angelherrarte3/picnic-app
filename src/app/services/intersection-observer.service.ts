import { Injectable, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { distinctUntilChanged, mergeMap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class IntersectionObserverService {
  _observer: IntersectionObserver | undefined;

  constructor() {}

  createAndObserve(element: ElementRef, threshold: number = 1.0): Observable<boolean> {
    return new Observable<IntersectionObserverEntry[]>((observer) => {
      const intersectionObserver = new IntersectionObserver(
        (entries) => {
          observer.next(entries);
        },
        {
          threshold: threshold,
        }
      );

      intersectionObserver.observe(element.nativeElement);

      return () => {
        intersectionObserver.disconnect();
      };
    }).pipe(
      mergeMap((entries: IntersectionObserverEntry[]) => entries),
      map((entry) => entry.isIntersecting),
      distinctUntilChanged()
    );
  }
}
