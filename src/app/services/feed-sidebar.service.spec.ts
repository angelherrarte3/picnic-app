import { TestBed } from '@angular/core/testing';

import { FeedSidebarService } from './feed-sidebar.service';

describe('FeedSidebarService', () => {
  let service: FeedSidebarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FeedSidebarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
