import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Centrifuge } from 'centrifuge';
import { CredentialsService } from '@app/auth';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  private _socket?: ChatSocket;

  constructor(private creds: CredentialsService) {}

  open() {
    if (!this._socket) {
      this._socket = new ChatSocket(this.creds);
    }
  }

  get socket() {
    return this._socket!;
  }
}

class ChatSocket {
  client!: Centrifuge;
  constructor(private creds: CredentialsService) {
    this.client = new Centrifuge(environment.chatUrl, {
      token: creds.credentials?.token,
    });
    this.client.connect();
  }

  // sendDirect(recipient: string, message: string) {
  //   return this.client.rpc("direct_send", {
  //     recipient: recipient,
  //     message: {
  //       text: message
  //     }
  //   })
  //   this.client.
  // }

  subscribeToChannel(id: string, func: (data: any) => void) {
    if (this.client.getSubscription(id)) {
      return;
    }
    const sub = this.client.newSubscription(id);
    sub.on('publication', (ctx: any) => {
      func(ctx.data);
    });
    sub.subscribe();
  }
}
