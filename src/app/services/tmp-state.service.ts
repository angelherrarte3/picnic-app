import { Injectable } from '@angular/core';
import { MyProfileGQL, PrivateProfile } from '@app/graphql/profile';
import { CredentialsService } from '@app/auth';
import { BehaviorSubject, map, Observable, ReplaySubject, Subject, Subscription } from 'rxjs';

// TMP service for storing shared state only for ChangeDetectionStrategy.Default mode
@Injectable({
  providedIn: 'root',
})
export class TmpStateService {
  // profile sync
  public profileTitle = 'Profile [from shared state]';

  public newFyp = true;

  profile?: PrivateProfile;
  profileSubject = new BehaviorSubject<PrivateProfile | undefined>(undefined);

  // circle
  public circleTitle = 'Circle [from shared state]';

  // feed
  public feedTitle = 'Feed [from shared state]';

  // dm
  public dmTitle = 'DM [from shared state]';

  constructor(private creds: CredentialsService, private myProfileGQL: MyProfileGQL) {}

  updateProfile() {
    this.myProfileGQL
      .fetch({}, { fetchPolicy: 'no-cache' })
      .pipe(map((res) => res.data.myProfile))
      .subscribe((p) => {
        this.profile = p;
        this.profileSubject.next(p);
      });
  }
}
