import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Chat, ChatsConnectionGQL, GetChatByIdGQL } from '@app/graphql/chat';

@Injectable({
  providedIn: 'root',
})
export class BubbleChatStateService {
  private activeChatsSub = new BehaviorSubject<Chat[]>([]);
  private activeModalChatsSub = new BehaviorSubject<Chat[]>([]);

  constructor(private chatsConnectionGQL: ChatsConnectionGQL, private getChatGQL: GetChatByIdGQL) {
    this.loadActiveChats();
  }

  loadActiveChats() {
    this.chatsConnectionGQL
      .fetch({
        chatTypes: ['SINGLE', 'CIRCLE', 'GROUP'],
        cursor: {
          id: '',
          limit: 3,
        },
      })
      .subscribe(({ data }) => {
        this.activeChatsSub.next(data.chatsConnection.edges.map((v) => v.node));
      });
  }

  // addChat(chatId: string) {
  //   const activeChats = this.activeChatsSub.getValue();
  //   activeChats.push(chatId);
  //   this.activeChatsSub.next(activeChats);
  // }

  removeChat(chatId: string) {
    const activeChats = this.activeChatsSub.getValue();
    const index = activeChats.findIndex((v) => v.id == chatId);
    if (index > -1) {
      activeChats.splice(index, 1);
    }
    this.activeChatsSub.next(activeChats);

    this.removeModalChat(chatId);
  }

  addModalChat(chat: Chat) {
    const activeModalChats = this.activeModalChatsSub.getValue();
    if (activeModalChats.find((v) => v.id === chat.id)) return;

    activeModalChats.push(chat);
    this.activeModalChatsSub.next(activeModalChats);
  }

  rebuildActiveChats(chat: Chat) {
    let result = this.activeChatsSub.getValue();
    const idx = result.findIndex((v) => v.id === chat.id);
    if (idx == 0) {
      return;
    }
    if (idx > 0) {
      result.splice(idx, 1);
    }
    result.unshift(chat);

    if (result.length > 3) {
      result.pop();
    }

    this.activeChatsSub.next(result);
  }

  removeModalChat(chatId: string) {
    const activeModalChats = this.activeModalChatsSub.getValue();
    const index = activeModalChats.findIndex((v) => v.id === chatId);
    if (index > -1) {
      activeModalChats.splice(index, 1);
    }
    this.activeModalChatsSub.next(activeModalChats);
  }

  get activeChats() {
    return this.activeChatsSub.asObservable();
  }

  get activeModalChats() {
    return this.activeModalChatsSub.asObservable();
  }

  createAndOpen(chatId: string) {
    this.getChatGQL.fetch({ id: chatId }).subscribe(({ data }) => {
      this.addModalChat(data.chat);
    });
  }
}
