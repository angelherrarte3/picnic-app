import { TestBed } from '@angular/core/testing';

import { TmpStateService } from './tmp-state.service';

describe('TmpStateService', () => {
  let service: TmpStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TmpStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
