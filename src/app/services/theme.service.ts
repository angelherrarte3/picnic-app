import { Injectable } from '@angular/core';
import { LocalStorageService } from '@app/services/local-storage.service';

export type PicnicTheme = 'light' | 'dark';

@Injectable({
  providedIn: 'root',
})
export class ThemeService {
  private currentTheme: PicnicTheme = 'light';

  constructor(private localStorage: LocalStorageService) {
    const savedTheme = localStorage.get<string>('theme');

    if (savedTheme) this.currentTheme = savedTheme as PicnicTheme;
    this.applyTheme(this.currentTheme);
  }

  public toggleTheme() {
    if (this.currentTheme === 'light') {
      this.currentTheme = 'dark';
    } else {
      this.currentTheme = 'light';
    }
    this.localStorage.set('theme', this.currentTheme);
    this.applyTheme(this.currentTheme);
  }

  setTheme(theme: PicnicTheme, options?: { saveLocal: boolean }) {
    this.currentTheme = theme;
    if (options?.saveLocal) this.localStorage.set('theme', this.currentTheme);
    this.applyTheme(this.currentTheme);
  }

  private applyTheme(theme: string) {
    try {
      document.documentElement.setAttribute('data-theme', theme);
    } catch {}
  }
}
