import { TestBed } from '@angular/core/testing';

import { BubbleChatStateService } from './bubble-chat-state.service';

describe('BubbleChatStateService', () => {
  let service: BubbleChatStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BubbleChatStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
