import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FeedSidebarService {
  isVisible: boolean = false;

  constructor() {}

  toggle() {
    this.isVisible = !this.isVisible;
  }

  hide() {
    this.isVisible = false;
  }

  show() {
    this.isVisible = true;
  }
}
