import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root',
})
export class PreferencesService {
  volume = new BehaviorSubject<number>(1);
  isMuted = new BehaviorSubject<boolean>(false);

  // Browser don't allow to autoplay videos with sound. This allows us to track if autoplay is allowed unmuted or not.
  isSystemMuted = new BehaviorSubject<boolean>(false);

  constructor(private localStorage: LocalStorageService) {
    this.initValues();
  }

  initValues() {
    // Set volume from local storage
    const volume = this.localStorage.get<number>('volume') ?? 1;
    this.volume.next(volume);
    const isMuted = this.localStorage.get<boolean>('isMuted') ?? false;
    this.isMuted.next(isMuted);
    const isSystemMuted = this.localStorage.get<boolean>('isSystemMuted') ?? false;
    this.isSystemMuted.next(isSystemMuted);
  }

  setVolume(volume: number) {
    this.localStorage.set('volume', volume);
    this.volume.next(volume);
  }

  setMuted(isMuted: boolean) {
    this.localStorage.set('isMuted', isMuted);
    this.isMuted.next(isMuted);
  }

  setSystemMuted(isSystemMuted: boolean) {
    this.localStorage.set('isSystemMuted', isSystemMuted);
    this.isSystemMuted.next(isSystemMuted);
  }

  bannerClosed() {
    return this.localStorage.get('banner_closed') === 'true' ?? false;
  }

  setBannerClosed(closed: boolean) {
    this.localStorage.set('banner_closed', closed.toString());
  }
}
