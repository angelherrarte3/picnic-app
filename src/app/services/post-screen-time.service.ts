import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { Subscription, fromEvent, Observable, BehaviorSubject, of } from 'rxjs';
import { auditTime } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class PostScreenTimeService {
  private scrollSubscription?: Subscription;
  private resizeSubscription?: Subscription;
  private elements: Element[] = [];
  private scrollSubject = new BehaviorSubject<number>(0);
  private windowHeight: number = 0;

  constructor(@Inject(PLATFORM_ID) private platformId: string) {}

  observePostItem(element: Element): Observable<PostItemVisibilityEvent> {
    if (!isPlatformBrowser(this.platformId)) {
      return of();
    }
    this.elements.push(element);
    if (this.scrollSubscription === undefined) {
      this.startScrollObserver();
    }

    return new Observable<PostItemVisibilityEvent>((observer) => {
      const subscription = this.scrollSubject.subscribe((x) => {
        const containerRect = element.getBoundingClientRect();
        const containerY = containerRect.top;
        const containerHeight = containerRect.height;

        const diff = Math.abs(this.windowHeight / 2 - containerHeight / 2 - containerY);

        observer.next({
          visible: diff < containerHeight,
        });
      });

      return () => {
        subscription.unsubscribe();
        this.elements = this.elements.filter((e) => e !== element);
        this.stopScrollObserver();
      };
    });
  }

  private startScrollObserver() {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    if (this.scrollSubscription === undefined) {
      this.windowHeight = window.innerHeight;
      this.scrollSubscription = fromEvent(window, 'scroll', { capture: true })
        .pipe(auditTime(300))
        .subscribe(() => {
          this.scrollSubject.next(0);
        });

      this.resizeSubscription = fromEvent(window, 'resize', { capture: true }).subscribe(() => {
        this.scrollSubject.next(0);
      });
    }
  }

  private stopScrollObserver() {
    if (this.elements.length === 0) {
      this.scrollSubscription?.unsubscribe();
      this.scrollSubscription = undefined;
      this.resizeSubscription?.unsubscribe();
      this.resizeSubscription = undefined;
    }
  }
}

export interface PostItemVisibilityEvent {
  visible: boolean;
}
