import { PublicProfile } from '@app/graphql/profile';
import { LinkMetaData } from '@app/graphql/content';
import { PageInfo } from '@app/graphql/page';
import { gql, Mutation, Query } from 'apollo-angular';
import { Injectable } from '@angular/core';
import { Cursor, SuccessPayload } from '@app/graphql/types';

export interface Comment {
  id: string;
  parentId?: string;
  parent?: Comment;
  postId?: string;
  authorId: string;
  author?: PublicProfile;
  text: string;
  attachments?: CommentAttachment[];
  imageContent: string;
  likesCount: number;
  repliesCount: number;
  createdAt: Date;
  deletedAt: Date;
  iReacted: boolean;
  repliesConnection?: {
    pageInfo: PageInfo;
    edges: CommentEdge[];
  };
}

export interface CommentAttachment {
  type: string;
  linkContent: CommentAttachmentLinkContent;
  imageContent: CommentAttachmentImageContent;
  videoContent: CommentAttachmentVideoContent;
}

export interface CommentAttachmentLinkContent {
  linkMetaData: LinkMetaData;
}

export interface CommentAttachmentImageContent {
  url: string;
  thumbnailUrl: string;
}

export interface CommentAttachmentVideoContent {
  url: string;
  thumbnailUrl: string;
  duration: string;
}

export interface CommentEdge {
  cursorId: string;
  node: Comment;
}

export interface GetCommentsInput {
  postId: string;
  parentId?: string;
  cursor: Cursor;
}

export interface CreateCommentInput {
  text: string;
  postId: string;
  parentId?: string;
}

export interface ReactionInput {
  id: string;
  react: boolean;
}

export interface GetCommentsResponse {
  getComments: {
    pageInfo: PageInfo;
    edges: CommentEdge[];
  };
}

export interface CreateCommentResponse {
  createComment: Comment;
}

export interface SetReactCommentResponse {
  setReactComment: SuccessPayload;
}

export interface GetCommentResponse {
  getComment: Comment;
}

@Injectable({
  providedIn: 'root',
})
export class GetTreeCommentsGQL extends Query<GetCommentsResponse, GetCommentsInput> {
  override document = gql`
    fragment comment on Comment {
      id
      text
      author {
        id
        username
        profileImage
      }
      repliesCount
      likesCount
      iReacted
      postId
      createdAt
      deletedAt
    }
    fragment pageInfo on PageInfo {
      firstId
      lastId
      hasNextPage
      hasPreviousPage
    }
    query ($postId: ID!, $parentId: String = "", $cursor: CursorInput!) {
      getComments(data: { postId: $postId, parentId: $parentId, cursor: $cursor }) {
        pageInfo {
          ...pageInfo
        }
        edges {
          node {
            ...comment
            repliesConnection(cursor: { id: "", limit: 1, dir: back }) {
              pageInfo {
                ...pageInfo
              }
              edges {
                node {
                  ...comment
                  repliesConnection(cursor: { id: "", limit: 20, dir: back }) {
                    pageInfo {
                      ...pageInfo
                    }
                    edges {
                      node {
                        ...comment
                        repliesConnection(cursor: { id: "", limit: 20, dir: back }) {
                          pageInfo {
                            ...pageInfo
                          }
                          edges {
                            node {
                              ...comment
                              repliesConnection(cursor: { id: "", limit: 2, dir: back }) {
                                pageInfo {
                                  ...pageInfo
                                }
                                edges {
                                  node {
                                    ...comment
                                    repliesConnection(cursor: { id: "", limit: 1 }) {
                                      pageInfo {
                                        ...pageInfo
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class CreateCommentGQL extends Mutation<CreateCommentResponse, CreateCommentInput> {
  override document = gql`
    mutation ($postId: ID!, $text: String!, $parentId: ID = "") {
      createComment(data: { postId: $postId, text: $text, parentId: $parentId }) {
        id
        text
        author {
          id
          username
          profileImage
        }
        repliesCount
        likesCount
        iReacted
        postId
        createdAt
        deletedAt
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class SetReactCommentGQL extends Mutation<SetReactCommentResponse, ReactionInput> {
  override document = gql`
    mutation ($id: String!, $react: Boolean!) {
      setReactComment(data: { id: $id, react: $react, reaction: ":heart:" }) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetCommentGQL extends Query<GetCommentResponse, { id: string }> {
  override document = gql`
    query ($id: ID!) {
      getComment(id: $id) {
        id
        text
        author {
          id
          username
          profileImage
        }
        repliesCount
        likesCount
        iReacted
        postId
        createdAt
        deletedAt
      }
    }
  `;
}
