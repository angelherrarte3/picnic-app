import { PageInfo } from '@app/graphql/page';
import { gql, Mutation, Query } from 'apollo-angular';
import { Injectable } from '@angular/core';
import { SeedsConnection } from '@app/graphql/seeds';
import { Chat } from '@app/graphql/chat';
import { Cursor, CursorInput, SuccessPayload } from '@app/graphql/types';
import { App } from './app';
import { GQLTemplateCircleApp } from './templates/gql_template_circle_app';

export interface Circle {
  id: string;
  name: string;
  urlName: string;
  description: string;
  kind: string;
  image?: string;
  imageFile?: string;
  coverImageFile: string;
  chat?: Chat;

  membersCount: number;
  viewsCount: number;
  postsCount: number;
  reportsCount: number;
  languageCode: string;
  role: string;
  iJoined: boolean;
  rulesText?: string;
  shareLink: string;
  isBanned: boolean;
  isVerified: boolean;
  visibility: string;
  seedsConnection?: SeedsConnection;
  options?: CircleOption[];
  metaWords: string[];
}

export interface CircleOption {
  name: Option;
  value: boolean;
  displayName: string;
  emoji: string;
  description: string;
}

export interface CircleEdge {
  cursorId: string;
  node: Circle;
}

// Also this means circle member
export interface ElectionParticipant {
  circleId: string;
  userId: string;
  role: string;
  user: MemberProfile;
  votesCount: number;
  votesPercent: number;
  isBanned: boolean;
  bannedAt: Date;
  bannedTime?: number;
  addModerators: boolean;
  iVoted: boolean;
}

export interface ElectionParticipantEdge {
  cursorId: string;
  node: ElectionParticipant;
}

export interface MemberProfile {
  id: string;
  username: string;
  fullName: string;
  bio?: string;
  followers: number;
  likes: number;
  views: number;
  isVerified: boolean;
  age: number;
  profileImage: string;
  isFollowing: boolean;
  followsMe: boolean;
  isBlocked: boolean;
}

export interface OnBoardingCirclesEdge {
  name: string;
  circles: Circle[];
}

export interface CircleReport {
  reportId: string;
  userId: string;
  circleId: string;
  anyId: string;
  reportType: ReportType;
  comment: string;
  reason: Reason;
  reporter: MemberProfile;
  contentAuthor: MemberProfile;
  moderator: MemberProfile;
  status: ResolveStatus;
  resolvedAt: Date;
}

export interface CircleApp {
  circleId: string;
  app: App;
}

export interface CircleReportEdge {
  cursorId: string;
  node: CircleReport;
}

export type ReportType = 'POST' | 'COMMENT' | 'USER' | 'USER_TEMP' | 'MESSAGE';

export type Reason = 'SPAM' | 'SEXUAL_CONTENT' | 'GRAPHIC_CONTENT' | 'OTHER';

type ResolveStatus = 'UNRESOLVED' | 'RESOLVED';

type CircleReportsFilterBy = 'ALL' | 'RESOLVED' | 'UNRESOLVED';

export interface BLWord {
  word: string;
}

type Option =
  | 'None'
  | 'Chatting'
  | 'Comments'
  | 'VideoPosts'
  | 'ThoughtPosts'
  | 'PollPosts'
  | 'LinkPosts'
  | 'Visibility'
  | 'PhotoPosts'
  | 'DisplayName'
  | 'Emoji';

export interface GroupsEdge {
  node: Group;
}

export interface Group {
  groupId: string;
  name: string;
  circles?: Circle[];
}

export interface CircleMember {
  circleId: string;
  userId: string;
  role: string;
  isBanned: boolean;
}

export interface GetUserCirclesResponse {
  getUserCircles: {
    pageInfo: PageInfo;
    edges: CircleEdge[];
  };
}

export interface GetCircleByIdResponse {
  getCircleById: Circle;
}

export interface GetMembersResponse {
  getMembers: {
    pageInfo: PageInfo;
    edges: ElectionParticipantEdge[];
  };
}

export interface OnBoardingCirclesConnectionResponse {
  onBoardingCirclesConnection: {
    edges: OnBoardingCirclesEdge[];
  };
}

export interface JoinCirclesResponse {
  joinCircles: SuccessPayload;
}

export interface LeaveCirclesResponse {
  leaveCircles: SuccessPayload;
}

export interface CirclesConnectionResponse {
  circlesConnection: {
    pageInfo: PageInfo;
    edges: CircleEdge[];
  };
}

export interface CircleReportsConnectionInput {
  circleId: string;
  cursor: Cursor;
  filterBy?: CircleReportsFilterBy;
}

export interface CircleReportsConnectionResponse {
  circleReportsConnection: {
    pageInfo: PageInfo;
    edges: CircleReportEdge[];
  };
}

export interface BLWConnectionRequest {
  circleId: string;
  searchQuery?: string;
  cursor: Cursor;
}

export interface BLWConnectionResponse {
  blwConnection: {
    pageInfo: PageInfo;
    edges: BLWord[];
  };
}

export interface RemoveCustomBLWordInput {
  circleId: string;
  words: string[];
}

export interface RemoveCustomBLWordResponse {
  removeCustomBLWords: SuccessPayload;
}

export interface UpdateCirclesInput {
  circleId: string;
  payload: CirclesInput;
}

export interface CirclesInput {
  kind?: string;
  name?: string;
  description?: string;
  image?: string;
  imageFile?: File;
  coverImage?: File;
  languageCode?: string;
  private?: boolean;
  hidden?: boolean;
  groupId?: string;
  parentId?: string;
  rulesText?: string;
  visibility?: string;
  options?: InputOptions[];
  metaWords?: string[];
}

export interface InputOptions {
  name: Option;
  value: boolean;
  displayName: string;
  emoji: string;
  description: string;
}

export interface UpdateCircleResponse {
  updateCircle: Circle;
}

export interface CreateCircleResponse {
  createCircle: Circle;
}

export interface BanUserInput {
  circleId: string;
  userId: string;
  bannedTime?: number;
}

export interface UnbanUserInCircleResponse {
  unbanUserInCircle: CircleMember;
}

export interface BanUserInCircleResponse {
  banUserInCircle: CircleMember;
}

export interface ResolveReportInput {
  reportId: string;
  circleId: string;
  fullFill?: boolean;
}

export interface ResolveReportResponse {
  resolveReport: SuccessPayload;
}

export interface ListGroupsInput {
  isTrending?: boolean;
  isWithCircles?: boolean;
}

export interface ListGroupsResponse {
  listGroups: {
    edges: GroupsEdge[];
  };
}

export interface CircleAppsInput {
  circleId: string;
  cursor: CursorInput;
}

export interface CircleAppsResponse {
  circleApps: {
    pageInfo: PageInfo;
    edges: CircleAppEdge[];
  };
}

export interface CircleAppEdge {
  cursorId: string;
  node: CircleApp;
}

export interface GetCircleByNameRequest {
  name: string;
}

export interface GetCircleByNameResponse {
  getCircleByName: Circle;
}

export interface GetLastViewedCirclesResponse {
  getLastViewedCircles: {
    pageInfo: PageInfo;
    edges: CircleEdge[];
  };
}

@Injectable({
  providedIn: 'root',
})
export class GetUserCirclesGQL extends Query<GetUserCirclesResponse> {
  override document = gql`
    query ($getUserCircles: GetUserCirclesRequest!) {
      getUserCircles(getUserCircles: $getUserCircles) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
        }
        edges {
          cursorId
          node {
            id
            name
            urlName
            description
            image
            imageFile
            membersCount
            languageCode
            rulesText
            kind
            isBanned
            isVerified
            iJoined
            role
            shareLink
            visibility
            reportsCount
            chat {
              id
              name
              chatType
              chatImage
              participantsCount
            }
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetCircleByIdGQL extends Query<GetCircleByIdResponse> {
  override document = gql`
    query ($circleId: ID!) {
      getCircleById(getCircleByIdInput: { circleId: $circleId }) {
        id
        name
        urlName
        description
        image
        imageFile
        coverImageFile
        membersCount
        languageCode
        rulesText
        kind
        isBanned
        isVerified
        iJoined
        role
        shareLink
        visibility
        reportsCount
        postsCount
        viewsCount
        seedsConnection {
          totalSeedsAmount
          edges {
            node {
              owner {
                id
                username
                profileImage
              }
              amountAvailable
              amountLocked
              amountTotal
            }
          }
        }
        chat {
          id
          name
          chatType
          chatImage
          participantsCount
          unreadMessagesCount
        }
        options {
          name
          value
          displayName
          emoji
          description
        }
        metaWords
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetCircleByNameGQL extends Query<GetCircleByNameResponse, GetCircleByNameRequest> {
  override document = gql`
    query ($name: String!) {
      getCircleByName(getCircleByNameInput: { name: $name }) {
        id
        name
        urlName
        description
        image
        imageFile
        coverImageFile
        membersCount
        languageCode
        rulesText
        kind
        isBanned
        isVerified
        iJoined
        role
        shareLink
        visibility
        reportsCount
        postsCount
        viewsCount
        seedsConnection {
          totalSeedsAmount
          edges {
            node {
              owner {
                id
                username
                profileImage
              }
              amountAvailable
              amountLocked
              amountTotal
            }
          }
        }
        chat {
          id
          name
          chatType
          chatImage
          participantsCount
          unreadMessagesCount
        }
        options {
          name
          value
          displayName
          emoji
          description
        }
        metaWords
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetMembersGQL extends Query<GetMembersResponse> {
  override document = gql`
    query ($circleId: ID!, $isBanned: Boolean, $cursor: CursorInput!, $roles: [CircleRole], $searchQuery: String) {
      getMembers(
        getMembersInput: {
          circleId: $circleId
          cursor: $cursor
          isBanned: $isBanned
          roles: $roles
          searchQuery: $searchQuery
        }
      ) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
        }
        edges {
          cursorId
          node {
            role
            user {
              id
              username
              fullName
              bio
              followers
              likes
              views
              isVerified
              age
              profileImage
              isBlocked
              isFollowing
              followsMe
            }
            isBanned
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class OnBoardingCirclesConnectionGQL extends Query<OnBoardingCirclesConnectionResponse> {
  override document = gql`
    query ooBoardingCirclesConnection {
      onBoardingCirclesConnection {
        edges {
          name
          circles {
            id
            name
            urlName
            description
            image
            membersCount
            languageCode
            rulesText
            kind
            isBanned
            isVerified
            iJoined
            role
            shareLink
            visibility
            reportsCount
            postsCount
            viewsCount
            metaWords
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class JoinCirclesGQL extends Mutation<JoinCirclesResponse> {
  override document = gql`
    mutation joinCircles($circlesIds: [ID!]!) {
      joinCircles(joinCircleInput: { circleIds: $circlesIds }) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class LeaveCirclesGQL extends Mutation<LeaveCirclesResponse> {
  override document = gql`
    mutation leaveCircles($circlesIds: [ID!]!) {
      leaveCircles(joinCircleInput: { circleIds: $circlesIds }) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class CirclesConnectionGQL extends Query<CirclesConnectionResponse> {
  override document = gql`
    query ($searchQuery: String, $cursor: CursorInput!, $isStrict: Boolean, $groupId: String) {
      circlesConnection(searchQuery: $searchQuery, cursor: $cursor, isStrict: $isStrict, groupId: $groupId) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
        }
        edges {
          cursorId
          node {
            id
            name
            urlName
            description
            image
            imageFile
            membersCount
            viewsCount
            postsCount
            languageCode
            rulesText
            kind
            isBanned
            isVerified
            iJoined
            coverImageFile
            role
            shareLink
            visibility
            reportsCount
            chat {
              id
              name
              chatType
              chatImage
              participantsCount
              unreadMessagesCount
            }
            options {
              name
              value
              displayName
              emoji
              description
            }
            metaWords
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class CircleReportsConnectionGQL extends Query<
  CircleReportsConnectionResponse,
  { data: CircleReportsConnectionInput }
> {
  override document = gql`
    query ($data: CircleReportsConnectionInput) {
      circleReportsConnection(circleReportsConnectionInput: $data) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
        }
        edges {
          cursorId
          node {
            reportId
            userId
            anyId
            reportType
            status
            reason
            comment
            resolvedAt
            moderator {
              id
              username
              profileImage
            }
            reporter {
              id
              username
              profileImage
            }
            contentAuthor {
              id
              username
              profileImage
            }
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class BLWConnectionGQL extends Query<BLWConnectionResponse, BLWConnectionRequest> {
  override document = gql`
    query ($circleId: ID!, $cursor: CursorInput!, $searchQuery: String) {
      blwConnection(cursor: $cursor, blwConnectionInput: { circleId: $circleId, searchQuery: $searchQuery }) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
          length
        }
        edges {
          word
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class RemoveCustomBLWordsGQL extends Mutation<RemoveCustomBLWordResponse, RemoveCustomBLWordInput> {
  override document = gql`
    mutation ($circleId: ID!, $words: [String!]) {
      removeCustomBLWords(removeCustomBLWordInput: { circleId: $circleId, words: $words }) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class UpdateCircleGQL extends Mutation<UpdateCircleResponse, { circleId: string; payload: CirclesInput }> {
  override document = gql`
    mutation updateCircle($circleId: ID!, $payload: CirclesInput!) {
      updateCircle(updateCircleInput: { circleId: $circleId, payload: $payload }) {
        id
        name
        urlName
        description
        image
        imageFile
        membersCount
        languageCode
        rulesText
        kind
        isBanned
        isVerified
        iJoined
        role
        shareLink
        visibility
        reportsCount
        options {
          name
          value
          displayName
          emoji
          description
        }
        chat {
          id
        }
        metaWords
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class CreateCircleGQL extends Mutation<CreateCircleResponse, { data: CirclesInput }> {
  override document = gql`
    mutation createCircle($data: CirclesInput!) {
      createCircle(createCircleInput: { payload: $data }) {
        id
        name
        description
        image
        imageFile
        membersCount
        languageCode
        rulesText
        kind
        isBanned
        isVerified
        iJoined
        role
        shareLink
        visibility
        reportsCount
        options {
          name
          value
          displayName
          emoji
          description
        }
        chat {
          id
        }
        metaWords
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class UnbanUserInCircleGQL extends Mutation<UnbanUserInCircleResponse, BanUserInput> {
  override document = gql`
    mutation ($circleId: ID!, $userId: ID!) {
      unbanUserInCircle(banUserInput: { circleId: $circleId, userId: $userId }) {
        userId
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class BanUserInCircleGQL extends Mutation<BanUserInCircleResponse, BanUserInput> {
  override document = gql`
    mutation ($circleId: ID!, $userId: ID!) {
      banUserInCircle(banUserInput: { circleId: $circleId, userId: $userId }) {
        userId
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class ResolveReportGQL extends Mutation<ResolveReportResponse, ResolveReportInput> {
  override document = gql`
    mutation ($reportId: ID!, $circleId: ID!, $fullFill: Boolean) {
      resolveReport(resolveReportInput: { circleId: $circleId, fullFill: $fullFill, reportId: $reportId }) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class ListGroupsGQL extends Query<ListGroupsResponse, ListGroupsInput> {
  override document = gql`
    query ($isTrending: Boolean, $isWithCircles: Boolean) {
      listGroups(listGroupsRequest: { isTrending: $isTrending, isWithCircles: $isWithCircles }) {
        edges {
          node {
            name
            groupId
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class CircleAppsGQL extends Query<CircleAppsResponse, CircleAppsInput> {
  override document = gql`
    query($circleId: ID!, $cursor: CursorInput!) {
      circleApps(input: { circleId: $circleId, cursor: $cursor }) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
        }
        edges {
          cursorId
          node {
            ${GQLTemplateCircleApp()}
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetLastViewedCirclesGQL extends Query<GetLastViewedCirclesResponse, { cursor?: CursorInput }> {
  override document = gql`
    query ($cursor: CursorInput) {
      getLastViewedCircles(cursor: $cursor) {
        edges {
          cursorId
          node {
            id
            name
            urlName
            description
            image
            imageFile
            membersCount
            languageCode
            rulesText
            kind
            isBanned
            isVerified
            iJoined
            role
            shareLink
            visibility
            reportsCount
            options {
              name
              value
              displayName
              emoji
              description
            }
            chat {
              id
            }
            metaWords
          }
        }
        pageInfo {
          lastId
          firstId
          length
          hasPreviousPage
          hasNextPage
        }
      }
    }
  `;
}
