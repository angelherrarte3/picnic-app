import { gql, Mutation } from 'apollo-angular';
import { Injectable } from '@angular/core';

export interface Attachment {
  id: string;
  filename: string;
  url: string;
  size: number;
  fileType: string;
  createdAt: Date;
}

export interface AttachmentInput {
  payload: File;
}

export interface UploadAttachmentResponse {
  uploadAttachment: Attachment;
}

@Injectable({
  providedIn: 'root',
})
export class UploadAttachmentGQL extends Mutation<UploadAttachmentResponse, AttachmentInput> {
  override document = gql`
    mutation ($payload: Upload!) {
      uploadAttachment(file: { payload: $payload }) {
        createdAt
        id
        url
        size
        fileType
        filename
      }
    }
  `;
}
