import { gql, Query } from 'apollo-angular';
import { PageInfo } from './page';
import { Cursor } from '@app/graphql/types';
import { Injectable } from '@angular/core';

export interface Notification {
  id: string;
  fromInfo: PopulatedData;
  receiverInfo: PopulatedData;
  sourceInfo: PopulatedData;
  subSourceInfo: PopulatedData;
  action: string;
  imageURL: string;
  readAt: Date;
  createdAt: Date;
}

export interface PopulatedData {
  id: string;
  type: string;
  name: string;
  avatar: string;
  relation: boolean;
}

export interface NotificationEdge {
  cursorId: string;
  node: Notification;
}

export interface NotificationsConnectionResponse {
  notificationGet: {
    edges: NotificationEdge[];
    pageInfo: PageInfo;
  };
}

@Injectable({
  providedIn: 'root',
})
export class NotificationsConnectionGQL extends Query<NotificationsConnectionResponse, { cursor: Cursor }> {
  override document = gql`
    query ($cursor: CursorInput!) {
      notificationGet(cursor: $cursor) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
          length
        }
        edges {
          node {
            id
            fromInfo {
              id
              type
              name
              avatar
              relation
            }
            receiverInfo {
              id
              type
              name
              avatar
            }
            sourceInfo {
              id
              type
              name
              avatar
            }
            subSourceInfo {
              id
              type
              name
              avatar
            }
            action
            readAt
            imageURL
            createdAt
          }
        }
      }
    }
  `;
}
