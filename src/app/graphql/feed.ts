import { Circle } from '@app/graphql/circle';
import { PageInfo } from '@app/graphql/page';
import { gql, Query } from 'apollo-angular';
import { Injectable } from '@angular/core';
import { PostEdge } from './content';
import { GQLTemplatePost } from './templates/gql_template';

export interface Feed {
  id: string;
  type: string;
  name: string;
  circle?: Circle;
  circleId?: string;
}

export interface FeedEdge {
  cursorId: string;
  node: Feed;
}

export interface FeedConnectionResponse {
  feedsConnection: {
    pageInfo: PageInfo;
    edges: FeedEdge[];
  };
}

export interface FeedPostsConnectionResponse {
  feedPostsConnection: {
    pageInfo: PageInfo;
    edges: PostEdge[];
  };
}

export interface PopularFeedResponse {
  popularFeed: Feed;
}

@Injectable({
  providedIn: 'root',
})
export class FeedConnectionGQL extends Query<FeedConnectionResponse> {
  override document = gql`
    query ($cursor: CursorInput!) {
      feedsConnection(cursor: $cursor) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
        }
        edges {
          cursorId
          node {
            id
            type
            name
            circle {
              id
              name
              urlName
              membersCount
              image
              imageFile
              metaWords
            }
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class FeedPostsConnectionGQL extends Query<FeedPostsConnectionResponse> {
  override document = gql`
    query feedPostsConnection($feedId: String!, $cursor: CursorInput!) {
      feedPostsConnection(feedId: $feedId, cursor: $cursor) {
        pageInfo {
          hasNextPage
          lastId
        }
        edges {
          cursorId
          node {
            ${GQLTemplatePost()}
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class PopularFeedGQL extends Query<PopularFeedResponse> {
  override document = gql`
    query popularFeed {
      popularFeed {
        id
        type
        name
      }
    }
  `;
}
