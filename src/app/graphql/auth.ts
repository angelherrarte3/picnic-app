import { PrivateProfile } from '@app/graphql/profile';
import { gql, Mutation, Query } from 'apollo-angular';
import { Injectable } from '@angular/core';

export interface ProfileMeta {
  pendingSteps: ProfileSetupStep[];
}

//Identify if the user needs to go through any of the onboarding steps
export enum ProfileSetupStep {
  Age,
  Email,
  Phone,
  Circles,
  Username,
  Languages,
  ProfileImage,
}

export interface UserAuthPayload {
  user: PrivateProfile;
  authInfo: AuthInfo;
}

export interface AuthInfo {
  accessToken: string;
  refreshToken: string;
}

export interface UsernameAvailabilityPayload {
  username: string;
  available: boolean;
}

export interface GetSignInCaptchaParamsPayload {
  recaptchaSiteKey: string;
}

export interface SignInWithPhoneNumberPayload {
  sessionInfo?: string;
}

export interface VerificationCodePayload {
  user: PrivateProfile;
  authInfo: AuthInfo;
}

export interface SignUpGuestPayload {
  userId: string;
  accessJwt: string;
  refreshJwt: string;
}

export interface SignInWithFirebaseResponse {
  signInWithFirebase: UserAuthPayload;
}

export interface SignUpWithFirebaseResponse {
  signUpWithFirebase: UserAuthPayload;
}

export interface SignInWithDiscordResponse {
  signInWithDiscord: UserAuthPayload;
}

export interface CheckUsernameResponse {
  checkUsername: UsernameAvailabilityPayload;
}

export interface GetSignInCaptchaParamsResponse {
  getSignInCaptchaParamsV3: GetSignInCaptchaParamsPayload;
}

export interface SignInWithPhoneNumberResponse {
  signInWithPhoneNumber: SignInWithPhoneNumberPayload;
}

export interface CheckVerificationCodeResponse {
  checkVerificationCode: VerificationCodePayload;
}

export interface RefreshTokensInput {
  accessToken: string;
  refreshToken: string;
}

export interface RefreshTokensResponse {
  refreshTokens: UserAuthPayload;
}

export interface SignUpGuestResponse {
  signUpGuest: SignUpGuestPayload;
}

@Injectable({
  providedIn: 'root',
})
export class SignInWithDiscordGQL extends Mutation<SignInWithDiscordResponse> {
  override document = gql`
    mutation signInWithDiscord($credentials: DiscordTokenInput!, $debugOption: DebugOptionInput) {
      signInWithDiscord(credentials: $credentials, debugOption: $debugOption) {
        user {
          id
          username
          fullName
          email
          phone
          isVerified
          languages
          melonsAmount
          seedsAmount
          profileImage
          meta {
            pendingSteps
          }
        }
        authInfo {
          accessToken
          refreshToken
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class SignInWithFirebaseGQL extends Mutation<SignInWithFirebaseResponse> {
  override document = gql`
    mutation signInWithFirebase($credentials: FirebaseAuthInput!) {
      signInWithFirebase(credentials: $credentials) {
        user {
          id
          username
          fullName
          email
          phone
          isVerified
          languages
          melonsAmount
          seedsAmount
          profileImage
        }
        authInfo {
          accessToken
          refreshToken
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class SignUpWithFirebaseGQL extends Mutation<SignUpWithFirebaseResponse> {
  override document = gql`
    mutation signUpWithFirebase($credentials: FirebaseAuthInput!, $userInfo: UserSignUpInput!) {
      signUpWithFirebase(credentials: $credentials, userInfo: $userInfo) {
        user {
          id
          username
          fullName
          email
          phone
          isVerified
          languages
          melonsAmount
          seedsAmount
          profileImage
        }
        authInfo {
          accessToken
          refreshToken
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class CheckUsernameGQL extends Query<CheckUsernameResponse> {
  override document = gql`
    query checkUsername($username: String!) {
      checkUsername(username: $username) {
        username
        available
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetSignInCaptchaParamsGQL extends Query<GetSignInCaptchaParamsResponse> {
  override document = gql`
    query {
      getSignInCaptchaParamsV3 {
        recaptchaSiteKey
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class SignInWithPhoneNumberGQL extends Mutation<SignInWithPhoneNumberResponse> {
  override document = gql`
    mutation signInWithPhoneNumber($phoneNumber: String!, $recaptchaToken: String!) {
      signInWithPhoneNumber(phoneNumber: $phoneNumber, recaptchaToken: $recaptchaToken, useCaptchaV3: true) {
        sessionInfo
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class CheckVerificationCodeGQL extends Mutation<
  CheckVerificationCodeResponse,
  { code: string; sessionInfo: string }
> {
  override document = gql`
    mutation ($code: String!, $sessionInfo: String!) {
      checkVerificationCode(code: $code, sessionInfo: $sessionInfo) {
        user {
          id
          username
          fullName
          email
          phone
          isVerified
          languages
          melonsAmount
          seedsAmount
          profileImage
        }
        authInfo {
          accessToken
          refreshToken
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class RefreshTokensGQL extends Mutation<RefreshTokensResponse, RefreshTokensInput> {
  override document = gql`
    mutation ($accessToken: String!, $refreshToken: String!) {
      refreshTokens(accessToken: $accessToken, refreshToken: $refreshToken) {
        authInfo {
          accessToken
          refreshToken
        }
        user {
          id
          username
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class SignUpGuestGQL extends Mutation<SignUpGuestResponse> {
  override document = gql`
    mutation {
      signUpGuest {
        accessJwt
        refreshJwt
        userId
      }
    }
  `;
}
