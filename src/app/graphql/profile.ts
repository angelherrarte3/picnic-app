import { gql, Mutation, Query } from 'apollo-angular';
import { Injectable } from '@angular/core';
import { SeedsConnection } from '@app/graphql/seeds';
import { PageInfo } from './page';
import { Cursor, SuccessPayload } from '@app/graphql/types';

export interface PrivateProfile {
  id: string;
  username: string;
  fullName: string;
  profileImage: string;
  likes: number;
  followers: number;
  views: number;
  seeds?: SeedsConnection;
  bio: string;
  meta: ProfileMeta;
  languages: string[];
}

export interface ProfileMeta {
  pendingSteps: ProfileSetupStep[];
}

type ProfileSetupStep = 'Age' | 'Email' | 'Phone' | 'Circles' | 'Username' | 'Languages' | 'ProfileImage';

export interface UserSettings {
  privacy?: PrivacySettings;
  languages?: string[];
  inviteLink?: string;
  shareLink?: string;
}

export interface PrivacySettings {
  directMessagesFromAccountsYouFollow: boolean;
}

export interface UserId {
  userId: string;
}

export interface MyProfileResponse {
  myProfile: PrivateProfile;
}

export interface UserIDPayload {
  profileGetUserIDByName: UserId;
}

export interface PublicProfile {
  id: string;
  username: string;
  fullName: string;
  bio: string;
  followers: number;
  likes: number;
  views: number;
  isVerified: boolean;
  age: number;
  profileImage: string;
  createdAt: Date;
  shareLink: string;
  isFollowing: boolean;
  isBlocked: boolean;
  followsMe: boolean;
}

export interface PublicProfileEdge {
  cursorId: string;
  node: PublicProfile;
  relations?: Relations;
}

export interface Relations {
  following: boolean;
  followedBy: boolean;
}

export interface UsersConnection {
  pageInfo: PageInfo;
  edges: PublicProfileEdge[];
}

export interface UsersConnectionResponse {
  usersConnection: UsersConnection;
}

export interface UserResponse {
  user: PublicProfile;
}

export interface FollowersConnectionInput {
  userId: string;
  searchQuery?: string;
  cursor?: Cursor;
}

export interface FollowersConnectionResponse {
  followersConnection: UsersConnection;
}

export interface BlockedConnectionResponse {
  blockedUsersConnection: UsersConnection;
}

export interface ChangeFollowStatusInput {
  userId: string;
  shouldFollow: boolean;
}

export interface ChangeFollowStatusResponse {
  changeFollowStatus: SuccessPayload;
}

export interface EditProfileResponse {
  updateProfileInfoStatus: SuccessPayload;
}

export interface UpdateLangResponse {
  updateProfileInfoStatus: SuccessPayload;
}

export interface ProfileStats {
  contentStatsForProfile: {
    likes: number;
    views: number;
    followers: number;
  };
}

export interface UpdateProfileInput {
  info: UserObject;
}

export interface CreateReportInput {
  info: ReportObject;
}

export interface UserObject {
  username?: string;
  name?: string;
  bio?: string;
  countryCode?: string;
  languageCodes?: string[];
  age?: number;
  notificationsEnabled?: boolean;
  phoneNumber?: string;
  email?: string;
  profileImage?: string;
}

export interface ReportObject {
  description?: string;
  reason?: string;
  entityType: string;
  entityId: string;
}

export interface SendReportResponse {
  updateProfileInfo: SuccessPayload;
}

export interface UpdateProfileInfoResponse {
  updateProfileInfo: SuccessPayload;
}

export interface UserSettingsInput {
  privacy?: PrivacySettingsInput;
  languages?: string[];
  inviteLink?: string;
  shareLink?: string;
}

export interface PrivacySettingsInput {
  directMessagesFromAccountsYouFollow: boolean;
}

export interface UpdateSettingsResponse {
  updateSettings: SuccessPayload;
}

export interface SettingsResponse {
  settings: UserSettings;
}

export interface UpdateUserPreferredLanguagesResponse {
  updateUserPreferredLanguages: SuccessPayload;
}

@Injectable({
  providedIn: 'root',
})
export class MyProfileGQL extends Query<MyProfileResponse> {
  override document = gql`
    query myProfile {
      myProfile {
        id
        username
        fullName
        profileImage
        likes
        followers
        views
        seeds {
          totalSeedsAmount
        }
        bio
        languages
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class UserConnectionGQL extends Query<UsersConnectionResponse> {
  override document = gql`
    query ($searchQuery: String!, $cursor: CursorInput!) {
      usersConnection(searchQuery: $searchQuery, cursor: $cursor) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
        }
        edges {
          cursorId
          node {
            id
            username
            bio
            profileImage
            isVerified
            followers
          }
          relations {
            followedBy
            following
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class UserGQL extends Query<UserResponse> {
  override document = gql`
    query ($userId: ID!) {
      user(id: $userId) {
        id
        username
        fullName
        bio
        followers
        likes
        views
        isVerified
        profileImage
        createdAt
        isFollowing
        followsMe
        isBlocked
        shareLink
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class FollowersConnectionGQL extends Query<FollowersConnectionResponse, FollowersConnectionInput> {
  override document = gql`
    query ($userId: ID!, $searchQuery: String, $cursor: CursorInput) {
      followersConnection(userId: $userId, searchQuery: $searchQuery, cursor: $cursor) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
        }
        edges {
          cursorId
          node {
            id
            username
            bio
            profileImage
            isVerified
            followers
          }
          relations {
            followedBy
            following
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class BlockedConnectionGQL extends Query<BlockedConnectionResponse> {
  override document = gql`
    query ($cursor: CursorInput) {
      blockedUsersConnection(cursor: $cursor) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
        }
        edges {
          cursorId
          node {
            id
            username
            profileImage
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class ChangeFollowStatusGQL extends Mutation<ChangeFollowStatusResponse, ChangeFollowStatusInput> {
  override document = gql`
    mutation ($userId: ID!, $shouldFollow: Boolean!) {
      changeFollowStatus(userId: $userId, shouldFollow: $shouldFollow) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class EditProfileGQL extends Mutation<EditProfileResponse> {
  override document = gql`
    mutation ($username: String, $name: String, $bio: String, $languagesCodes: [String!]) {
      updateProfileInfo(
        profile: { info: { username: $username, bio: $bio, name: $name, languageCodes: $languagesCodes } }
      ) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class ContentStatsForProfileGQL extends Query<ProfileStats> {
  override document = gql`
    query ($userID: String!) {
      contentStatsForProfile(userID: $userID) {
        views
        likes
        followers
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class ProfileGetUserIDByNameGQL extends Query<UserIDPayload> {
  override document = gql`
    query ($userName: String!) {
      profileGetUserIDByName(userName: $userName) {
        userId
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class UpdateProfileInfoGQL extends Mutation<UpdateProfileInfoResponse, { data: UpdateProfileInput }> {
  override document = gql`
    mutation ($data: UpdateProfileInput!) {
      updateProfileInfo(profile: $data) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GlobalReportGQL extends Mutation<SendReportResponse> {
  override document = gql`
    mutation ($reason: String!, $description: String!, $entityId: ID!, $entity: ReportEntity!) {
      createReport(info: { reason: $reason, description: $description, entityID: $entityId, entity: $entity }) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class UpdateSettingsGQL extends Mutation<UpdateSettingsResponse, UserSettingsInput> {
  override document = gql`
    mutation ($privacy: PrivacySettingsInput, $languages: [String!], $inviteLink: String, $shareLink: String) {
      updateSettings(
        userSettings: { languages: $languages, inviteLink: $inviteLink, shareLink: $shareLink, privacy: $privacy }
      ) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class SettingsGQL extends Query<SettingsResponse> {
  override document = gql`
    query {
      settings {
        privacy {
          directMessagesFromAccountsYouFollow
        }
        languages
        inviteLink
        shareLink
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class UpdateUserPreferredLanguagesGQL extends Mutation<
  UpdateUserPreferredLanguagesResponse,
  { languagesCodes: string[] }
> {
  override document = gql`
    mutation ($languagesCodes: [String!]) {
      updateUserPreferredLanguages(languagesCodes: $languagesCodes) {
        success
      }
    }
  `;
}
