import { gql, Query } from 'apollo-angular';
import { Injectable } from '@angular/core';

export interface PLanguage {
  tag: string;
  name: string;
  enabled: boolean;
  base: string;
  iso3: string;
  flag: string;
  nativeName: string;
}

export interface ListLanguagesResponse {
  listLanguages: PLanguage[];
}

@Injectable({
  providedIn: 'root',
})
export class ListLanguagesGQL extends Query<ListLanguagesResponse> {
  override document = gql`
    query listLanguages($filter: LanguageFilter!) {
      listLanguages(filter: $filter) {
        iso3
        flag
        name
        nativeName
        tag
        enabled
        base
      }
    }
  `;
}
