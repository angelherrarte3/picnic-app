export interface SuccessPayload {
  success: boolean;
}

export interface Cursor {
  id: string;
  limit?: number;
  dir?: CursorDirection;
}

export interface CursorInput {
  id: string;
  limit?: number;
  dir?: CursorDirection;
}

type CursorDirection = 'forward' | 'back';

export interface Embed {
  id: string;
  status: EmbedStatus;
  linkMetaData: LinkMetaData;
}

type EmbedStatus = 'LOADING' | 'SUCCESS' | 'ERROR';

export interface LinkMetaData {
  title?: string;
  description?: string;
  imageUrl?: string;
  host?: string;
  url?: string;
}
