import { Circle } from '@app/graphql/circle';
import { PublicProfile } from '@app/graphql/profile';
import { PageInfo } from '@app/graphql/page';
import { gql, Mutation, Query } from 'apollo-angular';
import { Injectable } from '@angular/core';
import { SuccessPayload } from '@app/graphql/types';
import { GQLTemplatePost } from './templates/gql_template';

export interface Post {
  id: string;
  shortId: string;
  createdAt: Date;
  title: string;
  type: string;
  videoContent?: VideoPostContent;
  imageContent?: ImagePostContent;
  textContent?: TextContent;
  linkContent?: LinkPostContent;
  pollContent?: PollPostContent;
  viewsCount: number;
  circle?: Circle;
  circleId: string;
  author: PublicProfile;
  authorId: string;
  shareLink: string;
  languageCode: string;
  context: PostContext;
  counters: ContentStatsForContent;
}

export enum PostReaction {
  None = '',
  Like = ':heart:',
  Dislike = ':thumbsdown:',
}

export class PostInfoWrapper {
  post: Post;
  _reaction: string;
  iSaved: boolean;
  likesCount: number;
  commentsCount: number;
  savesCount: number;
  sharesCount: number;

  get iLiked(): boolean {
    return this._reaction === PostReaction.Like;
  }

  get iDisliked(): boolean {
    return this._reaction === PostReaction.Dislike;
  }

  get reaction(): string {
    return this._reaction;
  }

  set reaction(reaction: string) {
    if (this._reaction !== PostReaction.Like && reaction === PostReaction.Like) {
      this.likesCount++;
    }
    if (this._reaction === PostReaction.Like && reaction !== PostReaction.Like) {
      this.likesCount--;
    }
    this._reaction = reaction;
  }

  constructor(post: Post) {
    this.post = post;
    this._reaction = post.context.reaction;
    this.iSaved = post.context.saved;
    this.likesCount = post.counters.reactions[PostReaction.Like] || 0;
    this.commentsCount = post.counters.comments || 0;
    this.savesCount = post.counters.saves || 0;
    this.sharesCount = post.counters.shares || 0;
  }
}

export interface PostEdge {
  cursorId: string;
  node: Post;
}

export interface TextContent {
  text: string;
  more?: string;
  color?: string;
}

export interface VideoPostContent {
  text?: string;
  thumbnailUrl?: string;
  duration?: number;
  url: string;
  downloadUrl?: string;
}

export interface ImagePostContent {
  url: string;
  text?: string;
}

export interface LinkPostContent {
  url?: string;
  linkMetaData?: LinkMetaData;
}

export interface LinkMetaData {
  title?: string;
  description?: string;
  imageUrl?: string;
  host?: string;
  url?: string;
}

export interface PollPostContent {
  question: string;
  answers: PollAnswer[];
  votesTotal: number;
  votedAnswer?: string;
}

export interface PostContext {
  reaction: string;
  saved: boolean;
  pollAnswerId: string;
}

export interface ContentStatsForContent {
  saves: number;
  shares: number;
  comments: number;
  reactions: { string: number };
  impressions: number;
}

export interface PollAnswer {
  id: string;
  imageUrl?: string;
  votesCount: number;
}

export interface UserFeed {
  edges: UserFeedEdge[];
}

export interface UserFeedEdge {
  node: Post;
}

export interface UserPostConnectionResponse {
  userPostsConnection: {
    pageInfo: PageInfo;
    edges: PostEdge[];
  };
}

export interface SortedCirclePostsConnectionResponse {
  sortedCirclePostsConnection: {
    pageInfo: PageInfo;
    edges: PostEdge[];
  };
}

export interface ReactToPostResponse {
  reactToPost: SuccessPayload;
}

export interface UnreactToPostResponse {
  unreactToPost: SuccessPayload;
}

export interface SharePostResponse {
  sharePost: SuccessPayload;
}

export interface GetPostResponse {
  getPost: Post;
}

export interface CreatePostResponse {
  createPost: Post;
}

export type PostType = 'TEXT' | 'LINK' | 'IMAGE' | 'VIDEO' | 'POLL';

export interface CreatePostRequest {
  title: string;
  type: PostType;
  circleId: string;
  soundId?: string;
  videoContent?: VideoPostContentInput;
  textContent?: TextPostContentInput;
  imageContent?: ImagePostContentInput;
  linkContent?: LinkPostContentInput;
  pollContent?: PollPostContentInput;
  mentions?: MentionsInput;
}

export interface VideoPostContentInput {
  text?: string;
  hlsTemplateOverride?: string;
  videoFile: File;
}

export interface TextPostContentInput {
  text: string;
  more?: string;
  color?: TextPostColor;
}

type TextPostColor = 'none' | 'blue' | 'purple' | 'yellow' | 'red' | 'green';

export interface ImagePostContentInput {
  text?: string;
  imageFile: File;
}

export interface LinkPostContentInput {
  text?: string;
  url: string;
}

export interface PollPostContentInput {
  question: String;
  answers: PollAnswerInput[];
}

export interface PollAnswerInput {
  answerType: PollAnswerType;
  imageContent?: File;
}

type PollAnswerType = 'IMAGE';

export interface MentionsInput {
  users: string[];
  contacts: string[];
  circles: string[];
}

export interface VoteInPollResponse {
  voteInPoll: SuccessPayload;
}

export interface ViewPostResponse {
  voteInPoll: SuccessPayload;
}

export interface SavePostScreenTimeResponse {
  savePostScreenTime: SuccessPayload;
}

export interface GetFeedResponse {
  getFeed: UserFeed;
}

export interface SavePostStatusResponse {
  savePostStatus: {
    success: boolean;
  };
}

export interface ReportInput {
  circleId: string;
  anyId: string;
  reportType: string;
  comment: string;
  reason: string;
  contentAuthorId: string;
}

export interface ReportResponse {
  report: SuccessPayload;
}

@Injectable({
  providedIn: 'root',
})
export class UserPostConnectionGQL extends Query<UserPostConnectionResponse> {
  override document = gql`
    query userPostsConnection($userId: String!, $cursor: CursorInput!) {
      userPostsConnection(userId: $userId, cursor: $cursor) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
        }
        edges {
          cursorId
          node {
            ${GQLTemplatePost()}
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class SortedCirclePostsConnectionGQL extends Query<SortedCirclePostsConnectionResponse> {
  override document = gql`
    query ($circleId: String!, $cursor: CursorInput!, $sortingType: PostsSortingType!) {
      sortedCirclePostsConnection(circleId: $circleId, cursor: $cursor, sortingType: $sortingType) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
          length
        }
        edges {
          cursorId
          node {
            ${GQLTemplatePost()}
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class ReactToPostGQL extends Mutation<ReactToPostResponse> {
  override document = gql`
    mutation ($postId: String!, $reaction: String!) {
      reactToPost(postId: $postId, reaction: $reaction) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class UnreactToPostGQL extends Mutation<UnreactToPostResponse> {
  override document = gql`
    mutation ($postId: String!) {
      unreactToPost(postId: $postId) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetPostByIdGQL extends Query<GetPostResponse> {
  override document = gql`
    query ($postId: String, $shortId: String) {
      getPost(postId: $postId, shortId: $shortId) {
        ${GQLTemplatePost()}
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class CreatePostGQL extends Mutation<CreatePostResponse, { data: CreatePostRequest }> {
  override document = gql`
    mutation createPost($data: CreatePostInput!) {
      createPost(data: $data) {
        ${GQLTemplatePost()}
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class VoteInPollGQL extends Mutation<VoteInPollResponse, { variantId: string; postId: string }> {
  override document = gql`
    mutation ($variantId: ID!, $postId: ID!) {
      voteInPoll(data: { variantId: $variantId, postId: $postId }) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class ViewPostByIdGQL extends Mutation<ViewPostResponse, { postId: string }> {
  override document = gql`
    mutation ($postId: ID!) {
      viewPost(id: $postId) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class SavePostScreenTimeGQL extends Mutation<SavePostScreenTimeResponse, { postID: string; duration: number }> {
  override document = gql`
    mutation ($postID: ID!, $duration: Int!) {
      savePostScreenTime(postID: $postID, duration: $duration) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetFeedGQL extends Query<GetFeedResponse, { limit: number }> {
  override document = gql`
    query($limit: Int!) {
      getFeed(limit: $limit) {
        edges {
          node {
            ${GQLTemplatePost()}
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class SavePostStatusGQL extends Mutation<SavePostStatusResponse, { postId: string; saveStatus: boolean }> {
  override document = gql`
    mutation SavePostStatus($postId: ID!, $saveStatus: Boolean!) {
      savePostStatus(postId: $postId, saveStatus: $saveStatus) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class ReportGQL extends Mutation<ReportResponse, { reportInput: ReportInput }> {
  override document = gql`
    mutation Report($reportInput: ReportInput!) {
      report(reportInput: $reportInput) {
        success
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class SharePostGQL extends Mutation<SharePostResponse> {
  override document = gql`
    mutation ($postId: ID!) {
      sharePost(postId: $postId) {
        success
      }
    }
  `;
}
