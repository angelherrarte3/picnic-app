import { GQLTemplatePublicProfile, GQLTemplateSimpleCircle } from './gql_template';

export function GQLTemplatePost(): string {
  return `
        id
        shortId
        createdAt
        title
        type
        videoContent {
            ${GQLTemplateVideoPostContent()}
        }
        imageContent {
            ${GQLTemplateImagePostContent()}
        }
        textContent {
            ${GQLTemplateTextPostContent()}
        }
        linkContent {
            ${GQLTemplateLinkPostContent()}
        }
        pollContent {
            ${GQLTemplatePollPostContent()}
        }
        viewsCount
        likesCount
        commentsCount
        sharesCount
        savesCount
        circle {
            ${GQLTemplateSimpleCircle()}
        }
        author {
            ${GQLTemplatePublicProfile()}
        }
        authorId
        shareLink
        iReacted
        iSaved
        languageCode
        context {
            ${GQLTemplatePostContext()}
        }
        counters {
            ${GQLTemplateContentStatsForContent()}
        }
    `;
}

export function GQLTemplateSound(): string {
  return `
        id
        title
        creator
        icon
        url
        usesCount
        duration
    `;
}

export function GQLTemplateTextPostContent(): string {
  return `
        text
        more
        color
    `;
}

export function GQLTemplateLinkMetaData(): string {
  return `
        title
        host
        imageUrl
        description
    `;
}

export function GQLTemplateLinkPostContent(): string {
  return `
        text
        url
        linkMetaData {
            ${GQLTemplateLinkMetaData()}
        }
    `;
}

export function GQLTemplateImagePostContent(): string {
  return `
        url
        text
    `;
}

export function GQLTemplateVideoPostContent(): string {
  return `
        text
        url
        thumbnailUrl
        duration
        downloadUrl
    `;
}

export function GQLTemplatePollPostContent(): string {
  return `
        question
        answers {
            ${GQLTemplatePollAnswer()}
        }
        votesTotal
        votedAnswer
    `;
}

export function GQLTemplatePollAnswer(): string {
  return `
        id
        imageUrl
        votesCount
    `;
}

export function GQLTemplateContentStatsForContent(): string {
  return `
        saves
        shares
        comments
        reactions
        impressions
    `;
}

export function GQLTemplatePostContext(): string {
  return `
        reaction
        saved
        pollAnswerId
    `;
}
