export function GQLTemplateSimpleCircle(): string {
  return `
        id
        name
        urlName
        description
        kind
        image
        imageFile
        coverImageFile
        membersCount
        viewsCount
        postsCount
        reportsCount
        languageCode
        role
        iJoined
        rulesText
        shareLink
        isBanned
        isVerified
        visibility
        metaWords
    `;
}
