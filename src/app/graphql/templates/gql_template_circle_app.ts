import { GQLTemplateApp } from './gql_template_app';

export function GQLTemplateCircleApp(): string {
  return `
        circleId
        app {
          ${GQLTemplateApp()}
        }
    `;
}
