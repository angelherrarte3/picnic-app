export function GQLTemplatePublicProfile(): string {
  return `
        id
        username
        fullName
        bio
        followers
        likes
        views
        isVerified
        age
        profileImage
        createdAt
        shareLink
        isFollowing
        isBlocked
        followsMe
    `;
}
