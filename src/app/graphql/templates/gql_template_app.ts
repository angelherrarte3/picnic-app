export function GQLTemplateApp(): string {
  return `
        id
        url
        name
        imageUrl
        description
        tags {
          ${GQLTemplateAppTag()}
        }
        permissions {
          ${GQLTemplateAppPermission()}
        }
        subscriptions {
          ${GQLTemplateAppSubscription()}
        }
        owner {
          ${GQLTemplateAppOwner()}
        }
        createdAt
        score
    `;
}

export function GQLTemplateAppTag(): string {
  return `
        id
        name
    `;
}

export function GQLTemplateAppPermission(): string {
  return `
        id
        dxName
        uxName
        description
        descriptors
    `;
}

export function GQLTemplateAppSubscription(): string {
  return `
        id
        descriptor
    `;
}

export function GQLTemplateAppOwner(): string {
  return `
        id
        name
    `;
}

export function GQLTemplateAppCounters(): string {
  return `
        circles
    `;
}
