import { Circle } from '@app/graphql/circle';
import { PublicProfile } from '@app/graphql/profile';
import { PageInfo } from '@app/graphql/page';
import { gql, Query } from 'apollo-angular';
import { Cursor } from '@app/graphql/types';
import { Injectable } from '@angular/core';

export interface SeedsConnection {
  totalSeedsAmount: number;
  pageInfo?: PageInfo;
  edges?: SeedEdge[];
}

export interface Seed {
  circle?: Circle;
  circleId: string;
  owner?: PublicProfile;
  ownerId: string;
  amountAvailable: number;
  amountLocked: number;
  amountTotal: number;
}

export interface SeedEdge {
  cursorId: string;
  node: Seed;
}

export interface CircleSeedsConnectionResponse {
  circleSeedsConnection: SeedsConnection;
}

export interface UserSeedsConnectionResponse {
  userSeedsConnection: SeedsConnection;
}

@Injectable({
  providedIn: 'root',
})
export class CircleSeedsConnectionGQL extends Query<
  CircleSeedsConnectionResponse,
  { circleID: string; cursor: Cursor }
> {
  override document = gql`
    query ($circleID: ID!, $curcor: CursorInput) {
      circleSeedsConnection(circleID: $circleID, cursor: $curcor) {
        pageInfo {
          firstId
          hasNextPage
          lastId
          hasPreviousPage
        }
        edges {
          node {
            circleId
            ownerId
            owner {
              id
              username
              profileImage
            }
            amountAvailable
            amountLocked
            amountTotal
          }
        }
        totalSeedsAmount
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class UserSeedsConnectionGQL extends Query<UserSeedsConnectionResponse, { userID: string; cursor?: Cursor }> {
  override document = gql`
    query ($userID: ID!, $cursor: CursorInput) {
      userSeedsConnection(userID: $userID, cursor: $cursor) {
        pageInfo {
          firstId
          hasNextPage
          lastId
          hasPreviousPage
        }
        edges {
          node {
            circleId
            circle {
              id
              name
              image
              imageFile
              membersCount
              metaWords
            }
            ownerId
            amountAvailable
            amountLocked
            amountTotal
          }
        }
        totalSeedsAmount
      }
    }
  `;
}
