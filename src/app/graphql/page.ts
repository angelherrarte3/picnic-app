export interface PageInfo {
  hasNextPage: boolean;
  hasPreviousPage?: boolean;
  lastId: string;
  firstId: string;
  length?: number;
}
