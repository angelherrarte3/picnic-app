import { PageInfo } from '@app/graphql/page';
import { PublicProfile } from '@app/graphql/profile';
import { Post } from '@app/graphql/content';
import { CursorInput } from '@app/graphql/types';
import { gql, Query } from 'apollo-angular';
import { GQLTemplatePost } from '@app/graphql/templates/gql_template_post';
import { Injectable } from '@angular/core';

export interface Collection {
  id: string;
  title: string;
  description: string;
  ownerId: string;
  owner?: PublicProfile;
  isPublic: boolean;
  createdAt: Date;
  counters: CollectionCounter;
  previewPosts?: Post[];
}

export interface CollectionCounter {
  posts: number;
}

export interface CollectionsEdge {
  cursorId: string;
  node: Collection;
}

export interface CollectionsConnection {
  pageInfo: PageInfo;
  edges: CollectionsEdge[];
}

export type CollectionsSortType = 'POPULAR_FIRST' | 'NEWEST_FIRST' | 'OLDEST_FIRST';

export interface CollectionsConnectionInput {
  userId: string;
  cursor?: CursorInput;
  withPreviewPosts?: boolean;
  returnSavedPostsCollection?: boolean;
  sortBy?: CollectionsSortType;
}

export interface CollectionsConnectionResponse {
  collectionsConnection: CollectionsConnection;
}

@Injectable({
  providedIn: 'root',
})
export class CollectionsConnectionGQL extends Query<CollectionsConnectionResponse, CollectionsConnectionInput> {
  override document = gql`
    query($userId: ID!, $cursor: CursorInput, $withPreviewPosts: Boolean = false, $returnSavedPostsCollection: Boolean, $sortBy: CollectionsSortType) {
      collectionsConnection(userId: $userId, cursor: $cursor, withPreviewPosts: $withPreviewPosts, returnSavedPostsCollection: $returnSavedPostsCollection, sortBy: $sortBy) {
        pageInfo {
          length
          firstId
          hasNextPage
          hasPreviousPage
          lastId
        }
        edges {
          cursorId
          node {
            title
            description
            ownerId
            isPublic
            createdAt
            id
            previewPosts {
              ${GQLTemplatePost()}
            }
            counters {
              posts
            }
          }
        }
      }
    }
  `;
}
