import { gql, Mutation, Query } from 'apollo-angular';
import { Injectable } from '@angular/core';
import { GQLTemplateApp } from './templates/gql_template_app';
import { Cursor } from '@app/graphql/types';
import { PageInfo } from '@app/graphql/page';

export interface App {
  id: string;

  url: string;
  name: string;
  imageUrl: string;
  description: string;

  tags?: AppTag[];
  permissions?: AppPermission[];
  subscriptions?: AppSubscription[];

  ownerId: string;
  owner?: AppOwner;

  createdAt: Date;

  score: number;
  counters: AppCounters;
}

export interface AppTag {
  id: string;
  name: string;
}

export interface AppPermission {
  id: string;
  dxName: string;
  uxName: string;
  description: string;
  descriptors: string;
}

export interface AppSubscription {
  id: string;
  descriptor: string;
}

export interface AppOwner {
  id: string;
  name: string;
}

export interface AppEdge {
  cursorId: string;
  node: App;
}

export interface AppCounters {
  circles: number;
}

export interface CreateAppInput {
  data: {
    url: string;
    name: string;
    tagIds?: string[];
    imageUrl: string;
    description: string;
    permissionIds?: string[];
    subscriptionIds?: string[];
  };
}

export interface CreateAppResponse {
  createApp: App;
}

export interface GetAppTagsResponse {
  getAppTags: AppTag[];
}

export interface GetAppPermissionsResponse {
  getAppPermissions: AppPermission[];
}

export interface GetAppSubscriptionsResponse {
  getAppSubscriptions: AppSubscription[];
}

export interface GetAppsResponse {
  getApps: App[];
}

export interface GetAppResponse {
  getApp: App;
}

export interface SearchInput {
  cursor?: Cursor;
  nameStartsWith: string;
  tagIds?: string[];
}

export interface SearchAppResponse {
  searchApps: {
    edges: AppEdge[];
    pageInfo: PageInfo;
  };
}

@Injectable({
  providedIn: 'root',
})
export class CreateAppGQL extends Mutation<CreateAppResponse, CreateAppInput> {
  override document = gql`
    mutation ($data: CreateAppInput!) {
      createApp(in: $data) {
        ${GQLTemplateApp()}
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetAppTagsGQL extends Query<GetAppTagsResponse, { ids?: string[] }> {
  override document = gql`
    query ($ids: [ID!]) {
      getAppTags(ids: $ids) {
        name
        id
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetAppPermissionsGQL extends Query<GetAppPermissionsResponse, { ids?: string[] }> {
  override document = gql`
    query ($ids: [ID!]) {
      getAppPermissions(ids: $ids) {
        descriptors
        description
        dxName
        uxName
        id
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetAppSubscriptionsGQL extends Query<GetAppSubscriptionsResponse, { ids?: string[] }> {
  override document = gql`
    query ($ids: [ID!]) {
      getAppSubscriptions(ids: $ids) {
        descriptor
        id
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetAppsGQL extends Query<GetAppsResponse, { ids: string[] }> {
  override document = gql`
    query ($ids: [ID!]!) {
      getApps(ids: $ids) {
        ${GQLTemplateApp()}
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetAppGQL extends Query<GetAppResponse, { id: string }> {
  override document = gql`
    query ($id: ID!) {
      getApp(id: $id) {
        ${GQLTemplateApp()}
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class SearchAppsGQL extends Query<SearchAppResponse, { data: SearchInput }> {
  override document = gql`
    query ($data: SearchInput) {
      searchApps(search: $data) {
        pageInfo {
          length
          lastId
          hasPreviousPage
          hasNextPage
          firstId
        }
        edges {
          cursorId
          node {
            ${GQLTemplateApp()}
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class MyAppsGQL extends Query<{ myApps: App[] }> {
  override document = gql`
    query {
      myApps {
        description
        name
        tags {
          name
          id
        }
        subscriptions {
          id
          descriptor
        }
        score
        permissions {
          id
          description
          uxName
          dxName
          descriptors
        }
        ownerId
        url
        imageUrl
        createdAt
        id
      }
    }
  `;
}
