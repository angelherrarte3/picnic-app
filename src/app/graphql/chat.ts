import { Cursor, Embed } from '@app/graphql/types';
import { PageInfo } from '@app/graphql/page';
import { Circle } from '@app/graphql/circle';
import { gql, Mutation, Query } from 'apollo-angular';
import { Injectable } from '@angular/core';
import { PublicProfile } from '@app/graphql/profile';
import { Attachment } from '@app/graphql/attachment';

export interface Chat {
  id: string;
  name: string;
  chatType: ChatType;
  participantsCount: number;
  chatImage?: string;
  circle?: Circle;
  lastMessageSentAt?: Date;
  unreadMessagesCount: number;
}

type ChatType = 'SINGLE' | 'GROUP' | 'CIRCLE';

export interface ChatMessage {
  id: string;
  chatId: string;
  author: PublicProfile;
  member?: ChatMember;
  authorId: string;
  content: string;
  replyToId?: string;
  replyContent?: ChatMessage;
  type: ChatMessageType;
  reactions?: ChatMessageReaction[];
  createdAt: Date;
  mentions?: ChatMention[];
  component?: ChatComponentData;
  attachmentIds?: string[];
  attachments?: Attachment[];
  embeds?: Embed[];
}

export interface ChatMember {
  userId: string;
  role: ChatRole;
  user: PublicProfile;
  lastReadMessage: Date;
}

type ChatRole = 'NONE' | 'MEMBER' | 'MODERATOR' | 'DIRECTOR';

type ChatMessageType = 'TEXT' | 'COMPONENT';

export interface ChatsEdge {
  cursorId: string;
  node: Chat;
}

export interface ChatMessagesEdge {
  cursorId: string;
  node: ChatMessage;
}

export interface ChatMessageReaction {
  reaction: string;
  hasReacted: boolean;
  count: number;
}

export interface ChatMention {
  user: PublicProfile;
}

type ChatComponentType = 'CIRCLE_INVITE' | 'SEEDS_EXCHANGE' | 'GLITTER_BOMB';

export interface ChatComponentData {
  type: ChatComponentType;
  payload: ChatComponentPayload;
}

export interface ChatCircleInvite {
  circleId: string;
  circle: Circle;
  userId: string;
}

export interface ChatSeedsExchange {
  offerId: string;
  userId: string;
}

export interface ChatGlitterBomb {
  senderId: string;
  sender: PublicProfile;
}

type ChatComponentPayload = ChatCircleInvite | ChatSeedsExchange | ChatGlitterBomb;

export interface ChatExcerpt {
  id: string;
  name: string;
  imageUrl: string;
  chatType: ChatType;
  language: string;
  participantsCount: number;
  messages: ChatMessage[];
  circle?: Circle;
}

export interface ChatExcerptEdge {
  cursorId: string;
  node: ChatExcerpt;
}

export interface ChatUnreadMetaData {
  chatId: string;
  chatType: ChatType;
  lastMessageAt: Date;
}

export interface ChatsConnectionInput {
  searchQuery?: String;
  chatTypes?: ChatType[];
  cursor?: Cursor;
}

export interface ChatsConnectionResponse {
  chatsConnection: {
    pageInfo: PageInfo;
    edges: ChatsEdge[];
  };
}

export interface ChatMessagesConnectionInput {
  chatId: string;
  searchQuery?: string;
  cursor?: Cursor;
}

export interface ChatMessagesConnectionResponse {
  chatMessagesConnection: {
    pageInfo: PageInfo;
    edges: ChatMessagesEdge[];
  };
}

export interface SendChatMessageInput {
  chatId: string;
  message: ChatMessageInput;
}

export interface ChatMessageInput {
  content: string;
  type: ChatMessageType;
  repliedToMessageId?: string;
  attachmentIds?: string[];
}

export interface SendChatMessageResponse {
  sendChatMessage: ChatMessage;
}

export interface GetChatByIdResponse {
  chat: Chat;
}

export interface SingleChatInput {
  userIds: string[];
}

export interface CreateSingleChatResponse {
  createSingleChat: Chat;
}

export interface ChatFeedConnectionInput {
  cursor?: Cursor;
}

export interface ChatFeedConnectionResponse {
  chatFeedConnection: {
    pageInfo: PageInfo;
    edges: ChatExcerptEdge[];
  };
}

export interface UnreadChatsResponse {
  unreadChats: ChatUnreadMetaData[];
}

export interface GroupChatInput {
  userIds: string[];
  name: string;
}

export interface CreateGroupChatResponse {
  createGroupChat: Chat;
}

@Injectable({
  providedIn: 'root',
})
export class ChatsConnectionGQL extends Query<ChatsConnectionResponse, ChatsConnectionInput> {
  override document = gql`
    query ($searchQuery: String, $chatTypes: [ChatType!], $cursor: CursorInput) {
      chatsConnection(searchQuery: $searchQuery, chatTypes: $chatTypes, cursor: $cursor) {
        pageInfo {
          firstId
          hasNextPage
          hasPreviousPage
          lastId
          length
        }
        edges {
          cursorId
          node {
            name
            id
            chatImage
            chatType
            lastMessageSentAt
            participantsCount
            unreadMessagesCount
            circle {
              image
              imageFile
              urlName
            }
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class ChatMessagesConnectionGQL extends Query<ChatMessagesConnectionResponse, ChatMessagesConnectionInput> {
  override document = gql`
    query ($chatId: ID!, $searchQuery: String, $cursor: CursorInput) {
      chatMessagesConnection(searchQuery: $searchQuery, chatId: $chatId, cursor: $cursor) {
        pageInfo {
          firstId
          hasNextPage
          hasPreviousPage
          lastId
          length
        }
        edges {
          cursorId
          node {
            chatId
            id
            attachmentIds
            attachments {
              id
              createdAt
              filename
              fileType
              size
              url
            }
            type
            createdAt
            authorId
            component {
              type
              payload {
                ... on ChatCircleInvite {
                  circle {
                    name
                    image
                    imageFile
                    membersCount
                    iJoined
                  }
                  circleId
                  userId
                }
                ... on ChatGlitterBomb {
                  sender {
                    username
                  }
                  senderId
                }
                ... on ChatSeedsExchange {
                  userId
                  offerId
                }
              }
            }
            member {
              user {
                username
                profileImage
              }
            }
            author {
              username
              profileImage
            }
            content
            replyToId
            replyContent {
              author {
                username
              }
              content
            }
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class SendChatMessageGQL extends Mutation<SendChatMessageResponse, SendChatMessageInput> {
  override document = gql`
    mutation ($chatId: ID!, $message: ChatMessageInput!) {
      sendChatMessage(chatId: $chatId, message: $message) {
        chatId
        id
        attachmentIds
        attachments {
          id
          createdAt
          filename
          fileType
          size
          url
        }
        type
        createdAt
        authorId
        component {
          type
          payload {
            ... on ChatCircleInvite {
              circle {
                name
              }
              circleId
              userId
            }
            ... on ChatGlitterBomb {
              sender {
                username
              }
              senderId
            }
            ... on ChatSeedsExchange {
              userId
              offerId
            }
          }
        }
        member {
          user {
            username
            profileImage
          }
        }
        content
        replyToId
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class GetChatByIdGQL extends Query<GetChatByIdResponse, { id: string }> {
  override document = gql`
    query ($id: ID!) {
      chat(id: $id) {
        name
        id
        chatImage
        chatType
        lastMessageSentAt
        participantsCount
        unreadMessagesCount
        circle {
          image
          imageFile
          urlName
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class CreateSingleChatGQL extends Mutation<CreateSingleChatResponse, SingleChatInput> {
  override document = gql`
    mutation ($userIds: [ID!]!) {
      createSingleChat(chatConfiguration: { userIds: $userIds }) {
        name
        id
        chatImage
        chatType
        lastMessageSentAt
        participantsCount
        unreadMessagesCount
        circle {
          image
          imageFile
          urlName
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class ChatFeedConnectionGQL extends Query<ChatFeedConnectionResponse, ChatFeedConnectionInput> {
  override document = gql`
    query ($cursor: CursorInput) {
      chatFeedConnection(cursor: $cursor) {
        pageInfo {
          firstId
          lastId
          hasNextPage
          hasPreviousPage
          length
        }
        edges {
          node {
            name
            id
            chatType
            imageUrl
            participantsCount
            language
            messages {
              id
              createdAt
              authorId
              author {
                username
                profileImage
              }
              content
              type
              replyToId
              replyContent {
                author {
                  username
                }
                content
              }
            }
            circle {
              imageFile
              image
              urlName
            }
          }
        }
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class UnreadChatsGQL extends Query<UnreadChatsResponse> {
  override document = gql`
    query {
      unreadChats {
        chatId
        chatType
        lastMessageAt
      }
    }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class CreateGroupChatGQL extends Mutation<CreateGroupChatResponse, { data: GroupChatInput }> {
  override document = gql`
    mutation ($data: GroupChatInput!) {
      createGroupChat(chatConfiguration: $data) {
        name
        id
        chatImage
        chatType
        lastMessageSentAt
        participantsCount
        unreadMessagesCount
      }
    }
  `;
}
