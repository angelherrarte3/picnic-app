import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SimpleDropdownComponent, SimpleDropdownItem } from '@app/components/simple-dropdown/simple-dropdown.component';
import { Circle, CirclesConnectionGQL } from '@app/graphql/circle';
import { CreatePostStateService } from '@app/create-post/create-post-state.service';
import { cloneDeep } from '@apollo/client/utilities';
import { GlobalModalService } from '@app/services/global-modal.service';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss'],
})
export class CreatePostComponent implements OnInit, OnDestroy {
  @ViewChild(SimpleDropdownComponent) dropdown?: SimpleDropdownComponent<string, Circle>;
  circleItems: SimpleDropdownItem<string, Circle>[] = [];
  cursorId = '';
  hasNextPage = false;
  loadingNext = false;
  query: string = '';

  constructor(
    private circlesConnection: CirclesConnectionGQL,
    private createPostState: CreatePostStateService,
    private modalService: GlobalModalService
  ) {}

  ngOnInit(): void {
    this.handleInput('').then(() => this.listenPreSelectedCircle());
  }

  async handleInput(value?: string) {
    this.dropdown?.resetScroll();
    this.query = value ?? '';
    this.cursorId = '';

    this.circlesConnection
      .fetch({
        searchQuery: this.query,
        cursor: {
          id: '',
          limit: 10,
          dir: 'forward',
        },
      })
      .subscribe(({ data }) => {
        this.circleItems = data.circlesConnection.edges.map((c) => {
          return {
            label: `${c.node.image} ${c.node.name}`,
            value: c.node.id,
            data: c.node,
          };
        });
      });
  }

  handleItemSelect(item: SimpleDropdownItem<string, Circle>) {
    this.createPostState.circle.next(item.data!);
  }

  handleCreateCircle() {
    this.modalService.open('create-circle', { dismissClickOutside: false });
  }

  handleLoadMore() {
    this.loadingNext = true;
    this.circlesConnection
      .fetch({
        searchQuery: this.query,
        cursor: {
          id: this.cursorId,
          limit: 10,
          dir: 'forward',
        },
      })
      .subscribe(({ data }) => {
        this.circleItems.push(
          ...data.circlesConnection.edges.map((e) =>
            cloneDeep({
              label: `${e.node.image} ${e.node.name}`,
              value: e.node.id,
              data: e.node,
            })
          )
        );

        this.loadingNext = false;
        this.cursorId = data.circlesConnection.pageInfo.lastId;
        this.hasNextPage = data.circlesConnection.pageInfo.hasNextPage;
      });
  }

  listenPreSelectedCircle() {
    this.createPostState.preSelectedCircle.subscribe((circle) => {
      if (circle) {
        const found = this.circleItems.find((c) => c.value === circle.id);
        if (!found) {
          this.circleItems.unshift({
            label: `${circle.image} ${circle.name}`,
            value: circle.id,
            data: circle,
          });
        }
        this.dropdown?.handleSelectItem(found ?? this.circleItems[0]);
      }
    });
  }

  ngOnDestroy(): void {
    this.createPostState.resetState();
  }
}
