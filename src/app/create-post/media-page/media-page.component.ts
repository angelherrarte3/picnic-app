import { Component, OnInit } from '@angular/core';
import { Circle } from '@app/graphql/circle';
import { CreatePostStateService } from '@app/create-post/create-post-state.service';
import { CreatePostGQL, PostType } from '@app/graphql/content';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UntilDestroy } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-media-page',
  templateUrl: './media-page.component.html',
  styleUrls: ['./media-page.component.scss'],
})
export class MediaPageComponent implements OnInit {
  text = '';
  file?: File;
  previewUrl: any;
  circle?: Circle;
  circleSubscription: Subscription;

  constructor(
    private createPostState: CreatePostStateService,
    private createPostGQL: CreatePostGQL,
    private router: Router
  ) {
    this.circleSubscription = this.createPostState.circle.subscribe((c) => {
      if (c) this.circle = c;
    });
  }

  ngOnInit(): void {}

  get enablePost(): boolean {
    return !!(this.circle && this.text && this.file);
  }

  choiceFile() {
    let input = document.createElement('input');
    input.type = 'file';
    input.accept = 'image/*,video/*';
    input.onchange = (event) => {
      this.file = input.files?.[0];

      this.previewImage(event);
    };
    input.click();
  }

  previewImage(event: any): void {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = () => {
      this.previewUrl = reader.result;
    };

    reader.readAsDataURL(file);
  }

  createPost() {
    if (!this.circle || !this.file) {
      return;
    }

    if (this.file.size > 25 * 1024 * 1024) {
      console.log('LARGE FILE!!!');
      return;
    }

    let type: PostType;
    let imageContent;
    let videoContent;
    if (this.file.type.startsWith('image')) {
      imageContent = {
        imageFile: this.file,
        text: this.text,
      };
      type = 'IMAGE';
    } else {
      videoContent = {
        text: this.text,
        videoFile: this.file,
      };
      type = 'VIDEO';
    }

    this.createPostGQL
      .mutate({
        data: {
          title: '',
          type: type,
          circleId: this.circle.id,
          videoContent: videoContent,
          imageContent: imageContent,
        },
      })
      .subscribe(({ data }) => {
        this.router.navigate(['/p/' + data!.createPost.id]);
        this.createPostState.resetState();
      });
  }
}
