import { Component, OnInit } from '@angular/core';
import { Circle } from '@app/graphql/circle';
import { CreatePostStateService } from '@app/create-post/create-post-state.service';
import { CreatePostGQL, PostType } from '@app/graphql/content';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UntilDestroy } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-poll-page',
  templateUrl: './poll-page.component.html',
  styleUrls: ['./poll-page.component.scss'],
})
export class PollPageComponent implements OnInit {
  text = '';
  file1?: File;
  file2?: File;
  previewUrl: any;
  previewUrl2: any;
  circle?: Circle;
  circleSubscription: Subscription;

  constructor(
    private createPostState: CreatePostStateService,
    private createPostGQL: CreatePostGQL,
    private router: Router
  ) {
    this.circleSubscription = this.createPostState.circle.subscribe((c) => {
      if (c) this.circle = c;
    });
  }

  previewImage1(event: any): void {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = () => {
      this.previewUrl = reader.result;
    };

    reader.readAsDataURL(file);
  }

  previewImage(event: any, previewUrl: string): void {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = () => {
      this[previewUrl] = reader.result;
    };

    reader.readAsDataURL(file);
  }
  previewImage2(event: any): void {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = () => {
      this.previewUrl2 = reader.result;
    };

    reader.readAsDataURL(file);
  }

  ngOnInit(): void {}

  get enablePost(): boolean {
    return !!(this.circle && this.text && this.file1 && this.file2);
  }

  choiceFile(file: 'first' | 'second') {
    let input = document.createElement('input');
    input.type = 'file';
    input.accept = 'image/*';
    input.onchange = (ev) => {
      switch (file) {
        case 'first':
          this.file1 = input.files?.[0];
          this.previewImage1(ev);
          break;
        case 'second':
          this.file2 = input.files?.[0];
          this.previewImage2(ev);

          break;
      }
    };
    input.click();
  }

  createPost() {
    if (!this.circle || !this.text || !this.file1 || !this.file2) {
      return;
    }

    const maxSize = 25 * 1024 * 1024;
    if (this.file1.size > maxSize || this.file2.size > maxSize) {
      console.log('LARGE FILE!!!');
      return;
    }

    this.createPostGQL
      .mutate({
        data: {
          title: '',
          type: 'POLL',
          circleId: this.circle!.id,
          pollContent: {
            question: this.text,
            answers: [
              {
                answerType: 'IMAGE',
                imageContent: this.file1!,
              },
              {
                answerType: 'IMAGE',
                imageContent: this.file2,
              },
            ],
          },
        },
      })
      .subscribe(({ data }) => {
        this.router.navigate(['/p/' + data!.createPost.id]);
        this.createPostState.resetState();
      });
  }
}
