import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreatePostComponent } from './create-post.component';
import { PollPageComponent } from './poll-page/poll-page.component';
import { MediaPageComponent } from './media-page/media-page.component';
import { ThouhtPageComponent } from './thouht-page/thouht-page.component';
import { RouterModule } from '@angular/router';
import { CreatePostRoutingModule } from './create-post-routing.module';
import { ComponentsModule } from '@app/components/components.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CreatePostComponent, ThouhtPageComponent, MediaPageComponent, PollPageComponent, MediaPageComponent],
  imports: [RouterModule, CommonModule, CreatePostRoutingModule, ComponentsModule, FormsModule],
})
export class CreatePostModule {}
