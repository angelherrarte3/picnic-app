import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThouhtPageComponent } from './thouht-page.component';

describe('ThouhtPageComponent', () => {
  let component: ThouhtPageComponent;
  let fixture: ComponentFixture<ThouhtPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ThouhtPageComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ThouhtPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
