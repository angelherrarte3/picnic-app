import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CreatePostStateService } from '@app/create-post/create-post-state.service';
import { Circle } from '@app/graphql/circle';
import { CreatePostGQL } from '@app/graphql/content';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Subscription } from 'rxjs';

@UntilDestroy()
@Component({
  selector: 'app-thouht-page',
  templateUrl: './thouht-page.component.html',
  styleUrls: ['./thouht-page.component.scss'],
})
export class ThouhtPageComponent implements OnInit {
  text = '';
  circle?: Circle;
  circleSubscription: Subscription;

  constructor(
    private createPostState: CreatePostStateService,
    private createPostGQL: CreatePostGQL,
    private router: Router
  ) {
    this.circleSubscription = this.createPostState.circle.subscribe((c) => {
      if (c) this.circle = c;
    });
  }

  get enablePost(): boolean {
    return !!(this.circle && this.text);
  }

  ngOnInit(): void {}

  createPost() {
    if (!this.circle || !this.text) return;

    this.createPostGQL
      .mutate({
        data: {
          type: 'TEXT',
          circleId: this.circle.id,
          title: '',
          textContent: {
            text: this.text,
            more: '',
            color: 'none',
          },
        },
      })
      .subscribe(({ data }) => {
        this.router.navigate(['/p/' + data!.createPost.id]);
        this.createPostState.resetState();
      });
  }
}
