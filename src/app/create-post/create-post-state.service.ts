import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Circle } from '@app/graphql/circle';

@Injectable({
  providedIn: 'root',
})
export class CreatePostStateService {
  circle = new ReplaySubject<Circle | undefined>(1);
  preSelectedCircle = new ReplaySubject<Circle | undefined>(1);

  resetState() {
    this.circle.next(undefined);
    this.preSelectedCircle.next(undefined);
  }
}
