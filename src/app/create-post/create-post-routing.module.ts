import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { CreatePostComponent } from './create-post.component';
import { ThouhtPageComponent } from './thouht-page/thouht-page.component';
import { MediaPageComponent } from './media-page/media-page.component';
import { PollPageComponent } from './poll-page/poll-page.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'create-post',
      component: CreatePostComponent,
      data: { title: marker('Create post') },
      children: [
        {
          path: '',
          redirectTo: 'thought',
          pathMatch: 'full',
        },
        {
          path: 'thought',
          component: ThouhtPageComponent,
        },
        {
          path: 'media',
          component: MediaPageComponent,
        },
        {
          path: 'poll',
          component: PollPageComponent,
        },
      ],
    },
    {
      path: '',
      redirectTo: 'create-post',
      pathMatch: 'full',
    },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreatePostRoutingModule {}
