import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmbedRoutingModule } from './embed-routing.module';
import { EmbedComponent } from './embed.component';

@NgModule({
  declarations: [EmbedComponent],
  imports: [CommonModule, EmbedRoutingModule],
})
export class EmbedModule {}
