import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GetPostByIdGQL, Post } from '@app/graphql/content';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-embed',
  templateUrl: './embed.component.html',
  styleUrls: ['./embed.component.scss'],
})
export class EmbedComponent implements OnInit {
  post?: Post;
  constructor(private route: ActivatedRoute, private getPostGQL: GetPostByIdGQL) {}

  ngOnInit(): void {
    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((p) => {
      let postId = p.get('id');
      this.getPostGQL
        .fetch({
          postId: postId,
        })
        .subscribe(({ data }) => {
          this.post = data.getPost;
        });
    });
  }
}
