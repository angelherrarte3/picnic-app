import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmbedComponent } from './embed.component';
import { AuthenticationGuard } from '@app/auth';

const routes: Routes = [
  {
    path: 'embed/:id',
    component: EmbedComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmbedRoutingModule {}
