import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ProfileStateService } from '@app/profile/profile-state.service';
import { TmpStateService } from '@app/services/tmp-state.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss'],
})
export class FeedComponent {
  showNavbar = true;

  constructor(public state: TmpStateService, private router: Router, private profileState: ProfileStateService) {
    this.navigateToForYou();
    this.listenNavbarState();
    this.listenForYouRedirect();
  }

  navigateToForYou() {
    if (this.router.url.includes('discover')) return;
    if (this.router.url.includes('p/')) return;
    if (this.router.url.includes('preview')) return;
    if (this.router.url.startsWith('/d')) return;
    if (this.router.url.startsWith('/my-circles')) return;
    if (this.router.url === '/' || this.router.url.length === 0) return;
    const redirectForYou = this.router.getCurrentNavigation()?.extras.state?.['redirectForYou'] ?? true;
    if (redirectForYou) this.router.navigateByUrl(`/`);
  }

  listenForYouRedirect() {
    this.router.events.pipe(untilDestroyed(this)).subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (event.url === '/feed/circle/fyp' || event.url === '/f/c/fyp') this.navigateToForYou();
      }
    });
  }

  listenNavbarState() {
    this.router.events.pipe(untilDestroyed(this)).subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.showNavbar = !event.url.includes('p/');
      }
    });
  }
}
