import { TestBed } from '@angular/core/testing';

import { ForYouFeedGuard } from './for-you-feed.guard';

describe('ForYouFeedGuard', () => {
  let guard: ForYouFeedGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ForYouFeedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
