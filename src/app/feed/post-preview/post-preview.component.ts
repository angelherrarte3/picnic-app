import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { GetPostByIdGQL, Post, ViewPostByIdGQL } from '@app/graphql/content';
import { cloneDeep } from '@apollo/client/utilities';
import { Location } from '@angular/common';
import { ProfileStateService } from '@app/profile/profile-state.service';
import { PreferencesService } from '@app/services/preferences.service';
import { GoBackNavigation } from '@app/types/go-back-navigation';
import { NavigationService } from '@app/services/navigation.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Meta } from '@angular/platform-browser';
import { environment } from '@env/environment.prod';
import { Subscription } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Title } from '@angular/platform-browser';

@UntilDestroy()
@Component({
  selector: 'app-post-preview',
  templateUrl: './post-preview.component.html',
  styleUrls: ['./post-preview.component.scss'],
  animations: [
    trigger('slideInOut', [
      state(
        'in',
        style({
          transform: 'translate3d(0, 0, 0)',
        })
      ),
      state(
        'out',
        style({
          transform: 'translate3d(100%, 0, 0)',
        })
      ),
      transition('in => out', animate('300ms ease-in-out')),
      transition('out => in', animate('300ms ease-in-out')),
    ]),
  ],
})
export class PostPreviewComponent extends GoBackNavigation implements OnInit {
  postId?: string;
  post?: Post;
  showMobileSidebar: boolean = false;
  loading: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private getPost: GetPostByIdGQL,
    private viewPost: ViewPostByIdGQL,
    private location: Location,
    private router: Router,
    private profileState: ProfileStateService,
    private prefsService: PreferencesService,
    protected override navigationService: NavigationService,
    private meta: Meta,
    private titleService: Title
  ) {
    super(navigationService);
    this.route.paramMap
      .pipe(untilDestroyed(this))
      .subscribe((params: ParamMap) => (this.postId = params.get('id') ?? ''));
  }

  ngOnInit() {
    let req = {};
    if (this.postId?.length === 36) {
      req['postId'] = this.postId;
    } else {
      req['shortId'] = this.postId;
    }

    this.loading = true;
    this.getPost
      .watch(req)
      .valueChanges.pipe(untilDestroyed(this))
      .subscribe(({ data }) => {
        this.post = cloneDeep(data.getPost);
        this.meta.updateTag({
          property: 'og:title',
          content: this.postCaption,
        });
        this.meta.updateTag({
          property: 'og:description',
          content: this.postCaption,
        });
        this.meta.updateTag({
          property: 'og:url',
          content: `${environment.host}/p/${this.postId}`,
        });

        if (this.post.videoContent || this.post.imageContent) {
          this.meta.updateTag({
            property: 'og:image',
            content: this.post.videoContent?.thumbnailUrl ?? this.post.imageContent?.url ?? '',
          });
        }
        const postTitle = this.postCaption || 'Post';
        const separator = ' : ';
        const circleName = this.post.circle?.name || 'Circle';
        const title = postTitle + separator + circleName;
        this.titleService.setTitle(title);
        this.loading = false;
      });

    this.viewPost
      .mutate({
        postId: this.postId ?? '',
      })
      .pipe(untilDestroyed(this))
      .subscribe(({ data }) => {
        if (this.post) this.post.viewsCount++;
      });
  }

  showSidebar = () => (this.showMobileSidebar = true);

  hideSidebar = () => (this.showMobileSidebar = false);

  get postCaption(): string {
    const captionByType = {
      IMAGE: this.post?.imageContent?.text,
      VIDEO: this.post?.title || this.post?.videoContent?.text,
      TEXT: this.post?.textContent?.text,
      LINK: this.post?.linkContent?.linkMetaData?.description,
      POLL: this.post?.pollContent?.question,
      default: undefined,
    };
    return captionByType[this.post?.type ?? 'default'] ?? this.post?.title ?? '';
  }

  get bannerClosed() {
    return this.prefsService.bannerClosed();
  }
}
