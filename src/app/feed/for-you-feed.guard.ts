import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { FeedConnectionGQL } from '@app/graphql/feed';
import { ProfileStateService } from '@app/profile/profile-state.service';
import { Observable, map, of, switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ForYouFeedGuard implements CanActivate {
  constructor(
    private profileState: ProfileStateService,
    private feedConnection: FeedConnectionGQL,
    private router: Router
  ) {}

  canActivate(): Observable<boolean> {
    return this.feedConnection
      .fetch({
        cursor: {
          id: '',
          limit: 1,
        },
      })
      .pipe(
        map(({ data }) => {
          const feedId = data.feedsConnection.edges[0].node.id;
          this.profileState.forYouFeedId.next(feedId);
          return true;
        })
      );
  }
}
