import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { FeedComponent } from '@app/feed/feed.component';
import { CircleFeedComponent } from './circle-feed/circle-feed.component';
import { ForYouFeedGuard } from './for-you-feed.guard';
import { PostPreviewComponent } from './post-preview/post-preview.component';
import { ModerationComponent } from './circle-feed/moderation/moderation.component';
import { QueuesComponent } from './circle-feed/moderation/queues/queues.component';
import { ModmailComponent } from './circle-feed/moderation/modmail/modmail.component';
import { UserManagementComponent } from './circle-feed/moderation/user-management/user-management.component';
import { ModLogComponent } from './circle-feed/moderation/mod-log/mod-log.component';
import { AutomodComponent } from './circle-feed/moderation/automod/automod.component';
import { RulesRemovalReasonsComponent } from './circle-feed/moderation/rules-removal-reasons/rules-removal-reasons.component';
import { ContentControlsComponent } from './circle-feed/moderation/content-controls/content-controls.component';
import { SafetyComponent } from './circle-feed/moderation/safety/safety.component';
import { GeneralSettingsComponent } from './circle-feed/moderation/general-settings/general-settings.component';
import { ModNotificationsComponent } from './circle-feed/moderation/mod-notifications/mod-notifications.component';
import { PostCommentsComponent } from './circle-feed/moderation/post-comments/post-comments.component';
import { RulesComponent } from '@app/feed/circle-feed/rules/rules.component';
import { MembersComponent } from '@app/feed/circle-feed/members/members.component';
import { DiscoverComponent } from './discover/discover.component';
import { MainPageComponent } from '@app/feed/discover/main-page/main-page.component';
import { DiscoverGroupPageComponent } from '@app/feed/discover/discover-group-page/discover-group-page.component';
import { MetaWordsComponent } from './circle-feed/moderation/meta-words/meta-words.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: '',
      component: FeedComponent,
      data: { title: marker('Feed') },
      // canActivate: [ForYouFeedGuard],
      children: [
        {
          path: 'feed/circle/:id',
          component: CircleFeedComponent,
          data: { title: marker('Circle Feed') },
        },
        {
          path: 'f/c/:id',
          component: CircleFeedComponent,
          data: { title: marker('Circle Feed') },
        },
        {
          path: '',
          component: CircleFeedComponent,
          data: { title: marker('Picnic - Dive into communities') },
        },
        {
          path: 'my-circles',
          component: CircleFeedComponent,
          data: { title: marker('Picnic - Dive into communities'), myCircles: true },
        },
        {
          path: 'p/:id',
          component: PostPreviewComponent,
          data: { title: marker('Post') },
        },
        {
          path: 'feed/preview/:id',
          component: PostPreviewComponent,
          data: { title: marker('Post') },
        },
        {
          path: 'feed/circle/:id/members',
          component: MembersComponent,
          data: { title: marker('Members') },
        },
        {
          path: 'feed/circle/:id/rules',
          component: RulesComponent,
          data: { title: marker('Rules') },
        },
        {
          path: 'feed/circle/:id/moderation',
          component: ModerationComponent,
          data: { title: marker('Moderation') },
          children: [
            {
              path: 'queues',
              component: QueuesComponent,
            },
            {
              path: 'modmail',
              component: ModmailComponent,
            },
            {
              path: 'user-management',
              component: UserManagementComponent,
            },
            {
              path: 'mod-log',
              component: ModLogComponent,
            },
            {
              path: 'automod',
              component: AutomodComponent,
            },
            {
              path: 'rules',
              component: RulesRemovalReasonsComponent,
            },
            {
              path: 'content-controls',
              component: ContentControlsComponent,
            },
            {
              path: 'safety',
              component: SafetyComponent,
            },
            {
              path: 'general-settings',
              component: GeneralSettingsComponent,
            },
            {
              path: 'post-comments',
              component: PostCommentsComponent,
            },
            {
              path: 'notifications',
              component: ModNotificationsComponent,
            },
            {
              path: 'meta-words',
              component: MetaWordsComponent,
            },
            {
              path: '',
              redirectTo: 'queues',
              pathMatch: 'full',
            },
          ],
        },
        {
          path: 'feed/discover',
          component: DiscoverComponent,
          data: { title: marker('Discover') },
          children: [
            {
              path: '',
              pathMatch: 'full',
              component: MainPageComponent,
            },
            {
              path: 'group/:id',
              component: DiscoverGroupPageComponent,
            },
          ],
        },
        {
          path: 'd',
          component: DiscoverComponent,
          data: { title: marker('Discover') },
          children: [
            {
              path: '',
              pathMatch: 'full',
              component: MainPageComponent,
            },
            {
              path: ':id',
              component: DiscoverGroupPageComponent,
            },
          ],
        },
      ],
    },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeedRoutingModule {}
