import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CircleFeedComponent } from './circle-feed.component';

describe('CircleFeedComponent', () => {
  let component: CircleFeedComponent;
  let fixture: ComponentFixture<CircleFeedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CircleFeedComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CircleFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
