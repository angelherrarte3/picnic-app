import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { CirclesConnectionGQL, ElectionParticipantEdge, GetCircleByNameGQL, GetMembersGQL } from '@app/graphql/circle';
import { TmpStateService } from '@app/services/tmp-state.service';
import {
  debounce,
  delay,
  distinctUntilChanged,
  filter,
  forkJoin,
  map,
  Observable,
  of,
  Subject,
  Subscription,
} from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
})
export class MembersComponent implements OnInit, OnDestroy {
  private readonly searchSubject = new Subject<string | undefined>();
  private searchSubscription?: Subscription;

  hasNext = false;
  circleId = '';
  searchQuery = '';
  //TODO: refactor - check if user viewing is director

  isDirector = false;

  participants: {
    mods: ElectionParticipantEdge[];
    members: ElectionParticipantEdge[];
  } = { mods: [], members: [] };

  private cursorId = '';

  constructor(
    private route: ActivatedRoute,
    private getCircleByNameGQL: GetCircleByNameGQL,
    private getMembersGQL: GetMembersGQL,
    public state: TmpStateService,
    private router: Router,
    private circlesConnection: CirclesConnectionGQL
  ) {}

  ngOnInit(): void {
    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params: ParamMap) => {
      this.getCircleByNameGQL
        .fetch(
          {
            name: params.get('id')!,
          },
          { errorPolicy: 'all' }
        )
        .subscribe(({ data, errors }) => {
          this.circleId = data.getCircleByName.id;
          this.getMembers();
        });

      this.initSearchSubscription();
    });
  }

  private getIdAndRoutePrefix() {
    const segment = this.route.snapshot.url.map((segment) => segment.path);
    const id = segment[1] || this.route.snapshot.paramMap.get('id') || '';
    const routePrefix = segment[0];
    this.circleId = id;
    return { id, routePrefix };
  }

  initSearchSubscription() {
    this.searchSubscription = this.searchSubject
      .pipe(
        filter(Boolean),
        distinctUntilChanged(),
        debounce((searchQuery) => this.debounceInput(searchQuery))
      )
      .subscribe((searchQuery) => this.getParticipantsByQuery(searchQuery));
  }

  debounceInput(input: string): Observable<string> {
    const debounceTime = input ? 100 : 20;
    return of(input).pipe(delay(debounceTime));
  }

  getMembers(query = '') {
    forkJoin([this.getGQL(['MODERATOR', 'DIRECTOR'], query), this.getGQL(['MEMBER'], query)]).subscribe(
      ([mods, members]) => {
        this.participants = {
          mods: mods.edges,
          members: [],
        };
        this.participants.members.push(...members.edges);
        this.cursorId = members.pageInfo.lastId;
        this.hasNext = members.pageInfo.hasNextPage;
      }
    );
  }

  getGQL(roles: string[], query = '', cursor = '') {
    return this.getMembersGQL
      .fetch({
        searchQuery: query,
        circleId: this.circleId,
        roles: roles,
        cursor: {
          id: cursor,
          limit: 20,
          dir: 'forward',
        },
      })
      .pipe(map(({ data }) => data.getMembers));
  }

  getNextMembers() {
    this.getGQL(['MEMBER'], this.searchQuery, this.cursorId).subscribe((data) => {
      this.participants.members.push(...data.edges);
      this.cursorId = data.pageInfo.lastId;
      this.hasNext = data.pageInfo.hasNextPage;
    });
  }

  getParticipantsByQuery(query: string) {
    this.getMembers(query);
    this.searchQuery = query;
  }

  onSearch(value: string) {
    if (!value) {
      this.getMembers();
      return;
    }

    this.searchSubject.next(value.trim());
  }

  onTapGoProfile(username: string | undefined) {
    this.router.navigate([`/u/${username}`]);
  }

  get mods() {
    return this.participants.mods.filter((m) => m.node.role == 'MODERATOR');
  }

  get members() {
    return this.participants.members;
  }

  get director() {
    return this.participants.mods.find((member) => member.node.role === 'DIRECTOR');
  }

  ngOnDestroy(): void {
    this.searchSubscription?.unsubscribe();
  }
}
