import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Circle, CirclesConnectionGQL, GetCircleByIdGQL } from '@app/graphql/circle';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss'],
})
export class RulesComponent implements OnInit {
  circleId = '';
  circle?: Circle;

  constructor(
    private getCircle: GetCircleByIdGQL,
    private route: ActivatedRoute,
    private circlesConnection: CirclesConnectionGQL
  ) {
    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params: ParamMap) => {
      this.circleId = params.get('id') ?? '';
      this.getCircle
        .watch({ circleId: this.circleId })
        .valueChanges.pipe(untilDestroyed(this))
        .subscribe(({ data }) => {
          this.circle = data.getCircleById;
        });
    });
  }

  ngOnInit(): void {
    console.log(this.getIdAndRoutePrefix().routePrefix);
    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params: ParamMap) => {
      if (this.getIdAndRoutePrefix().routePrefix === 'c') {
        //http://localhost:4200/c/tech_etek
        this.circlesConnection
          .fetch({
            searchQuery: this.getIdAndRoutePrefix().id,
            cursor: {
              id: '',
              limit: 1,
              dir: 'forward',
            },
          })
          .subscribe(({ data }) => {
            data.circlesConnection.edges.map((c) => {
              this.routeToCircleRules(c.node.id);
            });
          });
      } else {
        this.routeToCircleRules(this.getIdAndRoutePrefix().id);
      }
    });
  }

  private getIdAndRoutePrefix() {
    const segment = this.route.snapshot.url.map((segment) => segment.path);
    const id = segment[1] || this.route.snapshot.paramMap.get('id') || '';
    const routePrefix = segment[0];
    this.circleId = id;
    return { id, routePrefix };
  }

  private routeToCircleRules(id: string) {
    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params: ParamMap) => {
      this.circleId = params.get('id') ?? '';
      this.circleId = id;
      this.getCircle.watch({ circleId: this.circleId }).valueChanges.subscribe(({ data }) => {
        this.circle = data.getCircleById;
      });
    });
  }
}
