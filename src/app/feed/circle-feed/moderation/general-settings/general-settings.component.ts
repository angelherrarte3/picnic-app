import { animate, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Circle, UpdateCircleGQL } from '@app/graphql/circle';
import { ModerationStateService } from '../moderation-state.service';
import { TooltipConfig } from '@app/directives/tooltip/tooltip.directive';
import { ActivatedRoute } from '@angular/router';
import { TooltipService } from '@app/services/tooltip.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate('0.2s ease-out', style({ opacity: 1 }))]),
      transition(':leave', [style({ opacity: 1 }), animate('0.2s ease-in', style({ opacity: 0 }))]),
    ]),
    trigger('inOutSizeAnimation', [
      transition(':enter', [style({ height: 0 }), animate('0.2s ease-out', style({ height: 40 }))]),
      transition(':leave', [
        style({ height: 40, opacity: 1 }),
        animate('0.2s ease-in', style({ height: 0, opacity: 0 })),
      ]),
    ]),
  ],
})
export class GeneralSettingsComponent implements OnInit {
  circle?: Circle;
  input: {
    name: string;
    description: string;
    image?: string;
    imageFile?: File;
  } = { name: '', description: '' };
  imageFileSrc?: string;
  changeAvatar = false;
  changeAvatarMode?: 'emoji' | 'file';
  avatarEmojiMode = false;
  tooltipConfig: TooltipConfig = {
    text: 'changes saved!',
    customPosition: { top: 7, left: -128 },
    backgroundColor: 'transparent',
    textColor: '#939292',
    textSize: '15px',
    autoHideTimeout: 1800,
  };
  @ViewChild('saveChangesButtonRef') saveChangesButtonRef!: ElementRef;
  loadingSave: boolean = false;

  constructor(
    private moderationState: ModerationStateService,
    private route: ActivatedRoute,
    private updateCircleGQL: UpdateCircleGQL,
    private tooltip: TooltipService
  ) {}

  ngOnInit(): void {
    let route = this.route.parent;
    if (!route) route = this.route;

    this.moderationState.circleSub.pipe(untilDestroyed(this)).subscribe((circle) => {
      this.circle = circle;
      this.input = {
        name: this.circle?.name || '',
        description: this.circle?.description || '',
      };
    });
  }

  get avatar(): string {
    if (this.input?.imageFile) {
      this.avatarEmojiMode = false;
      return this.imageFileSrc!;
    }
    if (this.input?.image) {
      this.avatarEmojiMode = true;
      return this.input?.image;
    }
    if (this.circle?.imageFile) {
      this.avatarEmojiMode = false;
      return this.circle.imageFile;
    }
    if (this.circle?.image) {
      this.avatarEmojiMode = true;
      return this.circle.image;
    }
    return '';
  }

  onFileClick() {
    let input = document.createElement('input');
    input.type = 'file';
    input.accept = 'image/*';
    input.onchange = (_) => {
      this.changeAvatarMode = 'file';
      this.input!.image = undefined;
      this.input!.imageFile = input.files![0];

      let fr = new FileReader();
      fr.onload = (_) => {
        this.imageFileSrc = fr.result as string;
      };
      fr.readAsDataURL(input.files![0]);
    };
    input.click();
  }

  onSave() {
    if (this.input.name === '' || this.input.description === '') {
      return;
    }

    this.loadingSave = true;
    this.changeAvatar = false;
    this.changeAvatarMode = undefined;
    this.updateCircleGQL
      .mutate({
        circleId: this.circle!.id,
        payload: {
          name: this.input.name,
          description: this.input.description,
          image: this.input.image,
          imageFile: this.input.imageFile,
        },
      })
      .subscribe((_) => {
        this.loadingSave = false;
        if (this.saveChangesButtonRef) {
          this.tooltip.showTooltip(this.saveChangesButtonRef, this.tooltipConfig);
        }
      });
  }

  closeImageChange() {
    this.changeAvatar = false;
    this.input.image = undefined;
    this.input.imageFile = undefined;
    this.changeAvatarMode = undefined;
  }

  onEmojiSelect(emojiData: any) {
    console.log(emojiData.emoji);
    this.input.image = emojiData.emoji.native;
    this.input.imageFile = undefined;
  }

  toggleEmojiMode() {
    if (this.changeAvatarMode === 'emoji') {
      this.changeAvatarMode = undefined;
      return;
    }

    this.changeAvatarMode = 'emoji';
  }
}
