import { Component, OnInit } from '@angular/core';
import { ModerationStateService } from '../moderation-state.service';
import {
  BanUserInCircleGQL,
  Circle,
  CircleReport,
  CircleReportsConnectionGQL,
  ReportType,
  ResolveReportGQL,
} from '@app/graphql/circle';
import { SimpleDropdownItem } from '@app/components/simple-dropdown/simple-dropdown.component';
import { PostCommentMode } from '@app/components/post-comment/post-comment.component';
import { ActivatedRoute } from '@angular/router';
import { GetCommentGQL } from '@app/graphql/comment';
import { GetPostByIdGQL } from '@app/graphql/content';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { cloneDeep } from '@apollo/client/utilities';
import { map } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

export enum CircleReportPostSorting {
  RECENT = 'RECENT',
  IMPORTANT = 'IMPORTANT',
  CRITICAL = 'CRITICAL',
}

interface Report {
  report: CircleReport;
  object?: any;
}

@UntilDestroy()
@Component({
  selector: 'app-queues',
  templateUrl: './queues.component.html',
  styleUrls: ['./queues.component.scss'],
})
export class QueuesComponent extends GQLPaginationComponent implements OnInit {
  circle?: Circle;
  sortItems: SimpleDropdownItem<CircleReportPostSorting>[] = [
    {
      label: 'recent reports',
      value: CircleReportPostSorting.RECENT,
    },
    {
      label: 'new reports',
      value: CircleReportPostSorting.IMPORTANT,
    },
    {
      label: 'critical reports',
      value: CircleReportPostSorting.CRITICAL,
    },
  ];
  sortSelected: CircleReportPostSorting = CircleReportPostSorting.RECENT;
  reports: Report[] = [];
  reportCommentMode: PostCommentMode = PostCommentMode.Report;

  constructor(
    private moderationState: ModerationStateService,
    private route: ActivatedRoute,
    private getPost: GetPostByIdGQL,
    private getComment: GetCommentGQL,
    private circleReportsConnection: CircleReportsConnectionGQL,
    private resolveReportGQL: ResolveReportGQL,
    private banUserGQL: BanUserInCircleGQL
  ) {
    super();
  }

  ngOnInit(): void {
    this.moderationState.circleSub.pipe(untilDestroyed(this)).subscribe((circle) => {
      this.circle = circle;
      this.resetPagination();
      this.loadMoreItems();
    });
  }

  override loadMoreItems() {
    this.circleReportsConnection
      .fetch({
        data: {
          circleId: this.circle?.id ?? '',
          filterBy: 'UNRESOLVED',
          cursor: {
            id: this.cursorId,
            limit: 20,
            dir: 'forward',
          },
        },
      })
      .subscribe(({ data }) => {
        let reportsMap = new Map<
          string,
          {
            reports: Report[];
            type: ReportType;
          }
        >();
        this.reports.push(
          ...data.circleReportsConnection.edges.map((r) => {
            if (reportsMap[r.node.anyId] === undefined) {
              reportsMap.set(r.node.anyId, {
                reports: [],
                type: r.node.reportType,
              });
            }
            let res = {
              report: cloneDeep(r.node),
              object: undefined,
            };
            reportsMap.get(r.node.anyId)?.reports.push(res);
            return res;
          })
        );
        for (const [anyId, r] of reportsMap) {
          let sub;
          switch (r.type) {
            case 'POST':
              sub = this.getPost
                .fetch({ postId: anyId }, { errorPolicy: 'all' })
                .pipe(map(({ data }) => data?.getPost as any));
              break;
            case 'COMMENT':
              sub = this.getComment
                .fetch({ id: anyId }, { errorPolicy: 'all' })
                .pipe(map(({ data }) => data?.getComment as any));
              break;
          }
          sub?.subscribe((obj) => {
            if (!obj) {
              return;
            }
            let o = reportsMap.get(obj.id);
            if (!o) {
              return;
            }
            for (let r of o.reports) {
              r.object = obj;
            }
          });
        }

        this.cursorId = data.circleReportsConnection.pageInfo.lastId;
        this.hasNextPage = data.circleReportsConnection.pageInfo.hasNextPage;
      });
  }

  override resetPagination() {
    super.resetPagination();
    this.reports = [];
  }

  resolveReport(report: Report, action: 'delete' | 'ban' | 'no', userId?: string) {
    this.resolveReportGQL
      .mutate({
        circleId: this.circle!.id,
        reportId: report.report.reportId,
        fullFill: action === 'delete' || action === 'ban',
      })
      .subscribe((_) => {
        report.report.status = 'RESOLVED';
        if (action === 'ban') {
          this.banUserGQL
            .mutate({
              circleId: this.circle!.id,
              userId: userId!,
            })
            .subscribe();
        }
      });
  }

  get emptyReports() {
    return this.reports.length === 0;
  }
}
