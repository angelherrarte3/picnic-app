import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Circle, CircleOption, GetCircleByIdGQL, UpdateCircleGQL } from '@app/graphql/circle';
import { ModerationStateService } from '../moderation-state.service';
import { ActivatedRoute } from '@angular/router';
import { TooltipService } from '@app/services/tooltip.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-content-controls',
  templateUrl: './content-controls.component.html',
  styleUrls: ['./content-controls.component.scss'],
})
export class ContentControlsComponent implements OnInit {
  circle?: Circle;
  @ViewChild('saveChangesButtonRef') saveChangesButtonRef!: ElementRef;

  constructor(
    private moderationState: ModerationStateService,
    private route: ActivatedRoute,
    private updateCircleGQL: UpdateCircleGQL,
    private tooltip: TooltipService
  ) {}

  ngOnInit(): void {
    let route = this.route.parent;
    if (!route) route = this.route;

    this.moderationState.circleSub.pipe(untilDestroyed(this)).subscribe((circle) => {
      this.circle = circle;
    });
  }

  handleToggleItem(item: CircleOption) {
    item = { ...item, value: !item.value };
  }

  handleSave() {
    this.updateCircleGQL
      .mutate({
        circleId: this.circle!.id,
        payload: {
          options: this.circle!.options,
        },
      })
      .subscribe((res) => {
        if (this.saveChangesButtonRef) {
          this.tooltip.showTooltip(this.saveChangesButtonRef, {
            text: 'changes saved!',
            customPosition: { top: 7, left: -128 },
            backgroundColor: 'transparent',
            textColor: '#939292',
            textSize: '15px',
            autoHideTimeout: 1800,
          });
        }
      });
  }
}
