import { Component, OnInit } from '@angular/core';
import { Circle } from '@app/graphql/circle';
import { ModerationStateService } from '../moderation-state.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-rules-removal-reasons',
  templateUrl: './rules-removal-reasons.component.html',
  styleUrls: ['./rules-removal-reasons.component.scss'],
})
export class RulesRemovalReasonsComponent implements OnInit {
  circle?: Circle;

  constructor(private moderationState: ModerationStateService) {}

  ngOnInit(): void {
    this.moderationState.circleSub.pipe(untilDestroyed(this)).subscribe((circle) => {
      this.circle = circle;
    });
  }
}
