import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RulesRemovalReasonsComponent } from './rules-removal-reasons.component';

describe('RulesRemovalReasonsComponent', () => {
  let component: RulesRemovalReasonsComponent;
  let fixture: ComponentFixture<RulesRemovalReasonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RulesRemovalReasonsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(RulesRemovalReasonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
