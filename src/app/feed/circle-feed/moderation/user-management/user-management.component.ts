import { Component, OnInit } from '@angular/core';
import {
  BanUserInCircleGQL,
  Circle,
  ElectionParticipantEdge,
  GetMembersGQL,
  UnbanUserInCircleGQL,
} from '@app/graphql/circle';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { ModerationStateService } from '../moderation-state.service';
import { Subject, debounceTime } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss'],
})
export class UserManagementComponent extends GQLPaginationComponent implements OnInit {
  private readonly searchSub = new Subject<string>();
  private searchQuery = '';

  circle?: Circle;
  members: ElectionParticipantEdge[] = [];
  membersSearch: ElectionParticipantEdge[] = [];
  searchMode = false;
  searchCursorId = '';
  searchHasNextPage = false;

  constructor(
    private moderationState: ModerationStateService,
    private route: ActivatedRoute,
    private getMembersGQL: GetMembersGQL,
    private unbanUserInCircleGQL: UnbanUserInCircleGQL,
    private banUserInCircleGQL: BanUserInCircleGQL
  ) {
    super();
  }

  ngOnInit(): void {
    let route = this.route.parent;
    if (!route) route = this.route;

    this.moderationState.circleSub.pipe(untilDestroyed(this)).subscribe((circle) => {
      this.circle = circle;
      this.loadMoreItems();
    });

    this.searchSub.pipe(debounceTime(300), untilDestroyed(this)).subscribe((searchQuery) => {
      this.searchQuery = searchQuery;
      this.loadMoreItemsSearch(false);
    });
  }

  override loadMoreItems() {
    this.getMembersGQL
      .fetch({
        circleId: this.circle?.id,
        isBanned: true,
        roles: [],
        cursor: {
          id: this.cursorId,
          limit: 20,
          dir: 'forward',
        },
      })
      .subscribe(({ data }) => {
        this.members.push(...data.getMembers.edges);
        this.cursorId = data.getMembers.pageInfo.lastId;
        this.hasNextPage = data.getMembers.pageInfo.hasNextPage;
      });
  }

  override resetPagination() {
    super.resetPagination();
    this.members = [];
  }

  onUnban(m: ElectionParticipantEdge) {
    this.unbanUserInCircleGQL
      .mutate({
        userId: m.node.user.id,
        circleId: this.circle?.id ?? '',
      })
      .subscribe((_) => {
        this.members.splice(
          this.members.findIndex((v) => v.node.user.id === v.node.user.id),
          1
        );
      });
  }

  //Search mode
  loadMoreItemsSearch(fromPagination: boolean) {
    this.getMembersGQL
      .fetch({
        circleId: this.circle?.id,
        isBanned: false,
        searchQuery: this.searchQuery,
        roles: [],
        cursor: {
          id: this.searchCursorId,
          limit: 20,
          dir: 'forward',
        },
      })
      .subscribe(({ data }) => {
        if (fromPagination) this.membersSearch.push(...data.getMembers.edges);
        else this.membersSearch = data.getMembers.edges;
        this.searchCursorId = data.getMembers.pageInfo.lastId;
        this.searchHasNextPage = data.getMembers.pageInfo.hasNextPage;
      });
  }

  onBan(member: ElectionParticipantEdge) {
    this.banUserInCircleGQL
      .mutate({
        userId: member.node.user.id,
        circleId: this.circle?.id ?? '',
      })
      .subscribe(() => {
        this.members.push(member);
        this.hideSearchMode();
      });
  }

  onSearch = (value: string) => this.searchSub.next(value);

  showSearchMode() {
    this.searchMode = true;
    this.loadMoreItemsSearch(false);
  }

  hideSearchMode() {
    this.searchMode = false;
    this.searchCursorId = '';
    this.searchHasNextPage = false;
    this.searchQuery = '';
    this.membersSearch = [];
  }
}
