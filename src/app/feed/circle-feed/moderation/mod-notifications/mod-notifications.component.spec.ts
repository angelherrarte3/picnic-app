import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModNotificationsComponent } from './mod-notifications.component';

describe('ModNotificationsComponent', () => {
  let component: ModNotificationsComponent;
  let fixture: ComponentFixture<ModNotificationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModNotificationsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ModNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
