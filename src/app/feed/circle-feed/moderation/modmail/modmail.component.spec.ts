import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModmailComponent } from './modmail.component';

describe('ModmailComponent', () => {
  let component: ModmailComponent;
  let fixture: ComponentFixture<ModmailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModmailComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ModmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
