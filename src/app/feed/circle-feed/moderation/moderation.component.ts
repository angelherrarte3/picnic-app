import { Component, HostListener, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SettingItem } from '@app/components/settings-list/settings-list.component';
import { Circle, CirclesConnectionGQL, GetCircleByIdGQL } from '@app/graphql/circle';
import { ModerationStateService } from './moderation-state.service';

@Component({
  selector: 'app-moderation',
  templateUrl: './moderation.component.html',
  styleUrls: ['./moderation.component.scss'],
})
export class ModerationComponent implements OnInit {
  moderationSettings: SettingItem[] = [
    {
      label: 'queues',
      path: 'queues',
      iconName: 'queque',
    },
    {
      label: 'modmail',
      path: 'modmail',
      iconName: 'modmail',
      buildingFeature: true,
    },
    {
      label: 'user management',
      path: 'user-management',
      iconName: 'user-management',
    },
    {
      label: 'mod log',
      path: 'mod-log',
      iconName: 'mod-log',
      buildingFeature: true,
    },
    {
      label: 'automod',
      path: 'automod',
      iconName: 'automod',
      buildingFeature: true,
    },
    {
      label: 'rules & removal reasons',
      path: 'rules',
      iconName: 'rules',
    },
    {
      label: 'content controls',
      path: 'content-controls',
      iconName: 'content-controls',
    },
    {
      label: 'safety',
      path: 'safety',
      iconName: 'safety',
      buildingFeature: true,
    },
    {
      label: 'meta words',
      path: 'meta-words',
      iconName: 'mod-log',
    },
  ];
  otherSettings: SettingItem[] = [
    {
      label: 'general settings',
      path: 'general-settings',
      iconName: 'general-settings',
    },
    {
      label: 'post & comments',
      path: 'post-comments',
      iconName: 'posts-comments',
      buildingFeature: true,
    },
    {
      label: 'notifications',
      path: 'notifications',
      iconName: 'notification',
      buildingFeature: true,
    },
  ];

  circle?: Circle;
  showSidebar = false;
  selectedSetting?: SettingItem;

  windowWidth = window.innerWidth;
  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.windowWidth = event.target.innerWidth;
    this.isMobile ? (this.showSidebar = false) : (this.showSidebar = true);
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private circlesConnection: CirclesConnectionGQL,
    private moderationState: ModerationStateService
  ) {}

  ngOnInit(): void {
    this.initializeCircle();
    this.isMobile ? (this.showSidebar = false) : (this.showSidebar = true);
    this.selectedSetting = this.moderationSettings[0];
  }

  handleItemSelected(item: SettingItem) {
    this.selectedSetting = item;
    this.router.navigate([item.path], { relativeTo: this.route });
    if (this.isMobile) this.toggleSidebar();
  }

  goBack() {
    this.router.navigateByUrl(`/c/${this.circle?.urlName ? this.circle.urlName : this.circle?.name}`);
  }

  toggleSidebar = () => (this.showSidebar = !this.showSidebar);

  initializeCircle() {
    const segment = this.route.snapshot.url.map((segment) => segment.path);
    const circleName = segment[1] || this.route.snapshot.paramMap.get('id') || '';

    this.circlesConnection
      .fetch({
        searchQuery: circleName,
        cursor: {
          id: '',
          limit: 1,
          dir: 'forward',
        },
      })
      .subscribe(({ data }) => {
        data.circlesConnection.edges.map((c) => {
          this.circle = c.node;
          this.moderationState.setCircle(c.node);
          this.moderationSettings[0].notificationCount = this.circle?.reportsCount;
        });
      });
  }

  get isMobile() {
    return this.windowWidth <= 768;
  }
}
