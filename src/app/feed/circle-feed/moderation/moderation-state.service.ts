import { Injectable } from '@angular/core';
import { Circle } from '@app/graphql/circle';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ModerationStateService {
  public circleSub = new BehaviorSubject<Circle | undefined>(undefined);

  constructor() {}

  setCircle(circle: Circle) {
    this.circleSub.next(circle);
  }
}
