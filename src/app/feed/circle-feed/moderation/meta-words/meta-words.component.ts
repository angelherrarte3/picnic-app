import { Component, OnInit } from '@angular/core';
import { Circle, UpdateCircleGQL } from '@app/graphql/circle';
import { ModerationStateService } from '../moderation-state.service';
import { ActivatedRoute } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-meta-words',
  templateUrl: './meta-words.component.html',
  styleUrls: ['./meta-words.component.scss'],
})
export class MetaWordsComponent implements OnInit {
  circle?: Circle;
  newMetaWord: string = '';

  constructor(
    private moderationState: ModerationStateService,
    private route: ActivatedRoute,
    private updateCircleGQL: UpdateCircleGQL
  ) {}

  ngOnInit(): void {
    let route = this.route.parent;
    if (!route) route = this.route;

    this.moderationState.circleSub.pipe(untilDestroyed(this)).subscribe((circle) => {
      this.circle = circle;
    });
  }

  handleAdd() {
    if (this.newMetaWord && this.circle && !this.circle.metaWords.includes(this.newMetaWord)) {
      const newMetaWords = [...this.circle.metaWords, this.newMetaWord];
      this.saveMetaWords(newMetaWords);
      this.newMetaWord = '';
    }
  }

  handleDelete(word: string) {
    if (!this.circle) return;
    const newMetaWords = this.circle.metaWords.filter((w: string) => w !== word);
    if (newMetaWords.length === this.circle.metaWords.length) return;
    this.saveMetaWords(newMetaWords);
  }

  private saveMetaWords(metaWords: string[]) {
    if (this.circle) {
      metaWords.sort();
      this.updateCircleGQL
        .mutate({
          circleId: this.circle!.id,
          payload: {
            metaWords: metaWords,
          },
        })
        .subscribe((result) => {
          if (result.data?.updateCircle?.metaWords) {
            const newCircle: Circle = {
              ...this.circle!,
              metaWords: result.data!.updateCircle!.metaWords,
            };
            this.moderationState.setCircle(newCircle);
          }
        });
    }
  }
}
