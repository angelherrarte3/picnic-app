import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetaWordsComponent } from './meta-words.component';

describe('MetaWordsComponent', () => {
  let component: MetaWordsComponent;
  let fixture: ComponentFixture<MetaWordsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaWordsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaWordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
