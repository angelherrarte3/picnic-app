import { TestBed } from '@angular/core/testing';

import { ModerationStateService } from './moderation-state.service';

describe('ModerationStateService', () => {
  let service: ModerationStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModerationStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
