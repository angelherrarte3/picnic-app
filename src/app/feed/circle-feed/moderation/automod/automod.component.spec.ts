import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomodComponent } from './automod.component';

describe('AutomodComponent', () => {
  let component: AutomodComponent;
  let fixture: ComponentFixture<AutomodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AutomodComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AutomodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
