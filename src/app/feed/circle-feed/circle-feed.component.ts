import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  Inject,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { GetFeedGQL, Post, PostEdge, SortedCirclePostsConnectionGQL } from '@app/graphql/content';
import { ProfileStateService } from '@app/profile/profile-state.service';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { Circle, CirclesConnectionGQL, GetCircleByIdGQL, GetCircleByNameGQL } from '@app/graphql/circle';
import { CirclePostSorting } from '@app/circle/circle.component';
import { FeedPostsConnectionGQL } from '@app/graphql/feed';
import { Subscription, fromEvent } from 'rxjs';
import { auditTime } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';
import { TmpStateService } from '@app/services/tmp-state.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-circle-feed',
  templateUrl: './circle-feed.component.html',
  styleUrls: ['./circle-feed.component.scss'],
})
export class CircleFeedComponent extends GQLPaginationComponent implements OnInit, AfterViewInit {
  @ViewChild('anchor') anchor: ElementRef;
  private scrollSubscription?: Subscription;
  feedId: string = '';
  circleId: string = '';
  posts: Post[] = [];
  isForYouPage = false;
  override hasNextPage = true;
  loading = false;
  myCircles: boolean;

  circle?: Circle;
  postSorting: CirclePostSorting = CirclePostSorting.TRENDING_THIS_MONTH;
  postSortingOptions = [
    CirclePostSorting.TRENDING_THIS_MONTH,
    CirclePostSorting.TRENDING_THIS_WEEK,
    CirclePostSorting.NEW,
    CirclePostSorting.POPULARITY_ALL_TIME,
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sortedCirclePostsConnectionGQL: SortedCirclePostsConnectionGQL,
    private profileState: ProfileStateService,
    private getCircle: GetCircleByIdGQL,
    private postsGQL: FeedPostsConnectionGQL,
    @Inject(PLATFORM_ID) private platformId: string,
    private circlesConnection: CirclesConnectionGQL,
    private getCircleByNameGQL: GetCircleByNameGQL,
    private getFeedGQL: GetFeedGQL,
    private tmpState: TmpStateService
  ) {
    super();
  }

  ngAfterViewInit() {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    this.loading = true;
    new IntersectionObserver(
      ([entry]) => {
        if (entry.isIntersecting) {
          this.isForYouPage ? this.loadMoreForYouFeedItems() : this.loadMoreItems();
        }
      },
      { threshold: 0.1 }
    ).observe(this.anchor.nativeElement);
  }

  private setLoading(loading: boolean) {
    if (loading) {
      this.loadingNext = true;
    } else {
      this.loading = false;
      this.loadingNext = false;
    }
  }

  get emptyPostsCheck() {
    return this.posts.length === 0;
  }

  valueToDisplay(dsp: CirclePostSorting) {
    switch (dsp) {
      case CirclePostSorting.POPULARITY_ALL_TIME:
        return `🔥 popular all time`;
      case CirclePostSorting.NEW:
        return `🆕 new `;
      case CirclePostSorting.TRENDING_THIS_WEEK:
        return `⏰ trending this week`;
      case CirclePostSorting.TRENDING_THIS_MONTH:
        return `📅 trending this month`;
      default:
        return '';
    }
  }

  ngOnInit(): void {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    this.route.data.subscribe((d) => {
      this.myCircles = d['myCircles'];
      this.initialLoad();
    });
    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params: ParamMap) => {
      this.feedId = params.get('id') ?? '';
      this.initialLoad();
    });
  }

  initialLoad() {
    this.reInitiate();

    if (this.router.url === '/' || this.router.url.length === 0 || this.router.url === '/my-circles') {
      this.isForYouPage = true;
      this.profileState.forYouFeedId.pipe(untilDestroyed(this)).subscribe((f) => {
        this.feedId = f;
        if (!this.loading) {
          this.loadMoreForYouFeedItems();
        }
      });
    } else {
      this.isForYouPage = false;
      if (this.getIdAndRoutePrefix().routePrefix === 'circle') {
        this.loadMoreItems(true);
        this.watchCircleDataChanges();
      } else {
        this.getCircleByNameGQL
          .fetch({
            name: this.feedId,
          })
          .subscribe(({ data }) => {
            this.circleId = data.getCircleByName.id;
            this.circle = data.getCircleByName;
            this.watchCircleDataChanges();
            this.loadMoreItems(true);
          });
      }
    }
  }

  private reInitiate() {
    this.posts = [];
    this.circle = undefined;
    this.cursorId = '';
    this.hasNextPage = true;
    this.circleId = '';
    this.feedId = '';
    this.postSorting = CirclePostSorting.TRENDING_THIS_MONTH;
  }

  private watchCircleDataChanges() {
    this.getCircle
      .watch({ circleId: this.circleId })
      .valueChanges.pipe(untilDestroyed(this))
      .subscribe(({ data }) => {
        this.circle = data.getCircleById;
      });
  }

  private getIdAndRoutePrefix() {
    const segment = this.route.snapshot.url.map((segment) => segment.path);
    const id = this.route.snapshot.paramMap.get('id') || segment[1] || '';
    const routePrefix = segment[0];
    return { id, routePrefix };
  }

  //loads circle feed items
  //this is the function that is called when the user scrolls to the bottom of the page
  //this method loads more circle posts
  //The loadNextPage method sets isLoading to true to show the loading indicator, fetches the next page of data, and adds it to the items array. When the data is loaded, isLoading is set to false to hide the loading indicator.
  //By using this approach, the next page will only be loaded when the first page is complete and the user has scrolled to the bottom of the list.
  loadMoreItems(initialLoad: boolean = false): void {
    if (!this.hasNextPage) return;

    this.setLoading(true);
    this.sortedCirclePostsConnectionGQL
      .fetch({
        circleId: this.circleId,
        cursor: {
          id: this.cursorId,
          limit: 10,
          dir: 'forward',
        },
        sortingType: this.postSorting.toString(),
      })
      .subscribe(({ data }) => {
        this.posts = initialLoad
          ? data.sortedCirclePostsConnection.edges.map((v) => v.node)
          : [...this.posts, ...data.sortedCirclePostsConnection.edges.map((v) => v.node)];
        this.hasNextPage = data.sortedCirclePostsConnection.pageInfo.hasNextPage;
        this.cursorId = this.hasNextPage ? data.sortedCirclePostsConnection.pageInfo.lastId : this.cursorId;

        this.setLoading(false);
      });
  }

  private readonly _limit = 10;

  loadMoreForYouFeedItems(initialLoad: boolean = false): void {
    if (!this.myCircles) {
      this.setLoading(true);
      this.getFeedGQL
        .fetch(
          {
            limit: this._limit,
          },
          { fetchPolicy: 'network-only' }
        )
        .subscribe(({ data }) => {
          this.posts = initialLoad
            ? data.getFeed.edges.map((v) => v.node)
            : [...this.posts, ...data.getFeed.edges.map((v) => v.node)];

          this.setLoading(false);
        });
    } else {
      if (!this.hasNextPage) return; // If there are no more pages, return
      this.setLoading(true);
      this.postsGQL
        .fetch({
          feedId: this.feedId,
          cursor: {
            id: this.cursorId ?? '',
            limit: this._limit,
            dir: 'forward',
          },
        })
        .subscribe(({ data }) => {
          this.posts = initialLoad
            ? data.feedPostsConnection.edges.map((v) => v.node)
            : [...this.posts, ...data.feedPostsConnection.edges.map((v) => v.node)];
          this.hasNextPage = data.feedPostsConnection.pageInfo.hasNextPage;
          this.cursorId = this.hasNextPage ? data.feedPostsConnection.pageInfo.lastId : this.cursorId;

          this.setLoading(false);
        });
    }
  }

  //sorting is only used for circle posts (not for you feed)
  onTapSelectSorting(sorting: CirclePostSorting) {
    this.postSorting = sorting;
    this.cursorId = '';
    this.hasNextPage = true;
    this.loadMoreItems(true);
  }
}
