import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeedRoutingModule } from './feed-routing.module';
import { FeedComponent } from './feed.component';
import { ComponentsModule } from '@app/components/components.module';
import { CircleFeedComponent } from './circle-feed/circle-feed.component';
import { PostPreviewComponent } from './post-preview/post-preview.component';
import { PipesModule } from '@app/pipes/pipes.module';
import { MembersComponent } from './circle-feed/members/members.component';
import { RulesComponent } from './circle-feed/rules/rules.component';
import { ModerationComponent } from './circle-feed/moderation/moderation.component';
import { QueuesComponent } from './circle-feed/moderation/queues/queues.component';
import { ModmailComponent } from './circle-feed/moderation/modmail/modmail.component';
import { UserManagementComponent } from './circle-feed/moderation/user-management/user-management.component';
import { ModLogComponent } from './circle-feed/moderation/mod-log/mod-log.component';
import { AutomodComponent } from './circle-feed/moderation/automod/automod.component';
import { RulesRemovalReasonsComponent } from './circle-feed/moderation/rules-removal-reasons/rules-removal-reasons.component';
import { ContentControlsComponent } from './circle-feed/moderation/content-controls/content-controls.component';
import { SafetyComponent } from './circle-feed/moderation/safety/safety.component';
import { GeneralSettingsComponent } from './circle-feed/moderation/general-settings/general-settings.component';
import { PostCommentsComponent } from './circle-feed/moderation/post-comments/post-comments.component';
import { ModNotificationsComponent } from './circle-feed/moderation/mod-notifications/mod-notifications.component';
import { FormsModule } from '@angular/forms';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { DiscoverComponent } from './discover/discover.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MainPageComponent } from './discover/main-page/main-page.component';
import { DiscoverGroupPageComponent } from './discover/discover-group-page/discover-group-page.component';
import { MetaWordsComponent } from './circle-feed/moderation/meta-words/meta-words.component';

@NgModule({
  declarations: [
    FeedComponent,
    CircleFeedComponent,
    PostPreviewComponent,
    MembersComponent,
    RulesComponent,
    ModerationComponent,
    QueuesComponent,
    ModmailComponent,
    UserManagementComponent,
    ModLogComponent,
    AutomodComponent,
    RulesRemovalReasonsComponent,
    ContentControlsComponent,
    SafetyComponent,
    GeneralSettingsComponent,
    PostCommentsComponent,
    ModNotificationsComponent,
    DiscoverComponent,
    MainPageComponent,
    DiscoverGroupPageComponent,
    MetaWordsComponent,
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    FeedRoutingModule,
    PipesModule,
    FormsModule,
    PickerModule,
    InfiniteScrollModule,
  ],
})
export class FeedModule {}
