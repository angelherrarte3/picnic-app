import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscoverGroupPageComponent } from './discover-group-page.component';

describe('DiscoverGroupPageComponent', () => {
  let component: DiscoverGroupPageComponent;
  let fixture: ComponentFixture<DiscoverGroupPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DiscoverGroupPageComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DiscoverGroupPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
