import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Circle, CirclesConnectionGQL } from '@app/graphql/circle';
import { DiscoverStateService } from '@app/feed/discover/discover-state.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-discover-group-page',
  templateUrl: './discover-group-page.component.html',
  styleUrls: ['./discover-group-page.component.scss'],
})
export class DiscoverGroupPageComponent implements OnInit {
  loading = false;
  cursorId = '';
  groupId = '';
  hasNext = false;

  circles: Circle[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private circlesConnection: CirclesConnectionGQL,
    public discoverState: DiscoverStateService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((p) => {
      this.groupId = p.get('id') ?? '';
      this.loadCircles();
    });

    this.route.queryParamMap.pipe(untilDestroyed(this)).subscribe((p) => {
      this.cursorId = p.get('cursorId') ?? '';
      this.loadCircles();
    });
  }

  loadCircles() {
    this.loading = true;
    this.circlesConnection
      .fetch({
        cursor: {
          id: this.cursorId,
          limit: 8,
          dir: 'forward',
        },
        groupId: this.groupId,
      })
      .subscribe(({ data }) => {
        this.circles = data.circlesConnection.edges.map((v) => v.node);
        this.cursorId = data.circlesConnection.pageInfo.lastId;
        this.hasNext = data.circlesConnection.pageInfo.hasNextPage;
        this.loading = false;
      });
  }

  next() {
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        cursorId: this.cursorId,
      },
    });
  }
}
