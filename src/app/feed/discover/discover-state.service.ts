import { Injectable } from '@angular/core';
import { Group } from '@app/graphql/circle';

@Injectable({
  providedIn: 'root',
})
export class DiscoverStateService {
  currentGroup?: Group;
  constructor() {}
}
