import { AfterViewInit, Component, ElementRef, Inject, OnDestroy, OnInit, PLATFORM_ID, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Circle, CircleEdge, CirclesConnectionGQL, Group, ListGroupsGQL } from '@app/graphql/circle';
import { DiscoverStateService } from '@app/feed/discover/discover-state.service';
import { PostEdge } from '@app/graphql/content';
import { PublicProfileEdge, UserConnectionGQL } from '@app/graphql/profile';
import { Subject, Subscription, debounceTime, distinctUntilChanged, forkJoin, map, switchMap } from 'rxjs';
import { cloneDeep } from '@apollo/client/utilities';
import groupsList from '../../../../cache/discovery_groups.json';
import groupsListStage from '../../../../cache/discovery_groups_stage.json';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { environment } from '@env/environment.prod';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit, OnDestroy, AfterViewInit {
  loading: boolean = true;
  groups: {
    group: Group;
    circles: Circle[];
    cursorId: string;
    hasNext: boolean;
  }[] = [];
  loadingSearchResults: boolean = false;
  searchResults: {
    apps: [];
    circles: CircleEdge[];
    users: PublicProfileEdge[];
    posts: PostEdge[];
  } = { apps: [], circles: [], users: [], posts: [] };
  page = 0;

  list: any;

  @ViewChild('anchor') anchor: ElementRef;

  private readonly searchSubject = new Subject<string>();
  private searchSubscription?: Subscription;
  private searchLoadingSubscription?: Subscription;

  constructor(
    private listGroups: ListGroupsGQL,
    private circlesConnection: CirclesConnectionGQL,
    private route: ActivatedRoute,
    private router: Router,
    private discoverState: DiscoverStateService,
    private userConnection: UserConnectionGQL,
    @Inject(PLATFORM_ID) private platformId: string
  ) {}

  ngOnInit(): void {
    this.list = environment.production ? groupsList : groupsListStage;
    this.route.queryParamMap.pipe(untilDestroyed(this)).subscribe((p) => {
      this.page = +(p.get('p') ?? 1) - 1;
      this.groups = [];
      this.loadGroups();
    });
    if (!isPlatformServer(this.platformId)) {
      this.initSearchFeature();
    }
  }

  ngAfterViewInit() {
    if (isPlatformBrowser(this.platformId)) {
      new IntersectionObserver(
        ([entry]) => {
          if (entry.isIntersecting) {
            this.loadGroups();
          }
        },
        { threshold: 0.1 }
      ).observe(this.anchor.nativeElement);
    }
  }

  loadGroups() {
    if (!this.hasNext) {
      return;
    }
    let slice = this.list.data.listGroups.edges.slice(this.page * 3, this.page * 3 + 3);
    for (let g of slice) {
      let group = {
        group: g.node,
        circles: [] as Circle[],
        cursorId: '',
        hasNext: false,
      };

      this.circlesConnection
        .fetch({
          cursor: {
            id: '',
            limit: 8,
            dir: 'forward',
          },
          groupId: g.node.groupId,
        })
        .subscribe(({ data }) => {
          group.circles = data.circlesConnection.edges.map((v) => v.node);
          group.cursorId = data.circlesConnection.pageInfo.lastId;
          group.hasNext = data.circlesConnection.pageInfo.hasNextPage;
          this.loading = false;
        });

      this.groups.push(group);
    }
    this.page++;
  }

  initSearchFeature() {
    this.searchLoadingSubscription = this.searchSubject.pipe(distinctUntilChanged()).subscribe(() => {
      this.loadingSearchResults = true;
    });
    this.searchSubscription = this.searchSubject
      .pipe(
        debounceTime(300),
        switchMap((searchQuery) => {
          return forkJoin([
            this.circlesConnection
              .fetch({
                searchQuery: searchQuery,
                cursor: {
                  id: '',
                  limit: 5,
                  dir: 'forward',
                },
              })
              .pipe(map(({ data }) => cloneDeep(data.circlesConnection.edges))),
            this.userConnection
              .fetch({
                searchQuery: searchQuery,
                cursor: {
                  id: '',
                  limit: 20,
                  dir: 'forward',
                },
              })
              .pipe(map(({ data }) => data.usersConnection.edges)),
            // TODO: Fetch posts from the server
          ]);
        })
      )
      .subscribe((results) => {
        this.searchResults = {
          apps: [],
          circles: results[0],
          users: results[1],
          posts: [],
          // TODO: Fetch posts from the server
        };
        this.loadingSearchResults = false;
      });
  }

  navigateToGroup(g: any) {
    this.discoverState.currentGroup = g.group;
    this.router.navigate(['./', g.group.groupId], {
      relativeTo: this.route,
      queryParams: {
        cursorId: g.cursorId,
      },
    });
  }

  handleOnSearch = (searchQuery: string) => this.searchSubject.next(searchQuery.trim());

  ngOnDestroy(): void {
    this.searchSubscription?.unsubscribe();
    this.searchLoadingSubscription?.unsubscribe();
  }

  get hasNext() {
    return this.list.data.listGroups.edges.length > this.page * 3;
  }

  get hasPrev() {
    return this.page > 0;
  }

  onNext() {
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        p: this.page + 1,
      },
    });
  }

  onPrev() {
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        p: this.page - 1,
      },
    });
  }
}
