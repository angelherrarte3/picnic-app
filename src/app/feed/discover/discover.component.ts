import { trigger, transition, style, animate } from '@angular/animations';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CircleEdge, CirclesConnectionGQL, Group } from '@app/graphql/circle';
import { UserConnectionGQL } from '@app/graphql/profile';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate('150ms ease-out', style({ opacity: 1 }))]),
      transition(':leave', [style({ opacity: 1 }), animate('150ms ease-in', style({ opacity: 0 }))]),
    ]),
  ],
  encapsulation: ViewEncapsulation.None,
})
export class DiscoverComponent implements OnInit {
  groups: {
    group: Group;
    circles: CircleEdge[];
  }[] = [];

  constructor(private circlesConnection: CirclesConnectionGQL, private userConnection: UserConnectionGQL) {}

  ngOnInit(): void {}
}
