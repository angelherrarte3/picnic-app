import { Component, OnInit } from '@angular/core';
import { TmpStateService } from '@app/services/tmp-state.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PreferencesService } from '@app/services/preferences.service';
import { CredentialsService } from '@app/auth';

import { ChatSidebarService } from '@app/services/chat-sidebar.service';
import { FeedSidebarService } from '@app/services/feed-sidebar.service';
import { GlobalModalService } from '@app/services/global-modal.service';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
})
export class ShellComponent implements OnInit {
  bannerClosed = false;

  constructor(
    private state: TmpStateService,
    private prefsService: PreferencesService,
    public credentialsService: CredentialsService,
    private modalService: GlobalModalService,
    private router: Router,
    public chatSidebarService: ChatSidebarService,
    public feedSidebarService: FeedSidebarService
  ) {
    this.bannerClosed = this.prefsService.bannerClosed();
  }

  ngOnInit() {
    this.state.updateProfile();
  }

  onBannerClose() {
    this.bannerClosed = true;
    this.prefsService.setBannerClosed(true);
  }

  get isShowingSidebar() {
    return !this.router.url.match(/\/(c|circle)\/(.*)\/(settings|moderation)\/*/);
  }

  get isGuestUser() {
    return !this.credentialsService.isAuthenticated();
  }

  get isVisbleFeedSidebar() {
    return this.feedSidebarService.isVisible;
  }

  showSignUpModal = () => this.modalService.open('sign-up');

  onTouchStartFeed() {
    this.chatSidebarService.hide();
    if (this.isVisbleFeedSidebar) {
      this.feedSidebarService.hide();
    }
  }
}
