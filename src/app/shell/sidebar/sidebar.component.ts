import { Component, OnInit } from '@angular/core';
import { CredentialsService } from '@app/auth';
import { TmpStateService } from '@app/services/tmp-state.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  isAuthenticated: boolean = false;

  constructor(public state: TmpStateService, public credentials: CredentialsService) {}

  ngOnInit(): void {
    this.isAuthenticated = this.credentials.isAuthenticated();
  }
}
