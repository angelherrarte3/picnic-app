import { NgModule } from '@angular/core';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { ApolloClientOptions, InMemoryCache } from '@apollo/client/core';
import { HttpLink } from 'apollo-angular/http';
import { createUploadLink } from 'apollo-upload-client';
import { setContext } from '@apollo/client/link/context';
import { environment } from '@env/environment';
import { CredentialsService } from './auth';
import { makeStateKey, TransferState } from '@angular/platform-browser';

const STATE_KEY = makeStateKey<any>('apollo.state');

export function createApollo(
  httpLink: HttpLink,
  transferState: TransferState,
  credentialsService: CredentialsService
): ApolloClientOptions<any> {
  const authLink = setContext((_, { headers }) => {
    let token: string | undefined;
    let creds = credentialsService.credentials;
    if (creds) {
      token = creds.token;
    }

    headers = {
      ...headers,
    };

    if (token) {
      headers['Authorization'] = `Bearer ${token}`;
    }

    return {
      headers,
    };
  });

  const isBrowser = transferState.hasKey<any>(STATE_KEY);

  let link;
  if (isBrowser) {
    link = authLink.concat(createUploadLink({ uri: environment.serverUrl }));
  } else {
    link = authLink.concat(httpLink.create({ uri: environment.serverUrl }));
  }

  return {
    link: link,
    cache: new InMemoryCache(),
  };
}

@NgModule({
  exports: [ApolloModule],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink, TransferState, CredentialsService],
    },
  ],
})
export class GraphQLModule {}
