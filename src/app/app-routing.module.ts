import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostPreviewComponent } from '@app/feed/post-preview/post-preview.component';
import { ChatStandaloneComponent } from './chats/chat-standalone/chat-standalone.component';

const routes: Routes = [
  { path: 'post/:id', component: PostPreviewComponent },
  { path: 'chat/:id', component: ChatStandaloneComponent },
  { path: '**', redirectTo: '404', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabledBlocking',
    }),
  ],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
