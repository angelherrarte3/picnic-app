import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { CircleComponent } from '@app/circle/circle.component';
import { RulesComponent } from '@app/feed/circle-feed/rules/rules.component';
import { ModComponent } from '@app/circle/mod/mod.component';
import { PostsComponent } from '@app/circle/posts/posts.component';
import { MembersComponent } from '@app/feed/circle-feed/members/members.component';
import { CircleSettingsComponent } from './circle-settings/circle-settings.component';
import { ReportCircleComponent } from './circle-settings/report-circle/report-circle.component';
import { InviteCircleComponent } from './circle-settings/invite-circle/invite-circle.component';
import { BanUserCircleComponent } from './circle-settings/ban-user-circle/ban-user-circle.component';
import { BlacklistedWordsComponent } from './circle-settings/blacklisted-words/blacklisted-words.component';
import { EditCircleComponent } from './circle-settings/edit-circle/edit-circle.component';
import { CircleConfigComponent } from './circle-settings/circle-config/circle-config.component';
import { SeedsOwnershipComponent } from './circle-settings/seeds-ownership/seeds-ownership.component';
import { PushNotificationsComponent } from './circle-settings/push-notifications/push-notifications.component';
import { CircleRolesComponent } from './circle-settings/circle-roles/circle-roles.component';
import { PrivacyDiscoverabilityComponent } from './circle-settings/privacy-discoverability/privacy-discoverability.component';
import { PostReportsComponent } from './circle-settings/report-circle/post-reports/post-reports.component';
import { CommentReportsComponent } from './circle-settings/report-circle/comment-reports/comment-reports.component';
import { ChatReportsComponent } from './circle-settings/report-circle/chat-reports/chat-reports.component';
import { UserReportsComponent } from './circle-settings/report-circle/user-reports/user-reports.component';
import { AutomodComponent } from '@app/feed/circle-feed/moderation/automod/automod.component';
import { ContentControlsComponent } from '@app/feed/circle-feed/moderation/content-controls/content-controls.component';
import { GeneralSettingsComponent } from '@app/feed/circle-feed/moderation/general-settings/general-settings.component';
import { ModLogComponent } from '@app/feed/circle-feed/moderation/mod-log/mod-log.component';
import { ModNotificationsComponent } from '@app/feed/circle-feed/moderation/mod-notifications/mod-notifications.component';
import { ModerationComponent } from '@app/feed/circle-feed/moderation/moderation.component';
import { ModmailComponent } from '@app/feed/circle-feed/moderation/modmail/modmail.component';
import { PostCommentsComponent } from '@app/feed/circle-feed/moderation/post-comments/post-comments.component';
import { QueuesComponent } from '@app/feed/circle-feed/moderation/queues/queues.component';
import { RulesRemovalReasonsComponent } from '@app/feed/circle-feed/moderation/rules-removal-reasons/rules-removal-reasons.component';
import { SafetyComponent } from '@app/feed/circle-feed/moderation/safety/safety.component';
import { UserManagementComponent } from '@app/feed/circle-feed/moderation/user-management/user-management.component';
import { MetaWordsComponent } from '@app/feed/circle-feed/moderation/meta-words/meta-words.component';
import { CircleAppsComponent } from './circle-apps/circle-apps.component';
import { CircleAppComponent } from './circle-apps/circle-app/circle-app.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'circle/:id',
      component: CircleComponent,
      data: { title: marker('Circle') },
      children: [
        {
          path: '',
          redirectTo: 'posts',
          pathMatch: 'full',
        },
        {
          path: 'posts',
          component: PostsComponent,
        },
        {
          path: 'mods',
          component: ModComponent,
        },
        {
          path: 'rules',
          component: RulesComponent,
        },
      ],
    },
    {
      path: 'c/:id',
      component: CircleComponent,
      data: { title: marker('Circle') },
      children: [
        {
          path: 'posts',
          component: PostsComponent,
        },
        {
          path: 'mods',
          component: ModComponent,
        },
        {
          path: 'rules',
          component: RulesComponent,
        },
      ],
    },
    {
      path: 'circle/:id/members',
      component: MembersComponent,
      data: { title: marker('Members') },
    },
    {
      path: 'c/:id/members',
      component: MembersComponent,
      data: { title: marker('Members') },
    },
    {
      path: 'c/:id/pods',
      component: CircleAppsComponent,
      data: { title: marker('Pods') },
    },
    {
      path: 'c/:id/pods/:appId',
      component: CircleAppComponent,
      data: { title: marker('Picnic') },
    },
    {
      path: 'circle/:id/settings',
      component: CircleSettingsComponent,
      data: { title: marker('Settings') },
      children: [
        {
          path: 'reports',
          component: ReportCircleComponent,
          children: [
            {
              path: 'posts',
              component: PostReportsComponent,
            },
            {
              path: 'comments',
              component: CommentReportsComponent,
            },
            {
              path: 'chats',
              component: ChatReportsComponent,
            },
            {
              path: 'users',
              component: UserReportsComponent,
            },
            {
              path: '',
              redirectTo: 'posts',
              pathMatch: 'full',
            },
          ],
        },
        {
          path: 'push-notifications',
          component: PushNotificationsComponent,
        },
        {
          path: 'roles',
          component: CircleRolesComponent,
        },
        {
          path: 'invite',
          component: InviteCircleComponent,
        },
        {
          path: 'ban-user',
          component: BanUserCircleComponent,
        },
        {
          path: 'blacklisted-words',
          component: BlacklistedWordsComponent,
        },
        {
          path: 'edit-circle',
          component: EditCircleComponent,
        },
        {
          path: 'config',
          component: CircleConfigComponent,
        },
        {
          path: 'privacy-discoverability',
          component: PrivacyDiscoverabilityComponent,
        },
        {
          path: 'seeds-ownership',
          component: SeedsOwnershipComponent,
        },
        {
          path: '',
          redirectTo: 'reports',
          pathMatch: 'full',
        },
      ],
    },
    {
      path: 'c/:id/settings',
      component: CircleSettingsComponent,
      data: { title: marker('Settings') },
      children: [
        {
          path: 'reports',
          component: ReportCircleComponent,
          children: [
            {
              path: 'posts',
              component: PostReportsComponent,
            },
            {
              path: 'comments',
              component: CommentReportsComponent,
            },
            {
              path: 'chats',
              component: ChatReportsComponent,
            },
            {
              path: 'users',
              component: UserReportsComponent,
            },
            {
              path: '',
              redirectTo: 'posts',
              pathMatch: 'full',
            },
          ],
        },
        {
          path: 'push-notifications',
          component: PushNotificationsComponent,
        },
        {
          path: 'roles',
          component: CircleRolesComponent,
        },
        {
          path: 'invite',
          component: InviteCircleComponent,
        },
        {
          path: 'ban-user',
          component: BanUserCircleComponent,
        },
        {
          path: 'blacklisted-words',
          component: BlacklistedWordsComponent,
        },
        {
          path: 'edit-circle',
          component: EditCircleComponent,
        },
        {
          path: 'config',
          component: CircleConfigComponent,
        },
        {
          path: 'privacy-discoverability',
          component: PrivacyDiscoverabilityComponent,
        },
        {
          path: 'seeds-ownership',
          component: SeedsOwnershipComponent,
        },
        {
          path: '',
          redirectTo: 'reports',
          pathMatch: 'full',
        },
      ],
    },
    {
      path: 'c/:id/moderation',
      component: ModerationComponent,
      data: { title: marker('Moderation') },
      children: [
        {
          path: 'queues',
          component: QueuesComponent,
        },
        {
          path: 'modmail',
          component: ModmailComponent,
        },
        {
          path: 'user-management',
          component: UserManagementComponent,
        },
        {
          path: 'mod-log',
          component: ModLogComponent,
        },
        {
          path: 'automod',
          component: AutomodComponent,
        },
        {
          path: 'rules',
          component: RulesRemovalReasonsComponent,
        },
        {
          path: 'content-controls',
          component: ContentControlsComponent,
        },
        {
          path: 'safety',
          component: SafetyComponent,
        },
        {
          path: 'general-settings',
          component: GeneralSettingsComponent,
        },
        {
          path: 'post-comments',
          component: PostCommentsComponent,
        },
        {
          path: 'notifications',
          component: ModNotificationsComponent,
        },
        {
          path: 'meta-words',
          component: MetaWordsComponent,
        },
        {
          path: '',
          redirectTo: 'queues',
          pathMatch: 'full',
        },
      ],
    },
    {
      path: 'circle/:id/moderation',
      component: ModerationComponent,
      data: { title: marker('Moderation') },
      children: [
        {
          path: 'queues',
          component: QueuesComponent,
        },
        {
          path: 'modmail',
          component: ModmailComponent,
        },
        {
          path: 'user-management',
          component: UserManagementComponent,
        },
        {
          path: 'mod-log',
          component: ModLogComponent,
        },
        {
          path: 'automod',
          component: AutomodComponent,
        },
        {
          path: 'rules',
          component: RulesRemovalReasonsComponent,
        },
        {
          path: 'content-controls',
          component: ContentControlsComponent,
        },
        {
          path: 'safety',
          component: SafetyComponent,
        },
        {
          path: 'general-settings',
          component: GeneralSettingsComponent,
        },
        {
          path: 'post-comments',
          component: PostCommentsComponent,
        },
        {
          path: 'notifications',
          component: ModNotificationsComponent,
        },
        {
          path: 'meta-words',
          component: MetaWordsComponent,
        },
        {
          path: '',
          redirectTo: 'queues',
          pathMatch: 'full',
        },
      ],
    },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CircleRoutingModule {}
