import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CircleRoutingModule } from './circle-routing.module';
import { CircleComponent } from './circle.component';
import { ComponentsModule } from '@app/components/components.module';
import { ModComponent } from './mod/mod.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PostsComponent } from './posts/posts.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CircleSettingsComponent } from './circle-settings/circle-settings.component';
import { ReportCircleComponent } from './circle-settings/report-circle/report-circle.component';
import { InviteCircleComponent } from './circle-settings/invite-circle/invite-circle.component';
import { BanUserCircleComponent } from './circle-settings/ban-user-circle/ban-user-circle.component';
import { BlacklistedWordsComponent } from './circle-settings/blacklisted-words/blacklisted-words.component';
import { EditCircleComponent } from './circle-settings/edit-circle/edit-circle.component';
import { CircleConfigComponent } from './circle-settings/circle-config/circle-config.component';
import { SeedsOwnershipComponent } from './circle-settings/seeds-ownership/seeds-ownership.component';
import { PipesModule } from '@app/pipes/pipes.module';
import { PushNotificationsComponent } from './circle-settings/push-notifications/push-notifications.component';
import { CircleRolesComponent } from './circle-settings/circle-roles/circle-roles.component';
import { PrivacyDiscoverabilityComponent } from './circle-settings/privacy-discoverability/privacy-discoverability.component';
import { PostReportsComponent } from './circle-settings/report-circle/post-reports/post-reports.component';
import { CommentReportsComponent } from './circle-settings/report-circle/comment-reports/comment-reports.component';
import { ChatReportsComponent } from './circle-settings/report-circle/chat-reports/chat-reports.component';
import { UserReportsComponent } from './circle-settings/report-circle/user-reports/user-reports.component';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '@app/directives/directives.module';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ShorthandPipeModule } from '@app/pipes/shorthand.pipe';
import { CircleAppsComponent } from './circle-apps/circle-apps.component';
import { CircleAppComponent } from './circle-apps/circle-app/circle-app.component';

@NgModule({
  declarations: [
    CircleComponent,
    ModComponent,
    NavbarComponent,
    PostsComponent,
    CircleSettingsComponent,
    ReportCircleComponent,
    InviteCircleComponent,
    BanUserCircleComponent,
    BlacklistedWordsComponent,
    EditCircleComponent,
    CircleConfigComponent,
    SeedsOwnershipComponent,
    PushNotificationsComponent,
    CircleRolesComponent,
    PrivacyDiscoverabilityComponent,
    PostReportsComponent,
    CommentReportsComponent,
    ChatReportsComponent,
    UserReportsComponent,
    CircleAppsComponent,
    CircleAppComponent,
  ],
  imports: [
    CommonModule,
    CircleRoutingModule,
    ComponentsModule,
    InfiniteScrollModule,
    PipesModule,
    FormsModule,
    DirectivesModule,
    PickerModule,
    BrowserAnimationsModule,
    ShorthandPipeModule,
  ],
})
export class CircleModule {}
