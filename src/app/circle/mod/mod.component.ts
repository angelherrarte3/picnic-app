import { Component, OnInit } from '@angular/core';
import { ElectionParticipantEdge, GetMembersGQL } from '@app/graphql/circle';
import { CircleStateService } from '@app/circle/circle-state.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-mod',
  templateUrl: './mod.component.html',
  styleUrls: ['./mod.component.scss'],
})
export class ModComponent implements OnInit {
  members: ElectionParticipantEdge[] = [];

  constructor(private circleState: CircleStateService, private getMembers: GetMembersGQL) {}

  ngOnInit(): void {
    this.circleState.circle.pipe(untilDestroyed(this)).subscribe((circle) => {
      if (circle === null) {
        return;
      }
      this.getMembers
        .watch({
          circleId: circle.id,
          cursor: {
            id: '',
            limit: 20,
            dir: 'forward',
          },
          isBanned: false,
          roles: ['DIRECTOR', 'MODERATOR'],
          searchQuery: '',
        })
        .valueChanges.pipe(untilDestroyed(this))
        .subscribe(({ data }) => {
          this.members = data.getMembers.edges;
        });
    });
  }
}
