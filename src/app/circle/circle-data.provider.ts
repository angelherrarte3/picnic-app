import { BehaviorSubject, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Circle, CirclesConnectionGQL, GetCircleByIdGQL } from '@app/graphql/circle';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class CircleDataProviderService {
  private _circle: BehaviorSubject<Circle | null> = new BehaviorSubject<Circle | null>(null);

  circle: Observable<Circle> = this._circle.asObservable().pipe(
    filter((x) => x !== null),
    map((x) => x!)
  );

  constructor(
    private getCircleByIdGQL: GetCircleByIdGQL,
    private circlesConnection: CirclesConnectionGQL,
    private route: ActivatedRoute
  ) {
    this.updateCircle();
  }

  private updateCircle() {
    const idAndRoutePrefix = this.getIdAndRoutePrefix();
    if (idAndRoutePrefix.routePrefix === 'c') {
      this.circlesConnection
        .fetch({
          searchQuery: idAndRoutePrefix.id,
          cursor: {
            id: '',
            limit: 10,
            dir: 'forward',
          },
        })
        .subscribe(({ data }) => {
          const circle = data.circlesConnection.edges.find((c) => c.node.name === idAndRoutePrefix.id);
          this._circle.next(circle?.node || null);
        });
    } else {
      this.getCircleByIdGQL.fetch({ circleId: idAndRoutePrefix.id }).subscribe(({ data }) => {
        this._circle.next(data.getCircleById);
      });
    }
  }

  private getIdAndRoutePrefix() {
    const segments = this.route.snapshot.url.map((segment) => segment.path);
    const sIndex = segments.findIndex((segment) => segment === 'c');
    const id = segments[sIndex + 1] || this.route.snapshot.paramMap.get('id') || '';
    const routePrefix = 'c';
    return { id, routePrefix };
  }
}
