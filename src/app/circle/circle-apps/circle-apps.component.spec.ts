import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CircleAppsComponent } from './circle-apps.component';

describe('CircleAppsComponent', () => {
  let component: CircleAppsComponent;
  let fixture: ComponentFixture<CircleAppsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CircleAppsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CircleAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
