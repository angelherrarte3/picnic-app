import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { GoBackNavigation } from '@app/types/go-back-navigation';
import { NavigationService } from '@app/services/navigation.service';
import { Circle, CircleApp } from '@app/graphql/circle';
import { CircleDataProviderService } from '@app/circle/circle-data.provider';
import { AppDataProviderService } from './app-data.provider';
import { combineLatestWith, map } from 'rxjs/operators';
import { App } from '@app/graphql/app';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { CredentialsService } from '@app/auth';

@Component({
  selector: 'app-circle-app[circleApp]',
  templateUrl: './circle-app.component.html',
  styleUrls: ['./circle-app.component.scss'],
  providers: [CircleDataProviderService, AppDataProviderService],
})
export class CircleAppComponent extends GoBackNavigation implements OnInit {
  @Input() circleApp: CircleApp;
  @ViewChild('appframe', { static: false }) appframe: ElementRef;
  circle?: Circle | null;
  app?: App | null;

  urlSafe?: SafeResourceUrl;

  constructor(
    private sanitizer: DomSanitizer,
    protected override navigationService: NavigationService,
    private circleDataProviderService: CircleDataProviderService,
    private appDataProviderService: AppDataProviderService,
    private credentialsService: CredentialsService
  ) {
    super(navigationService);
  }

  ngOnInit(): void {
    const token = this.credentialsService.credentials?.token;
    if (token) {
      this.circleDataProviderService.circle
        .pipe(
          combineLatestWith(this.appDataProviderService.app),
          map(([circle, app]) => {
            this.circle = circle;
            this.app = app;
            const host = app.url;
            const url = `${host}?token=Bearer%20${token}&circleId=${circle.id}`;
            this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(url);
          })
        )
        .subscribe();
    }
  }
}
