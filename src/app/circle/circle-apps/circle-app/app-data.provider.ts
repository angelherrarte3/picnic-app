import { BehaviorSubject, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Injectable } from '@angular/core';
import { App, GetAppGQL } from '@app/graphql/app';

@Injectable()
export class AppDataProviderService {
  private _app: BehaviorSubject<App | null> = new BehaviorSubject<App | null>(null);

  app: Observable<App> = this._app.asObservable().pipe(
    filter((x) => x !== null),
    map((x) => x!)
  );

  constructor(private getAppGQL: GetAppGQL, private route: ActivatedRoute) {
    this.updateApp();
  }

  private updateApp() {
    const id = this.getAppId();
    if (id) {
      this.getAppGQL.fetch({ id: id }).subscribe(({ data }) => {
        this._app.next(data.getApp);
      });
    }
  }

  private getAppId(): string | undefined {
    const segments = this.route.snapshot.url.map((segment) => segment.path);
    const sIndex = segments.findIndex((segment) => segment === 'pods');
    const id = segments[sIndex + 1];
    return id;
  }
}
