import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GoBackNavigation } from '@app/types/go-back-navigation';
import { NavigationService } from '@app/services/navigation.service';
import { Circle, CircleApp, CircleAppsGQL } from '@app/graphql/circle';
import { CircleDataProviderService } from '../circle-data.provider';
import { App } from '@app/graphql/app';

import { CredentialsService } from '@app/auth';
import { PicnicModalService } from '@app/services/picnic-modal.service';
import { GlobalModalService } from '@app/services/global-modal.service';

@Component({
  selector: 'app-circle-apps',
  templateUrl: './circle-apps.component.html',
  styleUrls: ['./circle-apps.component.scss'],
  providers: [CircleDataProviderService],
})
export class CircleAppsComponent extends GoBackNavigation implements OnInit {
  private circle?: Circle | null;
  circleApps: CircleApp[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    protected navigationservice: NavigationService,
    private circleAppsGQL: CircleAppsGQL,
    private circleDataProviderService: CircleDataProviderService,
    private modalService: GlobalModalService,
    private picnicModal: PicnicModalService,
    private credentialsService: CredentialsService
  ) {
    super(navigationservice);
  }

  ngOnInit(): void {
    this.circleDataProviderService.circle.subscribe((circle) => {
      this.setCircle(circle);
    });
  }

  private setCircle(circle: Circle) {
    this.circle = circle;
    this.circleAppsGQL
      .fetch({
        circleId: circle.id,
        cursor: { id: '', limit: 100, dir: 'forward' },
      })
      .subscribe(({ data }) => {
        this.circleApps = data.circleApps.edges
          .map((edge) => edge.node)
          .filter((node) => !node.app.url.includes('c.ai'));
      });
  }

  onTapPod(pod: App) {
    if (this.isGuestUser) {
      this.modalService.open('sign-up');
      return;
    }

    if (pod.owner?.name === 'character.ai') {
      this.modalService.open('building-feature');
      return;
    }

    this.router.navigate([pod.id], { relativeTo: this.route });
  }

  get isGuestUser() {
    return !this.credentialsService.isAuthenticated();
  }
}
