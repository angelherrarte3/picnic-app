import { Component, OnInit } from '@angular/core';
import { PostEdge, SortedCirclePostsConnectionGQL } from '@app/graphql/content';
import { CircleStateService } from '@app/circle/circle-state.service';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { cloneDeep } from '@apollo/client/utilities';
import { SimpleDropdownItem } from '@app/components/simple-dropdown/simple-dropdown.component';
import { CirclePostSorting } from '@app/circle/circle.component';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'circle-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss'],
})
export class PostsComponent extends GQLPaginationComponent implements OnInit {
  posts: PostEdge[] = [];
  sortItems: SimpleDropdownItem<CirclePostSorting>[] = [
    {
      label: '🔥  popular all time',
      value: CirclePostSorting.POPULARITY_ALL_TIME,
    },
    {
      label: '🆕  new',
      value: CirclePostSorting.NEW,
    },
    {
      label: '⏰  trending this week',
      value: CirclePostSorting.TRENDING_THIS_WEEK,
    },
    {
      label: '📅  trending this month',
      value: CirclePostSorting.TRENDING_THIS_MONTH,
    },
  ];
  sortSelected: CirclePostSorting = CirclePostSorting.POPULARITY_ALL_TIME;

  constructor(private circleState: CircleStateService, private getPostsGQL: SortedCirclePostsConnectionGQL) {
    super();
  }

  ngOnInit(): void {
    this.circleState.circle.pipe(untilDestroyed(this)).subscribe((circle) => {
      if (circle === null) {
        return;
      }

      this.getPostsGQL
        .fetch({
          circleId: circle.id,
          cursor: {
            id: '',
            limit: 10,
            dir: 'forward',
          },
          sortingType: this.sortSelected.toString(),
        })
        .subscribe(({ data }) => {
          this.posts = data.sortedCirclePostsConnection.edges;
          this.cursorId = data.sortedCirclePostsConnection.pageInfo.lastId;
          this.hasNextPage = data.sortedCirclePostsConnection.pageInfo.hasNextPage;
        });
    });
  }

  override loadMoreItems(): void {
    if (!this.hasNextPage) return;

    this.circleState.circle.pipe(untilDestroyed(this)).subscribe((circle) => {
      if (circle === null) {
        return;
      }

      this.getPostsGQL
        .fetch({
          circleId: circle.id,
          cursor: {
            id: this.cursorId,
            limit: 10,
            dir: 'forward',
          },
          sortingType: this.sortSelected.toString(),
        })
        .subscribe(({ data }) => {
          this.posts = this.posts.concat(
            ...data.sortedCirclePostsConnection.edges.map((e) => {
              return cloneDeep(e);
            })
          );

          this.loadingNext = false;
          this.cursorId = data.sortedCirclePostsConnection.pageInfo.lastId;
          this.hasNextPage = data.sortedCirclePostsConnection.pageInfo.hasNextPage;
        });
    });
  }

  handleSortChange(sortItem: SimpleDropdownItem<CirclePostSorting>): void {
    this.resetPagination();
    this.sortSelected = sortItem.value;

    this.circleState.circle.pipe(untilDestroyed(this)).subscribe((circle) => {
      if (circle === null) {
        return;
      }

      this.getPostsGQL
        .fetch({
          circleId: circle.id,
          cursor: {
            id: this.cursorId,
            limit: 10,
            dir: 'forward',
          },
          sortingType: this.sortSelected.toString(),
        })
        .subscribe(({ data }) => {
          this.posts = data.sortedCirclePostsConnection.edges;
          this.loadingNext = false;
          this.cursorId = data.sortedCirclePostsConnection.pageInfo.lastId;
          this.hasNextPage = data.sortedCirclePostsConnection.pageInfo.hasNextPage;
        });
    });
  }

  get emptyPosts(): boolean {
    return this.posts.length === 0;
  }
}
