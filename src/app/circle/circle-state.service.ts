import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Observer, of } from 'rxjs';
import { Circle } from '@app/graphql/circle';

@Injectable()
export class CircleStateService {
  circle$: BehaviorSubject<Circle | null> = new BehaviorSubject<Circle | null>(null);
  circle = this.circle$.asObservable();

  setCircle(circle: Circle) {
    this.circle$.next(circle);
  }
}
