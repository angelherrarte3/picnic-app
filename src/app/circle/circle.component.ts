import { Component, OnInit, isDevMode, AfterViewInit, ElementRef, ViewChild, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Circle, CirclesConnectionGQL, GetCircleByNameGQL, JoinCirclesGQL, LeaveCirclesGQL } from '@app/graphql/circle';
import { CircleStateService } from '@app/circle/circle-state.service';

import { PostEdge, SortedCirclePostsConnectionGQL } from '@app/graphql/content';
import { CredentialsService } from '@app/auth';
import { CreatePostStateService } from '@app/create-post/create-post-state.service';
import { isPlatformBrowser } from '@angular/common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { environment } from '@env/environment.prod';
import { Meta, Title } from '@angular/platform-browser';
import { GetChatByIdGQL } from '@app/graphql/chat';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';
import { GlobalModalService } from '@app/services/global-modal.service';

export enum CirclePostSorting {
  TRENDING_THIS_MONTH = 'TRENDING_THIS_MONTH',
  TRENDING_THIS_WEEK = 'TRENDING_THIS_WEEK',
  NEW = 'NEW',
  POPULARITY_ALL_TIME = 'POPULARITY_ALL_TIME',
}

@UntilDestroy()
@Component({
  selector: 'app-circle',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.scss'],
  providers: [CircleStateService],
})
export class CircleComponent implements OnInit, AfterViewInit {
  circleId = '';
  circle?: Circle;
  loading = true;
  devMode: boolean | undefined;
  postSorting: CirclePostSorting = CirclePostSorting.TRENDING_THIS_MONTH;
  postSortingOptions = [
    CirclePostSorting.TRENDING_THIS_MONTH,
    CirclePostSorting.TRENDING_THIS_WEEK,
    CirclePostSorting.NEW,
    CirclePostSorting.POPULARITY_ALL_TIME,
  ];

  cursorId = '';
  length = 0;
  hasNext = true;
  hasPrev = false;
  cursorStack: string[] = [];
  firstCursor = '';

  pages: {
    posts: PostEdge[];
    cursorId: string;
    prevCursor?: string;
  }[] = [];

  @ViewChild('anchorDown') anchor: ElementRef<HTMLElement>;
  @ViewChild('anchorUp') anchorUp: ElementRef<HTMLElement>;

  constructor(
    private route: ActivatedRoute,
    private getCircleByNameGQL: GetCircleByNameGQL,
    private circleState: CircleStateService,
    private modalService: GlobalModalService,
    private sortedCirclePostsConnectionGQL: SortedCirclePostsConnectionGQL,
    private credentialsService: CredentialsService,
    private router: Router,
    private joinCirclesGQL: JoinCirclesGQL,
    private leaveCircle: LeaveCirclesGQL,
    private circlesConnection: CirclesConnectionGQL,
    private createPostState: CreatePostStateService,
    private getChatByIdGQL: GetChatByIdGQL,
    private bubblesChatService: BubbleChatStateService,
    private meta: Meta,
    private titleService: Title,
    @Inject(PLATFORM_ID) private platformId: string
  ) {}

  private readonly _pageSize = 4;
  private readonly _maxPages = 2;

  ngOnInit() {
    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params: ParamMap) => {
      this.reInitiate();
      this.getCircleByNameGQL
        .fetch(
          {
            name: params.get('id')!,
          },
          { errorPolicy: 'all' }
        )
        .subscribe(({ data }) => {
          this.circle = data.getCircleByName;
          this.circleId = this.circle.id;
          this.circleState.setCircle(this.circle);
          this.loadPosts();

          this.meta.updateTag({
            property: 'og:title',
            content: this.circle.name,
          });
          this.meta.updateTag({
            property: 'og:description',
            content: this.circle.description,
          });
          this.meta.updateTag({
            property: 'og:url',
            content: `${environment.host}/c/${this.circle.urlName}`,
          });
          this.meta.updateTag({
            property: 'og:image',
            content: this.circle.imageFile ?? '',
          });
          this.titleService.setTitle(this.circle.name != null ? this.circle.name : 'Circle');
        });
    });
    this.route.queryParamMap.pipe(untilDestroyed(this)).subscribe((p) => {
      this.pages = [];
      this.cursorId = p.get('cursorId') ?? '';
      const sort = p.get('sort');
      if (sort) this.postSorting = CirclePostSorting[sort];
      this.loadPosts();
    });

    this.devMode = isDevMode();
  }

  ngAfterViewInit() {
    if (isPlatformBrowser(this.platformId)) {
      new IntersectionObserver(
        ([entry]) => {
          if (this.loading || !this.hasNext) {
            return;
          }
          if (entry.isIntersecting) {
            this.loadPosts('down');
          }
        },
        { threshold: 0.1 }
      ).observe(this.anchor.nativeElement);
      new IntersectionObserver(
        ([entry]) => {
          if (this.loading) {
            return;
          }
          if (entry.isIntersecting) {
            this.loadPosts('up');
          }
        },
        { threshold: 0.1 }
      ).observe(this.anchorUp.nativeElement);
    }
  }

  private reInitiate() {
    this.pages = [];
    this.circle = undefined;
    this.circleId = '';
    this.postSorting = CirclePostSorting.TRENDING_THIS_MONTH;
    this.cursorId = '';
    this.hasNext = true;
    this.hasPrev = false;
    this.cursorStack = [];
  }

  get coverImage() {
    if (!this.circle?.coverImageFile?.trim()) return undefined;
    return `url(${this.circle.coverImageFile})`;
  }

  get isGuestUser() {
    return !this.credentialsService.isAuthenticated();
  }

  handlePostCreate() {
    if (this.isGuestUser) {
      this.showSignUpModal();
      return;
    }
    this.createPostState.preSelectedCircle.next(this.circle);
    this.router.navigateByUrl(`create-post`).then((r) => console.log(r));
  }

  get isDirector(): boolean {
    return this.circle?.role === 'DIRECTOR';
  }

  get circleFollowed(): boolean {
    return this.circle?.iJoined ?? true;
  }

  showSignUpModal = () => this.modalService.open('sign-up');

  loadPosts(direction: 'up' | 'down' = 'down') {
    this.loading = true;
    let cursorId = '';
    switch (direction) {
      case 'up':
        if (!this.hasPrev) {
          return;
        }
        cursorId = this.pages[this.pages.length - Math.min(this.pages.length, this._maxPages) - 1]?.prevCursor ?? '';
        break;
      case 'down':
        cursorId = this.pages[this.pages.length - 1]?.cursorId ?? this.cursorId;
        break;
    }

    return this.sortedCirclePostsConnectionGQL
      .fetch({
        circleId: this.circleId,
        cursor: {
          id: cursorId,
          limit: this._pageSize,
          dir: 'forward',
        },
        sortingType: this.postSorting.toString(),
      })
      .subscribe(({ data }) => {
        switch (direction) {
          case 'up':
            this.pages.unshift({
              cursorId: data.sortedCirclePostsConnection.pageInfo.lastId,
              prevCursor: this.cursorStack.pop(),
              posts: data.sortedCirclePostsConnection.edges,
            });
            this.pages.pop();
            this.hasNext = true;
            if (this.cursorStack.length == 0) {
              this.hasPrev = false;
            }
            break;
          case 'down':
            this.pages.push({
              cursorId: data.sortedCirclePostsConnection.pageInfo.lastId,
              prevCursor: this.pages[this.pages.length - 1]?.cursorId,
              posts: data.sortedCirclePostsConnection.edges,
            });

            if (this.pages.length > this._maxPages) {
              this.cursorStack.push(this.pages[0]?.prevCursor ?? '');
              this.pages.splice(0, 1);
              this.hasPrev = true;
            }
            this.hasNext = data.sortedCirclePostsConnection.pageInfo.hasNextPage;
            if (this.cursorId) {
              this.hasPrev = data.sortedCirclePostsConnection.pageInfo.hasPreviousPage!;
            }
            this.firstCursor = data.sortedCirclePostsConnection.pageInfo.firstId;
        }

        this.loading = false;
      });
  }

  onTapJoinCircle() {
    if (this.isGuestUser) {
      this.showSignUpModal();
      return;
    }

    this.joinCirclesGQL
      .mutate({
        circlesIds: [this.circleId],
      })
      .subscribe(({ data }) => {
        if (data?.joinCircles.success && this.circle) {
          this.circle = { ...this.circle, iJoined: true };
          this.circleState.setCircle(this.circle);
        }
      });
  }

  valueToDisplay(dsp: CirclePostSorting) {
    switch (dsp) {
      case CirclePostSorting.POPULARITY_ALL_TIME:
        return `🔥 popular all time`;
      case CirclePostSorting.NEW:
        return `🆕 new `;
      case CirclePostSorting.TRENDING_THIS_WEEK:
        return `⏰ trending this week`;
      case CirclePostSorting.TRENDING_THIS_MONTH:
        return `📅 trending this month`;
      default:
        return '';
    }
  }

  get url() {
    return `c/${this.circle?.urlName ? this.circle.urlName : this.circle?.name}`;
  }

  goToMembers() {
    this.router.navigateByUrl(`${this.url}/members`).then((r) => console.log(r));
  }

  onTapSelectSorting(sorting: CirclePostSorting) {
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        sort: sorting,
      },
    });
  }

  onTapLeaveCircle() {
    if (this.isGuestUser) {
      this.showSignUpModal();
      return;
    }

    this.leaveCircle
      .mutate({
        circlesIds: [this.circleId],
      })
      .subscribe(({ data }) => {
        if (data?.leaveCircles.success && this.circle) {
          this.circle = { ...this.circle, iJoined: false };
          this.circleState.setCircle(this.circle);
        }
      });
  }

  goToSettings() {
    this.router.navigateByUrl(`${this.url}/moderation`);
  }

  goToApps() {
    this.router.navigateByUrl(`${this.url}/pods`);
  }

  onTapInviteFriends() {
    this.modalService.open('building-feature', {
      dismissClickOutside: true,
    });
  }

  onTapCreatePost() {
    if (this.isGuestUser) {
      this.showSignUpModal();
      return;
    }

    this.router.navigateByUrl(`create-post`);
  }

  get isModerator(): boolean {
    return this.circle?.role === 'MODERATOR';
  }

  get emptyPosts() {
    return this.pages.length === 0;
  }

  get posts() {
    return this.pages.reduce((a: PostEdge[], v) => {
      a.push(...v.posts);
      return a;
    }, []);
  }

  onChatClick() {
    if (!this.credentialsService.isAuthenticated()) {
      this.showBuildFeatureModal();
      return;
    }

    this.getChatByIdGQL
      .fetch({
        id: this.circle!.chat!.id,
      })
      .subscribe(({ data }) => {
        this.bubblesChatService.addModalChat(data.chat);
      });
  }

  private showBuildFeatureModal() {
    this.modalService.open('building-feature', {
      dismissClickOutside: true,
    });
  }

  onNext() {
    this.cursorStack.push(this.cursorId);
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        cursorId: this.pages[this.pages.length - 1].cursorId,
        sort: this.postSorting == CirclePostSorting.TRENDING_THIS_MONTH ? undefined : this.postSorting,
      },
    });
  }

  onPrev() {
    let prevCursorId = '';
    if (this.cursorStack.length > 0) {
      prevCursorId = this.cursorStack.pop()!;
    } else {
      prevCursorId = this.firstCursor;
    }
    if (!prevCursorId) {
      this.hasPrev = false;
    }
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        cursorId: prevCursorId,
        sort: this.postSorting == CirclePostSorting.TRENDING_THIS_MONTH ? undefined : this.postSorting,
      },
    });
  }
}
