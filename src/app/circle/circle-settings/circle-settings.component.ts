import { Component, HostListener, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { SettingItem } from '@app/components/settings-list/settings-list.component';
import { Circle, GetCircleByIdGQL } from '@app/graphql/circle';
import { GoBackNavigation } from '@app/types/go-back-navigation';
import { NavigationService } from '@app/services/navigation.service';

@Component({
  selector: 'app-circle-settings',
  templateUrl: './circle-settings.component.html',
  styleUrls: ['./circle-settings.component.scss'],
})
export class CircleSettingsComponent extends GoBackNavigation implements OnInit {
  moderationSettings: SettingItem[] = [
    {
      label: '🚩 reports',
      path: 'reports',
    },
    {
      label: '⛔ ban user',
      path: 'ban-user',
    },
    {
      label: '🌰 seeds ownership',
      path: 'seeds-ownership',
    },
    {
      label: '📝 privacy & discoverability',
      path: 'privacy-discoverability',
    },
    {
      label: '🚫 blacklisted words',
      path: 'blacklisted-words',
    },
    {
      label: '✍️ edit circle',
      path: 'edit-circle',
    },
    {
      label: '⚙️ circle config',
      path: 'config',
    },
    {
      label: '📧 invite',
      path: 'invite',
    },
    {
      label: '🔔 send push notification',
      path: 'push-notifications',
    },
  ];
  circle?: Circle;
  showSidebar = false;

  windowWidth = window.innerWidth;
  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.windowWidth = event.target.innerWidth;
    this.isMobile ? (this.showSidebar = false) : (this.showSidebar = true);
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private getCircleById: GetCircleByIdGQL,
    protected override navigationService: NavigationService
  ) {
    super(navigationService);
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((p) => {
      let circleId = p.get('id') ?? '';
      this.getCircleById
        .fetch({
          circleId: circleId,
        })
        .subscribe(({ data }) => {
          this.circle = data.getCircleById;
          this.moderationSettings[0].notificationCount = this.circle?.reportsCount;
        });
    });

    this.isMobile ? (this.showSidebar = false) : (this.showSidebar = true);
  }

  handleItemSelected(item: SettingItem) {
    this.router.navigate([item.path], { relativeTo: this.route });
    if (this.isMobile) this.toggleSidebar();
  }

  toggleSidebar = () => (this.showSidebar = !this.showSidebar);

  get isMobile() {
    return this.windowWidth <= 768;
  }
}
