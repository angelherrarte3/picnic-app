import { Component, OnInit } from '@angular/core';
import { BLWConnectionGQL, BLWord, RemoveCustomBLWordsGQL } from '@app/graphql/circle';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { ActivatedRoute } from '@angular/router';
import { debounceTime, Subject } from 'rxjs';

@Component({
  selector: 'app-blacklisted-words',
  templateUrl: './blacklisted-words.component.html',
  styleUrls: ['./blacklisted-words.component.scss'],
})
export class BlacklistedWordsComponent extends GQLPaginationComponent implements OnInit {
  circleId = '';
  words: BLWord[] = [];

  private readonly searchSub = new Subject<string>();
  private searchQuery = '';

  constructor(
    private route: ActivatedRoute,
    private blwConnection: BLWConnectionGQL,
    private removeCustomBLWords: RemoveCustomBLWordsGQL
  ) {
    super();
  }

  ngOnInit(): void {
    let route = this.route.parent;
    if (!route) {
      route = this.route;
    }
    route.paramMap.subscribe((p) => {
      this.circleId = p.get('id') ?? '';
      this.resetPagination();
      this.loadMoreItems();
    });

    this.searchSub.pipe(debounceTime(300)).subscribe((searchQuery) => {
      this.searchQuery = searchQuery;
      this.resetPagination();
      this.loadMoreItems();
    });
  }

  override loadMoreItems() {
    this.blwConnection
      .fetch({
        circleId: this.circleId,
        searchQuery: this.searchQuery,
        cursor: {
          id: this.cursorId,
          limit: 20,
          dir: 'forward',
        },
      })
      .subscribe(({ data }) => {
        this.words.push(...data.blwConnection.edges);
      });
  }

  override resetPagination() {
    super.resetPagination();
    this.words = [];
  }

  onSearch(query: string) {
    this.searchSub.next(query);
  }

  onDelete(word: string) {
    this.removeCustomBLWords
      .mutate({
        words: [word],
        circleId: this.circleId,
      })
      .subscribe(({ data }) => {
        if (!data?.removeCustomBLWords.success) {
          return;
        }

        this.words.splice(
          this.words.findIndex((v) => v.word === word),
          1
        );
      });
  }
}
