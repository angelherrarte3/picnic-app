import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlacklistedWordsComponent } from './blacklisted-words.component';

describe('BlacklistedWordsComponent', () => {
  let component: BlacklistedWordsComponent;
  let fixture: ComponentFixture<BlacklistedWordsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BlacklistedWordsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(BlacklistedWordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
