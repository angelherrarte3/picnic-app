import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CircleRolesComponent } from './circle-roles.component';

describe('CircleRolesComponent', () => {
  let component: CircleRolesComponent;
  let fixture: ComponentFixture<CircleRolesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CircleRolesComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CircleRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
