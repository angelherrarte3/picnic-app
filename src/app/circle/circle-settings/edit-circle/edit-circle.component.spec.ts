import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCircleComponent } from './edit-circle.component';

describe('EditCircleComponent', () => {
  let component: EditCircleComponent;
  let fixture: ComponentFixture<EditCircleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EditCircleComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(EditCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
