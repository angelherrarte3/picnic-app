import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Circle, GetCircleByIdGQL, UpdateCircleGQL } from '@app/graphql/circle';
import { ActivatedRoute } from '@angular/router';
import { TooltipService } from '@app/services/tooltip.service';
import { TooltipConfig } from '@app/directives/tooltip/tooltip.directive';
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-edit-circle',
  templateUrl: './edit-circle.component.html',
  styleUrls: ['./edit-circle.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate('0.2s ease-out', style({ opacity: 1 }))]),
      transition(':leave', [style({ opacity: 1 }), animate('0.2s ease-in', style({ opacity: 0 }))]),
    ]),
    trigger('inOutSizeAnimation', [
      transition(':enter', [style({ height: 0 }), animate('0.2s ease-out', style({ height: 40 }))]),
      transition(':leave', [
        style({ height: 40, opacity: 1 }),
        animate('0.2s ease-in', style({ height: 0, opacity: 0 })),
      ]),
    ]),
  ],
})
export class EditCircleComponent implements OnInit {
  circle?: Circle;
  input: {
    name: string;
    description: string;
    image?: string;
    imageFile?: File;
  } = { name: '', description: '' };
  imageFileSrc?: string;

  changeAvatar = false;
  changeAvatarMode?: 'emoji' | 'file';
  avatarEmojiMode = false;
  tooltipConfig: TooltipConfig = {
    text: 'changes saved!',
    customPosition: { top: 7, left: -128 },
    backgroundColor: 'transparent',
    textColor: '#939292',
    textSize: '15px',
    autoHideTimeout: 1800,
  };
  @ViewChild('saveChangesButtonRef') saveChangesButtonRef!: ElementRef;
  loadingSave: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private getCircleById: GetCircleByIdGQL,
    private updateCircleGQL: UpdateCircleGQL,
    private tooltip: TooltipService
  ) {}

  ngOnInit(): void {
    let route = this.route.parent;
    if (!route) {
      route = this.route;
    }
    route.paramMap.subscribe((p) => {
      let circleId = p.get('id') ?? '';
      this.getCircleById
        .fetch({
          circleId: circleId,
        })
        .subscribe(({ data }) => {
          this.circle = data.getCircleById;
          this.input = {
            name: this.circle.name,
            description: data.getCircleById.description,
          };
        });
    });
  }

  get avatar(): string {
    if (this.input?.imageFile) {
      this.avatarEmojiMode = false;
      return this.imageFileSrc!;
    }
    if (this.input?.image) {
      this.avatarEmojiMode = true;
      return this.input?.image;
    }
    if (this.circle?.imageFile) {
      this.avatarEmojiMode = false;
      return this.circle.imageFile;
    }
    if (this.circle?.image) {
      this.avatarEmojiMode = true;
      return this.circle.image;
    }
    return '';
  }

  onFileClick() {
    let input = document.createElement('input');
    input.type = 'file';
    input.accept = 'image/*';
    input.onchange = (_) => {
      this.changeAvatarMode = 'file';
      this.input!.image = undefined;
      this.input!.imageFile = input.files![0];

      let fr = new FileReader();
      fr.onload = (_) => {
        this.imageFileSrc = fr.result as string;
      };
      fr.readAsDataURL(input.files![0]);
    };
    input.click();
  }

  onSave() {
    if (this.input.name === '' || this.input.description === '') {
      return;
    }

    this.loadingSave = true;
    this.changeAvatar = false;
    this.changeAvatarMode = undefined;
    this.updateCircleGQL
      .mutate({
        circleId: this.circle!.id,
        payload: {
          name: this.input.name,
          description: this.input.description,
          image: this.input.image,
          imageFile: this.input.imageFile,
        },
      })
      .subscribe((_) => {
        this.loadingSave = false;
        if (this.saveChangesButtonRef) {
          this.tooltip.showTooltip(this.saveChangesButtonRef, this.tooltipConfig);
        }
      });
  }

  closeImageChange() {
    this.changeAvatar = false;
    this.input.image = undefined;
    this.input.imageFile = undefined;
    this.changeAvatarMode = undefined;
  }

  onEmojiSelect(emojiData: any) {
    console.log(emojiData.emoji);
    this.input.image = emojiData.emoji.native;
    this.input.imageFile = undefined;
  }

  toggleEmojiMode() {
    if (this.changeAvatarMode === 'emoji') {
      this.changeAvatarMode = undefined;
      return;
    }

    this.changeAvatarMode = 'emoji';
  }
}
