import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteCircleComponent } from './invite-circle.component';

describe('InviteCircleComponent', () => {
  let component: InviteCircleComponent;
  let fixture: ComponentFixture<InviteCircleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InviteCircleComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(InviteCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
