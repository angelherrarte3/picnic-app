import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BanUserInCircleGQL, ElectionParticipantEdge, GetMembersGQL, UnbanUserInCircleGQL } from '@app/graphql/circle';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { Subject, debounceTime } from 'rxjs';

@Component({
  selector: 'app-ban-user-circle',
  templateUrl: './ban-user-circle.component.html',
  styleUrls: ['./ban-user-circle.component.scss'],
})
export class BanUserCircleComponent extends GQLPaginationComponent implements OnInit {
  circleId = '';
  members: ElectionParticipantEdge[] = [];

  private readonly searchSub = new Subject<string>();
  private searchQuery = '';
  membersSearch: ElectionParticipantEdge[] = [];
  searchMode = false;
  searchCursorId = '';
  searchHasNextPage = false;

  constructor(
    private route: ActivatedRoute,
    private getMembersGQL: GetMembersGQL,
    private unbanUserInCircleGQL: UnbanUserInCircleGQL,
    private banUserInCircleGQL: BanUserInCircleGQL
  ) {
    super();
  }

  ngOnInit(): void {
    let route = this.route.parent;
    if (!route) {
      route = this.route;
    }
    route.paramMap.subscribe((p) => {
      this.circleId = p.get('id') ?? '';
      this.loadMoreItems();
    });

    this.searchSub.pipe(debounceTime(300)).subscribe((searchQuery) => {
      this.searchQuery = searchQuery;
      this.loadMoreItemsSearch(false);
    });
  }

  override loadMoreItems() {
    this.getMembersGQL
      .fetch({
        circleId: this.circleId,
        isBanned: true,
        roles: [],
        cursor: {
          id: this.cursorId,
          limit: 20,
          dir: 'forward',
        },
      })
      .subscribe(({ data }) => {
        this.members.push(...data.getMembers.edges);
        this.cursorId = data.getMembers.pageInfo.lastId;
        this.hasNextPage = data.getMembers.pageInfo.hasNextPage;
      });
  }

  override resetPagination() {
    super.resetPagination();
    this.members = [];
  }

  onUnban(m: ElectionParticipantEdge) {
    this.unbanUserInCircleGQL
      .mutate({
        userId: m.node.user.id,
        circleId: this.circleId,
      })
      .subscribe((_) => {
        this.members.splice(
          this.members.findIndex((v) => v.node.user.id === v.node.user.id),
          1
        );
      });
  }

  //Search mode
  loadMoreItemsSearch(fromPagination: boolean) {
    this.getMembersGQL
      .fetch({
        circleId: this.circleId,
        isBanned: false,
        searchQuery: this.searchQuery,
        roles: [],
        cursor: {
          id: this.searchCursorId,
          limit: 20,
          dir: 'forward',
        },
      })
      .subscribe(({ data }) => {
        if (fromPagination) this.membersSearch.push(...data.getMembers.edges);
        else this.membersSearch = data.getMembers.edges;
        this.searchCursorId = data.getMembers.pageInfo.lastId;
        this.searchHasNextPage = data.getMembers.pageInfo.hasNextPage;
      });
  }

  onBan(member: ElectionParticipantEdge) {
    this.banUserInCircleGQL
      .mutate({
        userId: member.node.user.id,
        circleId: this.circleId,
      })
      .subscribe(() => {
        this.members.push(member);
        this.hideSearchMode();
      });
  }

  onSearch = (value: string) => this.searchSub.next(value);

  showSearchMode() {
    this.searchMode = true;
    this.loadMoreItemsSearch(false);
  }

  hideSearchMode() {
    this.searchMode = false;
    this.searchCursorId = '';
    this.searchHasNextPage = false;
    this.searchQuery = '';
    this.membersSearch = [];
  }
}
