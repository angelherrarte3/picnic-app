import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BanUserCircleComponent } from './ban-user-circle.component';

describe('BanUserCircleComponent', () => {
  let component: BanUserCircleComponent;
  let fixture: ComponentFixture<BanUserCircleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BanUserCircleComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(BanUserCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
