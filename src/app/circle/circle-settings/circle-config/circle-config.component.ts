import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Circle, CircleOption, GetCircleByIdGQL, UpdateCircleGQL } from '@app/graphql/circle';
import { cloneDeep } from '@apollo/client/utilities';
import { TooltipService } from '@app/services/tooltip.service';

@Component({
  selector: 'app-circle-config',
  templateUrl: './circle-config.component.html',
  styleUrls: ['./circle-config.component.scss'],
})
export class CircleConfigComponent implements OnInit {
  circle?: Circle;
  @ViewChild('saveChangesButtonRef') saveChangesButtonRef!: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private getCircleById: GetCircleByIdGQL,
    private updateCircleGQL: UpdateCircleGQL,
    private tooltip: TooltipService
  ) {}

  ngOnInit(): void {
    let route = this.route.parent;
    if (!route) {
      route = this.route;
    }
    route.paramMap.subscribe((p) => {
      let circleId = p.get('id') ?? '';
      this.getCircleById
        .fetch({
          circleId: circleId,
        })
        .subscribe(({ data }) => {
          this.circle = cloneDeep(data.getCircleById);
        });
    });
  }

  handleToggleItem(item: CircleOption) {
    item.value = !item.value;
  }

  handleSave() {
    this.updateCircleGQL
      .mutate({
        circleId: this.circle!.id,
        payload: {
          options: this.circle!.options,
        },
      })
      .subscribe((res) => {
        if (this.saveChangesButtonRef) {
          this.tooltip.showTooltip(this.saveChangesButtonRef, {
            text: 'changes saved!',
            customPosition: { top: 7, left: -128 },
            backgroundColor: 'transparent',
            textColor: '#939292',
            textSize: '15px',
            autoHideTimeout: 1800,
          });
        }
      });
  }
}
