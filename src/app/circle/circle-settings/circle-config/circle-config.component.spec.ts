import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CircleConfigComponent } from './circle-config.component';

describe('CircleConfigComponent', () => {
  let component: CircleConfigComponent;
  let fixture: ComponentFixture<CircleConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CircleConfigComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CircleConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
