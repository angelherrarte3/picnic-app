import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyDiscoverabilityComponent } from './privacy-discoverability.component';

describe('PrivacyDiscoverabilityComponent', () => {
  let component: PrivacyDiscoverabilityComponent;
  let fixture: ComponentFixture<PrivacyDiscoverabilityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PrivacyDiscoverabilityComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PrivacyDiscoverabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
