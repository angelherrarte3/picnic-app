import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CircleSettingsComponent } from './circle-settings.component';

describe('CircleSettingsComponent', () => {
  let component: CircleSettingsComponent;
  let fixture: ComponentFixture<CircleSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CircleSettingsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CircleSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
