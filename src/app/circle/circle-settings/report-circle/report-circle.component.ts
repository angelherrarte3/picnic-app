import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Circle, CircleReportEdge, CircleReportsConnectionGQL, GetCircleByIdGQL } from '@app/graphql/circle';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';

@Component({
  selector: 'app-report-circle',
  templateUrl: './report-circle.component.html',
  styleUrls: ['./report-circle.component.scss'],
})
export class ReportCircleComponent extends GQLPaginationComponent implements OnInit {
  circle?: Circle;
  reports: CircleReportEdge[] = [];

  constructor(
    private route: ActivatedRoute,
    private circleReportsConnection: CircleReportsConnectionGQL,
    private getCircleById: GetCircleByIdGQL
  ) {
    super();
  }

  ngOnInit(): void {
    let route = this.route.parent;
    if (!route) {
      route = this.route;
    }
    route.paramMap.subscribe((p) => {
      let circleId = p.get('id') ?? '';
      this.getCircleById
        .fetch({
          circleId: circleId,
        })
        .subscribe(({ data }) => {
          this.circle = data.getCircleById;
          this.resetPagination();
          this.loadMoreItems();
        });
    });
  }

  override loadMoreItems() {
    this.circleReportsConnection
      .fetch({
        data: {
          circleId: this.circle!.id,
          cursor: {
            id: this.cursorId,
            limit: 20,
            dir: 'forward',
          },
        },
      })
      .subscribe(({ data }) => {
        this.reports.push(...data.circleReportsConnection.edges);
        this.cursorId = data.circleReportsConnection.pageInfo.lastId;
        this.hasNextPage = data.circleReportsConnection.pageInfo.hasNextPage;
      });
  }

  override resetPagination() {
    super.resetPagination();
    this.reports = [];
  }
}
