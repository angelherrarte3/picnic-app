import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportCircleComponent } from './report-circle.component';

describe('ReportCircleComponent', () => {
  let component: ReportCircleComponent;
  let fixture: ComponentFixture<ReportCircleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReportCircleComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ReportCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
