import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeedsOwnershipComponent } from './seeds-ownership.component';

describe('SeedsOwnershipComponent', () => {
  let component: SeedsOwnershipComponent;
  let fixture: ComponentFixture<SeedsOwnershipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SeedsOwnershipComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SeedsOwnershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
