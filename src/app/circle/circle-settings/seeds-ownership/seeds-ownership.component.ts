import { Component, OnInit } from '@angular/core';
import { CircleSeedsConnectionGQL, SeedEdge } from '@app/graphql/seeds';
import { ActivatedRoute, Router } from '@angular/router';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { PublicProfile } from '@app/graphql/profile';

@Component({
  selector: 'app-seeds-ownership',
  templateUrl: './seeds-ownership.component.html',
  styleUrls: ['./seeds-ownership.component.scss'],
})
export class SeedsOwnershipComponent extends GQLPaginationComponent implements OnInit {
  circleId = '';
  seeds: SeedEdge[] = [];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private circleSeedsConnectionGQL: CircleSeedsConnectionGQL
  ) {
    super();
  }

  ngOnInit(): void {
    let route = this.route.parent;
    if (!route) {
      route = this.route;
    }
    route.paramMap.subscribe((p) => {
      this.circleId = p.get('id') ?? '';
      this.resetPagination();
      this.loadMoreItems();
    });
  }

  override loadMoreItems() {
    this.circleSeedsConnectionGQL
      .fetch({
        circleID: this.circleId,
        cursor: {
          id: this.cursorId,
          limit: 50,
          dir: 'forward',
        },
      })
      .subscribe(({ data }) => {
        this.seeds.push(...data.circleSeedsConnection.edges!);
        this.cursorId = data.circleSeedsConnection.pageInfo!.lastId;
        this.hasNextPage = data.circleSeedsConnection.pageInfo!.hasNextPage;
      });
  }

  override resetPagination() {
    super.resetPagination();
    this.seeds = [];
  }

  get sumSeeds() {
    return this.seeds.reduce((sum, seed) => sum + seed.node.amountTotal, 0); // TODO: take from back-end, when provide it
  }

  goToProfile(p: PublicProfile) {
    this.router.navigate(['/u/' + p.username]);
  }
}
