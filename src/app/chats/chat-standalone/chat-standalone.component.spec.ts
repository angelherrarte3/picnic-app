import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatStandaloneComponent } from './chat-standalone.component';

describe('ChatStandaloneComponent', () => {
  let component: ChatStandaloneComponent;
  let fixture: ComponentFixture<ChatStandaloneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChatStandaloneComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ChatStandaloneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
