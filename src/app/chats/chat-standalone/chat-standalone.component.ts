import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { UploadAttachmentGQL } from '@app/graphql/attachment';
import { Chat, GetChatByIdGQL, SendChatMessageGQL } from '@app/graphql/chat';
import { ShortNumberPipe } from '@app/pipes/short-number.pipe';
import { ShorthandPipe } from '@app/pipes/shorthand.pipe';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';
import { ChatSidebarService } from '@app/services/chat-sidebar.service';
import { NavigationService } from '@app/services/navigation.service';
import { ThemeService } from '@app/services/theme.service';
import { ChatComponent } from '@app/types/chat-component';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'chat-standalone',
  templateUrl: './chat-standalone.component.html',
  styleUrls: ['./chat-standalone.component.scss'],
})
export class ChatStandaloneComponent extends ChatComponent implements OnInit {
  override chat?: Chat;
  initialLoading: boolean = true;

  constructor(
    bubbleChatState: BubbleChatStateService,
    chatSidebarService: ChatSidebarService,
    sendMessageGQL: SendChatMessageGQL,
    uploadAttachmentGQL: UploadAttachmentGQL,
    router: Router,
    private route: ActivatedRoute,
    private getChatByIdGQL: GetChatByIdGQL,
    private themeService: ThemeService,
    private navigationService: NavigationService
  ) {
    super(bubbleChatState, chatSidebarService, router, sendMessageGQL, uploadAttachmentGQL);
  }

  ngOnInit() {
    this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params: ParamMap) => {
      const chatId = params.get('id');

      this.getChatByIdGQL
        .fetch({
          id: chatId ?? '',
        })
        .subscribe(({ data }) => {
          this.chat = data.chat;
          this.initialLoading = false;
        });
    });
  }

  onTapGoBack = () => this.navigationService.goBack();
}
