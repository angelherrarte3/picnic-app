import { Component } from '@angular/core';
import { GlobalModalService } from '@app/services/global-modal.service';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss'],
})
export class ChatsComponent {
  constructor(private modalService: GlobalModalService) {}

  onTapStartChat() {
    this.modalService.open('create-chat');
  }
}
