import { isPlatformServer } from '@angular/common';
import { Component, HostListener, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { Router } from '@angular/router';
import { ChatExcerpt, ChatExcerptEdge, ChatFeedConnectionGQL, GetChatByIdGQL } from '@app/graphql/chat';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';

@Component({
  selector: 'app-chat-feed',
  templateUrl: './chat-feed.component.html',
  styleUrls: ['./chat-feed.component.scss'],
})
export class ChatFeedComponent extends GQLPaginationComponent implements OnInit {
  windowWidth: number = window.innerWidth;
  chatFeed: ChatExcerptEdge[] = [];

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.windowWidth = event.target.innerWidth;
  }

  constructor(
    private chatFeedGQL: ChatFeedConnectionGQL,
    private getChatByIdGQL: GetChatByIdGQL,
    private bubbleChatState: BubbleChatStateService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: string
  ) {
    super();
  }

  ngOnInit() {
    if (isPlatformServer(this.platformId)) return;
    this.loadMoreItems();
  }

  get isMobileLayout() {
    return this.windowWidth <= 768;
  }

  override loadMoreItems() {
    this.chatFeedGQL
      .fetch({
        cursor: {
          id: this.cursorId,
          limit: 10,
          dir: 'forward',
        },
      })
      .subscribe(({ data }) => {
        this.chatFeed.push(...data.chatFeedConnection.edges);
        this.cursorId = data.chatFeedConnection.pageInfo.lastId;
        this.hasNextPage = data.chatFeedConnection.pageInfo.hasNextPage;
      });
  }

  onOpenChatFeed(f: ChatExcerpt) {
    if (this.isMobileLayout) {
      this.router.navigate(['chat', f.id]);
      return;
    }

    this.getChatByIdGQL
      .fetch({
        id: f.id,
      })
      .subscribe(({ data }) => {
        this.bubbleChatState.addModalChat(data.chat);
      });
  }
}
