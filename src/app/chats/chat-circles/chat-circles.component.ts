import { isPlatformServer } from '@angular/common';
import { Component, HostListener, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { Router } from '@angular/router';
import { Chat } from '@app/graphql/chat';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';
import { ChatSidebarService } from '@app/services/chat-sidebar.service';

@Component({
  selector: 'app-chat-circles',
  templateUrl: './chat-circles.component.html',
  styleUrls: ['./chat-circles.component.scss'],
})
export class ChatCirclesComponent implements OnInit {
  windowWidth: number = window.innerWidth;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.windowWidth = event.target.innerWidth;
  }

  constructor(
    public chatSidebarService: ChatSidebarService,
    private bubbleChatState: BubbleChatStateService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: string
  ) {}

  ngOnInit() {
    if (isPlatformServer(this.platformId)) {
      return;
    }
    this.chatSidebarService.loadChats();
  }

  get isMobileLayout() {
    return this.windowWidth <= 768;
  }

  circleImage(c: Chat) {
    return c.circle?.imageFile ? c.circle?.imageFile : c.circle?.image;
  }

  circleEmoji(c: Chat) {
    return !c.circle?.imageFile;
  }

  onTapOpen(c: Chat) {
    if (this.isMobileLayout) {
      this.router.navigate(['chat', c.id]);
      return;
    }

    this.bubbleChatState.addModalChat(c);
  }
}
