import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatCirclesComponent } from './chat-circles.component';

describe('ChatCirclesComponent', () => {
  let component: ChatCirclesComponent;
  let fixture: ComponentFixture<ChatCirclesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChatCirclesComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ChatCirclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
