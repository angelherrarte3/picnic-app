import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatsRoutingModule } from './chats-routing.module';
import { ChatsComponent } from './chats.component';
import { ComponentsModule } from '@app/components/components.module';
import { ChatFeedComponent } from './chat-feed/chat-feed.component';
import { ChatCirclesComponent } from './chat-circles/chat-circles.component';
import { ChatDmsComponent } from './chat-dms/chat-dms.component';
import { PipesModule } from '@app/pipes/pipes.module';
import { ShorthandPipeModule } from '@app/pipes/shorthand.pipe';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FormsModule } from '@angular/forms';
import { ChatStandaloneComponent } from './chat-standalone/chat-standalone.component';

@NgModule({
  declarations: [ChatsComponent, ChatFeedComponent, ChatCirclesComponent, ChatDmsComponent, ChatStandaloneComponent],
  imports: [
    CommonModule,
    ChatsRoutingModule,
    ComponentsModule,
    PipesModule,
    ShorthandPipeModule,
    InfiniteScrollModule,
    FormsModule,
  ],
})
export class ChatsModule {}
