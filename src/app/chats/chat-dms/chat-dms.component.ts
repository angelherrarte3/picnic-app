import { isPlatformServer } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  Inject,
  OnInit,
  PLATFORM_ID,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { SearchInputComponent } from '@app/components/search-input/search-input.component';
import { ChatExcerptEdge, CreateSingleChatGQL, Chat } from '@app/graphql/chat';
import { PublicProfileEdge, UserConnectionGQL, PublicProfile } from '@app/graphql/profile';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';
import { ChatSidebarService } from '@app/services/chat-sidebar.service';
import { Subject, debounceTime, switchMap, map } from 'rxjs';

@Component({
  selector: 'app-chat-dms',
  templateUrl: './chat-dms.component.html',
  styleUrls: ['./chat-dms.component.scss'],
})
export class ChatDmsComponent implements OnInit, AfterViewInit {
  private readonly searchDmsSubject = new Subject<string>();
  @ViewChild('searchRef') searcRef: SearchInputComponent;
  chatFeed: ChatExcerptEdge[] = [];
  searchingDmsDropdown: boolean = false;
  searchDmsUsers: PublicProfileEdge[] = [];
  windowWidth: number = window.innerWidth;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.windowWidth = event.target.innerWidth;
  }

  constructor(
    public chatSidebarService: ChatSidebarService,
    private bubbleChatState: BubbleChatStateService,
    private userConnection: UserConnectionGQL,
    private createSingleChatGQL: CreateSingleChatGQL,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: string
  ) {}

  ngOnInit() {
    if (isPlatformServer(this.platformId)) {
      return;
    }

    this.searchDmsSubject
      .pipe(
        debounceTime(300),
        switchMap((searchQuery) => {
          return this.userConnection
            .fetch({
              searchQuery: searchQuery,
              cursor: {
                id: '',
                limit: 20,
                dir: 'forward',
              },
            })
            .pipe(map(({ data }) => data.usersConnection.edges));
        })
      )
      .subscribe((users) => {
        this.searchDmsUsers = users;
      });

    this.chatSidebarService.loadChats();
  }

  ngAfterViewInit() {
    if (window.history.state.create) {
      this.searcRef.focus();
    }
  }

  get isMobileLayout() {
    return this.windowWidth <= 768;
  }

  onTapOpen(c: Chat) {
    if (this.isMobileLayout) {
      this.router.navigate(['chat', c.id]);
      return;
    }

    this.bubbleChatState.addModalChat(c);
  }

  onDmsSearch(v: string) {
    if (!v) {
      this.searchingDmsDropdown = false;
      return;
    }
    this.searchingDmsDropdown = true;
    this.searchDmsSubject.next(v);
  }

  onCreateDm(u: PublicProfile) {
    this.createSingleChatGQL
      .mutate({
        userIds: [u.id],
      })
      .subscribe(({ data }) => {
        if (this.isMobileLayout) {
          this.router.navigate(['chat', data?.createSingleChat.id]);
          return;
        }

        this.bubbleChatState.addModalChat(data!.createSingleChat);
      });
    this.searchingDmsDropdown = false;
    this.chatSidebarService.hide();
  }
}
