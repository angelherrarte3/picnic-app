import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatDmsComponent } from './chat-dms.component';

describe('ChatDmsComponent', () => {
  let component: ChatDmsComponent;
  let fixture: ComponentFixture<ChatDmsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChatDmsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ChatDmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
