import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { ChatsComponent } from './chats.component';
import { ChatFeedComponent } from './chat-feed/chat-feed.component';
import { ChatCirclesComponent } from './chat-circles/chat-circles.component';
import { ChatDmsComponent } from './chat-dms/chat-dms.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'chats',
      data: { title: marker('Chats') },
      component: ChatsComponent,
      children: [
        {
          path: '',
          redirectTo: 'feed',
          pathMatch: 'full',
        },
        {
          path: 'feed',
          component: ChatFeedComponent,
        },
        {
          path: 'circles',
          component: ChatCirclesComponent,
        },
        {
          path: 'dms',
          component: ChatDmsComponent,
        },
      ],
    },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChatsRoutingModule {}
