export class ClassUtils {
  static removeNullEmptyKeys<T>(obj: T): T {
    const newObj = { ...obj };

    for (const key in newObj) {
      if ((newObj as Object).hasOwnProperty(key)) {
        const value = newObj[key];

        if (value === null || value === '' || value === undefined) {
          delete newObj[key];
        }
      }
    }

    return newObj;
  }
}
