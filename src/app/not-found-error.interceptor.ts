import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class NotFoundErrorInterceptor implements HttpInterceptor {
  grapqhlPathsToCheck = ['getPost', 'chat', 'getCircleByName', 'profileGetUserIDByName'];

  constructor(private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap({
        next: (event) => {
          if (event instanceof HttpResponse) {
            const errors = event.body.errors;
            if (errors && errors.length > 0) {
              if (errors[0].message.includes('NotFound') && this.grapqhlPathsToCheck.includes(errors[0].path[0])) {
                this.router.navigate(['404']);
              }
            }
          }
          return event;
        },
      })
    );
  }
}
