import { TestBed } from '@angular/core/testing';

import { NotFoundErrorInterceptor } from './not-found-error.interceptor';

describe('NotFoundErrorInterceptor', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [NotFoundErrorInterceptor],
    })
  );

  it('should be created', () => {
    const interceptor: NotFoundErrorInterceptor = TestBed.inject(NotFoundErrorInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
