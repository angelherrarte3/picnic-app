import player from 'lottie-web';
import { ConfigService } from '@app/services/config.service';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from '@env/environment';
import { getAuth, provideAuth } from '@angular/fire/auth';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getAnalytics, provideAnalytics } from '@angular/fire/analytics';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { CommonModule } from '@angular/common';
import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { RouteReuseStrategy, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ApiPrefixInterceptor, ErrorHandlerInterceptor, RouteReusableStrategy, SharedModule } from '@shared';
import { ShellModule } from '@app/shell/shell.module';
import { FeedModule } from '@app/feed/feed.module';
import { NotificationsModule } from '@app/notifications/notifications.module';
import { AuthModule, CredentialsService } from '@app/auth';
import { ProfileModule } from '@app/profile/profile.module';
import { CircleModule } from '@app/circle/circle.module';
import { PodsModule } from '@app/pods/pods.module';
import { OnboardingModule } from '@app/onboarding/onboarding.module';
import { EmbedModule } from '@app/embed/embed.module';
import { DmModule } from '@app/dm/dm.module';
import { DebugModule } from '@app/debug/debug.module';
import { ProgressModule } from '@app/progress/progress.module';
import { CreatePostModule } from '@app/create-post/create-post.module';
import { ClipboardModule } from 'ngx-clipboard';
import { DevelopersModule } from '@app/developers/developers.module';
import { MarkdownModule } from 'ngx-markdown';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { AngularSvgIconPreloaderModule } from 'angular-svg-icon-preloader';
import { LottieModule } from 'ngx-lottie';
import { ScreenTrackingService } from '@angular/fire/compat/analytics';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AppComponent } from '@app/app.component';
import { AppRoutingModule } from '@app/app-routing.module';
import { RecaptchaV3Module } from 'ng-recaptcha';
import { GraphQLModule } from '@app/graphql.module';
import { forkJoin } from 'rxjs';
import { ChatsModule } from './chats/chats.module';
import { MomentModule } from 'ngx-moment';
import { PageNotFoundModule } from '@app/page-not-found/page-not-found.module';
import { NotFoundErrorInterceptor } from './not-found-error.interceptor';
import { ComponentsModule } from './components/components.module';
import { PrivacyPolicyModule } from '@app/privacy-policy/privacy-policy.module';

export function playerFactory() {
  return player;
}

export function initFactory(config: ConfigService, credentials: CredentialsService) {
  return () => forkJoin([config.loadConfig(), credentials.checkToken()]);
}

@NgModule({
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    provideAuth(() => getAuth()),
    provideFirebaseApp(() => initializeApp(environment.firebaseConfig)),
    provideAnalytics(() => getAnalytics()),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    CommonModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    FormsModule,
    HttpClientModule,
    HammerModule,
    RouterModule,
    TranslateModule.forRoot(),
    NgbModule,
    SharedModule,
    ShellModule,
    FeedModule,
    NotificationsModule,
    AuthModule,
    ProfileModule,
    CircleModule,
    PodsModule,
    OnboardingModule,
    EmbedModule,
    DmModule,
    DebugModule,
    ProgressModule,
    CreatePostModule,
    ClipboardModule,
    DevelopersModule,
    PageNotFoundModule,
    ComponentsModule,
    ChatsModule,
    MarkdownModule.forRoot({}),
    MomentModule,
    AngularSvgIconModule.forRoot(),
    LottieModule.forRoot({ player: playerFactory }),
    NgbDropdownModule,
    GraphQLModule,
    PrivacyPolicyModule,
    RecaptchaV3Module,
    AppRoutingModule, // must be imported as the last module as it contains the fallback route
  ],
  declarations: [AppComponent],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initFactory,
      deps: [ConfigService, CredentialsService],
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiPrefixInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NotFoundErrorInterceptor,
      multi: true,
    },
    {
      provide: RouteReuseStrategy,
      useClass: RouteReusableStrategy,
    },
    ScreenTrackingService,
  ],
})
export class GeneralModule {}
