import { Component, OnInit } from '@angular/core';
import { NotificationEdge, NotificationsConnectionGQL } from '@app/graphql/notifications';
import { ChangeFollowStatusGQL } from '@app/graphql/profile';
import { Router } from '@angular/router';
import { cloneDeep } from '@apollo/client/utilities';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit {
  notifications: NotificationEdge[] = [];
  hasNext = false;
  cursorId = '';
  loading = false;

  constructor(
    private notificationsGQL: NotificationsConnectionGQL,
    private router: Router,
    private changeFollowStatus: ChangeFollowStatusGQL
  ) {}

  ngOnInit(): void {
    this.loadMore();
  }

  loadMore() {
    this.loading = true;
    this.notificationsGQL
      .fetch({
        cursor: {
          dir: 'forward',
          limit: 20,
          id: this.cursorId,
        },
      })
      .subscribe(({ data }) => {
        this.notifications.push(...cloneDeep(data.notificationGet.edges));
        this.hasNext = data.notificationGet.pageInfo.hasNextPage;
        this.cursorId = data.notificationGet.pageInfo.lastId;
        this.loading = false;
      });
  }

  onTapProfile(id: string) {
    this.router.navigate([`/user/${id}`]).then((r) => console.log(r));
  }

  onTapToggleFollow(notificationEdge: NotificationEdge): void {
    this.changeFollowStatus
      .mutate({
        shouldFollow: !notificationEdge.node?.fromInfo?.relation,
        userId: notificationEdge.node?.fromInfo?.id,
      })
      .subscribe((_) => {
        notificationEdge.node.fromInfo.relation = !notificationEdge.node?.fromInfo?.relation;
      });
  }
}
