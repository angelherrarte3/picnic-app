import { Component, OnInit, OnDestroy, PLATFORM_ID, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { merge, Subscription } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

import { environment } from '@env/environment';
import { Logger, UntilDestroy, untilDestroyed } from '@shared';
import { I18nService } from '@app/i18n';
import { PreferencesService } from './services/preferences.service';
import { AuthenticationService } from './auth';
import { NavigationService } from './services/navigation.service';
import { isPlatformBrowser } from '@angular/common';
import { GlobalModalService } from './services/global-modal.service';
import { CreateChatModalComponent } from './components/create-chat-modal/create-chat-modal.component';
import { SignUpModalComponent } from './components/sign-up-modal/sign-up-modal.component';
import { BuildingFeatureModalComponent } from './components/building-feature-modal/building-feature-modal.component';
import { CreateCircleModalComponent } from './components/create-circle-modal/create-circle-modal.component';
import { ReportModalComponent } from './components/report-modal/report-modal.component';

const log = new Logger('App');

@UntilDestroy()
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(CreateChatModalComponent)
  createChatModal: CreateChatModalComponent;
  @ViewChild(SignUpModalComponent) signUpModal: SignUpModalComponent;
  @ViewChild(BuildingFeatureModalComponent)
  buildingFeatureModal: BuildingFeatureModalComponent;
  @ViewChild(CreateCircleModalComponent)
  createCircleModal: CreateCircleModalComponent;
  @ViewChild(ReportModalComponent)
  reportContentModal: ReportModalComponent;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private translateService: TranslateService,
    private i18nService: I18nService,
    private preferencesService: PreferencesService,
    private authService: AuthenticationService,
    private navigationService: NavigationService,
    private globalModalService: GlobalModalService,
    @Inject(PLATFORM_ID) private platformId: string
  ) {}

  ngOnInit() {
    if (isPlatformBrowser(this.platformId) && 'serviceWorker' in navigator) {
      navigator.serviceWorker.getRegistrations().then(function (registrations) {
        //returns installed service workers
        if (registrations.length) {
          for (let registration of registrations) {
            registration.unregister();
          }
        }
      });
    }

    // Setup preferences
    this.preferencesService.initValues();

    // Save navigation history
    this.navigationService.startSaveHistory();

    // Setup logger
    if (environment.production) {
      Logger.enableProductionMode();
    }

    log.debug('init');

    // Setup translations
    this.i18nService.init(environment.defaultLanguage, environment.supportedLanguages);

    const onNavigationEnd = this.router.events.pipe(filter((event) => event instanceof NavigationEnd));

    // Change page title on navigation or language change, based on route data
    merge(this.translateService.onLangChange, onNavigationEnd)
      .pipe(
        map(() => {
          let route = this.activatedRoute;
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        filter((route) => route.outlet === 'primary'),
        switchMap((route) => route.data),
        untilDestroyed(this)
      )
      .subscribe((event) => {
        const title = event['title'];
        if (title) {
          this.titleService.setTitle(this.translateService.instant(title));
        }
      });
  }

  ngAfterViewInit() {
    // Setup global modals
    this.globalModalService.registerModals([
      this.createChatModal,
      this.signUpModal,
      this.buildingFeatureModal,
      this.createCircleModal,
      this.reportContentModal,
    ]);
  }

  ngOnDestroy() {
    this.i18nService.destroy();
  }
}
