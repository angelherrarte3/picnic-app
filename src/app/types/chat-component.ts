import { Directive, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Attachment, UploadAttachmentGQL } from '@app/graphql/attachment';
import { Chat, ChatMessage, SendChatMessageGQL } from '@app/graphql/chat';
import { ShortNumberPipe } from '@app/pipes/short-number.pipe';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';
import { ChatSidebarService } from '@app/services/chat-sidebar.service';

@Directive()
export abstract class ChatComponent {
  @Input() chat?: Chat;
  shortNumberPipe = new ShortNumberPipe();
  replyTo?: ChatMessage;
  attachments: Attachment[] = [];
  files: File[] = [];
  first = true;

  constructor(
    public bubbleChatState: BubbleChatStateService,
    public chatSidebarService: ChatSidebarService,
    public router: Router,
    private sendMessageGQL: SendChatMessageGQL,
    private uploadAttachmentGQL: UploadAttachmentGQL
  ) {}

  get avatar() {
    if (this.chat?.chatType === 'SINGLE') {
      return this.chat?.chatImage;
    }

    if (this.chat?.chatType === 'CIRCLE') {
      return this.chat.circle?.imageFile ? this.chat.circle.imageFile : this.chat.circle?.image;
    }

    return undefined;
  }

  get emojiMode() {
    return this.chat?.chatType === 'CIRCLE' && !this.chat.circle?.imageFile;
  }

  get isSingleChat() {
    return this.chat?.chatType === 'SINGLE';
  }

  get isGroupChat() {
    return this.chat?.chatType === 'GROUP';
  }

  get isCircleChat() {
    return this.chat?.chatType === 'CIRCLE';
  }

  get infoLabelChat(): string {
    if (this.chat?.chatType === 'GROUP' || this.chat?.chatType === 'CIRCLE') {
      return this.shortNumberPipe.transform(this.chat?.participantsCount ?? 0);
    }

    return '';
  }

  onTapProfile(username: any) {
    this.router.navigateByUrl(`/u/${username}`);
  }

  onTapCircle() {
    this.router.navigateByUrl(`/c/${this.chat?.circle?.urlName}`);
  }

  onTapReply(m: ChatMessage) {
    this.replyTo = m;
  }

  onTapCancelReply() {
    this.replyTo = undefined;
  }

  onTapDeleteAttachment(i: number) {
    this.files.splice(i, 1);
    this.attachments.splice(i, 1);
  }

  onTapUploadAttachment() {
    let input = document.createElement('input');
    input.type = 'file';
    input.accept = 'image/*,video/*';
    input.onchange = (_) => {
      this.uploadAttachmentGQL
        .mutate({
          payload: input.files![0],
        })
        .subscribe(({ data }) => {
          this.files.push(input.files![0]);
          this.attachments.push(data!.uploadAttachment);
        });
    };
    input.click();
  }

  onTapSendMessage(message: string, fromMobile?: boolean) {
    if (message.trim().length === 0) return;

    this.sendMessageGQL
      .mutate({
        chatId: this.chat?.id ?? '',
        message: {
          repliedToMessageId: this.replyTo?.id,
          content: message,
          type: 'TEXT',
          attachmentIds: this.attachments.map((v) => v.id),
        },
      })
      .subscribe(() => {
        this.attachments = [];
        this.files = [];
        this.replyTo = undefined;

        if (!fromMobile) {
          this.bubbleChatState.rebuildActiveChats(this.chat!);
          this.chatSidebarService.addChat(this.chat!);
        }
      });
  }
}
