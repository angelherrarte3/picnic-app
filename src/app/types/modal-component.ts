import { ElementRef } from '@angular/core';
import { PicnicModalOptions, PicnicModalType } from '@app/components/picnic-modal/picnic-modal.component';
import { ModalName } from '@app/services/global-modal.service';

export abstract class ModalComponent {
  abstract name: ModalName;
  options: PicnicModalOptions = {
    dismissClickOutside: true,
    type: PicnicModalType.Custom,
  };
  isOpen = false;

  constructor(public element: ElementRef) {}

  onCloseOutside(event: MouseEvent) {
    if (this.options.dismissClickOutside) {
      this.close();
    }
  }

  open(options?: PicnicModalOptions) {
    if (options) this.options = options;
    this.isOpen = true;
    document.body.appendChild(this.element.nativeElement);
  }

  close() {
    this.isOpen = false;
    this.element.nativeElement.remove();
    this.resetState();
  }

  abstract resetState(): void;
}
