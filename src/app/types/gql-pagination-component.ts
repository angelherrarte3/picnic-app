export abstract class GQLPaginationComponent {
  cursorId: string = '';
  hasNextPage: boolean = false;
  loadingNext: boolean = false;

  abstract loadMoreItems(): void;

  resetPagination(): void {
    this.cursorId = '';
    this.hasNextPage = false;
    this.loadingNext = false;
  }
}
