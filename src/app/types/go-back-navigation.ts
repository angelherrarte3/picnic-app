import { NavigationService } from '@app/services/navigation.service';

export abstract class GoBackNavigation {
  constructor(protected navigationService: NavigationService) {}

  goBack = () => this.navigationService.goBack();

  get history(): string[] {
    return this.navigationService.getHistory();
  }

  get previousUrl(): string {
    return this.navigationService.getPreviousUrl();
  }
}
