import { NgModule, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorthand',
})
export class ShorthandPipe implements PipeTransform {
  transform(value: number | undefined): string {
    if (value === undefined) {
      return '0';
    }
    if (value >= 1000 && value < 1000000) {
      return (value / 1000).toFixed(1) + 'K';
    } else if (value >= 1000000) {
      return (value / 1000000).toFixed(1) + 'M';
    } else {
      return value.toString();
    }
  }
}

@NgModule({
  declarations: [ShorthandPipe],
  exports: [ShorthandPipe],
})
export class ShorthandPipeModule {}
