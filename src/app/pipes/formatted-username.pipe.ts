import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formattedUsername',
})
export class FormattedUsernamePipe implements PipeTransform {
  transform(username: string): string {
    return `@${username}`;
  }
}
