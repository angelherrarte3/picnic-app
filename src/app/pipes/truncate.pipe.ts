import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate',
})
export class TruncatePipe implements PipeTransform {
  transform(value: string, limit: number, completeWords?: boolean, suffix?: string): string {
    if (!value) return '';

    if (value.length <= limit) return value;

    if (completeWords && value.indexOf(' ', limit) !== -1) {
      limit = value.indexOf(' ', limit);
    }

    return value.substring(0, limit) + (suffix || '...');
  }
}
