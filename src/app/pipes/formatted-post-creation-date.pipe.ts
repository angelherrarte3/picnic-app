import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formattedPostCreationDate',
})
export class FormattedPostCreationDatePipe implements PipeTransform {
  constructor(private datePipe: DatePipe) {}

  transform(createdAt: Date | undefined): string {
    let date = createdAt ?? new Date();
    return this.datePipe.transform(date, 'longDate')!;
  }
}
