import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countryCodeToFlag',
})
export class CountryCodeToFlagPipe implements PipeTransform {
  /** The `countryCode` is in ISO 3166-1 Alpha 2 format */
  transform(countryCode: string): string {
    const codePoints = countryCode
      .toUpperCase()
      .split('')
      .map((char) => 127397 + char.charCodeAt(0));
    return String.fromCodePoint(...codePoints);
  }
}
