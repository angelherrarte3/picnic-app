import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormattedUsernamePipe } from './formatted-username.pipe';
import { FormattedPostCreationDatePipe } from './formatted-post-creation-date.pipe';
import { TruncatePipe } from './truncate.pipe';
import { CountryCodeToFlagPipe } from './country-code-to-flag.pipe';
import { ShorthandPipeModule } from '@app/pipes/shorthand.pipe';
import { ShortNumberPipe } from './short-number.pipe';

@NgModule({
  declarations: [
    FormattedUsernamePipe,
    FormattedPostCreationDatePipe,
    CountryCodeToFlagPipe,
    TruncatePipe,
    ShortNumberPipe,
  ],
  imports: [CommonModule, ShorthandPipeModule],
  providers: [DatePipe, FormattedUsernamePipe, TruncatePipe, CountryCodeToFlagPipe],
  exports: [FormattedUsernamePipe, FormattedPostCreationDatePipe, TruncatePipe, ShortNumberPipe],
})
export class PipesModule {}
