import { Component, OnInit } from '@angular/core';
import { BlockedConnectionGQL, PrivateProfile, PublicProfileEdge } from '@app/graphql/profile';
import { TmpStateService } from '@app/services/tmp-state.service';
import { CredentialsService } from '@app/auth';
import { debounceTime, Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { cloneDeep } from '@apollo/client/utilities';

@Component({
  selector: 'app-blocked',
  templateUrl: './blocked.component.html',
  styleUrls: ['./blocked.component.scss'],
})
export class BlockedComponent extends GQLPaginationComponent implements OnInit {
  blocked: PublicProfileEdge[] = [];
  searchQuery = '';
  profileId = '';
  state!: TmpStateService;
  credentialsService!: CredentialsService;
  myProfile?: PrivateProfile;
  loading: boolean | undefined;
  override hasNextPage = true;

  private readonly searchSub = new Subject<string>();

  constructor(
    private route: ActivatedRoute,
    private cs: CredentialsService,
    private router: Router,
    private s: TmpStateService,
    private blockedGQL: BlockedConnectionGQL
  ) {
    super();
    this.state = s;
    this.credentialsService = cs;
  }

  ngOnInit(): void {
    this.getBlocked();
  }

  private getBlocked() {
    this.route.paramMap.subscribe((p) => {
      this.state.profileSubject.subscribe((p) => {
        this.myProfile = p;
        if (p === undefined) {
          return;
        }
        this.profileId = this.myProfile!.id;
        this.searchQuery = '';
        this.resetPagination();
        this.loadMoreItems();
      });
    });

    this.searchSub.pipe(debounceTime(300)).subscribe((searchQuery) => {
      this.searchQuery = searchQuery;
      this.resetPagination();
      this.loadMoreItems();
    });
  }

  override loadMoreItems() {
    this.loading = true;

    this.blockedGQL
      .fetch({
        cursor: {
          id: this.cursorId,
          dir: 'forward',
          limit: 50,
        },
      })
      .subscribe(({ data }) => {
        this.blocked.push(...cloneDeep(data.blockedUsersConnection.edges));
        this.cursorId = data.blockedUsersConnection.pageInfo.lastId;
        this.hasNextPage = data.blockedUsersConnection.pageInfo.hasNextPage;
        this.loading = false;
      });
  }

  override resetPagination() {
    super.resetPagination();
    this.blocked = [];
  }

  onTapProfile(p?: PublicProfileEdge) {
    this.router.navigate([`/u/${p?.node.username}`]);
  }
}
