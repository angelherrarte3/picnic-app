import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { ProfileComponent } from './profile.component';
import { PostsComponent } from '@app/profile/posts/posts.component';
import { CirclesComponent } from '@app/profile/circles/circles.component';
import { CollectionsComponent } from '@app/profile/collections/collections.component';
import { ProfileSettingsComponent } from './profile-settings/profile-settings.component';
import { FollowersComponent } from '@app/profile/followers/followers.component';
import { SeedsComponent } from '@app/profile/seeds/seeds.component';
import { EditComponent } from '@app/profile/edit/edit.component';
import { BlockedComponent } from '@app/profile/blocked/blocked.component';
import { ReportComponent } from '@app/profile/report/report.component';
import { CommunityGuidelinesComponent } from './community-guidelines/community-guidelines.component';
import { ProfileLanguageComponent } from './profile-language/profile-language.component';
import { AuthenticationGuard } from '@app/auth';

const childrenRoutes: Routes = [
  {
    path: '',
    redirectTo: 'posts',
    pathMatch: 'full',
  },
  {
    path: 'posts',
    component: PostsComponent,
  },
  {
    path: 'circles',
    component: CirclesComponent,
  },
  {
    path: 'collections',
    component: CollectionsComponent,
  },
];

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'user',
      component: ProfileComponent,
      data: { ownProfile: true },
      children: childrenRoutes,
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'u',
      component: ProfileComponent,
      data: { ownProfile: true },
      children: childrenRoutes,
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'user/followers',
      component: FollowersComponent,
      data: { ownProfile: true },
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'u/followers',
      component: FollowersComponent,
      data: { ownProfile: true },
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'user/settings',
      component: ProfileSettingsComponent,
      data: { ownProfile: true },
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'u/blocked',
      component: BlockedComponent,
      data: { ownProfile: true },
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'u/report',
      component: ReportComponent,
      data: { ownProfile: true },
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'u/community-guidelines',
      component: CommunityGuidelinesComponent,
      data: { ownProfile: true },
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'u/language',
      component: ProfileLanguageComponent,
      data: { ownProfile: true },
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'u/settings',
      component: ProfileSettingsComponent,
      data: { ownProfile: true },
      canActivate: [AuthenticationGuard],
    },

    {
      path: 'user/seeds',
      component: SeedsComponent,
      data: { ownProfile: true },
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'user/edit',
      component: EditComponent,
      data: { ownProfile: true },
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'u/seeds',
      component: SeedsComponent,
      data: { ownProfile: true },
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'user/:id',
      component: ProfileComponent,
      data: { ownProfile: false },
      children: childrenRoutes,
    },
    {
      path: 'u/:id',
      component: ProfileComponent,
      data: { ownProfile: false },
      children: childrenRoutes,
    },
    {
      path: 'user/:id/followers',
      component: FollowersComponent,
    },
    {
      path: 'u/:id/followers',
      component: FollowersComponent,
    },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
