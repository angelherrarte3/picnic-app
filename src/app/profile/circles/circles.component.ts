import { Component, OnInit } from '@angular/core';
import { ProfileStateService } from '@app/profile/profile-state.service';
import { CircleEdge, GetUserCirclesGQL } from '@app/graphql/circle';

@Component({
  selector: 'app-circles',
  templateUrl: './circles.component.html',
  styleUrls: ['./circles.component.scss'],
})
export class CirclesComponent implements OnInit {
  modCircles: CircleEdge[] = [];
  circles: CircleEdge[] = [];

  constructor(private getCircles: GetUserCirclesGQL, private profileState: ProfileStateService) {}

  ngOnInit() {
    this.profileState.profile.subscribe((p) => {
      this.getCircles
        .watch({
          getUserCircles: {
            roles: ['DIRECTOR', 'MODERATOR'],
            userId: p.id,
          },
          cursor: {
            id: '',
            limit: 10,
            dir: 'forward',
          },
        })
        .valueChanges.subscribe(({ data }) => {
          this.modCircles = data.getUserCircles.edges;
        });

      this.getCircles
        .watch({
          getUserCircles: {
            roles: ['MEMBER'],
            userId: p.id,
          },
          cursor: {
            id: '',
            limit: 24,
            dir: 'forward',
          },
        })
        .valueChanges.subscribe(({ data }) => {
          this.circles = data.getUserCircles.edges;
        });
    });
  }
}
