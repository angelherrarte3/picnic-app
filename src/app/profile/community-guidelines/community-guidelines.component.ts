import { Component } from '@angular/core';
import { GlobalModalService } from '@app/services/global-modal.service';

@Component({
  selector: 'app-community-guidelines',
  templateUrl: './community-guidelines.component.html',
  styleUrls: ['./community-guidelines.component.scss'],
})
export class CommunityGuidelinesComponent {
  constructor(private modalService: GlobalModalService) {}

  onTapReportLink() {
    this.modalService.open('building-feature', {
      dismissClickOutside: true,
    });
  }
}
