import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { SettingItem } from '@app/components/settings-list/settings-list.component';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/auth';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.scss'],
})
export class ProfileSettingsComponent implements OnInit {
  profileSettings: SettingItem[] = [
    {
      label: '🌐 language',
      path: 'u/language',
    },
    {
      label: '⛔ blocked list',
      path: 'u/blocked',
    },
    {
      label: '❗️report',
      path: 'u/report',
    },
    {
      label: '📚 community guidelines',
      path: 'u/community-guidelines',
    },
    {
      label: '✅ get verified',
      path: 'https://airtable.com/shrPbk4xxOIseMYh0',
    },
  ];

  isMobileLayout = false;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    @Inject(PLATFORM_ID) private platformId: string
  ) {
    this.isMobileLayout = window.innerWidth < 600;
  }

  ngOnInit(): void {}

  onTapView(item: string): void {
    if (this.isValidHttpUrl(item)) {
      window.open(item, '_blank');
    } else {
      this.router.navigate([item]).then((r) => console.log(r));
    }
  }

  isValidHttpUrl(url: string): boolean {
    let newUrl;

    try {
      newUrl = new URL(url);
    } catch (_) {
      return false;
    }

    return newUrl.protocol === 'http:' || newUrl.protocol === 'https:';
  }

  onSignOut() {
    this.authenticationService.logout();
    window.location.href = '/';
  }
}
