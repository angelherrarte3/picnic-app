import { Component, OnInit } from '@angular/core';
import { EditProfileGQL, MyProfileGQL, PrivateProfile } from '@app/graphql/profile';
import { ProfileStateService } from '@app/profile/profile-state.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit {
  profile: PrivateProfile;
  username: string;
  name: string;
  bio: string;
  loading: boolean = false;
  error: string;

  constructor(
    private profileState: ProfileStateService,
    private myProfileGQL: MyProfileGQL,
    private editProfileGQL: EditProfileGQL,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.loading = true;
    this.route.data.subscribe((data) => {
      this.getProfile();
    });
  }

  private getProfile() {
    this.myProfileGQL.fetch().subscribe(({ data }) => {
      this.profile = data.myProfile;
      this.profileState.profile.next(this.profile);
      this.username = this.profile.username;
      this.name = this.profile.fullName;
      this.bio = this.profile.bio;

      this.loading = false;
    });
  }

  onTapUpdateProfile() {
    this.loading = true;
    this.editProfileGQL
      .mutate({
        bio: this.bio,
        username: this.username,
        name: this.name,
      })
      .subscribe(
        ({ data }) => {
          this.error = '';

          const profileCopy = { ...this.profile };

          profileCopy.bio = this.bio;
          profileCopy.username = this.username;
          profileCopy.fullName = this.name;

          this.profile = {
            ...profileCopy,
          };

          console.log(this.profile);
          this.profileState.profile.next(this.profile);
          this.getProfile();
          console.log('2');

          this.loading = false;
        },
        (error) => {
          this.error = 'username already exists';
          this.loading = false;
        }
      );
  }
}
