import { Component, OnInit } from '@angular/core';
import { ProfileStateService } from '@app/profile/profile-state.service';
import { PostEdge, UserPostConnectionGQL } from '@app/graphql/content';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { cloneDeep } from '@apollo/client/utilities';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss'],
})
export class PostsComponent extends GQLPaginationComponent implements OnInit {
  posts: PostEdge[] = [];

  constructor(private profileState: ProfileStateService, private userPosts: UserPostConnectionGQL) {
    super();
  }

  ngOnInit(): void {
    this.profileState.profile.subscribe((p) => {
      this.userPosts
        .watch({
          userId: p.id,
          cursor: {
            id: '',
            limit: 10,
            dir: 'forward',
          },
        })
        .valueChanges.subscribe(({ data }) => {
          this.posts = data.userPostsConnection.edges;
          this.cursorId = data.userPostsConnection.pageInfo.lastId;
          this.hasNextPage = data.userPostsConnection.pageInfo.hasNextPage;
        });
    });
  }

  override loadMoreItems(): void {
    if (!this.hasNextPage) return;

    this.loadingNext = true;
    this.profileState.profile.subscribe((p) => {
      this.userPosts
        .watch({
          userId: p.id,
          cursor: {
            id: this.cursorId,
            limit: 10,
            dir: 'forward',
          },
        })
        .valueChanges.subscribe(({ data }) => {
          this.posts = this.posts.concat(
            ...data.userPostsConnection.edges.map((e) => {
              return cloneDeep(e);
            })
          );
          this.loadingNext = false;
          this.cursorId = data.userPostsConnection.pageInfo.lastId;
          this.hasNextPage = data.userPostsConnection.pageInfo.hasNextPage;
        });
    });
  }
}
