import { BehaviorSubject, ReplaySubject, Subject } from 'rxjs';
import { PrivateProfile, PublicProfile } from '@app/graphql/profile';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProfileStateService {
  profile = new ReplaySubject<PublicProfile | PrivateProfile>(1);
  ownProfile = false;
  forYouFeedId = new BehaviorSubject<string>('');
}
