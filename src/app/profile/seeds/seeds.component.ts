import { Component, OnInit } from '@angular/core';
import { SeedEdge, UserSeedsConnectionGQL } from '@app/graphql/seeds';
import { TmpStateService } from '@app/services/tmp-state.service';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { Router } from '@angular/router';
import { Circle } from '@app/graphql/circle';

@Component({
  selector: 'app-seeds',
  templateUrl: './seeds.component.html',
  styleUrls: ['./seeds.component.scss'],
})
export class SeedsComponent extends GQLPaginationComponent implements OnInit {
  profileId = '';
  seeds: SeedEdge[] = [];

  constructor(
    private userSeedsConnection: UserSeedsConnectionGQL,
    private state: TmpStateService,
    private router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.state.profileSubject.subscribe((p) => {
      if (!p) {
        return;
      }

      this.profileId = p.id;
      this.resetPagination();
      this.loadMoreItems();
    });
  }

  override loadMoreItems() {
    this.userSeedsConnection
      .fetch({
        userID: this.profileId,
        cursor: {
          id: this.cursorId,
          dir: 'forward',
          limit: 50,
        },
      })
      .subscribe(({ data }) => {
        this.seeds.push(...data.userSeedsConnection.edges!);
        this.cursorId = data.userSeedsConnection.pageInfo!.lastId;
        this.hasNextPage = data.userSeedsConnection.pageInfo!.hasNextPage;
      });
  }

  override resetPagination() {
    super.resetPagination();
    this.seeds = [];
  }

  goToCircle(c: Circle) {
    this.router.navigate(['/c/' + c.name]);
  }
}
