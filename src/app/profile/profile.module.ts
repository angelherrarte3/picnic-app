import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { SharedModule } from '@app/@shared';
import { ComponentsModule } from '@app/components/components.module';
import { PostsComponent } from './posts/posts.component';
import { CirclesComponent } from './circles/circles.component';
import { CollectionsComponent } from './collections/collections.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ProfileSettingsComponent } from './profile-settings/profile-settings.component';
import { FollowersComponent } from './followers/followers.component';
import { SeedsComponent } from './seeds/seeds.component';
import { EditComponent } from './edit/edit.component';
import { FormsModule } from '@angular/forms';
import { OnboardingModule } from '@app/onboarding/onboarding.module';
import { BlockedComponent } from './blocked/blocked.component';
import { ReportComponent } from './report/report.component';
import { CommunityGuidelinesComponent } from './community-guidelines/community-guidelines.component';
import { ProfileLanguageComponent } from './profile-language/profile-language.component';

@NgModule({
  declarations: [
    ProfileComponent,
    PostsComponent,
    CirclesComponent,
    CollectionsComponent,
    ProfileSettingsComponent,
    FollowersComponent,
    SeedsComponent,
    EditComponent,
    BlockedComponent,
    ReportComponent,
    CommunityGuidelinesComponent,
    ProfileLanguageComponent,
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    ComponentsModule,
    InfiniteScrollModule,
    FormsModule,
    OnboardingModule,
  ],
})
export class ProfileModule {}
