import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { TmpStateService } from '@app/services/tmp-state.service';
import {
  ChangeFollowStatusGQL,
  ContentStatsForProfileGQL,
  MyProfileGQL,
  PrivateProfile,
  ProfileGetUserIDByNameGQL,
  ProfileStats,
  PublicProfile,
  UserGQL,
} from '@app/graphql/profile';
import { ProfileStateService } from '@app/profile/profile-state.service';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';

import { CredentialsService } from '@app/auth';
import { BubbleChatStateService } from '@app/services/bubble-chat-state.service';
import { CreateSingleChatGQL } from '@app/graphql/chat';
import { Meta, Title } from '@angular/platform-browser';
import { environment } from '@env/environment.prod';
import { GlobalModalService } from '@app/services/global-modal.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  ownProfile: boolean = true;
  profile?: PublicProfile | PrivateProfile;
  profileStats?: ProfileStats;
  gridChild?: GQLPaginationComponent;

  constructor(
    public state: TmpStateService,
    private profileState: ProfileStateService,
    private route: ActivatedRoute,
    private userGQL: UserGQL,
    private myProfileGQL: MyProfileGQL,
    private profileGetUserIDByNameGQL: ProfileGetUserIDByNameGQL,
    private contentStatsForProfileGQL: ContentStatsForProfileGQL,
    private modalService: GlobalModalService,
    private credentialsService: CredentialsService,
    private router: Router,
    private bubbleChatService: BubbleChatStateService,
    private createSingleChatGQL: CreateSingleChatGQL,
    private changeFollowStatus: ChangeFollowStatusGQL,
    private meta: Meta,
    private titleService: Title
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const [routePrefix, id] = this.route.snapshot.url.map((segment) => segment.path);

      if (routePrefix === 'u' && id !== undefined) {
        //for example : http://localhost:4200/u/emily/posts

        this.profileGetUserIDByNameGQL.fetch({ userName: id }, { errorPolicy: 'all' }).subscribe((result) => {
          const theId = result.data.profileGetUserIDByName.userId;
          this.getProfileData(theId);
        });
      } else {
        this.getProfileData(id);
      }
    });
  }

  private getProfileData(theId: string | undefined): void {
    this.route.data.subscribe((data) => {
      this.ownProfile = this.profileState.ownProfile = data['ownProfile'];

      if (!this.ownProfile) {
        this.route.paramMap.subscribe((param) => {
          this.userGQL.fetch({ userId: theId }, { errorPolicy: 'all' }).subscribe((result) => {
            this.profile = result.data.user;
            this.profileState.profile.next(this.profile);
            this.fetchProfileStats(theId);
            this.meta.updateTag({
              property: 'og:title',
              content: this.profile.username,
            });
            this.meta.updateTag({
              property: 'og:description',
              content: this.profile.bio,
            });
            this.meta.updateTag({
              property: 'og:url',
              content: `${environment.host}/u/${this.profile.username}`,
            });
            this.meta.updateTag({
              property: 'og:image',
              content: this.profile.profileImage,
            });
            this.titleService.setTitle(this.profile.fullName != null ? this.profile.fullName : this.profile.username);
          });
        });
      } else {
        this.myProfileGQL.fetch().subscribe((result) => {
          this.profile = result.data.myProfile;
          this.profileState.profile.next(this.profile);
          if (this.profile?.id) {
            this.fetchProfileStats(this.profile.id);
          }
        });
      }
    });
  }

  private fetchProfileStats(userID: string | undefined): void {
    this.contentStatsForProfileGQL
      .fetch({ userID }) // Change back to userId
      .subscribe(({ data }) => {
        this.profileStats = {
          contentStatsForProfile: data.contentStatsForProfile,
        };
      });
  }

  get myProfile(): PrivateProfile | undefined {
    return this.profile ? (this.profile as PrivateProfile) : undefined;
  }

  get isGuestUser() {
    return !this.credentialsService.isAuthenticated();
  }

  get isFollowing() {
    return this.profile ? (this.profile as PublicProfile).isFollowing : false;
  }

  edit() {
    this.router.navigateByUrl(`/user/edit`).then((r) => console.log(r));
  }
  handlePostCreate() {
    if (this.isGuestUser) {
      this.showSignUpModal();
      return;
    }

    this.router.navigateByUrl(`create-post`).then((r) => console.log(r));
  }

  showSignUpModal = () => this.modalService.open('sign-up');

  onScroll = () => this.gridChild?.loadMoreItems();

  onActivate = (event: any) => (this.gridChild = event instanceof GQLPaginationComponent ? event : undefined);

  onTapSeeds = () => this.router.navigate(['/u/seeds']);

  onDmClick() {
    this.createSingleChatGQL
      .mutate({
        userIds: [this.profile!.id],
      })
      .subscribe(({ data }) => {
        this.bubbleChatService.addModalChat(data!.createSingleChat);
      });
  }

  onTapToggleFollow(): void {
    if (this.ownProfile === true) return;

    const publicProfile = this.profile as PublicProfile;
    this.changeFollowStatus
      .mutate({
        shouldFollow: !publicProfile.isFollowing,
        userId: publicProfile.id,
      })
      .subscribe((_) => {
        this.profile = {
          ...publicProfile,
          isFollowing: !publicProfile.isFollowing,
        };
      });
  }
}
