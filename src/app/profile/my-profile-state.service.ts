import { ReplaySubject } from 'rxjs';
import { PrivateProfile, PublicProfile } from '@app/graphql/profile';
import { Injectable } from '@angular/core';
import { MyProfileGQL } from '@app/graphql/profile';

@Injectable({
  providedIn: 'root',
})
export class MyProfileStateService {
  profile = new ReplaySubject<PublicProfile | PrivateProfile>(1);

  constructor(myProfileGQL: MyProfileGQL) {
    myProfileGQL.fetch().subscribe(({ data }) => {
      this.profile.next(data.myProfile);
    });
  }
}
