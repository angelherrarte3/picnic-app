import { Component, OnInit } from '@angular/core';
import { GlobalReportGQL } from '@app/graphql/profile';
import { SimpleDropdownItem } from '@app/components/simple-dropdown/simple-dropdown.component';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnInit {
  text = '';
  enableSendReport = false;
  reason = 'spam';
  circleItems: SimpleDropdownItem<string, void>[] = [
    { label: 'sexual content', value: 'sexual content' },
    {
      label: 'graphic content',
      value: 'graphic content',
    },
    {
      label: 'other',
      value: 'other',
    },
  ];

  constructor(private globalReportGql: GlobalReportGQL) {}

  ngOnInit(): void {}

  handleItemSelect(item: SimpleDropdownItem<string, void>) {
    this.reason = item.label;
  }

  sendReport() {
    this.globalReportGql
      .mutate({
        description: this.text,
        reason: this.reason,
        entityId: '',
        entity: 'ALL',
      })
      .subscribe((result) => {
        console.log('success');
      });
  }
}
