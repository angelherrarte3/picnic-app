import { Component, OnInit } from '@angular/core';
import { ListLanguagesGQL, PLanguage } from '@app/graphql/languages';
import { PrivateProfile, UpdateUserPreferredLanguagesGQL } from '@app/graphql/profile';
import { TmpStateService } from '@app/services/tmp-state.service';

@Component({
  selector: 'app-profile-language',
  templateUrl: './profile-language.component.html',
  styleUrls: ['./profile-language.component.scss'],
})
export class ProfileLanguageComponent implements OnInit {
  languages: PLanguage[] = [];
  loading: boolean | undefined;
  loadingSave: boolean = false;
  initialLanguage: PLanguage | undefined;
  selectedLanguage: PLanguage | undefined;
  profile?: PrivateProfile;

  constructor(
    private languagesGQL: ListLanguagesGQL,
    private state: TmpStateService,
    private updateUserPreferredLanguagesGQL: UpdateUserPreferredLanguagesGQL
  ) {}

  ngOnInit(): void {
    this.state.profileSubject.subscribe((p) => {
      this.profile = p;
      this.languagesGQL
        .fetch({
          filter: 'ENABLED',
        })
        .subscribe(({ data, loading }) => {
          this.languages = data.listLanguages.filter((language) => language.enabled && language.iso3);
          this.setInitialLanguage();
          this.loading = false;
        });
    });
    this.loading = true;
  }

  onTapSaveChanges() {
    this.loadingSave = true;
    this.updateUserPreferredLanguagesGQL
      .mutate({
        languagesCodes: [this.selectedLanguage!.tag],
      })
      .subscribe(({ data }) => {
        this.loadingSave = false;
      });
  }

  onLanguageSelect(language: PLanguage) {
    this.selectedLanguage = language;
  }

  setInitialLanguage() {
    const userInitialLanguage = this.languages.find((v) => v.tag == this.profile?.languages[0]);
    this.initialLanguage = userInitialLanguage;
    this.selectedLanguage = userInitialLanguage;
  }
}
