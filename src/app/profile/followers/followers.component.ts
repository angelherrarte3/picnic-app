import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TmpStateService } from '@app/services/tmp-state.service';
import {
  ChangeFollowStatusGQL,
  FollowersConnectionGQL,
  PrivateProfile,
  ProfileGetUserIDByNameGQL,
  PublicProfileEdge,
} from '@app/graphql/profile';
import { GQLPaginationComponent } from '@app/types/gql-pagination-component';
import { cloneDeep } from '@apollo/client/utilities';
import { debounceTime, Subject } from 'rxjs';
import { CredentialsService } from '@app/auth';

@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.scss'],
})
export class FollowersComponent extends GQLPaginationComponent implements OnInit {
  followers: PublicProfileEdge[] = [];
  searchQuery = '';
  profileId = '';
  state!: TmpStateService;
  credentialsService!: CredentialsService;
  myProfile?: PrivateProfile;
  loading: boolean | undefined;
  override hasNextPage = true;

  private readonly searchSub = new Subject<string>();

  constructor(
    private route: ActivatedRoute,
    private cs: CredentialsService,
    private router: Router,
    private s: TmpStateService,
    private profileGetUserIDByNameGQL: ProfileGetUserIDByNameGQL,
    private followersGQL: FollowersConnectionGQL,
    private changeFollowStatus: ChangeFollowStatusGQL
  ) {
    super();
    this.state = s;
    this.credentialsService = cs;
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((p) => {
      const [routePrefix, id] = this.route.snapshot.url.map((segment) => segment.path);

      this.profileId = p.get('id') ?? '';
      if (this.profileId === '') {
        this.state.profileSubject.subscribe((profile) => {
          this.myProfile = profile;
          if (profile === undefined) {
            return;
          }
          this.profileId = this.myProfile!.id;
          this.initiateFollowerList();
        });
      } else {
        if (routePrefix === 'u') {
          this.profileGetUserIDByNameGQL.fetch({ userName: id }).subscribe((result) => {
            this.profileId = result.data.profileGetUserIDByName.userId;
            this.initiateFollowerList();
          });
        } else {
          this.initiateFollowerList();
        }
      }
    });

    this.searchSub.pipe(debounceTime(300)).subscribe((searchQuery) => {
      this.searchQuery = searchQuery;
      this.resetPagination();
      this.loadMoreItems();
    });
  }

  private initiateFollowerList() {
    this.searchQuery = '';
    this.resetPagination();
    this.loadMoreItems();
  }

  override loadMoreItems() {
    this.loading = true;
    this.followersGQL
      .fetch({
        userId: this.profileId,
        searchQuery: this.searchQuery,
        cursor: {
          id: this.cursorId,
          dir: 'forward',
          limit: 50,
        },
      })
      .subscribe(({ data }) => {
        this.followers.push(...cloneDeep(data.followersConnection.edges));
        this.cursorId = data.followersConnection.pageInfo.lastId;
        this.hasNextPage = data.followersConnection.pageInfo.hasNextPage;
        this.loading = false;
      });
  }

  override resetPagination() {
    super.resetPagination();
    this.followers = [];
  }

  onSearch(query: string) {
    this.searchSub.next(query);
  }

  onTapFollow(profile: PublicProfileEdge) {
    this.changeFollowStatus
      .mutate({
        shouldFollow: !profile.relations!.following,
        userId: profile.node.id,
      })
      .subscribe((_) => {
        profile.relations!.following = !profile.relations!.following;
      });
  }

  onTapProfile(p?: PublicProfileEdge) {
    this.router.navigate([`/u/${p?.node.username}`]);
  }
}
