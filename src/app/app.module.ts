import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import player from 'lottie-web';
import { ConfigService } from '@app/services/config.service';
import { GeneralModule } from '@app/general.module';
import { AngularSvgIconPreloaderModule } from 'angular-svg-icon-preloader';

export function playerFactory() {
  return player;
}

export function initFactory(config: ConfigService) {
  return () => config.loadConfig();
}

@NgModule({
  imports: [
    AngularSvgIconPreloaderModule.forRoot({
      configUrl: './assets/icons/icons-config.json',
    }),
    GeneralModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
