import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { I18nModule } from '@app/i18n';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login.component';
import { ComponentsModule } from '@app/components/components.module';
import { DownloadPageComponent } from './dowload-picnic/download-page.component';
import { RECAPTCHA_V3_SITE_KEY, RecaptchaModule } from 'ng-recaptcha';
import { ConfigService } from '@app/services/config.service';
import { LoginContainerComponent } from '@app/auth/login-container.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    NgbModule,
    I18nModule,
    AuthRoutingModule,
    ComponentsModule,
    RecaptchaModule,
  ],
  declarations: [LoginComponent, DownloadPageComponent, LoginContainerComponent],
  providers: [
    {
      provide: RECAPTCHA_V3_SITE_KEY,
      useFactory: (config: ConfigService) => {
        return config.recaptchaSiteKey;
      },
      deps: [ConfigService],
    },
  ],
})
export class AuthModule {}
