import { Injectable, Injector } from '@angular/core';
import { BehaviorSubject, Observable, of, tap, throwError } from 'rxjs';
import { LocalStorageService } from '@app/services/local-storage.service';
import { catchError, map } from 'rxjs/operators';
import { RefreshTokensGQL, SignUpGuestGQL } from '@app/graphql/auth';
import jwtDecode from 'jwt-decode';

export interface Credentials {
  userid: string;
  username: string;
  token: string;
  refreshToken: string;
  guest?: boolean;
}

const credentialsKey = 'credentials';

/**
 * Provides storage for authentication credentials.
 * The Credentials interface should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root',
})
export class CredentialsService {
  private _credentials: Credentials | null = null;
  private _credentialsSubject = new BehaviorSubject<Credentials | null>(null);

  constructor(private localStorage: LocalStorageService, private injector: Injector) {
    const savedCredentials = localStorage.get<Credentials>(credentialsKey);
    if (savedCredentials) {
      this._credentials = savedCredentials;
      this._credentialsSubject.next(this._credentials);
    }
  }

  /**
   * Checks is the user is authenticated.
   * @return True if the user is authenticated.
   */
  isAuthenticated(): boolean {
    return !!this.credentials && !this.credentials.guest;
  }

  /**
   * Gets the user credentials.
   * @return The user credentials or null if the user is not authenticated.
   */
  get credentials(): Credentials | null {
    return this._credentials;
  }

  get credentialsObservable(): Observable<Credentials | null> {
    return this._credentialsSubject;
  }

  /**
   * Sets the user credentials.
   * The credentials may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the credentials are only persisted for the current session.
   * @param credentials The user credentials.
   */
  setCredentials(credentials?: Credentials) {
    this._credentials = credentials || null;
    this._credentialsSubject.next(this._credentials);

    if (credentials) {
      this.localStorage.set(credentialsKey, credentials);
    } else {
      this.localStorage.remove(credentialsKey);
    }
  }

  checkToken() {
    if (!this.credentials) {
      return this.setGuestToken();
    }
    const payload = jwtDecode<{ exp: number }>(this.credentials.token);

    const now = new Date();
    const unix =
      Date.UTC(
        now.getUTCFullYear(),
        now.getUTCMonth(),
        now.getUTCDate(),
        now.getUTCHours(),
        now.getUTCMinutes(),
        now.getUTCSeconds()
      ) / 1000;

    if (payload.exp - unix < 3 * 60 * 60) {
      return this.refreshToken();
    }
    return of(true);
  }

  refreshToken() {
    const refreshTokenGQL = this.injector.get<RefreshTokensGQL>(RefreshTokensGQL);
    return refreshTokenGQL
      .mutate(
        {
          refreshToken: this.credentials!.refreshToken,
          accessToken: this.credentials!.token,
        },
        { errorPolicy: 'all' }
      )
      .pipe(
        tap(({ data }) => {
          this.setCredentials({
            token: data!.refreshTokens.authInfo.accessToken,
            refreshToken: data!.refreshTokens.authInfo.refreshToken,
            userid: data!.refreshTokens.user.id,
            username: data!.refreshTokens.user.username,
          });
        }),
        catchError((err) => {
          this.setCredentials();
          location.href = '/login';
          return throwError(err);
        })
      );
  }

  setGuestToken() {
    const signUpGuest = this.injector.get<SignUpGuestGQL>(SignUpGuestGQL);
    return signUpGuest.mutate().pipe(
      tap(({ data }) => {
        this.setCredentials({
          token: data!.signUpGuest.accessJwt,
          refreshToken: data!.signUpGuest.refreshJwt,
          guest: true,
          username: '',
          userid: data!.signUpGuest.userId,
        });
      })
    );
  }
}
