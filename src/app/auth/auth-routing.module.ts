import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

import { LoginComponent } from './login.component';
import { OnboardingComponent } from '@app/onboarding/onboarding.component';
import { AgePageComponent } from '@app/onboarding/age-page/age-page.component';
import { LanguagePageComponent } from '@app/onboarding/language-page/language-page.component';
import { LocationPageComponent } from '@app/onboarding/location-page/location-page.component';
import { SplashPageComponent } from '@app/onboarding/splash-page/splash-page.component';
import { SignupPageComponent } from '@app/onboarding/signup-page/signup-page.component';
import { VerificationCodePageComponent } from '@app/onboarding/verification-code-page/verification-code-page.component';
import { UsernamePageComponent } from '@app/onboarding/username-page/username-page.component';
import { PhotoPageComponent } from '@app/onboarding/photo-page/photo-page.component';
import { WelcomePageComponent } from '@app/onboarding/welcome-page/welcome-page.component';
import { CirclesSelectionPageComponent } from '@app/onboarding/circles-selection-page/circles-selection-page.component';
import { OnboardingGuard } from '@app/onboarding/onboarding.guard';
import { DownloadPageComponent } from './dowload-picnic/download-page.component';
import { LoginContainerComponent } from '@app/auth/login-container.component';

const routes: Routes = [
  {
    path: 'onboarding',
    component: OnboardingComponent,
    canActivate: [OnboardingGuard],
    children: [
      {
        path: '',
        redirectTo: 'splash',
        pathMatch: 'full',
      },
      {
        path: 'splash',
        component: SplashPageComponent,
      },
      {
        path: 'location',
        component: LocationPageComponent,
      },
      {
        path: 'language',
        component: LanguagePageComponent,
      },
      {
        path: 'age',
        component: AgePageComponent,
      },
      {
        path: 'sign-up',
        component: SignupPageComponent,
      },
      {
        path: 'verify-code',
        component: VerificationCodePageComponent,
      },
      {
        path: 'username',
        component: UsernamePageComponent,
      },
      {
        path: 'photo',
        component: PhotoPageComponent,
      },
      {
        path: 'welcome',
        component: WelcomePageComponent,
      },
      {
        path: 'circles',
        component: CirclesSelectionPageComponent,
      },
    ],
  },
  {
    path: 'login',
    component: LoginContainerComponent,
    data: { title: marker('Login') },
    children: [
      {
        path: '',
        component: LoginComponent,
        pathMatch: 'full',
      },
      {
        path: 'verify-code',
        component: VerificationCodePageComponent,
        data: {
          signIn: true,
        },
      },
    ],
  },
  {
    path: 'download',
    component: DownloadPageComponent,
    data: { title: marker('Download') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class AuthRoutingModule {}
