import { Injectable } from '@angular/core';
import { map, Observable, of, ReplaySubject, Subject, switchMap } from 'rxjs';
import { Credentials, CredentialsService } from './credentials.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import * as fireAuth from 'firebase/auth';
import { AuthInfo, SignInWithDiscordGQL, SignInWithFirebaseGQL, SignInWithPhoneNumberGQL } from '@app/graphql/auth';
import { PrivateProfile } from '@app/graphql/profile';
import { Apollo } from 'apollo-angular';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { environment } from '@env/environment';

export interface LoginContext {
  userid: string;
  username: string;
  token: string;
  refreshToken: string;
}

export interface AuthResult {
  success: boolean;
  error: string;
  info?: AuthInfo;
  firebaseToken?: string;
  firebaseThirdParty?: string;
  profile?: PrivateProfile;
}

export interface DiscordAuthInfo {
  discordCode: string | null;
  discordError: string | null;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(
    private credentialsService: CredentialsService,
    private afAuth: AngularFireAuth,
    private signInWithFirebaseGQL: SignInWithFirebaseGQL,
    private signInWithPhoneNumberGQL: SignInWithPhoneNumberGQL,
    private signInWithDiscordGQL: SignInWithDiscordGQL,
    private apollo: Apollo,
    private recaptchaV3Service: ReCaptchaV3Service
  ) {}

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  login(context: LoginContext): Observable<Credentials> {
    const data = {
      userid: context.userid,
      username: context.username,
      token: context.token,
      refreshToken: context.refreshToken,
    };
    this.credentialsService.setCredentials(data);
    this.apollo.getClient().resetStore();
    return of(data);
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.credentialsService.setCredentials();
    this.apollo.getClient().resetStore();
    return of(true);
  }

  googleAuth(login = true) {
    return this.firebaseAuth(new fireAuth.GoogleAuthProvider(), login);
  }

  appleAuth(login = true) {
    const provider = new fireAuth.OAuthProvider('apple.com');
    provider.addScope('email');
    provider.addScope('name');
    return this.firebaseAuth(provider, login);
  }

  //Discord authentication
  discordAuth() {
    const sub = new Subject<DiscordAuthInfo>();
    const win = window.open(this.createDiscordOAuthLoginUrl(), 'discord', 'width=500,height=700');
    const handleCode = (event: MessageEvent) => {
      if ('discordCode' in event.data) {
        win!.close();
        window.removeEventListener('message', handleCode, false);
        sub.next(event.data);
      }
    };

    window.addEventListener('message', handleCode);
    return sub.asObservable();
  }

  //Backend authentication with oauth code
  discordLogin(discordAuthCode: string): Observable<AuthResult> {
    return this.signInWithDiscordGQL
      .mutate(
        {
          credentials: {
            code: discordAuthCode,
            redirectUri: environment.discord.redirectUri,
          },
        },
        { errorPolicy: 'all' }
      )
      .pipe(
        map(({ data, errors }) => {
          if (errors) {
            return {
              success: false,
              error: errors[0].message,
            };
          }

          this.login({
            userid: data!.signInWithDiscord.user.id,
            username: data!.signInWithDiscord.user.username,
            token: data!.signInWithDiscord.authInfo.accessToken,
            refreshToken: data!.signInWithDiscord.authInfo.refreshToken,
          });

          return {
            success: true,
            error: '',
            info: data?.signInWithDiscord.authInfo,
            profile: data?.signInWithDiscord.user,
          };
        })
      );
  }

  requestPhoneCode(phone: string) {
    return this.recaptchaV3Service.execute('requestPhoneCode').pipe(
      switchMap((token) => {
        return this.signInWithPhoneNumberGQL
          .mutate(
            {
              phoneNumber: phone,
              recaptchaToken: token,
            },
            { errorPolicy: 'all' }
          )
          .pipe(
            map(({ data, errors }) => {
              if (errors) {
                return {
                  session: undefined,
                  errors,
                };
              }

              return {
                errors: undefined,
                session: data!.signInWithPhoneNumber.sessionInfo,
              };
            })
          );
      })
    );
  }

  fireBaseRequestPhoneCode(phone: string, fireRecaptchaVerifier: fireAuth.RecaptchaVerifier) {
    const subject = new ReplaySubject<{ result?: any; error?: string }>();
    this.afAuth
      .signInWithPhoneNumber(phone, fireRecaptchaVerifier)
      .then((res) => {
        subject.next({ result: res });
      })
      .catch((err) => {
        if (err.toString().includes('auth/invalid-phone-number')) {
          subject.next({
            error: 'invalid_phone_number',
          });
          return;
        }
        subject.next({
          error: err,
        });
      });
    return subject.asObservable();
  }

  fireBaseConfirmPhoneCode(session: any, code: string) {
    const subject = new ReplaySubject<AuthResult>();
    session
      .confirm(code)
      .then((res: any) => {
        res.user?.getIdToken().then((token: any) => {
          this.signInWithFirebase(res.user?.uid ?? '', token).subscribe((data) => {
            if (data === undefined) {
              subject.next({
                error: 'user_not_found',
                success: false,
                firebaseToken: token,
                firebaseThirdParty: res.user?.uid,
              });
              return;
            }
            this.login({
              userid: data.profile.id,
              username: data.profile.username,
              token: data.credentials.accessToken,
              refreshToken: data.credentials.refreshToken,
            });
            subject.next({
              success: true,
              info: data.credentials,
              error: '',
              firebaseToken: token,
              firebaseThirdParty: res.user?.uid,
            });
          });
        });
      })
      .catch((err: any) => {
        if (err.toString().includes('auth/invalid-verification-code')) {
          subject.next({
            error: 'invalid_verification_code',
            success: false,
          });
          return;
        }

        subject.next({
          error: err,
          success: false,
        });
      });
    return subject.asObservable();
  }

  private firebaseAuth(provider: any, login = true): Observable<AuthResult> {
    const subject = new ReplaySubject<AuthResult>();
    this.afAuth
      .signInWithPopup(provider)
      .then((res) => {
        res.user
          ?.getIdToken()
          .then((token) => {
            this.signInWithFirebase(res.user?.uid ?? '', token).subscribe((data) => {
              if (data === undefined) {
                subject.next({
                  error: 'user_not_found',
                  success: false,
                  firebaseToken: token,
                  firebaseThirdParty: res.user?.uid,
                });
                return;
              }

              if (login) {
                this.login({
                  userid: data.profile.id,
                  username: data.profile.username,
                  token: data.credentials.accessToken,
                  refreshToken: data.credentials.refreshToken,
                });
              }
              subject.next({
                success: true,
                info: data.credentials,
                error: '',
                firebaseToken: token,
                firebaseThirdParty: res.user?.uid,
              });
            });
          })
          .catch((err) => {
            console.error('firebase get token err', err);
            subject.next({
              error: 'firebase get token err',
              success: false,
            });
          });
      })
      .catch((err) => {
        if (err.toString().includes('auth/popup-closed-by-user')) {
          subject.next({
            error: '',
            success: false,
          });
          return;
        }
        console.error('firebase auth err', err);
        subject.next({
          error: err,
          success: false,
        });
      });

    return subject.asObservable();
  }

  private signInWithFirebase(
    uid: string,
    accessToken: string
  ): Observable<{ profile: PrivateProfile; credentials: AuthInfo } | undefined> {
    return this.signInWithFirebaseGQL
      .mutate(
        {
          credentials: {
            thirdPartyUserid: uid,
            accessToken: accessToken,
          },
        },
        { errorPolicy: 'all' }
      )
      .pipe(
        map((res) => {
          if (res.errors) {
            return undefined;
          }
          return {
            profile: res.data!.signInWithFirebase.user,
            credentials: res.data!.signInWithFirebase.authInfo,
          };
        })
      );
  }

  protected createDiscordOAuthLoginUrl(
    loginHint = '',
    customRedirectUri = '',
    noPrompt = false,
    params: object = {}
  ): string {
    let redirectUri: string;

    if (customRedirectUri) {
      redirectUri = customRedirectUri;
    } else {
      redirectUri = environment.discord.redirectUri;
    }
    const state = this.createNonce();

    const seperationChar = environment.discord.loginUrl.indexOf('?') > -1 ? '&' : '?';

    let scope = 'identify';

    let url =
      environment.discord.loginUrl +
      seperationChar +
      'response_type=' +
      encodeURIComponent('code') +
      '&client_id=' +
      encodeURIComponent(environment.discord.clientId) +
      '&state=' +
      encodeURIComponent(state) +
      '&redirect_uri=' +
      encodeURIComponent(redirectUri) +
      '&scope=' +
      encodeURIComponent(scope);

    if (loginHint) {
      url += '&login_hint=' + encodeURIComponent(loginHint);
    }

    if (noPrompt) {
      url += '&prompt=none';
    }

    for (const key of Object.keys(params)) {
      url += '&' + encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
    }

    return url;
  }

  protected createNonce(): string {
    /*
     * This alphabet is from:
     * https://tools.ietf.org/html/rfc7636#section-4.1
     *
     * [A-Z] / [a-z] / [0-9] / "-" / "." / "_" / "~"
     */
    const unreserved = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~';
    let size = 45;
    let id = '';

    const crypto = typeof self === 'undefined' ? null : self.crypto || self['msCrypto'];
    if (crypto) {
      let bytes = new Uint8Array(size);
      crypto.getRandomValues(bytes);

      // Needed for IE
      if (!bytes.map) {
        (bytes as any).map = Array.prototype.map;
      }

      bytes = bytes.map((x) => unreserved.charCodeAt(x % unreserved.length));
      id = String.fromCharCode.apply(null, Array.from(bytes));
    } else {
      while (0 < size--) {
        id += unreserved[(Math.random() * unreserved.length) | 0];
      }
    }

    return this.base64UrlEncode(id);
  }

  base64UrlEncode(str: string): string {
    const base64 = btoa(str);
    return base64.replace(/\+/g, '-').replace(/\//g, '_').replace(/=/g, '');
  }
}
