import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { environment } from '@env/environment';
import { Logger, UntilDestroy, untilDestroyed } from '@shared';
import { AuthenticationService } from './authentication.service';
import { TextInputDropdownItem } from '@app/components/text-input-dropdown/text-input-dropdown.component';
import { TmpStateService } from '@app/services/tmp-state.service';
import { allCountries } from 'country-telephone-data';
import { CountryCodeToFlagPipe } from '@app/pipes/country-code-to-flag.pipe';
import { ConfigService } from '@app/services/config.service';
import { OnboardingStateService } from '@app/onboarding/onboarding-state.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';
import { AnalyticsService } from '@app/services/analytics/analytics.service';

const log = new Logger('Login');

@UntilDestroy()
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  items: TextInputDropdownItem<string>[] = [];
  // Format: ISO Alpha 2 Code + dialCode. Example: 'us 1'
  // The purpose is to differentiate same dial codes like united states (+1) and canada (+1)
  initialValue = 'us 1';

  version: string | null = environment.version;
  error: string | undefined;
  loginForm!: FormGroup;
  loginWithPhoneForm!: FormGroup;
  isLoading = false;

  selectedDialCode = '1';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public state: TmpStateService,
    private authenticationService: AuthenticationService,
    private countryCodeToFlag: CountryCodeToFlagPipe,
    public config: ConfigService,
    private onboardingState: OnboardingStateService,
    private analytics: AnalyticsService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.items = this.getMappedCountries();

    // When the user is authenticated with Discord, the auth code is appended to the url
    // This code is then used to authenticate with the backend
    const discordAuthCode = this.extractDiscordAuthCode();

    if (discordAuthCode.code || discordAuthCode.error) {
      window.opener.postMessage(
        {
          discordCode: discordAuthCode.code,
          discordError: discordAuthCode.error,
        },
        location.origin
      );
    }
  }

  loginWithService(service: 'google' | 'apple') {
    let ob;
    switch (service) {
      case 'google':
        this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingSignInGoogle));
        ob = this.authenticationService.googleAuth();
        break;
      case 'apple':
        this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingSignInApple));
        ob = this.authenticationService.appleAuth();
        break;
    }
    ob.subscribe((res) => {
      if (!res.success) {
        if (res.error === 'user_not_found') {
          this.router.navigate(['/onboarding/location']);
        } else if (res.error !== '') {
          alert('authentication error look at console'); //TODO:
        }

        return;
      }

      this.state.updateProfile();
      this.redirectToHome();
    });
  }

  loginWithPhone() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingSignInPhoneContinueButton));
    const phone = '+' + this.selectedDialCode + this.loginWithPhoneForm.value.phone;
    this.authenticationService.requestPhoneCode(phone).subscribe(({ session, errors }) => {
      if (errors) {
        if (errors[0].message.includes('not found')) {
          this.router.navigate(['/onboarding/location']);
        } else {
          alert('unknown error');
          console.error(errors);
        }
        return;
      }
      this.onboardingState.phone = phone;
      this.onboardingState.sessionSMSCode = session!;
      this.router.navigate(['./verify-code'], {
        relativeTo: this.route,
      });
    });
  }

  discordAuth() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingSignInDiscord));
    this.authenticationService.discordAuth().subscribe((res) => {
      if (res.discordError) {
        alert('error');
        console.error(res.discordError);
        return;
      }
      this.authenticationService.discordLogin(res.discordCode!).subscribe(() => {
        this.state.updateProfile();
        this.redirectToHome();
      });
    });
  }

  onSelectPhoneCode(code: any) {
    this.selectedDialCode = code.dialCode;
  }

  // Extract the discord auth code from the url
  extractDiscordAuthCode(): { code: string | null; error: string | null } {
    const urlObj = new URL(window.location.href);
    const queryParams = urlObj.searchParams;
    return { code: queryParams.get('code'), error: queryParams.get('error') };
  }

  onSearchCountry(search: string) {
    if (!search) {
      this.items = this.getMappedCountries();
      return;
    }

    const filteredCountries = this.getMappedCountries().filter((country) => {
      return country.label.toLowerCase().includes(search.toLowerCase());
    });
    this.items = filteredCountries;
  }

  private redirectToHome() {
    // this.router.navigate([this.route.snapshot.queryParams['redirect'] || '/feed'], { replaceUrl: true });
    location.href = '/';
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: true,
    });
    this.loginWithPhoneForm = this.formBuilder.group({
      phone: ['', Validators.required],
      remember: true,
    });
  }

  private getMappedCountries = () =>
    allCountries.map((country) => {
      const flag = this.countryCodeToFlag.transform(country.iso2);
      const dialCode = country.dialCode;
      const isoCode = country.iso2;
      const name = country.name.replace(/\s?\([^)]*\)/g, '');

      return {
        label: `${flag} ${name} (+${dialCode})`,
        value: `${isoCode} ${Number(dialCode)}`,
        preffix: `${flag} +${dialCode}`,
        dialCode: dialCode,
      };
    });
}
