import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { DebugComponent } from '@app/debug/debug.component';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { ProgressComponent } from '@app/progress/progress.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'progress',
      component: ProgressComponent,
      data: { title: marker('Progress') },
    },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProgressRoutingModule {}
