import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.scss'],
})
export class WelcomePageComponent implements OnInit {
  constructor(private router: Router, private route: ActivatedRoute, private analytics: AnalyticsService) {}

  ngOnInit(): void {}

  onContinue() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingWelcomeContinueButton));
    this.router.navigate(['../circles'], {
      relativeTo: this.route,
    });
  }
}
