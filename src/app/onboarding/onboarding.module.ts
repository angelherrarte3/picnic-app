import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OnboardingComponent } from './onboarding.component';
import { SplashPageComponent } from './splash-page/splash-page.component';
import { LocationPageComponent } from './location-page/location-page.component';
import { LanguagePageComponent } from './language-page/language-page.component';
import { AgePageComponent } from './age-page/age-page.component';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '@app/components/components.module';

import { SignupPageComponent } from './signup-page/signup-page.component';
import { VerificationCodePageComponent } from './verification-code-page/verification-code-page.component';
import { UsernamePageComponent } from './username-page/username-page.component';
import { PhotoPageComponent } from './photo-page/photo-page.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { CirclesSelectionPageComponent } from './circles-selection-page/circles-selection-page.component';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    OnboardingComponent,
    SplashPageComponent,
    LocationPageComponent,
    LanguagePageComponent,
    LocationPageComponent,
    LanguagePageComponent,
    AgePageComponent,
    SignupPageComponent,
    VerificationCodePageComponent,
    UsernamePageComponent,
    PhotoPageComponent,
    WelcomePageComponent,
    CirclesSelectionPageComponent,
  ],
  imports: [CommonModule, RouterModule, ComponentsModule, NgScrollbarModule, FormsModule, ReactiveFormsModule],
})
export class OnboardingModule {}
