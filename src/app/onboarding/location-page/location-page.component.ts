import { Component, OnInit } from '@angular/core';
import { SimpleDropdownItem } from '@app/components/simple-dropdown/simple-dropdown.component';
import countries from 'countries-list';
import { OnboardingStateService } from '@app/onboarding/onboarding-state.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';

@Component({
  selector: 'app-location-page',
  templateUrl: './location-page.component.html',
  styleUrls: ['./location-page.component.scss'],
})
export class LocationPageComponent implements OnInit {
  items: SimpleDropdownItem<string>[] = [];

  constructor(
    public state: OnboardingStateService,
    private route: ActivatedRoute,
    private router: Router,
    private analytics: AnalyticsService
  ) {}

  ngOnInit(): void {
    this.items = Object.entries(countries.countries)
      .map(([code, country]) => {
        return {
          label: country.name,
          value: code,
        };
      })
      .sort((a, b) => {
        if (a.value < b.value) {
          return -1;
        }
        if (a.value > b.value) {
          return 1;
        }
        return 0;
      });
  }

  onSelect(code: string) {
    this.state.country = code.toLowerCase();
  }

  onClickContinue() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingCountryContinueButton));
    this.router.navigate(['../language'], { relativeTo: this.route });
  }
}
