import { AfterViewInit, Component, inject, NgZone, OnInit } from '@angular/core';
import { TextInputDropdownItem } from '@app/components/text-input-dropdown/text-input-dropdown.component';
import { OnboardingStateService } from '@app/onboarding/onboarding-state.service';
import { AuthenticationService } from '@app/auth';
import { ActivatedRoute, Router } from '@angular/router';
import { allCountries } from 'country-telephone-data';
import { CountryCodeToFlagPipe } from '@app/pipes/country-code-to-flag.pipe';
import * as fireAuth from 'firebase/auth';
import { Auth } from '@angular/fire/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TmpStateService } from '@app/services/tmp-state.service';
import { UpdateProfileInfoGQL } from '@app/graphql/profile';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';
import { AnalyticsService } from '@app/services/analytics/analytics.service';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss'],
})
export class SignupPageComponent implements OnInit, AfterViewInit {
  items: TextInputDropdownItem<string>[] = [];
  // Format: ISO Alpha 2 Code + dialCode. Example: 'us 1'
  // The purpose is to differentiate same dial codes like united states (+1) and canada (+1)
  initialValue = 'us 1';

  loginWithPhoneForm!: FormGroup;

  selectedDialCode = '1';

  recaptchaVerifier!: fireAuth.RecaptchaVerifier;
  private fireAuth: Auth = inject(Auth);

  constructor(
    private onboardingState: OnboardingStateService,
    private state: TmpStateService,
    private auth: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private countryCodeToFlag: CountryCodeToFlagPipe,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private ngZone: NgZone,
    private updateProfileGQL: UpdateProfileInfoGQL,
    private analytics: AnalyticsService
  ) {
    this.loginWithPhoneForm = this.formBuilder.group({
      phone: ['', Validators.required],
      remember: true,
    });
  }

  ngOnInit(): void {
    this.items = this.getMappedCountries();
  }

  ngAfterViewInit() {
    this.recaptchaVerifier = new fireAuth.RecaptchaVerifier(
      'continue-btn',
      {
        size: 'invisible',
        callback: () => {
          this.ngZone.run(() => this.loginWithPhone());
        },
      },
      this.fireAuth
    );
    this.recaptchaVerifier.render();
  }

  loginWithPhone(): void {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingPhoneContinueButton));
    const phone = '+' + this.selectedDialCode + this.loginWithPhoneForm.value.phone;
    this.auth.fireBaseRequestPhoneCode(phone, this.recaptchaVerifier).subscribe((session) => {
      if (session.error) {
        switch (session.error) {
          case 'invalid_phone_number':
            alert('invalid_phone_number');
            break;
          default:
            console.error(session.error);
            alert('unknown error');
        }
        return;
      }
      this.onboardingState.firebaseConfirmationResult = session.result;
      this.recaptchaVerifier.clear();
      this.onboardingState.phone = phone;
      this.router.navigate(['/onboarding/verify-code']);
    });
  }

  loginWithService(service: 'google' | 'apple') {
    let ob;
    switch (service) {
      case 'google':
        this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingSignUpGoogle));
        ob = this.auth.googleAuth();
        break;
      case 'apple':
        this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingSignUpApple));
        ob = this.auth.appleAuth();
        break;
    }

    ob.subscribe((res) => {
      if (res.success) {
        location.href = '/';
        return;
      }

      if (res.error !== 'user_not_found') {
        alert('failed to login');
        return;
      }

      this.onboardingState.firebaseToken = res.firebaseToken!;
      this.onboardingState.firebaseThirdPartyId = res.firebaseThirdParty!;
      this.router.navigate(['../username'], { relativeTo: this.route });
    });
  }

  private getMappedCountries = () =>
    allCountries.map((country) => {
      const flag = this.countryCodeToFlag.transform(country.iso2);
      const dialCode = country.dialCode;
      const isoCode = country.iso2;
      const name = country.name.replace(/\s?\([^)]*\)/g, '');

      return {
        label: `${flag} ${name} (+${dialCode})`,
        value: `${isoCode} ${Number(dialCode)}`,
        preffix: `${flag} +${dialCode}`,
        dialCode: dialCode,
      };
    });

  onSelectPhoneCode(code: any) {
    this.selectedDialCode = code.dialCode;
  }

  discordAuth() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingSignUpDiscord));
    this.authenticationService.discordAuth().subscribe((res) => {
      if (res.discordError) {
        alert('error');
        console.error(res.discordError);
        return;
      }
      this.authenticationService.discordLogin(res.discordCode!).subscribe((res) => {
        if (res.error) {
          alert('unknown error');
          return;
        }
        if (res.profile?.meta.pendingSteps.indexOf('Age') ?? -1 >= 0) {
          this.updateProfileGQL
            .mutate({
              data: {
                info: {
                  age: this.onboardingState.age,
                },
              },
            })
            .subscribe(() => {
              this.state.updateProfile();
              location.href = '/';
            });
        } else {
          this.state.updateProfile();
          location.href = '/';
        }
      });
    });
  }

  onSearchCountry(search: string) {
    if (!search) {
      this.items = this.getMappedCountries();
      return;
    }

    this.items = this.getMappedCountries().filter((country) => {
      return country.label.toLowerCase().includes(search.toLowerCase());
    });
  }
}
