import { Injectable } from '@angular/core';
import { PLanguage } from '@app/graphql/languages';
import * as fireAuth from 'firebase/auth';

@Injectable({
  providedIn: 'root',
})
export class OnboardingStateService {
  started = false;

  firebaseConfirmationResult?: any;
  sessionSMSCode = '';

  country = '';
  language?: PLanguage;
  age: number = 0;
  firebaseToken = '';
  firebaseThirdPartyId = '';
  username = '';
  profileImage?: File;
  phone = '';
}
