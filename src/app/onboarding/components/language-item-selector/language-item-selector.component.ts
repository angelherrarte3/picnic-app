import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PLanguage } from '@app/graphql/languages';

@Component({
  selector: 'language-item-selector[items]',
  templateUrl: './language-item-selector.component.html',
  styleUrls: ['./language-item-selector.component.scss'],
})
export class LanguageItemSelectorComponent implements OnInit {
  _items: PLanguage[] = [];
  @Input() set items(items: PLanguage[]) {
    this._items = items;
    this.handleSelectItem(items[0]);
  }
  @Input() initialValue?: PLanguage;
  @Output() private bySelect: EventEmitter<PLanguage> = new EventEmitter();

  selectedValue?: PLanguage;

  constructor() {}

  ngOnInit() {
    if (this.initialValue) this.handleSelectItem(this.initialValue);
  }

  handleSelectItem(value?: PLanguage) {
    if (this.selectedValue === value) return;

    this.selectedValue = value;
    this.bySelect.emit(value);
  }
}
