import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageItemSelectorComponent } from './language-item-selector.component';

describe('LanguageItemSelectorComponent', () => {
  let component: LanguageItemSelectorComponent;
  let fixture: ComponentFixture<LanguageItemSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LanguageItemSelectorComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(LanguageItemSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
