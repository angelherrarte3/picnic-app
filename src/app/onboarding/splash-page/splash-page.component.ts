import { Component, OnInit } from '@angular/core';
import { OnboardingStateService } from '@app/onboarding/onboarding-state.service';
import { PicnicModalService } from '@app/services/picnic-modal.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';

@Component({
  selector: 'app-splash-page',
  templateUrl: './splash-page.component.html',
  styleUrls: ['./splash-page.component.scss'],
})
export class SplashPageComponent implements OnInit {
  constructor(
    public state: OnboardingStateService,
    private router: Router,
    private route: ActivatedRoute,
    private analytics: AnalyticsService
  ) {}

  ngOnInit(): void {}

  onGetStarted() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingGetStartedButton));
    this.state.started = true;
    this.router.navigate(['../location'], {
      relativeTo: this.route,
    });
  }

  onLogin() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingLoginButton));
    this.router.navigate(['/login']);
  }
}
