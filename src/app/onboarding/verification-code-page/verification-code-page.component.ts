import { AfterViewInit, Component, inject, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '@app/auth';
import { CheckVerificationCodeGQL } from '@app/graphql/auth';
import { TmpStateService } from '@app/services/tmp-state.service';
import { OnboardingStateService } from '@app/onboarding/onboarding-state.service';
import * as fireAuth from 'firebase/auth';
import { Auth } from '@angular/fire/auth';

@Component({
  selector: 'app-verification-code-page',
  templateUrl: './verification-code-page.component.html',
  styleUrls: ['./verification-code-page.component.scss'],
})
export class VerificationCodePageComponent implements OnInit, AfterViewInit {
  resend: boolean = false;
  secondsLeft: number = 0;

  signIn = false;

  code = '';

  recaptchaVerifier?: fireAuth.RecaptchaVerifier;
  private fireAuth: Auth = inject(Auth);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private checkVerificationCodeGQL: CheckVerificationCodeGQL,
    private authenticationService: AuthenticationService,
    private state: TmpStateService,
    private onboardingState: OnboardingStateService,
    private ngZone: NgZone
  ) {}
  ngOnInit(): void {
    this.route.data.subscribe((d) => {
      this.signIn = d['signIn'];
    });
  }

  ngAfterViewInit() {
    this.startCountdown();

    this.recaptchaVerifier?.clear();

    if (!this.signIn) {
      this.recaptchaVerifier = new fireAuth.RecaptchaVerifier(
        'resend-btn',
        {
          size: 'invisible',
          callback: () => {
            this.ngZone.run(() => this.resendFirebaseSMS());
          },
        },
        this.fireAuth
      );
      this.recaptchaVerifier.render();
    }
  }

  startCountdown() {
    this.resend = false;
    let countdown = new Date();
    countdown.setSeconds(countdown.getSeconds() + 30);
    let t = setInterval(() => {
      let distance = countdown.getTime() - new Date().getTime();
      this.secondsLeft = Math.floor((distance % (1000 * 60)) / 1000);
      if (distance < 0) {
        clearInterval(t);
        this.resend = true;
      }
    });
  }

  verify() {
    if (this.signIn) {
      this.checkVerificationCodeGQL
        .mutate(
          {
            code: this.code,
            sessionInfo: this.onboardingState.sessionSMSCode,
          },
          { errorPolicy: 'all' }
        )
        .subscribe(({ data, errors }) => {
          if (errors) {
            if (errors[0].message.includes('invalid code')) {
              alert('invalid code');
            } else {
              alert('unknown error');
              console.error(errors);
            }
            return;
          }
          this.authenticationService.login({
            userid: data!.checkVerificationCode.user.id,
            username: data!.checkVerificationCode.user.username,
            token: data!.checkVerificationCode.authInfo.accessToken,
            refreshToken: data!.checkVerificationCode.authInfo.refreshToken,
          });
          this.state.updateProfile();
          location.href = '/';
        });
      return;
    }

    this.authenticationService
      .fireBaseConfirmPhoneCode(this.onboardingState.firebaseConfirmationResult, this.code)
      .subscribe((res) => {
        if (res.success) {
          this.router.navigate(['/feed'], { replaceUrl: true });
          return;
        }

        if (res.error !== 'user_not_found') {
          switch (res.error) {
            case 'invalid_phone_number':
              alert('invalid_verification_code');
              break;
            default:
              console.error(res.error);
              alert('unknown error');
          }
          return;
        }

        if (res.error !== 'user_not_found') {
          alert('failed to login');
          return;
        }

        this.onboardingState.firebaseToken = res.firebaseToken!;
        this.onboardingState.firebaseThirdPartyId = res.firebaseThirdParty!;
        this.recaptchaVerifier?.clear();
        location.href = '/';
      });
  }

  resendSMS() {
    if (!this.signIn) {
      return;
    }

    this.startCountdown();

    this.authenticationService.requestPhoneCode(this.onboardingState.phone).subscribe(({ session, errors }) => {
      if (errors) {
        alert('unknown error');
        console.error(errors);
        return;
      }
    });
  }

  resendFirebaseSMS() {
    this.startCountdown();

    this.authenticationService
      .fireBaseRequestPhoneCode(this.onboardingState.phone, this.recaptchaVerifier!)
      .subscribe((session) => {
        if (session.error) {
          switch (session.error) {
            case 'invalid_phone_number':
              alert('invalid_phone_number');
              break;
            default:
              console.error(session.error);
              alert('unknown error');
          }
          return;
        }
        this.onboardingState.firebaseConfirmationResult = session.result;
      });
  }
}
