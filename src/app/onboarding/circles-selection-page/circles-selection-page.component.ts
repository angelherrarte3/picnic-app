import { Component, OnInit } from '@angular/core';
import { Chip } from '@app/components/chip-list/chip-list.component';
import { Circle, JoinCirclesGQL, OnBoardingCirclesConnectionGQL } from '@app/graphql/circle';
import { Router } from '@angular/router';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';

@Component({
  selector: 'app-circles-selection-page',
  templateUrl: './circles-selection-page.component.html',
  styleUrls: ['./circles-selection-page.component.scss'],
})
export class CirclesSelectionPageComponent implements OnInit {
  groups: {
    name: string;
    chips: Chip<Circle>[];
  }[] = [];

  selectedCircles: Circle[] = [];

  constructor(
    private onboardingConnection: OnBoardingCirclesConnectionGQL,
    private joinCirclesGQL: JoinCirclesGQL,
    private router: Router,
    private analytics: AnalyticsService
  ) {}

  ngOnInit(): void {
    this.onboardingConnection.watch().valueChanges.subscribe(({ data }) => {
      this.groups = data.onBoardingCirclesConnection.edges.map((v) => {
        return {
          name: v.name,
          chips: v.circles.map((v) => {
            return {
              label: v.image + ' ' + v.name,
              selected: false,
              value: v,
            };
          }),
        };
      });
    });
  }

  get remainingCircles(): number {
    return Math.max(5 - this.selectedCircles.length, 0);
  }

  selectCircle(chip: Chip<Circle>) {
    if (chip.selected) {
      this.selectedCircles.push(chip.value);
      return;
    }

    this.selectedCircles.splice(
      this.selectedCircles.findIndex((v) => v.id == chip.value.id),
      1
    );
  }

  submit() {
    if (this.remainingCircles > 0) {
      return;
    }

    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingCirclesConfirmButton));

    this.joinCirclesGQL
      .mutate({
        circlesIds: this.selectedCircles.map((v) => v.id),
      })
      .subscribe(({ data }) => {
        if (!data?.joinCircles.success) {
          return;
        }

        this.router.navigate(['/']);
      });
  }
}
