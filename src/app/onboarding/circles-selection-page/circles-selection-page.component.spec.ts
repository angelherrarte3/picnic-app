import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CirclesSelectionPageComponent } from './circles-selection-page.component';

describe('CirclesSelectionPageComponent', () => {
  let component: CirclesSelectionPageComponent;
  let fixture: ComponentFixture<CirclesSelectionPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CirclesSelectionPageComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CirclesSelectionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
