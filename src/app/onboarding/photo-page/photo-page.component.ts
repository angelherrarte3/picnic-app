import { Component, OnInit } from '@angular/core';
import { OnboardingStateService } from '@app/onboarding/onboarding-state.service';
import { SignUpWithFirebaseGQL } from '@app/graphql/auth';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '@app/auth';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';

@Component({
  selector: 'app-photo-page',
  templateUrl: './photo-page.component.html',
  styleUrls: ['./photo-page.component.scss'],
})
export class PhotoPageComponent implements OnInit {
  constructor(
    private state: OnboardingStateService,
    private signUpWithFirebase: SignUpWithFirebaseGQL,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private analytics: AnalyticsService
  ) {}

  ngOnInit(): void {}

  openPhotoDialog() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingPhotoAddButton));
    let input = document.createElement('input');
    input.type = 'file';
    input.accept = 'image/*';
    input.onchange = (_) => {
      this.state.profileImage = input.files?.[0];
      this.register();
    };
    input.click();
  }

  register() {
    this.signUpWithFirebase
      .mutate({
        credentials: {
          thirdPartyUserid: this.state.firebaseThirdPartyId,
          accessToken: this.state.firebaseToken,
        },
        userInfo: {
          username: this.state.username,
          age: this.state.age,
          countryCode: this.state.country,
          languageCodes: [this.state.language?.tag],
          notificationsEnabled: false,
          email: '',
          phone: this.state.phone,
          profileImage: this.state.profileImage,
        },
      })
      .subscribe(({ data }) => {
        this.authenticationService.login({
          userid: data!.signUpWithFirebase.user.id,
          username: this.state.username,
          token: data!.signUpWithFirebase.authInfo.accessToken,
          refreshToken: data!.signUpWithFirebase.authInfo.refreshToken,
        });
        this.router.navigate(['../welcome'], { relativeTo: this.activatedRoute });
      });
  }

  onSkip() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingPhotoSkipButton));
    this.register();
  }
}
