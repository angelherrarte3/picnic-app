import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { CredentialsService } from '@app/auth';
import { OnboardingStateService } from '@app/onboarding/onboarding-state.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class OnboardingGuard implements CanActivate {
  constructor(
    private credentialsService: CredentialsService,
    private router: Router,
    private state: OnboardingStateService
  ) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    //Check that discord auth is complete
    if (state.url.includes('code') && !state.url.includes('error')) return true;

    if (this.credentialsService.isAuthenticated() && state.url !== '/onboarding/circles') {
      this.router.navigate(['/']);
      return false;
    }
    if (state.url !== '/onboarding/splash' && state.url !== '/onboarding/circles' && !this.state.started) {
      this.router.navigate(['/onboarding']);
      return false;
    }
    return true;
  }
}
