import { Component, OnInit } from '@angular/core';
import { OnboardingStateService } from '@app/onboarding/onboarding-state.service';
import { CheckUsernameGQL } from '@app/graphql/auth';
import { ActivatedRoute, Router } from '@angular/router';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';

@Component({
  selector: 'app-username-page',
  templateUrl: './username-page.component.html',
  styleUrls: ['./username-page.component.scss'],
})
export class UsernamePageComponent implements OnInit {
  loading = false;
  valid = false;
  username = '';

  constructor(
    public state: OnboardingStateService,
    private checkUsernameGQL: CheckUsernameGQL,
    private router: Router,
    private route: ActivatedRoute,
    private analytics: AnalyticsService
  ) {}

  ngOnInit(): void {}

  get cleaned() {
    return this.username.length <= 0;
  }

  usernameInput() {
    this.loading = true;
    this.checkUsernameGQL.fetch({ username: this.username }).subscribe(({ data }) => {
      this.valid = data.checkUsername.available;
      this.loading = false;
    });
  }

  onContinue() {
    this.state.username = this.username;
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingUsernameContinueButton));
    this.router.navigate(['../photo'], {
      relativeTo: this.route,
    });
  }
}
