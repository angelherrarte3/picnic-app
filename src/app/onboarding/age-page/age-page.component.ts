import { Component, OnInit } from '@angular/core';
import { OnboardingStateService } from '@app/onboarding/onboarding-state.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';

@Component({
  selector: 'app-age-page',
  templateUrl: './age-page.component.html',
  styleUrls: ['./age-page.component.scss'],
})
export class AgePageComponent implements OnInit {
  age?: number;
  constructor(
    public state: OnboardingStateService,
    private router: Router,
    private route: ActivatedRoute,
    private analytics: AnalyticsService
  ) {}

  ngOnInit(): void {}

  submit() {
    this.state.age = this.age ?? 0;
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingAgeContinueButton));
    this.router.navigate(['../sign-up'], {
      relativeTo: this.route,
    });
  }

  get disabled() {
    return (this.age ?? 0) < 13;
  }
}
