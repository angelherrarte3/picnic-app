import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OnboardingStateService } from '@app/onboarding/onboarding-state.service';
import { ThemeService } from '@app/services/theme.service';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss'],
})
export class OnboardingComponent implements OnInit {
  constructor(private state: OnboardingStateService, private themeService: ThemeService) {}

  ngOnInit(): void {
    this.themeService.setTheme('light', { saveLocal: false });
  }
}
