import { Component, OnInit } from '@angular/core';
import { ListLanguagesGQL, PLanguage } from '@app/graphql/languages';
import { OnboardingStateService } from '@app/onboarding/onboarding-state.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AnalyticsService } from '@app/services/analytics/analytics.service';
import { AnalyticsEventTap } from '@app/services/analytics/events/analytics-event-tap';
import { AnalyticsTapTarget } from '@app/services/analytics/events/analytics-tap-target';

@Component({
  selector: 'app-language-page',
  templateUrl: './language-page.component.html',
  styleUrls: ['./language-page.component.scss'],
})
export class LanguagePageComponent implements OnInit {
  languages: PLanguage[] = [];
  loading: boolean | undefined;

  constructor(
    public state: OnboardingStateService,
    private languagesGQL: ListLanguagesGQL,
    private router: Router,
    private route: ActivatedRoute,
    private analytics: AnalyticsService
  ) {}

  ngOnInit(): void {
    this.loading = true;

    this.languagesGQL
      .watch({
        filter: 'ENABLED',
      })
      .valueChanges.subscribe(({ data, loading }) => {
        this.languages = data.listLanguages;

        this.loading = false;
      });
  }

  selectLanguage(language?: PLanguage) {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingLanguageSelectButton));
    this.state.language = language;
  }

  onContinue() {
    this.analytics.logEvent(new AnalyticsEventTap(AnalyticsTapTarget.onboardingLanguageContinueButton));
    this.router.navigate(['../age'], {
      relativeTo: this.route,
    });
  }
}
