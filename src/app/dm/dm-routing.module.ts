import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { DmComponent } from '@app/dm/dm.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'dm',
      component: DmComponent,
      data: { title: marker('Dm') },
    },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DmRoutingModule {}
