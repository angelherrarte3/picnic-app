import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PodsComponent } from './pods.component';
import { ComponentsModule } from '@app/components/components.module';
import { RouterLink } from '@angular/router';
import { PodsRoutingModule } from '@app/pods/pods-routing.module.';
import { ListComponent } from './list/list.component';
import { PodComponent } from './pod/pod.component';

@NgModule({
  declarations: [PodsComponent, ListComponent, PodComponent],
  imports: [CommonModule, ComponentsModule, PodsRoutingModule, RouterLink],
})
export class PodsModule {}
