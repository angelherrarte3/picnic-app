import { Component, OnInit } from '@angular/core';
import { App, AppEdge, SearchAppsGQL } from '@app/graphql/app';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pods',
  templateUrl: './pods.component.html',
  styleUrls: ['./pods.component.scss'],
})
export class PodsComponent {}
