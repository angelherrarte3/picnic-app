import { Component, OnInit } from '@angular/core';
import { App, AppEdge, SearchAppsGQL } from '@app/graphql/app';
import { ActivatedRoute, Router } from '@angular/router';

import { CredentialsService } from '@app/auth';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { GlobalModalService } from '@app/services/global-modal.service';

@UntilDestroy()
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  apps: AppEdge[] = [];
  cursorId = '';
  hasNext = false;
  hasPrev = false;
  cursorStack: string[] = [];

  constructor(
    private searchAppsGQL: SearchAppsGQL,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: GlobalModalService,
    private credentialsService: CredentialsService
  ) {}

  ngOnInit() {
    this.route.queryParamMap.pipe(untilDestroyed(this)).subscribe((p) => {
      this.cursorId = p.get('cursorId') ?? '';

      if (this.cursorId === this.cursorStack[this.cursorStack.length - 1]) {
        this.cursorStack.pop();
      }

      this.cursorStack.push(this.cursorId);
      this.loadApps();
    });
  }

  get isGuestUser() {
    return !this.credentialsService.isAuthenticated();
  }

  onTapPod(pod: App) {
    if (this.isGuestUser) {
      this.modalService.open('sign-up');
      return;
    }

    if (pod.owner?.name === 'character.ai') {
      this.modalService.open('building-feature');
      return;
    }

    this.router.navigate([pod.id], { relativeTo: this.route });
  }

  loadApps() {
    this.searchAppsGQL
      .fetch({
        data: {
          cursor: {
            id: this.cursorId,
            limit: 14,
          },
          nameStartsWith: '',
        },
      })
      .subscribe(({ data }) => {
        this.apps = data.searchApps.edges;
        this.hasNext = data.searchApps.pageInfo.hasNextPage;
        this.hasPrev = data.searchApps.pageInfo.hasPreviousPage!;
        this.cursorId = data.searchApps.pageInfo.lastId;
      });
  }

  onNext() {
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        cursorId: this.cursorId,
      },
    });
  }

  onPrev() {
    if (this.cursorStack.length > 0) {
      this.cursorStack.pop();
      const prevCursorId = this.cursorStack.length > 0 ? this.cursorStack[this.cursorStack.length - 1] : '';

      this.router.navigate(['.'], {
        relativeTo: this.route,
        queryParams: {
          cursorId: prevCursorId,
        },
      });
    }
  }
}
