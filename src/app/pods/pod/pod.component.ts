import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { App, GetAppGQL } from '@app/graphql/app';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NavigationService } from '@app/services/navigation.service';
import { CredentialsService } from '@app/auth';
import { ActivatedRoute, Router } from '@angular/router';
import { GoBackNavigation } from '@app/types/go-back-navigation';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-pod',
  templateUrl: './pod.component.html',
  styleUrls: ['./pod.component.scss'],
})
export class PodComponent extends GoBackNavigation implements OnInit {
  @ViewChild('appframe', { static: false }) appframe: ElementRef;
  app?: App;
  urlSafe?: SafeResourceUrl;

  constructor(
    private sanitizer: DomSanitizer,
    private credentialsService: CredentialsService,
    private route: ActivatedRoute,
    private getApp: GetAppGQL,
    protected override navigationService: NavigationService,
    private router: Router
  ) {
    super(navigationService);
  }

  ngOnInit(): void {
    const token = this.credentialsService.credentials?.token;
    if (token) {
      this.route.paramMap.pipe(untilDestroyed(this)).subscribe((p) => {
        this.getApp
          .fetch(
            {
              id: p.get('id')!,
            },
            { errorPolicy: 'all' }
          )
          .subscribe(({ data, errors }) => {
            this.app = data.getApp;
            const host = data.getApp.url;
            const url = `${host}?token=Bearer%20${token}`;
            this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(url);
          });
      });
    }
  }
}
