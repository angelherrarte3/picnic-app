import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { PodsComponent } from '@app/pods/pods.component';
import { ListComponent } from '@app/pods/list/list.component';
import { PodComponent } from '@app/pods/pod/pod.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'pods',
      component: PodsComponent,
      children: [
        {
          path: '',
          component: ListComponent,
        },
        {
          path: ':id',
          component: PodComponent,
        },
      ],
    },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PodsRoutingModule {}
