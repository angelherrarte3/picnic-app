#!/bin/sh

_term() {
  echo "Caught SIGTERM signal!"
  kill -TERM "$child" 2>/dev/null
}

trap _term SIGTERM

ENTRYPOINT=dist/server/main.js

if [ "$APP_ENV" = "prod" ]; then
  ENTRYPOINT=dist-prod/server/main.js
fi

node $ENTRYPOINT &

child=$!
wait "$child"
