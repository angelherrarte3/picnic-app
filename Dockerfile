# syntax=docker/dockerfile:1

ARG NODE_VERSION=19.7
ARG ALPINE_VERSION=3.16

FROM node:${NODE_VERSION}-alpine${ALPINE_VERSION}

WORKDIR /app

COPY . /app
RUN npm install -g @angular/cli
RUN npm install
RUN npm run write:env -s

RUN ng build --configuration=development
RUN ng run picnic:server:development

RUN ng build --configuration=production
RUN ng run picnic:server:production

RUN chmod +x /app/.cfg/entrypoint.sh

ENTRYPOINT /app/.cfg/entrypoint.sh
